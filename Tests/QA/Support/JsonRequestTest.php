<?php
namespace Mildberry\Kangaroo\Tests\QA\Support;

use Mildberry\Kangaroo\QA\Support\JsonRequest;
use PHPUnit_Framework_TestCase;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class JsonRequestTest extends PHPUnit_Framework_TestCase
{
    public function testGet()
    {
        $request = new JsonRequest('GET', [
            'page' => 7,
            'perPage' => 56
        ]);

        $this->assertEquals(7, $request->query('page'));
        $this->assertEquals(56, $request->query('perPage'));

        $request = JsonRequest::make('test-url', 'GET', [
            'page' => 10,
            'perPage' => 87
        ]);

        $this->assertEquals(10, $request->query('page'));
        $this->assertEquals(87, $request->query('perPage'));
    }

    public function testDelete()
    {
        $request = JsonRequest::make('test-url', 'DELETE', [
            'param1' => 123
        ]);

        $this->assertEquals(123, $request->query('param1'));
    }
} 