<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/08/16
 * Time: 13:33
 */

namespace Mildberry\Kangaroo\Tests\QA\Endpoints\Specification;


use Illuminate\Support\Collection;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class EntityMock
{
    private $id;

    private $name;

    private $description;

    private $entity;

    private $collection;

    /**
     * @return mixed
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @param mixed $collection
     * @return EntityMock
     */
    public function setCollection(Collection $collection)
    {
        $this->collection = $collection;
        return $this;
    }
    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $entity
     * @return EntityMock
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
        return $this;
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return EntityMock
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return EntityMock
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return EntityMock
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
}