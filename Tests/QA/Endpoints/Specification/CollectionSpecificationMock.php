<?php
namespace Mildberry\Kangaroo\Tests\QA\Endpoints\Specification;

use Mildberry\Kangaroo\Libraries\Specification\Types\Collection\CollectionType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class CollectionSpecificationMock extends CollectionType
{
    /**
     * @return array
     */
    public function specification()
    {
        return function(){
            return new SpecificationMock();
        };
    }
}