<?php
namespace Mildberry\Kangaroo\Tests\QA\Endpoints\Specification;

use Mildberry\Kangaroo\Libraries\Specification\Types\Collection\CollectionType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Custom\PhoneType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class EntitySpecificationMock extends ObjectType
{
    /**
     * @return array
     */
    public function structure()
    {
        return [
            'id' => new IntegerType(),
            'name' => new StringType(),
            'description' => new PhoneType(),
            'entity' => new SubEntitySpecificationMock(),
            'collection' => new CollectionType(function(){
                return new IntegerType();
            })
        ];
    }
}