<?php
namespace Mildberry\Kangaroo\Tests\QA\Endpoints\Specification;

use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class RequestSpecificationMock extends ObjectType
{

    protected $specificationType = self::REQUEST_SPECIFICATION;
    /**
     * @return array
     */
    public function specification()
    {
        return [
            'id' => new IntegerType(),
            'name?' => new StringType()
        ];
    }
}