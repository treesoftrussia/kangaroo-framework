<?php
namespace Mildberry\Kangaroo\Tests\QA\Endpoints\Specification;

use Mildberry\Kangaroo\Libraries\Specification\ASpecification;
use Mildberry\Kangaroo\Libraries\Specification\AType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Collection\CollectionType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Custom\PhoneType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\BooleanType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\FloatType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class SpecificationMock extends ASpecification
{

    protected $specificationType = ASpecification::REQUEST_SPECIFICATION;


    public function route()
    {
        return new ObjectType([
            'id' => new IntegerType()
        ]);
    }


    public function options()
    {
        return new ObjectType([
            'perPage' => 10,
            'currentPage' => 1
        ]);
    }

    /**
     * @return AType
     */
    public function body()
    {
        return new ObjectType([
            'field' => new ObjectType([
                'string' => new StringType(),
                'enum' => new CollectionType(function () {
                    return new StringType();
                }),
                'string2' => new StringType(),
                'array1' => new CollectionType(function () {
                    return new SingleLevelSpecificationMock();
                }, false, false),
                'array2' => new CollectionType(function () {
                    return new StringType();
                }, false, false),
                'array3' => new CollectionType(function () {
                    return new FloatType();
                }),
                'emptyArray' => new CollectionType(function () {
                    return new FloatType();
                }),
                'emptyArray2' => new CollectionType(function () {
                    return new FloatType();
                }),
                'bool' => new BooleanType(),
                'int' => new IntegerType(),
                'phone' => new PhoneType()
            ])
        ]);
    }
}