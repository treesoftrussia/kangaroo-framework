<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Modifier;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ModifiersProvider
{
    public function addParams($value, $p1, $p2)
    {
        return $value + $p1 + $p2;
    }

    public function addThree($value)
    {
        return $value + 3;
    }
} 