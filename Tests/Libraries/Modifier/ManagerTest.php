<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Modifier;

use Mildberry\Kangaroo\Libraries\Modifier\Manager;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testModifyWithCallbacks()
    {
        $manager = new Manager();

        $manager->register('by2', function ($value) {
            return $value * 2;
        });

        $this->assertEquals(4, $manager->modify(2, 'by2'));

        $manager->register('plus1', function ($value) {
            return $value + 1;
        });

        $this->assertEquals(5, $manager->modify(2, 'by2|plus1'));
    }

    public function testModifyWithProviders()
    {
        $manager = new Manager();
        $manager->registerProvider(new ModifiersProvider());

        $this->assertEquals(17, $manager->modify(2, ['add_params' => [2, 10], 'add_three']));
    }

    public function testModifyWithMixed()
    {
        $manager = new Manager();

        $manager->register('by2', function ($value) {
            return $value * 2;
        });

        $manager->registerProvider(new ModifiersProvider());

        $manager->register('plus1', function ($value) {
            return $value + 1;
        });

        $manager->registerProvider(new SecondModifiersProvider());

        $this->assertEquals(48, $manager->modify(2, ['plus1', 'by3', 'add_params' => [2, 10], 'add_three', 'by2']));
        $this->assertEquals(3, $manager->modify(2, ['plus1']));
    }
} 