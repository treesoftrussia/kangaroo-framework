<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Modifier;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class SecondModifiersProvider
{
    public function by3($value)
    {
        return $value * 3;
    }

    public function addThree($value)
    {
        return 0;
    }
} 