<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Adapter;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ModifiersProvider
{
    public function add30($value)
    {
        return $value + 30;
    }
} 