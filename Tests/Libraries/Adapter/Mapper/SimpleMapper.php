<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Adapter\Mapper;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractMapper;
use Mildberry\Kangaroo\Libraries\Enum\Enum;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class SimpleMapper extends AbstractMapper
{
    /**
     * @return array|Enum[]
     */
    protected function map()
    {
        return [
            'one' => new EnumMock(EnumMock::VALUE_1),
            'two' => new EnumMock(EnumMock::VALUE_2),
            'three' => new EnumMock(EnumMock::VALUE_3),
        ];
    }
}