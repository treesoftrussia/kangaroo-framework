<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Adapter\Mapper;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractMapper;
use Mildberry\Kangaroo\Libraries\Enum\Enum;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class TestVerifiedMappersMapper extends AbstractMapper
{
    private $enum;

    /**
     * @param Enum|null $enum
     */
    public function setEnum(Enum $enum = null)
    {
        $this->enum = $enum;
    }

    /**
     * @return array|Enum[]
     */
    protected function map()
    {
        return [
            'one' => new EnumMock(EnumMock::VALUE_1),
            'two' => $this->enum ?: new EnumMock(EnumMock::VALUE_2),
            'three' => new EnumMock(EnumMock::VALUE_3),
        ];
    }

    /**
     * @param array|mixed $value
     * @param Enum|null $enum
     * @return mixed
     */
    public static function toCore($value, Enum $enum = null)
    {
        $object = new static();
        $object->setEnum($enum);
        return $object->toEnum($value);
    }
} 