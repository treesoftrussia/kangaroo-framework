<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Adapter\Mapper;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractMapper;
use Mildberry\Kangaroo\Libraries\Enum\Enum;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class MixedClassesMapper extends AbstractMapper
{
    /**
     * @return array|Enum[]
     */
    protected function map()
    {
        return [
            'two' => new EnumMock(EnumMock::VALUE_2),
            'different' => new DifferentEnum(DifferentEnum::VALUE_2),
            'one' => new EnumMock(EnumMock::VALUE_1),
        ];
    }
}