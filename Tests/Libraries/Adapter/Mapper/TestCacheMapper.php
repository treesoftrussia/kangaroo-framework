<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Adapter\Mapper;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractMapper;
use Mildberry\Kangaroo\Libraries\Enum\Enum;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class TestCacheMapper extends AbstractMapper
{
    /**
     * @return array|Enum[]
     */
    protected function map()
    {
        return [
            'one' => new TestCacheEnum(TestCacheEnum::VALUE_1),
            'two' => new TestCacheEnum(TestCacheEnum::VALUE_2),
            'three' => new TestCacheEnum(TestCacheEnum::VALUE_3),
        ];
    }
}