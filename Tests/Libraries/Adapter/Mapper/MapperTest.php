<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Adapter\Mapper;

use RuntimeException;
use ReflectionClass;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class MapperTest extends \PHPUnit_Framework_TestCase
{
    public function testFromCoreAndToCore()
    {
        $this->assertEquals('two', SimpleMapper::fromCore(new EnumMock(EnumMock::VALUE_2)));

        $enum = SimpleMapper::toCore('three');

        $this->assertEquals($enum->value(), 3);
    }

    public function testCollection()
    {
        $this->assertEquals(['one', 'two'],
            SimpleMapper::fromCore([new EnumMock(EnumMock::VALUE_1), new EnumMock(EnumMock::VALUE_2)]));

        /**
         * @var EnumMock[] $enums
         */
        $enums = SimpleMapper::toCore(['three', 'two']);

        $this->assertEquals([$enums[0]->value(), $enums[1]->value()], [3, 2]);

    }

    public function testVerifyMixedClassesException()
    {
        $this->setExpectedException(
            RuntimeException::class,
            'All the values must be instance of the same enum class.'
        );

        MixedClassesMapper::toCore('two');
    }

    public function testCache()
    {
        $reflection = new ReflectionClass(get_parent_class(TestCacheMapper::class));

        $cacheProperty = $reflection->getProperty('cache');

        $cacheProperty->setAccessible(true);

        $cacheKey = TestCacheEnum::class . ':2';

        $this->assertFalse(isset($cacheProperty->getValue($reflection)[$cacheKey]));

        TestCacheMapper::fromCore(new TestCacheEnum(TestCacheEnum::VALUE_2));

        $this->assertEquals('two', $cacheProperty->getValue($reflection)[$cacheKey]);

        $cacheProperty->setValue($reflection, [$cacheKey => 'updated']);

        $this->assertEquals('updated', TestCacheMapper::fromCore(new TestCacheEnum(TestCacheEnum::VALUE_2)));
    }

    public function testVerifiedMappers()
    {
        $reflection = new ReflectionClass(get_parent_class(TestVerifiedMappersMapper::class));

        $cacheProperty = $reflection->getProperty('verifiedMappers');

        $cacheProperty->setAccessible(true);

        TestVerifiedMappersMapper::toCore('two');

        TestVerifiedMappersMapper::toCore('one', new DifferentEnum(DifferentEnum::VALUE_2));

        $cacheProperty->setValue($reflection, []);

        $this->setExpectedException(
            RuntimeException::class,
            'All the values must be instance of the same enum class.'
        );

        TestVerifiedMappersMapper::toCore('three', new DifferentEnum(DifferentEnum::VALUE_2));
    }
} 