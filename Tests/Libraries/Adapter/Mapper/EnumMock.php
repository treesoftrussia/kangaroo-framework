<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Adapter\Mapper;

use Mildberry\Kangaroo\Libraries\Enum\Enum;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class EnumMock extends Enum
{
    const VALUE_1 = 1;
    const VALUE_2 = 2;
    const VALUE_3 = 3;
} 