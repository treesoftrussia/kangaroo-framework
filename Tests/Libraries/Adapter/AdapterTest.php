<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use PHPUnit_Framework_TestCase;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class AdapterTest extends PHPUnit_Framework_TestCase
{
    public function testDefault()
    {
        AbstractAdapter::setSharedModifiersProvider(AbstractAdapter::M_SANITIZER, new SanitizersProvider());
        AbstractAdapter::setSharedModifiersProvider(AbstractAdapter::M_MODIFIER, new ModifiersProvider());

        $adapter = new AdapterMock();

        $this->assertEquals([12, 40], $adapter->transform());
        $this->assertEquals([11, 21], $adapter->extract());
    }
} 