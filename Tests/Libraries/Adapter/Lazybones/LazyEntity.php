<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Adapter\Lazybones;

use Mildberry\Kangaroo\Libraries\Adapter\Lazybones\LazybonesTrait;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class LazyEntity extends Entity
{
    use LazybonesTrait;

    public function getRelatedEntities()
    {
        return $this->callbacks()->getRelatedEntities();
    }

    public function getRelatedEntitiesWithParams($param1, $param2)
    {
        return $this->callbacks()->getRelatedEntitiesWithParams($param1, $param2);
    }

    public function getRelatedEntitiesWithDefault($def = 'default')
    {
        return $this->callbacks()->getRelatedEntitiesWithDefault($def);
    }

    public function getCountCalls()
    {
        return $this->callbacks()->getCountCalls();
    }

    public function hasSomething()
    {
        return $this->callbacks()->hasSomething();
    }

    public function isTest()
    {
        return $this->callbacks()->isTest();
    }

    public function getItems()
    {
        return $this->callbacks()->getItems();
    }

    public function getInitializedValue()
    {
        return $this->callbacks()->getInitializedValue();
    }

    public function getChangedValue()
    {
        return $this->callbacks()->getChangedValue();
    }

    public function getChangedAllowableValue()
    {
        return $this->callbacks(true)->getChangedAllowableValue();
    }
} 