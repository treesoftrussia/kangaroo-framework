<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Adapter\Lazybones;

use Traversable;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Entity
{
    private $entities;
    private $entitiesWithParams;
    private $entitiesWithDefault;
    private $countCalls;

    private $hasValue;
    private $isTest;

    private $items;

    private $initializedValue = 'wrong';

    private $changedValue;

    private $changedAllowableValue;

    public function setRelatedEntities($entities)
    {
        $this->entities = $entities;
    }

    public function getRelatedEntities()
    {
        return $this->entities;
    }

    public function getRelatedEntitiesWithParams($text, $number)
    {
        return $this->entitiesWithParams;
    }

    public function setRelatedEntitiesWithParams($entities)
    {
        $this->entitiesWithParams = $entities;
    }

    public function getRelatedEntitiesWithDefault($default = 'default')
    {
        return $this->entitiesWithDefault;
    }

    public function setRelatedEntitiesWithDefault($entities)
    {
        $this->entitiesWithDefault = $entities;
    }

    public function getValue()
    {
        return 'value';
    }

    public function getCountCalls()
    {
        return $this->countCalls;
    }

    public function setCountCalls($value)
    {
        $this->countCalls = $value;
    }

    public function hasSomething()
    {
        return $this->hasValue;
    }

    public function setSomething($flag)
    {
        $this->hasValue = $flag;
    }

    public function isTest()
    {
        return $this->isTest;
    }

    public function setTest($flag)
    {
        $this->isTest = $flag;
    }

    public function setItems(Traversable $items)
    {
        $this->items = $items;
    }
    public function getItems()
    {
        return $this->items;
    }

    public function setInitializedValue($value)
    {
        $this->initializedValue = $value;
    }

    public function getInitializedValue()
    {
        return $this->initializedValue;
    }

    public function setChangedValue($value)
    {
        return $this->changedValue = $value;
    }

    public function getChangedValue()
    {
        return $this->changedValue;
    }

    public function setChangedAllowableValue($value)
    {
        return $this->changedAllowableValue = $value;
    }

    public function getChangedAllowableValue()
    {
        return $this->changedAllowableValue;
    }
} 