<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Adapter\Lazybones;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class CallbacksProvider
{
    private $counter = 0;

    public function getRelatedEntities()
    {
        return 'faker';
    }

    public function getRelatedEntitiesWithParams($param1, $param2)
    {
        return $param1 . '-' . $param2;
    }

    public function getRelatedEntitiesWithDefault($def = 'default')
    {
        return $def . '-test';
    }

    public function getCountCalls()
    {
        $this->counter++;

        return $this->counter;
    }

    public function hasSomething()
    {
        return false;
    }

    public function isTest()
    {
        return true;
    }

    public function getItems()
    {
        return null;
    }

    public function getInitializedValue()
    {
        return 'initialized value';
    }

    public function getChangedValue()
    {
        return 'changed value';
    }

    public function getChangedAllowableValue()
    {
        return 'changed allowable value';
    }
} 