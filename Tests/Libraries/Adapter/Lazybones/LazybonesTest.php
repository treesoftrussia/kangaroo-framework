<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Adapter\Lazybones;

use RuntimeException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class LazybonesTest extends \PHPUnit_Framework_TestCase
{
    public function testDefault()
    {
        /**
         * @var Entity $entity
         */
        $entity = LazyEntity::make(new CallbacksProvider());

        $this->assertInstanceOf(Entity::class, $entity);

        $this->assertEquals('faker', $entity->getRelatedEntities());
        $this->assertEquals('hello-11', $entity->getRelatedEntitiesWithParams('hello', 11));
        $this->assertEquals('default-test', $entity->getRelatedEntitiesWithDefault());
        $this->assertEquals('value', $entity->getValue());
        $this->assertNull($entity->getItems());


        $entity->getCountCalls();
        $entity->getCountCalls();
        $entity->getCountCalls();

        $this->assertEquals(1, $entity->getCountCalls());

        $this->assertFalse($entity->hasSomething());
        $this->assertTrue($entity->isTest());
    }

    public function textValueHasBeenInitializedAlready()
    {
        /**
         * @var Entity $entity
         */
        $entity = LazyEntity::make(new CallbacksProvider());

        $this->setExpectedException(RuntimeException::class, 'Value has been initialized already.');

        $entity->getInitializedValue();
    }

    public function testValueHasBeenChangedBySetter()
    {
        /**
         * @var Entity $entity
         */
        $entity = LazyEntity::make(new CallbacksProvider());


        $this->assertEquals('changed allowable value', $entity->getChangedAllowableValue());

        $entity->setChangedAllowableValue('custom value');

        $this->assertEquals('custom value', $entity->getChangedAllowableValue());


        $this->assertEquals('changed value', $entity->getChangedValue());

        $entity->setChangedValue('custom value');

        $this->setExpectedException(RuntimeException::class, 'Value has been initialized already.');

        $entity->getChangedValue();
    }
} 