<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\Modifier\Manager;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class AdapterMock extends AbstractAdapter
{
    /**
     * A hook allowing to register custom sanitizers
     *
     * @param Manager $manager
     */
    protected function registerSanitizer(Manager $manager)
    {
        $manager->register('add10', function ($value) {
            return $value + 10;
        });
    }

    /**
     * A hook allowing to register custom modifiers
     *
     * @param Manager $manager
     */
    protected function registerModifier(Manager $manager)
    {
        $manager->register('add2', function ($value) {
            return $value + 2;
        });
    }

    public function transform($model = null)
    {
        $modifier = $this->modifier()->make();

        return [
            $modifier(10, 'add2'),
            $modifier(10, 'add30')
        ];
    }

    public function extract()
    {
        $sanitizer = $this->sanitizer()->make();

        return [
            $sanitizer(1, 'add10'),
            $sanitizer(1, 'add20')
        ];
    }
} 