<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Mocks\Converter;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class CompanyMockPersistable
{
    private $name;
    private $pending;
    private $contactName;
    private $field;

    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param mixed $field
     * @return CompanyMockPersistable
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return CompanyMockPersistable
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPending()
    {
        return $this->pending;
    }

    /**
     * @param mixed $pending
     * @return CompanyMockPersistable
     */
    public function setPending($pending)
    {
        $this->pending = $pending;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * @param mixed $contactName
     * @return CompanyMockPersistable
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

}