<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Mocks;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */

class ContactMock
{
    private $firstName;
    private $lastName;
    private $type;

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }
}