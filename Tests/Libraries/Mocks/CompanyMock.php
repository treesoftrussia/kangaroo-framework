<?php

namespace Mildberry\Kangaroo\Tests\Libraries\Mocks;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */

/**
 * Class CompanyMock
 * @package Mildberry\Kangaroo\Tests\Libraries\Mocks
 */
class CompanyMock
{
    private $name;
    private $pending;
    private $contact;
    private $canField;

    /**
     * @return mixed
     */
    public function canField()
    {
        return $this->canField;
    }

    /**
     * @param mixed $canField
     * @return CompanyMock
     */
    public function setField($canField)
    {
        $this->canField = $canField;

        return $this;
    }

    /**
     * @return ContactMock
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param ContactMock $contact
     * @return $this
     */
    public function setContact(ContactMock $contact)
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param $pending
     * @return $this
     */
    public function setPending($pending)
    {
        $this->pending = $pending;
        return $this;
    }

    /**
     * @return mixed
     */
    public function isPending()
    {
        return $this->pending;
    }

    /**
     * @return mixed
     */
    public function getPending()
    {
        return $this->pending;
    }

    /**
     * @return bool
     */
    public function hasContact()
    {
        return $this->contact !== null;
    }

}