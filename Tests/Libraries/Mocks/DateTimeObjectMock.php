<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Mocks;

use DateTime;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class DateTimeObjectMock
{
    /**
     * @var DateTime
     */
    private $datetime;

    /**
     * @return DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @param DateTime $datetime
     * @return DateTimeObjectMock
     */
    public function setDatetime(DateTime $datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

}