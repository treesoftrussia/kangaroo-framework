<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Mocks\Enum;

use Mildberry\Kangaroo\Libraries\Enum\Enum;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class Test2Enum extends Enum
{
    const TEST_FIELD = 'test-field';
}