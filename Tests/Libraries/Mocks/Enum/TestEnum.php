<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Mocks\Enum;

use Mildberry\Kangaroo\Libraries\Enum\Enum;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class TestEnum extends Enum
{
    const FIELD_1 = 'field-1';
    const FIELD_2 = 'field-2';
    const FIELD_3 = 'field-3';
}