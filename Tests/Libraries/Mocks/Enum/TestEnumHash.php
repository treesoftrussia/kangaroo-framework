<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Mocks\Enum;
use Mildberry\Kangaroo\Libraries\Enum\EnumHash;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class TestEnumHash extends EnumHash
{
    /**
     * @return string
     */
    public function getEnumClass()
    {
        return TestEnum::class;
    }
}