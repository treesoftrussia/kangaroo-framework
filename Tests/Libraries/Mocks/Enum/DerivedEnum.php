<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Mocks\Enum;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class DerivedEnum extends TestEnum
{
    const DERIVED_FIELD = 'derived-field';
}