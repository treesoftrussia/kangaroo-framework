<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Mocks;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\Enum\TestEnumHash;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ObjectWithHash
{
    /**
     * @var TestEnumHash
     */
     private $levelTypes;

    /**
     * @return TestEnumHash
     */
    public function getLevelTypes()
    {
        return $this->levelTypes;
    }

    /**
     * @param TestEnumHash $levelTypes
     * @return ObjectWithHash
     */
    public function setLevelTypes(TestEnumHash $levelTypes)
    {
        $this->levelTypes = $levelTypes;

        return $this;
    }
}