<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Mocks;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ObjectTakeParent
{
    private $subObject;
    private $emptySubObject;
    private $protected;

    /**
     * @return mixed
     */
    protected function getProtected()
    {
        return $this->protected;
    }

    /**
     * @param mixed $protected
     * @return ObjectTakeParent
     */
    public function setProtected($protected)
    {
        $this->protected = $protected;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmptySubObject()
    {
        return $this->emptySubObject;
    }

    /**
     * @param mixed $emptySubObject
     * @return ObjectTakeParent
     */
    public function setEmptySubObject($emptySubObject)
    {
        $this->emptySubObject = $emptySubObject;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubObject()
    {
        return $this->subObject;
    }

    /**
     * @param mixed $subObject
     * @return ObjectTakeParent
     */
    public function setSubObject($subObject)
    {
        $this->subObject = $subObject;

        return $this;
    }
}