<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Mocks;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ObjectTakeChild
{
    private $name;
    private $emptyField;

    /**
     * @return mixed
     */
    public function getEmptyField()
    {
        return $this->emptyField;
    }

    /**
     * @param mixed $emptyField
     * @return ObjectTakeChild
     */
    public function setEmptyField($emptyField)
    {
        $this->emptyField = $emptyField;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return ObjectTakeChild
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}