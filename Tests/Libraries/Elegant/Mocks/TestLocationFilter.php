<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks;
use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Maxim Sergeev<raincoven@gmail.com>
 */
class TestLocationFilter
{
    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $country;

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return TestLocationFilter
     */
    public function setAddress($address)
    {
        $this->address = Cast::string($address);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     * @return TestLocationFilter
     */
    public function setCountry($country)
    {
        $this->country = Cast::string($country);
        return $this;
    }


}