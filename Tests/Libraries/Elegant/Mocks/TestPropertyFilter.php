<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks;

/**
 * @author Maxim Sergeev<raincoven@gmail.com>
 */
class TestPropertyFilter
{
    /**
     * @var TestLocationFilter
     */
    private $location;

    /**
     * @return object
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param TestLocationFilter $location
     * @return TestPropertyFilter
     */
    public function setLocation(TestLocationFilter $location)
    {
        $this->location = $location;
        return $this;
    }
}