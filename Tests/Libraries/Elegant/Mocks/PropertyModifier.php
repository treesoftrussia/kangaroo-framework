<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks;
use Mildberry\Kangaroo\Libraries\Elegant\AbstractModifier;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class PropertyModifier extends AbstractModifier
{
    /**
     * @param QueryMock $query
     * @param $object
     */
    public function applyLocation(QueryMock $query, $object)
    {
        $query->modify(new LocationModifier($object));
    }

    /**
     * @return string|string[]
     */
    protected function getSupportedModelClasses()
    {
        return ModelMock::class;
    }
}