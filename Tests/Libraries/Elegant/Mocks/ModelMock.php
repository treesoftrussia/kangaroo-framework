<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks;

use Illuminate\Database\Eloquent\Model;

/**
 * @author Sergei Melnikov <me@rnr.name>
 * @property mixed $attribute1
 * @property mixed $attribute2
 * @property mixed $table1_attribute3
 * @property mixed $attribute4
 * @property mixed $attribute5
 * @property mixed $attribute6
 */
class ModelMock extends Model
{
    protected $guarded = [];
}