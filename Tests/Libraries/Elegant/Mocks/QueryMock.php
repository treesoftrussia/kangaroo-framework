<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks;

use Mildberry\Kangaroo\Libraries\Elegant\AbstractModifier;
use Mildberry\Kangaroo\Libraries\Elegant\Sorting\AbstractSortSpecification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class QueryMock extends Builder
{
    private $trace = [];

    private $modelMock;

    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->modelMock = $model;
    }

    /**
     * @return Model
     */
    public function getModel()
    {
        return $this->modelMock;
    }

    /**
     * @param $method
     * @return mixed
     */
    public function getTrace($method)
    {
        return $this->trace[$method];
    }

    /**
     * @param $method
     * @param $value
     */
    public function addTrace($method, $value)
    {
        $this->trace[$method] = $value;
    }

    /**
     * @param AbstractModifier $modifier
     * @return $this
     */
    public function modify(AbstractModifier $modifier)
    {
        $modifier->modify($this);

        return $this;
    }

    /**
     * @param AbstractSortSpecification $sortOptions
     * @param string $key
     * @param string $direction
     * @return $this
     */
    public function sort(AbstractSortSpecification $sortOptions, $key, $direction)
    {
        $sortOptions->sort($this, $key, $direction);

        return $this;
    }

    /**
     * @param string $field
     * @param string $direction
     */
    public function orderBy($field, $direction)
    {
        $this->addTrace(__METHOD__ . "({$field})", "{$field}:{$direction}");
    }
}