<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks;

use Mildberry\Kangaroo\Libraries\Elegant\Elegant;

/**
 * @author Sergei Melnikov <me@rnr.name>
 * @property mixed $attribute4
 * @property mixed $attribute5
 * @property mixed $attribute6
 */
class Table2ModelMock extends Elegant
{
    /**
     * @return array
     */
    public function getColumns()
    {
        return ['attribute4', 'attribute5', 'attribute6'];
    }
}