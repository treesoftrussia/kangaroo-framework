<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks;

use Mildberry\Kangaroo\Libraries\Elegant\Sorting\AbstractSortSpecification;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class TestSortSpecification extends AbstractSortSpecification
{
    /**
     * @return string|string[]
     */
    protected function getSupportedModelClasses()
    {
        return ModelMock::class;
    }

    /**
     * @return array
     */
    public function specification()
    {
        return [
            'keyword' => 'keyword-field',
            'level1.level2' => 'level1-level2',
            'l1.l2.l3' => new NestedSortSpecification(),
            'relation' => function ($query, $direction, $key) {
                /**
                 * @var QueryMock $query
                 */
                $query->orderBy('relation-map', $direction);
            },
            'nested' => new NestedSortSpecification(),
        ];
    }
}