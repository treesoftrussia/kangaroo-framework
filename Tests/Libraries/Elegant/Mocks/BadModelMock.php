<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks;
use Illuminate\Database\Eloquent\Model;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class BadModelMock extends Model
{

} 