<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks;
use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Maxim Sergeev<raincoven@gmail.com>
 */
class TestFilter
{
    /**
     * @var array
     */
    private $ids;

    /**
     * @var string
     */
    private $keyword;

    /**
     * @var TestPropertyFilter
     */
    private $property;

    /**
     * @return array
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param array $ids
     * @return TestFilter
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
        return $this;
    }

    /**
     * @return string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     * @return TestFilter
     */
    public function setKeyword($keyword)
    {
        $this->keyword = Cast::string($keyword);
        return $this;
    }

    /**
     * @return array
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param TestPropertyFilter $property
     * @return TestFilter
     */
    public function setProperty(TestPropertyFilter $property)
    {
        $this->property = $property;
        return $this;
    }
}