<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks;
use Mildberry\Kangaroo\Libraries\Elegant\AbstractModifier;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ConstraintModifier extends AbstractModifier
{
    /**
     * @return string|string[]
     */
    protected function getSupportedModelClasses()
    {
        return [
            ModelMock2::class,
            ModelMock::class
        ];
    }
}