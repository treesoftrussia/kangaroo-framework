<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Elegant;

use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\BadModelMock;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\ChildModelMock;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\ConstraintModifier;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\LocationModifier;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\ModelMock2;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\QueryMock;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\TestFilter;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\TestLocationFilter;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\ModelMock;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\TestModifier;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\TestPropertyFilter;
use PHPUnit_Framework_TestCase;
use Mockery;
use RuntimeException;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ModifierTest extends PHPUnit_Framework_TestCase
{
    public function testModifier1()
    {
        $location = (new TestLocationFilter())
            ->setAddress('Test Street')
            ->setCountry('Test Country');

        $property = (new TestPropertyFilter())
            ->setLocation($location);

        $ids = [1, 3, 5, 8, 13];

        /**
         * @var $filter TestFilter
         */
        $filter = (new TestFilter())
            ->setIds($ids)
            ->setKeyword('Test Keyword')
            ->setProperty($property);

        $modifier = new TestModifier($filter);

        $query = new QueryMock(new ModelMock());

        $query->modify($modifier);

        $this->assertEquals($ids, $query->getTrace(TestModifier::class.'::applyIds'));
        $this->assertEquals('Test Keyword', $query->getTrace(TestModifier::class.'::applyKeyword'));

        $this->assertEquals('Test Street', $query->getTrace(LocationModifier::class.'::applyAddress'));
        $this->assertEquals('Test Country', $query->getTrace(LocationModifier::class.'::applyCountry'));
    }

    public function testSupportedModelClasses()
    {
        $modifier = new ConstraintModifier(new TestFilter());

        $query = new QueryMock(new ChildModelMock());
        $query->modify($modifier);

        $query = new QueryMock(new ModelMock2());
        $query->modify($modifier);

        $this->setExpectedException(RuntimeException::class, 'The model must be instance of one of the specified model classes in the "'
            .ConstraintModifier::class.'" modifier. The instance of "'.BadModelMock::class.'" has been given.');

        $query = new QueryMock(new BadModelMock());
        $query->modify($modifier);
    }
}