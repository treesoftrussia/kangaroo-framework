<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Elegant;

use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\ModelMock;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\Table1ModelMock;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\Table2ModelMock;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Database\Eloquent\Collection;
use PHPUnit_Framework_TestCase;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class CreateModelFromQueryResultTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    public function testItem()
    {
        $item = $this->createModel();
        $data = $this->getData($item);
        $this->assertItem($item, $data['table1'], $data['table2']);
    }

    public function testCollection()
    {
        $items = [];

        for ($i = $this->faker->numberBetween(5, 10); $i > 0; $i--) {
            $items[] = $this->createModel();
        }

        $items = new Collection($items);
        $data = $this->getData($items);

        foreach ($items as $key => $item) {
            $this->assertItem($item, $data['table1'][$key], $data['table2'][$key]);
        }
    }

    public function testRow()
    {
        $item = $this->createStdClass();
        $data = $this->getData($item);
        $this->assertItem($item, $data['table1'], $data['table2']);
    }

    public function testArray()
    {
        $items = [];

        for ($i = $this->faker->numberBetween(5, 10); $i > 0; $i--) {
            $items[] = $this->createStdClass();
        }

        $data = $this->getData($items);

        foreach ($items as $key => $item) {
            $this->assertItem($item, $data['table1'][$key], $data['table2'][$key]);
        }
    }

    /**
     * @param $item
     * @return array
     */
    private function getData($item)
    {
        /**
         * @var Table1ModelMock $table1
         */
        $table1 = Table1ModelMock::fromMixed($item, [
            'map' => ['table1_attribute3' => 'attribute3']
        ]);

        /**
         * @var Table2ModelMock $table2
         */
        $table2 = Table2ModelMock::fromMixed($item, [
            'ignore' => ['attribute6']
        ]);

        return [
            'table1' => $table1,
            'table2' => $table2
        ];
    }

    /**
     * @return ModelMock
     */
    private function createModel()
    {
        $item = new ModelMock();

        $item->fill([
            'attribute1' => $this->faker->text,
            'attribute2' => $this->faker->text,
            'table1_attribute3' => $this->faker->text,
            'attribute4' => $this->faker->text,
            'attribute5' => $this->faker->text,
            'attribute6' => $this->faker->text
        ]);

        return $item;
    }

    /**
     * @return object
     */
    private function createStdClass()
    {
        return (object)[
            'attribute1' => $this->faker->text,
            'attribute2' => $this->faker->text,
            'table1_attribute3' => $this->faker->text,
            'attribute4' => $this->faker->text,
            'attribute5' => $this->faker->text,
            'attribute6' => $this->faker->text
        ];
    }

    /**
     * @param $expected
     * @param $table1
     * @param $table2
     */
    private function assertItem($expected, $table1, $table2)
    {
        $this->assertSame($expected->attribute1, $table1->attribute1);
        $this->assertSame($expected->attribute2, $table1->attribute2);
        $this->assertSame($expected->table1_attribute3, $table1->attribute3);
        $this->assertSame($expected->attribute4, $table2->attribute4);
        $this->assertSame($expected->attribute5, $table2->attribute5);
        $this->assertEmpty($table2->attribute6);
    }
}