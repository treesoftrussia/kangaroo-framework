<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Elegant;

use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\ModelMock;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\QueryMock;
use Mildberry\Kangaroo\Tests\Libraries\Elegant\Mocks\TestSortSpecification;
use PHPUnit_Framework_TestCase;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class SortSpecificationTest extends PHPUnit_Framework_TestCase
{
    public function testSortable()
    {
        $keys = [
            'keyword' => 'keyword-field',
            'level1.level2' => 'level1-level2',
            'l1.l2.l3.field' => 'nested-string',
            'l1.l2.l3.callable' => 'nested-callable',
            'relation' => 'relation-map',
            'nested.field' => 'nested-string',
            'nested.callable' => 'nested-callable'
        ];

        foreach ($keys as $key => $field) {
            $specification = new TestSortSpecification();

            $query = new QueryMock(new ModelMock());

            foreach (['asc', 'desc'] as $direction) {
                $query->sort($specification, $key, $direction);

                $this->assertEquals("{$field}:{$direction}",
                    $query->getTrace(QueryMock::class . "::orderBy({$field})"));
            }
        }
    }
}