<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;

use Mildberry\Kangaroo\Libraries\Processor\SharedModifiers;
use Mildberry\Kangaroo\QA\Support\JsonRequest;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use PHPUnit_Framework_TestCase;

class ModifierTest extends PHPUnit_Framework_TestCase
{
    public function testExplodeModifier()
    {
        TestProcessor::setSharedModifiersProvider(new SharedModifiers());

        $request = new JsonRequest(Request::METHOD_GET, [
            'ids' => '1,2,3,4,5122,234',
        ]);

        $container = new Container();
        $container->instance(Request::class, $request);

        $processor = new TestProcessor($container);

        $this->assertEquals([1,2,3,4,5122,234], $processor->get('ids', null, 'explode'));

        $request = new JsonRequest(Request::METHOD_GET, [
            'LOOK' => 'OBOI',
        ]);

        $container->instance(Request::class, $request);

        $processor = new TestProcessor($container);

        $this->assertEquals(['OBOI'], $processor->get('LOOK', null, 'explode'));

        $container->instance(Request::class, new JsonRequest(Request::METHOD_GET));

        $processor = new TestProcessor($container);

        $this->assertEquals([], $processor->get('tenouttaten', null, 'explode'));
    }
}
