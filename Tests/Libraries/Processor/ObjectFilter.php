<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;
use Mildberry\Kangaroo\Libraries\Cast\Cast;
use DateTime;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ObjectFilter 
{
    private $string;
    private $number;
    private $date;
    private $enum;
    private $object;

    public function setString($string)
    {
        $this->string = Cast::string($string);
    }

    public function getString()
    {
        return $this->string;
    }

    public function setNumber($number)
    {
        $this->number = Cast::int($number);
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setDate(DateTime $datetime)
    {
        $this->date = $datetime;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setEnum(FilterEnum $enum)
    {
        $this->enum = $enum;
    }

    public function getEnum()
    {
        return $this->enum;
    }

    public function setObject(SubObjectFilter $object)
    {
        $this->object = $object;
    }

    public function getObject()
    {
        return $this->object;
    }
} 