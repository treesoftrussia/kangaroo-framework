<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class TestOptionsHolder
{
    private $keepInQueue;

    private $notify;

    public function setKeepInQueue($value)
    {
        $this->keepInQueue = $value;
    }

    public function getKeepInQueue()
    {
        return $this->keepInQueue;
    }

    public function setNotify(NotifyOptionsHolder $holder)
    {
        $this->notify = $holder;
    }

    /**
     * @return NotifyOptionsHolder
     */
    public function getNotify()
    {
        return $this->notify;
    }
} 