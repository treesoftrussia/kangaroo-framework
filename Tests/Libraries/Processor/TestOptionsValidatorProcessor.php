<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;
use Mildberry\Kangaroo\Libraries\Processor\AbstractProcessor;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class TestOptionsValidatorProcessor extends AbstractProcessor
{
    protected function rules()
    {
        return [
            'key1' => 'min:2',
            'key2' => 'min:2'
        ];
    }
}