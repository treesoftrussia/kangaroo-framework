<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;
use Mildberry\Kangaroo\Libraries\Processor\AbstractOptionsProcessor;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class TestOptionsProcessor extends AbstractOptionsProcessor
{
    protected function rules()
    {
        return [
            'option1' => 'min:2',
            'option2' => 'min:2',
        ];
    }

    protected function allowable()
    {
        return array_merge(array_keys($this->rules()), [
            'keepInQueue', 'notify.client', 'notify.appraiser'
        ]);
    }

    /**
     * @return TestOptionsHolder
     */
    public function createOptions()
    {
        return $this->populate(new TestOptionsHolder());
    }
} 