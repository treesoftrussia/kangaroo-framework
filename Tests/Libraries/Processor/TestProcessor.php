<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;

use Mildberry\Kangaroo\Libraries\Modifier\Manager;
use Mildberry\Kangaroo\Libraries\Processor\AbstractProcessor;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class TestProcessor extends AbstractProcessor
{
    protected function rules()
    {
        return [
            'key4' => 'max:10|min:5',
            'key5' => 'min:10',
        ];
    }

    protected function modifiers(Manager $manager)
    {
        $manager->register('multiple', function ($value) {
            return $value * 3;
        });
    }
}