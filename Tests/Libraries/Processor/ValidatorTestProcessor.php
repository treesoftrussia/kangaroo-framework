<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;

use Mildberry\Kangaroo\Libraries\Processor\AbstractProcessor;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ValidatorTestProcessor extends AbstractProcessor
{
    protected function rules()
    {
        return [
            [
                'key1' => 'max:100', // will pass
                'key2' => 'max:5', // will pass
                'key3' => 'required' // will fail
            ],

            [
                'key1' => 'max:5', // will fail
                'key2' => 'max:5' // will pass
            ]
        ];
    }
}