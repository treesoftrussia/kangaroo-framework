<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;

use Illuminate\Validation\Validator;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ValidatorMock extends Validator
{
} 