<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;

use Mildberry\Kangaroo\Libraries\Enum\Enum;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class FilterEnum extends Enum
{
    const VALUE = 10;
} 