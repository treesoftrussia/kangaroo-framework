<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class SharedModifiersProvider
{
    public function wrap($value)
    {
        return '_' . $value . '_';
    }
} 