<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;

use Mildberry\Kangaroo\Libraries\Processor\AbstractProcessor;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class RequestValidatorProcessor extends AbstractProcessor
{
    public function allowable()
    {
        return ['name', 'contact.firstName', 'contact'];
    }

    public function populate($object, array $options = [], array $data = null, $try = false)
    {
        return parent::populate($object, $options, $data, $try);
    }
}