<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class NotifyOptionsHolder 
{
    private $client;
    private $appraiser;
    private $none;

    public function setClient($value)
    {
        $this->client = $value;
    }

    /**
     * @return bool
     */
    public function getClient()
    {
        return $this->client;
    }

    public function setAppraiser($value)
    {
        $this->appraiser = $value;
    }

    /**
     * @return bool
     */
    public function getAppraiser()
    {
        return $this->appraiser;
    }

    public function setNone($value)
    {
        $this->none = $value;
    }

    /**
     * @return bool
     */
    public function getNone()
    {
        return $this->none;
    }
} 