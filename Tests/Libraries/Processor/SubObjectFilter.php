<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;
use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class SubObjectFilter 
{
    private $value;

    public function setValue($value)
    {
        $this->value = Cast::string($value);
    }

    public function getValue()
    {
        return $this->value;
    }
} 