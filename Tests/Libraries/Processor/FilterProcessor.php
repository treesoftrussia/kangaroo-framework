<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;
use Mildberry\Kangaroo\Libraries\Processor\AbstractFilterProcessor;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class FilterProcessor extends AbstractFilterProcessor
{
    protected function rules()
    {
        return [
            'string' => 'min:5'
        ];
    }

    protected function allowable()
    {
        return [
            'string',
            'date',
            'enum',
            'object.value'
        ];
    }

    /**
     * @return ObjectFilter
     */
    public function createFilter()
    {
        return $this->populate(new ObjectFilter());
    }
} 