<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Processor;

use Illuminate\Validation\Factory;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ValidatorFactoryMock extends Factory
{
    public function __construct()
    {
        //
    }

    public function make(array $data, array $rules, array $messages = array(), array $customAttributes = array())
    {
        return new ValidatorMock(new TranslatorMock(), $data, $rules, $messages, $customAttributes);
    }
} 