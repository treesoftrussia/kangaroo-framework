<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Enum;

use Mildberry\Kangaroo\Tests\Libraries\Mocks\Enum\DerivedEnum;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\Enum\Test2Enum;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\Enum\TestEnum;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\Enum\TestEnumHash;
use PHPUnit_Framework_TestCase;
use RuntimeException;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class EnumHashTest extends PHPUnit_Framework_TestCase
{
    public function testSimpleHash()
    {
        $enumHash = new TestEnumHash();
        $enumHash->set(new TestEnum(TestEnum::FIELD_1), 'field5555');

        $this->assertTrue($enumHash->has(new TestEnum(TestEnum::FIELD_1)));
        $this->assertFalse($enumHash->has(new TestEnum(TestEnum::FIELD_2)));
        $this->assertEquals($enumHash->get(new TestEnum(TestEnum::FIELD_1)), 'field5555');
        $this->assertEquals($enumHash->get(new TestEnum(TestEnum::FIELD_2), 'test'), 'test');

        $keys = [];

        foreach ($enumHash->keys() as $key) {
            $keys[] = $key->value();
        }

        $this->assertEquals([TestEnum::FIELD_1], $keys);
    }

    public function testWrongType()
    {
        $testClass = TestEnum::class;
        $test2Class = Test2Enum::class;

        $this->setExpectedException(RuntimeException::class,
            "Key should be '{$testClass}' but received '{$test2Class}'");
        $enumHash = new TestEnumHash();
        $enumHash->set(new Test2Enum(Test2Enum::TEST_FIELD), 'test-value');
    }

    public function testCheckInheritance()
    {
        $enumHash = new TestEnumHash();
        $enumHash->set(new DerivedEnum(DerivedEnum::FIELD_1), 'field5555');
        $this->assertEquals($enumHash->get(new TestEnum(TestEnum::FIELD_1)), 'field5555');

        $testClass = TestEnum::class;
        $test2Class = DerivedEnum::class;

        $this->setExpectedException(RuntimeException::class,
            "Key should be '{$testClass}' but received '{$test2Class}'");
        $enumHash->set(new DerivedEnum(DerivedEnum::DERIVED_FIELD), 'field5555');
    }
}