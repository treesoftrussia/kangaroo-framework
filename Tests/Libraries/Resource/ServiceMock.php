<?php

namespace Mildberry\Kangaroo\Tests\Libraries\Resource;
use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Service\AbstractService;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class ServiceMock extends AbstractService
{
    public function getAll(){
        return new Collection([new EntityMock(),new EntityMock(),new EntityMock(),new EntityMock()]);
    }

    public function getTotal(){
        return 4;
    }
}