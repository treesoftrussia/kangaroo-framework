<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Resource;

use Mildberry\Kangaroo\Libraries\Specification\ASpecification;
use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class SpecificationMock extends ASpecification
{

    protected $specificationType = ASpecification::RESPONSE_SPECIFICATION;

    public function body()
    {
        return new ObjectType([
            'id' => new IntegerType(),
            'name' => new StringType(),
            'description' => new StringType()
        ]);
    }
}