<?php

namespace Mildberry\Kangaroo\Tests\Libraries\Resource;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class EntityMock
{
    private $id;

    private $name;

    private $description;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return EntityMock
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return EntityMock
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return EntityMock
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
}