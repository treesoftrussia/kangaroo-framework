<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Resource;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Foundation\Application;
use Illuminate\Support\Collection;
use League\Flysystem\Config;
use Mildberry\Kangaroo\Libraries\Options\Objects\PaginationOptions;
use Mildberry\Kangaroo\Libraries\Resource\Error;
use Mildberry\Kangaroo\Libraries\Resource\Integrations\Transformer;
use Mildberry\Kangaroo\Libraries\Resource\Resource\Manager;
use Mildberry\Kangaroo\Libraries\Resource\Response\JsonResponseFactory;
use Mildberry\Kangaroo\Libraries\Service\DefaultPaginationServiceAdapter;
use Mildberry\Kangaroo\Tests\Libraries\Processor\TranslatorMock;
use PHPUnit_Framework_TestCase;
use Illuminate\Container\Container;
use Illuminate\Validation\Factory as ValidationFactory;
use Illuminate\Contracts\Validation\Factory as FactoryContract;
use Symfony\Component\Translation\TranslatorInterface;
use Mildberry\Kangaroo\Libraries\Container\Container as KangarooContainer;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class ResourceTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->app = new Application();
        $this->createContainer();
    }

    private function createContainer()
    {
        $container = new Container();
        $container->bind(FactoryContract::class, ValidationFactory::class);
        $container->bind(TranslatorInterface::class, TranslatorMock::class);
        $container->singleton('config', Config::class);

        Container::setInstance($container);
    }


    private function createManagerInstance()
    {
        $responseFactory = new JsonResponseFactory();

        return new Manager(new Transformer(), $responseFactory, new Error($responseFactory));
    }

    function testSingleResponseGeneration()
    {
        $managerInstance = $this->createManagerInstance();

        $entityMock = new EntityMock();
        $response = $managerInstance->make($entityMock, SpecificationMock::class);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('{"id":null,"name":null,"description":null}', $response->getContent());
    }

    function testCollectionResponseGeneration()
    {
        $managerInstance = $this->createManagerInstance();

        $entityMock = new EntityMock();
        $collectionMock = new Collection([$entityMock, $entityMock, $entityMock, $entityMock]);

        $response = $managerInstance->makeAll($collectionMock, SpecificationMock::class);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('[{"id":null,"name":null,"description":null},{"id":null,"name":null,"description":null},{"id":null,"name":null,"description":null},{"id":null,"name":null,"description":null}]', $response->getContent());
    }

    function testPaginationResponseGeneration()
    {
        $managerInstance = $this->createManagerInstance();

        $collectionMock = new DefaultPaginationServiceAdapter(new ServiceMock(new KangarooContainer(Container::getInstance())));
        $allEntries = $collectionMock->getAll((new PaginationOptions())->setPageNum(0)->setPerPage(10));

        $response = $managerInstance->makeAll($allEntries, SpecificationMock::class);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('{"items":[{"id":null,"name":null,"description":null},{"id":null,"name":null,"description":null},{"id":null,"name":null,"description":null},{"id":null,"name":null,"description":null}],"pagination":{"totalEntries":4,"entriesOnCurrentPage":4,"entriesPerPageRequested":10,"currentPage":0,"totalPages":1}}', $response->getContent());
    }
}