<?php
namespace Mildberry\Kangaroo\Tests\Libraries\JoinsNormalizer;

use Mildberry\Kangaroo\Libraries\Elegant\JoinsNormalizer;
use Illuminate\Database\Query\JoinClause;

/**
 * @author Maxim Sergeev<raincoven@gmail.com>
 */
class JoinsNormalizerTest extends \PHPUnit_Framework_TestCase
{
    public function testEmptyJoin()
    {
        $joins = [];
        $JoinsNormalizer = new JoinsNormalizer($joins);
        $this->assertEquals([], $JoinsNormalizer->process(), 'Empty array expected.');
    }

    public function testSingleJoin()
    {
        $joins = [
            (new JoinClause('left', 'dummy_table'))
                ->on('foo', '=', 'bar')
                ->where('tinky', '=', 'winky'),
        ];

        $JoinsNormalizer = new JoinsNormalizer($joins);

        $this->assertEquals(1, count($JoinsNormalizer->process()), 'Array expected to have 1 item.');
        $this->assertEquals($joins, $JoinsNormalizer->process(), 'Input and result arrays expected to be the same.');
    }

    public function testNoDuplicates()
    {
        $joins = [
            (new JoinClause('left', 'dummy_table'))
                ->on('foo', '=', 'bar'),
            (new JoinClause('left', 'pokemon_table'))
                ->on('charmander', '=', 'snorlax'),
        ];

        $JoinsNormalizer = new JoinsNormalizer($joins);

        $this->assertEquals(2, count($JoinsNormalizer->process()), 'Array expected to have 2 items.');
        $this->assertEquals($joins, $JoinsNormalizer->process(), 'Input and result arrays expected to be the same.');
    }

    public function testSimpleDuplicates()
    {
        $joins = [
            (new JoinClause('left', 'dummy_table'))
                ->on('foo', '=', 'bar'),
            (new JoinClause('left', 'dummy_table'))
                ->on('foo', '=', 'bar'),
        ];

        $JoinsNormalizer = new JoinsNormalizer($joins);

        $this->assertEquals(1, count($JoinsNormalizer->process()), 'Array expected to have 1 item ('.__CLASS__.':'.__LINE__.')');
        $this->assertEquals([$joins[0]], $JoinsNormalizer->process());
    }

    public function testComplexDuplicates()
    {
        $joins = [
            (new JoinClause('left', 'dummy_table'))
                ->on('foo', '=', 'bar'),
            (new JoinClause('right', 'dummy_table'))
                ->on('foo', '=', 'bar'),
            (new JoinClause('left', 'pokemon_table'))
                ->on('charmander', '=', 'snorlax'),
            (new JoinClause('left', 'pokemon_table'))
                ->on('snorlax', '=', 'charmander'),
            (new JoinClause('left', 'pokemon_table'))
                ->on('snorlax', '=', 'charmander')
                ->on('bulbasaur', '=', 'charizard'),

        ];

        $JoinsNormalizer = new JoinsNormalizer($joins);

        $this->assertEquals(4, count($JoinsNormalizer->process()), 'Array expected to have 4 items.');
    }

    /**
     * Test shows that JoinClause object comparing works correct.
     */
    public function testJoinsComparing()
    {
        $joins = [
            (new JoinClause('left', 'dummy_table'))
                ->on('foo', '=', 'bar')
                ->where('tinky', '=', 'winky'),
            (new JoinClause('left', 'dummy_table'))
                ->on('foo', '=', 'bar')
                ->where('tinky', '=', 'winky'),
            (new JoinClause('left', 'dummy_table'))
                ->on('foo', '=', 'bar')
                ->on('tinky', '=', 'winky'),
            (new JoinClause('inner', 'dummy_table'))
                ->on('foo', '=', 'bar')
                ->on('tinky', '=', 'winky')
        ];

        $this->assertTrue($joins[0] == $joins[1], 'Expected the same JoinClause statements');
        $this->assertFalse($joins[1] == $joins[2], 'JoinClause statements expected to be different');
        $this->assertFalse($joins[2] == $joins[3], 'JoinClause statements expected to be different');
        $this->assertFalse($joins[1] == $joins[3], 'JoinClause statements expected to be different');
    }

    public function testSameJoinsWithReverseOrder()
    {
        $joins = [
            (new JoinClause('left', 'dummy_table'))
                ->on('foo', '=', 'bar'),
            (new JoinClause('left', 'dummy_table'))
                ->on('bar', '=', 'foo'),
        ];

        $JoinsNormalizer = new JoinsNormalizer($joins);

        $this->assertEquals(1, count($JoinsNormalizer->process()), 'Array expected to have 1 item.');
        $this->assertEquals([$joins[0]], $JoinsNormalizer->process(), 'Objects expected to be equal.');

        $joins = [
            (new JoinClause('left', 'dummy_table'))
                ->on('foo', '=', 'bar')
                ->on('tinky', '=', 'winky'),
            (new JoinClause('left', 'dummy_table'))
                ->on('bar', '=', 'foo')
                ->on('tinky', '=', 'winky'),
        ];

        $JoinsNormalizer->setJoins($joins);

        $this->assertEquals([$joins[0]], $JoinsNormalizer->process(), 'Objects expected to be equal.');
        $this->assertEquals(1, count($JoinsNormalizer->process()), 'Array expected to have 1 item.');

        $joins = [
            (new JoinClause('left', 'dummy_table'))
                ->on('foo', '=', 'bar')
                ->on('snorlax', '=', 'charmander')
                ->on('tinky', '=', 'winky'),

            (new JoinClause('left', 'dummy_table'))
                ->on('bar', '=', 'foo')
                ->on('tinky', '=', 'winky')
                ->on('snorlax', '=', 'charmander'),

            (new JoinClause('left', 'dummy_table'))
                ->on('winky', '=', 'tinky')
                ->on('snorlax', '=', 'charmander')
                ->on('bar', '=', 'foo'),
        ];

        $JoinsNormalizer->setJoins($joins);

        $this->assertEquals([$joins[0]], $JoinsNormalizer->process(), 'Objects expected to be equal.');
        $this->assertEquals(1, count($JoinsNormalizer->process()), 'Array expected to have 1 item.');
    }
}