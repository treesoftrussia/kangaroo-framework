<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Extractor;

use Mildberry\Kangaroo\Libraries\Converter\Extractor\Extractor;
use Mildberry\Kangaroo\Libraries\Modifier\Manager;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\EnumMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\ExtractorModifierMock;
use PHPUnit_Framework_TestCase;
use DateTime;
use DateInterval;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ModifierTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Manager
     */
    private $modifierManager;
    /**
     * @var ExtractorModifierMock
     */
    private $data;

    public function setUp()
    {
        parent::setUp();

        $this->modifierManager = new Manager();

        $this->modifierManager->register('timezone', function ($value) {
            /**
             * @var DateTime $value
             */
            $date = clone $value;
            return $date->add(new DateInterval('PT1H'));
        });

        $this->modifierManager->register('datetime', function ($value) {
            /**
             * @var DateTime $value
             */
            return $value->format('Y-m-d H:i:s');
        });

        $this->modifierManager->register('enum', function ($value) {
            static $strings = [
                EnumMock::TEST_VALUE_1 => 'Test value 1',
                EnumMock::TEST_VALUE_2 => 'Test value 2'
            ];

            return $strings[$value];
        });

        $this->modifierManager->register('swap', function ($value) {
            static $map = [
                EnumMock::TEST_VALUE_1 => EnumMock::TEST_VALUE_2,
                EnumMock::TEST_VALUE_2 => EnumMock::TEST_VALUE_1
            ];

            /**
             * @var EnumMock $value
             */

            return new EnumMock($map[$value->value()]);
        });

        $this->data = $this->getObject();
    }

    public function testBefore()
    {
        $array = (new Extractor([
            'modify' => [
                'before' => [
                    'created' => 'timezone'
                ]
            ]
        ]))->modify('before', 'enum', 'swap')
            ->setModifierManager($this->modifierManager)->extract($this->data);

        $this->assertInternalType('array', $array);
        $this->assertSame($this->data->getCreated()->add(new DateInterval('PT1H'))
            ->format('Y-m-d H:i:s'), $array['created']);
        $this->assertSame(EnumMock::TEST_VALUE_2, $array['enum']);
    }

    public function testAfter()
    {
        $array = (new Extractor())->modify('after', 'enum', 'enum')
            ->setModifierManager($this->modifierManager)->extract($this->data);

        $this->assertInternalType('array', $array);
        $this->assertSame('Test value 1', $array['enum']);

    }

    /**
     * @return ExtractorModifierMock
     */
    protected function getObject()
    {
        $object = new ExtractorModifierMock();
        $object->setCreated(new DateTime())
            ->setEnum(new EnumMock(EnumMock::TEST_VALUE_1));

        return $object;
    }
}