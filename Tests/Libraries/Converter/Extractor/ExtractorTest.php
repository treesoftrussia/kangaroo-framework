<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Extractor;

use Mildberry\Kangaroo\Libraries\Converter\Extractor\Extractor;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\AppraisalMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\ClientMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\CompanyEnumMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\Insensitive;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\CompanyMock;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\ContactMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\EnumMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\ObjectWithArrayMock;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\DateTimeObjectMock;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\Enum\TestEnum;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\Enum\TestEnumHash;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\ObjectWithHash;
use Faker\Factory;
use DateTime;

use Faker\Generator;
use PHPUnit_Framework_TestCase;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ExtractorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    public function testSuccessObject()
    {
        $company = $this->fillObject(new CompanyMock());
        /** @var ContactMock $contact */
        $contact = $company->getContact();

        $array = (new Extractor())->extract($company);
        $this->assertInternalType('array', $array);

        $this->assertArrayKeyEqual('name', $array, $company->getName());

        $this->assertArrayHasKey('contact', $array);
        $this->assertInternalType('array', $array['contact']);

        $this->assertArrayKeyEqual('pending', $array, $company->getPending());
        $this->assertArrayKeyEqual('isPending', $array, $company->isPending());
        $this->assertArrayKeyEqual('hasContact', $array, $company->hasContact());
        $this->assertArrayKeyEqual('canField', $array, $company->canField());

        $contactArray = $array['contact'];

        $this->assertContact($contactArray, $contact);
    }

    public function testEnums()
    {
        /** @var CompanyEnumMock $company */
        $company = $this->fillObject(new CompanyEnumMock());

        $enumValue = new EnumMock(2);
        $company->setEnumerableField($enumValue);

        $array = (new Extractor())->extract($company);

        $this->assertInternalType('array', $array);
        $this->assertArrayKeyEqual('enumerableField', $array, $enumValue->value());
    }

    public function testIgnore()
    {
        $company = $this->fillObject(new CompanyMock());

        $array = (new Extractor())->ignore(['contact'])->extract($company);
        $array2 = (new Extractor([
            'ignore' => ['name', 'pending', 'contact.firstName']
        ]))->extract($company);

        $this->assertInternalType('array', $array);
        $this->assertArrayNotHasKey('contact', $array);

        $this->assertInternalType('array', $array2);
        $this->assertArrayNotHasKey('name', $array2);
        $this->assertArrayNotHasKey('pending', $array2);
        $this->assertArrayHasKey('contact', $array2);
        $this->assertArrayNotHasKey('firstName', $array2['contact']);
    }

    public function testArray()
    {
        $objectWithArray = new ObjectWithArrayMock();
        $contacts = [
            $this->createContact(),
            $this->createContact()
        ];

        $objectWithArray->setContacts($contacts);

        $array = (new Extractor())->extract($objectWithArray);

        $this->assertInternalType('array', $array);
        $this->assertArrayHasKey('contacts', $array);
        $this->assertEquals(count($array['contacts']), count($contacts));

        for ($i = 0; $i < count($contacts); $i++) {
            $this->assertInternalType('array', $array['contacts'][$i]);
            $this->assertContact($array['contacts'][$i], $contacts[$i]);
        }
    }

    public function testInsensitive()
    {
        $object = new Insensitive();
        $object->setInsensitivefield($this->faker->randomNumber);

        $array = (new Extractor())->ignore(['insensitiveField'])->extract($object);
        $this->assertInternalType('array', $array);
        $this->assertEmpty($array);
    }

    public function testMap()
    {
        $object = new AppraisalMock();
        $client = new ClientMock();
        $client->setFullName($this->faker->name);
        $object->setClient($client);

        $array = (new Extractor([
            'map' => [
                'client.fullName' => 'name'
            ]
        ]))->extract($object);

        $this->assertEquals($array['client']['name'], $client->getFullName());
    }

    public function testDateTime()
    {
        $object = new DateTimeObjectMock();
        $currentDate = new DateTime();
        $object->setDatetime($currentDate);

        $array = (new Extractor())->extract($object);
        $this->assertEquals($array['datetime'], $object->getDatetime()->format('Y-m-d H:i:s'));
    }

    public function testHashEnum() {
        $object = new ObjectWithHash();
        $hash = new TestEnumHash();
        $hash->set(new TestEnum(TestEnum::FIELD_1), $this->faker->company);
        $hash->set(new TestEnum(TestEnum::FIELD_3), $this->faker->company);

        $object->setLevelTypes($hash);

        $array = (new Extractor())->extract($object);

        $this->assertArrayHasKey('levelTypes', $array);
        $this->assertArrayHasKey(TestEnum::FIELD_1, $array['levelTypes']);
        $this->assertArrayHasKey(TestEnum::FIELD_3, $array['levelTypes']);

        $this->assertEquals($object->getLevelTypes()->get(new TestEnum(TestEnum::FIELD_1)),
            $array['levelTypes'][TestEnum::FIELD_1]);
        $this->assertEquals($object->getLevelTypes()->get(new TestEnum(TestEnum::FIELD_3)),
            $array['levelTypes'][TestEnum::FIELD_3]);
    }

    /**
     * @param CompanyMock $company
     * @return CompanyMock
     */
    protected function fillObject($company)
    {
        $contact = $this->createContact();

        $company->setName($this->faker->company)
            ->setPending($this->faker->boolean(50))
            ->setContact($contact);

        return $company;
    }

    /**
     * @return ContactMock
     */
    protected function createContact()
    {
        $contact = new ContactMock();

        $contact->setFirstName($this->faker->firstName)
            ->setLastName($this->faker->lastName);

        return $contact;
    }

    /**
     * @param array $expected
     * @param ContactMock $actual
     */
    protected function assertContact($expected, $actual)
    {
        $this->assertArrayKeyEqual('firstName', $expected, $actual->getFirstName());
        $this->assertArrayKeyEqual('lastName', $expected, $actual->getLastName());
    }

    /**
     * @param string $key
     * @param array $array
     * @param mixed $value
     * @param string $message
     */
    protected function assertArrayKeyEqual($key, $array, $value, $message = '')
    {
        $this->assertArrayHasKey($key, $array, $message);
        $this->assertEquals($array[$key], $value, $message);
    }
}