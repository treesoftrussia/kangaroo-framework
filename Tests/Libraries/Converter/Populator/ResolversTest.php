<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Populator;

use Mildberry\Kangaroo\Libraries\Converter\Populator\Populator;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\ContactMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\ContactMockResolver;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\MultipleContactMock;
use PHPUnit_Framework_TestCase;

use Faker\Factory;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ResolversTest extends PHPUnit_Framework_TestCase
{
    private $faker;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    public function testResolver()
    {
        $array = [
            'borrower' => [
                'firstName' => $this->faker->firstName,
                'lastName' => $this->faker->lastName,
            ],
            'owner' => [
                'firstName' => $this->faker->firstName,
                'lastName' => $this->faker->lastName,
            ]
        ];

        /** @var MultipleContactMock $object */
        $object = (new Populator([
            'resolver' => [
                'borrower' => new ContactMockResolver('borrower')
            ]]))->resolver('owner', new ContactMockResolver('owner'))
            ->populate($array, new MultipleContactMock());

        $this->assertInstanceOf(MultipleContactMock::class, $object);

        $borrower = $object->getBorrower();
        $owner = $object->getOwner();

        $this->assertInstanceOf(ContactMock::class, $borrower);
        $this->assertEquals($borrower->getType(), 'borrower');
        $this->assertInstanceOf(ContactMock::class, $owner);
        $this->assertEquals($owner->getType(), 'owner');
    }
}