<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Populator;

use Mildberry\Kangaroo\Libraries\Converter\Populator\PopulatorException;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Populator;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\AppraisalMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\ExtendedContactMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\Insensitive;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\CompanyEnumMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\VectorTestMock;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\CompanyMock;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\ContactMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\EnumMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\ObjectWithObjectWithArrayMock;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\Enum\TestEnum;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\ObjectWithHash;
use Faker\Factory;
use Faker\Generator;
use PHPUnit_Framework_TestCase;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class PopulatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    public function testSuccessArray()
    {
        $array = $this->getArray();

        /**
         * @var CompanyMock $object
         */
        $object = (new Populator())->populate($array, new CompanyMock());

        $this->assertInstanceOf(CompanyMock::class, $object);
        /**
         * @var ContactMock $contact
         */
        $contact = $object->getContact();
        $this->assertInstanceOf(ContactMock::class, $contact);

        $this->assertEquals($object->getName(), $array['name']);
        $this->assertEquals($object->isPending(), $array['isPending']);
        $this->assertEquals($object->hasContact(), $array['hasContact']);
        $this->assertEquals($object->canField(), $array['canField']);

        $this->assertEquals($contact->getFirstName(), $array['contact']['firstName']);
        $this->assertEquals($contact->getLastName(), $array['contact']['lastName']);
    }

    public function testEnum()
    {
        $array = $this->getArray();
        $array['enumerableField'] = 2;

        /**
         * @var CompanyEnumMock $object
         */
        $object = (new Populator())->populate($array, new CompanyEnumMock());

        /**
         * @var EnumMock $enumValue
         */
        $enumValue = $object->getEnumerableField();
        $this->assertInstanceOf(EnumMock::class, $enumValue);
        $this->assertEquals($enumValue->value(), $array['enumerableField']);
    }

    /**
     * @return array
     */
    protected function getArray()
    {
        $isPending = true;

        return [
            'contact' =>
                [
                    'firstName' => $this->faker->firstName,
                    'lastName' => $this->faker->lastName,
                ],
            'name' => $this->faker->company,
            'pending' => $isPending,
            'isPending' => $isPending,
            'hasContact' => true,
            'canField' => $this->faker->text
        ];
    }

    public function testInsensitive()
    {
        $array = [
            'insensitiveField' => $this->faker->sentence
        ];

        /**
         * @var Insensitive $object
         */
        $object = (new Populator())->populate($array, new Insensitive());

        $this->assertEquals($object->getInsensitivefield(), $array['insensitiveField']);
    }

    public function testNonexistence()
    {
        (new Populator())->populate([], new CompanyMock());
    }

    public function testIgnore()
    {
        $array = $this->getArray();

        /**
         * @var CompanyMock $object
         */
        $object = (new Populator())->ignore(['contact'])->populate($array, new CompanyMock());
        /**
         * @var CompanyMock $object2
         */
        $object2 = (new Populator([
            'ignore' => ['name', 'pending', 'contact.firstName', 'isPending']
        ]))->populate($array, new CompanyMock());

        $this->assertInstanceOf(CompanyMock::class, $object);
        $this->assertEmpty($object->getContact());

        $this->assertInstanceOf(CompanyMock::class, $object2);
        $this->assertEmpty($object2->getName());
        $this->assertEmpty($object2->getPending());
        $this->assertNotEmpty($object2->getContact());
        $this->assertEmpty($object2->getContact()->getFirstName());
    }

    public function testNotAllowedArray()
    {
        $this->setExpectedException(
            PopulatorException::class,
            "Can't populate object key 'object.contacts' has not allowed array.");
        $array = $this->getNestedArray();

        (new Populator())->populate($array, new ObjectWithObjectWithArrayMock());
    }

    public function testAllowedArray()
    {
        $array = $this->getNestedArray();

        (new Populator())->allow(['object.contacts'])
            ->populate($array, new ObjectWithObjectWithArrayMock());

        (new Populator(['allow' => ['object.contacts']]))
            ->populate($array, new ObjectWithObjectWithArrayMock());
    }


    public function testHint()
    {
        $array = [
            'contact' => [
                'firstName' => $this->faker->firstName,
                'lastName' => $this->faker->lastName,
                'extendField' => $this->faker->sentence
            ]
        ];

        /**
         * @var ObjectWithObjectWithArrayMock $object
         */
        $object = (new Populator())->hint('contact', ExtendedContactMock::class)
            ->populate($array, new ObjectWithObjectWithArrayMock());

        $this->assertInstanceOf(ObjectWithObjectWithArrayMock::class, $object);
        $this->assertInstanceOf(ExtendedContactMock::class, $object->getContact());
    }

    public function testMap()
    {
        $populator = new Populator([
            'map' => [
                'client.name' => 'fullName'
            ]
        ]);

        $data = [
            'client' => [
                'name' => 'Test User'
            ]
        ];

        /**
         * @var AppraisalMock $appraisal
         */
        $appraisal = $populator->populate($data, new AppraisalMock());

        $this->assertEquals('Test User', $appraisal->getClient()->getFullName());
    }

    public function testUnusedField()
    {
        $data = [
            'unusedField' => 'test',
            'client' => [
                'fullName' => $this->faker->firstName
            ]
        ];

        (new Populator())->populate($data, new AppraisalMock());
    }

    public function testBoolField()
    {
        $data = [
            'isPending' => true
        ];

        /**
         * @var CompanyMock $object
         */
        $object = (new Populator())->populate($data, new CompanyMock());
        $this->assertTrue($object->isPending());
    }

    public function testVectorResolver()
    {
        $values = [12, 'string', false, true, null, 99];

        $data = [
            'values' => $values
        ];

        $object = (new Populator())->populate($data, new VectorTestMock());

        $this->assertTrue(array_equal($values, $object->getValues()));
    }

    public function testEnumHash()
    {
        $data = [
            'levelTypes' => [
                'field-1' => [
                    'firstName' => $this->faker->firstName,
                    'lastName' => $this->faker->lastName,
                ],
                'field-3' => [
                    'firstName' => $this->faker->firstName,
                    'lastName' => $this->faker->lastName,
                ],
            ]
        ];

        /**
         * @var ObjectWithHash $object
         */
        $object = (new Populator())->hint('levelTypes', ContactMock::class)
            ->populate($data, new ObjectWithHash());

        $this->assertEquals($data['levelTypes']['field-1']['firstName'],
            $object->getLevelTypes()->get(new TestEnum(TestEnum::FIELD_1))->getFirstName());
        $this->assertEquals($data['levelTypes']['field-1']['lastName'],
            $object->getLevelTypes()->get(new TestEnum(TestEnum::FIELD_1))->getLastName());

        $this->assertEquals($data['levelTypes']['field-3']['firstName'],
            $object->getLevelTypes()->get(new TestEnum(TestEnum::FIELD_3))->getFirstName());
        $this->assertEquals($data['levelTypes']['field-3']['lastName'],
            $object->getLevelTypes()->get(new TestEnum(TestEnum::FIELD_3))->getLastName());

        $this->assertEmpty($object->getLevelTypes()->get(new TestEnum(TestEnum::FIELD_2)));
    }

    public function testDefault()
    {
        $defaults = [
            'name' => $this->faker->company,
            'contact.firstName' => $this->faker->firstName,
            'contact.lastName' => $this->faker->lastName
        ];

        do {
            $lastName = $this->faker->lastName;
        } while ($lastName === $defaults['contact.lastName']);

        /**
         * @var CompanyMock $object
         */
        $object = (new Populator)->defaults($defaults)->populate([
            'contact' => [
                'lastName' => $lastName
            ]
        ], new CompanyMock());

        $this->assertEquals($object->getName(), $defaults['name']);
        $this->assertEquals($object->getContact()->getFirstName(),
            $defaults['contact.firstName']);
        $this->assertEquals($object->getContact()->getLastName(),
            $lastName);
    }

    /**
     * @param ObjectWithObjectWithArrayMock $object
     * @param $array
     */
    protected function assertHints($object, $array)
    {
        $this->assertInstanceOf(ObjectWithObjectWithArrayMock::class, $object);
        $contacts = $object->getObject()->getContacts();
        $this->assertInternalType('array', $contacts);
        $this->assertEquals(count($array['object']['contacts']), count($contacts));

        for ($i = 0; $i < count($contacts); $i++) {
            $contact = $contacts[$i];
            $this->assertInstanceOf(ContactMock::class, $contact);
            $this->assertContact($contact, $array['object']['contacts'][$i]);
        }
    }

    /**
     * @param ContactMock $contact
     * @param $array
     */
    protected function assertContact(ContactMock $contact, $array)
    {
        $this->assertEquals($contact->getFirstName(), $array['firstName']);
        $this->assertEquals($contact->getLastName(), $array['lastName']);
    }

    /**
     * @return array
     */
    protected function getNestedArray()
    {
        return [
            'testField' => $this->faker->word,
            'contact' => [
                'firstName' => $this->faker->firstName,
                'lastName' => $this->faker->lastName
            ],
            'object' => [
                'contacts' => [
                    [
                        'firstName' => $this->faker->firstName,
                        'lastName' => $this->faker->lastName
                    ],
                    [
                        'firstName' => $this->faker->firstName,
                        'lastName' => $this->faker->lastName
                    ]
                ]
            ]
        ];
    }
}