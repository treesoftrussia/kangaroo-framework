<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Populator;

use Mildberry\Kangaroo\Libraries\Cast\Cast;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Populator;
use Mildberry\Kangaroo\Libraries\Modifier\Manager;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\ModifierTestMock;
use PHPUnit_Framework_TestCase;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ModifierTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Manager
     */
    private $modifierManager;
    private $data;

    public function setUp()
    {
        parent::setUp();

        $this->modifierManager = new Manager();
        $this->data = $this->getArray();
    }

    public function testBeforeModifier()
    {
        $this->modifierManager->register('int', function ($value) {
            return Cast::int($value);
        });

        $this->modifierManager->register('trim', function ($value) {
            return trim($value);
        });

        /**
         * @var ModifierTestMock $object
         */
        $object = (new Populator([
            'modify' => [
                'before' => [
                    'toInt' => 'int'
                ]
            ]
        ]))->modify('before', 'trimmedString', 'trim')
            ->setModifierManager($this->modifierManager)
            ->populate($this->data, new ModifierTestMock());

        $this->assertInstanceOf(ModifierTestMock::class, $object);

        $this->assertSame(Cast::int($this->data['toInt']), $object->getToInt());
        $this->assertSame($this->data['toString'], $object->getToString());
        $this->assertSame(trim($this->data['trimmedString']), $object->getTrimmedString());
    }

    public function testAfterModifier()
    {
        $this->modifierManager->register('trim', function ($value) {
            return trim($value);
        });

        /**
         * @var ModifierTestMock $object
         */
        $object = (new Populator())->modify('after', 'trimmedString', 'trim')
            ->setModifierManager($this->modifierManager)
            ->populate($this->data, new ModifierTestMock());

        $this->assertInstanceOf(ModifierTestMock::class, $object);
        $this->assertSame(trim($this->data['trimmedString']), $object->getTrimmedString());
    }

    protected function getArray()
    {
        return [
            'toInt' => '3',
            'toString' => '3',
            'trimmedString' => '   test test   '
        ];
    }
}