<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Skeleton;

use Mildberry\Kangaroo\Libraries\Converter\Skeleton\Skeleton;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\AppraisalMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\CompanyEnumMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\ExtendedContactMock;
use Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks\ObjectWithObjectWithArrayMock;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\CompanyMock;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\ContactMock;
use PHPUnit_Framework_TestCase;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class SkeletonTest extends PHPUnit_Framework_TestCase
{
    public function testSimple()
    {
        $flatArray = (new Skeleton([
            'ignore' => ['pending']
        ]))->flatten(CompanyMock::class);

        $this->assertFlatten($flatArray, ['name', 'isPending', 'hasContact', 'canField',
            'contact.firstName', 'contact.lastName', 'contact.type']);
    }

    public function testEnum()
    {
        $flatArray = (new Skeleton())->flatten(CompanyEnumMock::class);

        $this->assertFlatten($flatArray, ['name', 'pending', 'isPending', 'hasContact', 'canField',
            'contact.firstName', 'contact.lastName', 'contact.type', 'enumerableField']);
    }

    public function testHint()
    {
        $flatArray = (new Skeleton([
            'hint' => ['object.contacts' => ContactMock::class]
        ]))->hint('contact', ExtendedContactMock::class)
            ->flatten(ObjectWithObjectWithArrayMock::class);

        $this->assertFlatten($flatArray, ['testField',
            'object.contacts.type', 'object.contacts.firstName', 'object.contacts.lastName',
            'contact.firstName', 'contact.lastName', 'contact.type', 'contact.extendField']);
    }

    public function testMap()
    {
        $flatArray = (new Skeleton([
            'map' => [
                'client.fullName' => 'name'
            ]
        ]))->flatten(AppraisalMock::class);

        $this->assertFlatten($flatArray, ['client.name']);
    }

    /**
     * @param $actual
     * @param $expected
     */
    private function assertFlatten($actual, $expected)
    {
        $this->assertInternalType('array', $actual);

        sort($actual);
        sort($expected);

        $this->assertSameArray($expected, $actual, true);
    }

    /**
     * @param $expected
     * @param $actual
     * @param bool $strict
     * @param string $message
     */
    private function assertSameArray($expected, $actual, $strict = false, $message = '')
    {
        // I didn't find method nested compare two arrays so
        // I check if both arrays are subset each other
        $this->assertArraySubset($expected, $actual, $strict, $message);
        $this->assertArraySubset($actual, $expected, $strict, $message);
    }
}
