<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Support;

use Mildberry\Kangaroo\Libraries\Converter\Support\Scope;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ScopeTest extends TestCase
{
    public function testSuccess()
    {
        $scope = new Scope();

        $scope->create('field1');
        $this->assertEquals('field1', $scope->get());
        $this->assertEquals('field1.field1', $scope->get('field1'));

        $scope->create('field1');

        $this->assertEquals('field1.field1', $scope->get());

        $scope->drop();
        $this->assertEquals('field1', $scope->get());
        $this->assertEquals('field1.field1', $scope->get('Field1'));
        $this->assertEquals('field1.Field1', $scope->get('Field1', true));
    }
}