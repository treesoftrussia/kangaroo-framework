<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Transferer;

use Mildberry\Kangaroo\Libraries\Converter\Transferer\Transferer;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\CompanyMock;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\ContactMock;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\Converter\CompanyMockPersistable;
use PHPUnit_Framework_TestCase;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class TransfererTest extends PHPUnit_Framework_TestCase
{
    public function testSimpler()
    {
        $object = new CompanyMock();

        $object->setName('testName')
            ->setPending(true)
            ->setContact((new ContactMock())->setFirstName('contactName'));

        /**
         * @var CompanyMockPersistable $persistable
         */
        $persistable = (new Transferer([
            'map' => ['contact.firstName' => 'contactName'],
            'defaults' => [
                'field' => 15,
                'name' => 'defaultName']
        ]))->transfer($object, new CompanyMockPersistable());

        $this->assertSame($object->getName(), $persistable->getName());
        $this->assertSame($object->getPending(), $persistable->getPending());
        $this->assertSame($object->getContact()->getFirstName(), $persistable->getContactName());
        $this->assertSame(15, $persistable->getField());
    }
}