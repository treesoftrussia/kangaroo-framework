<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks;

use Mildberry\Kangaroo\Libraries\Enum\Enum;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class EnumMock extends Enum
{
    const TEST_VALUE_1 = 1;
    const TEST_VALUE_2 = 2;
}