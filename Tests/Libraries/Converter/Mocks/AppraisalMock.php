<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class AppraisalMock
{
    private $client;

    public function setClient(ClientMock $client)
    {
        $this->client = $client;
    }

    /**
     * @return ClientMock
     */
    public function getClient()
    {
        return $this->client;
    }
} 