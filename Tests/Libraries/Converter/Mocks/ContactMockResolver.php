<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks;

use Mildberry\Kangaroo\Libraries\Converter\Populator\Populator;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\ContactMock;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ContactMockResolver
{
    private $type;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function __invoke(Populator $populator, array $array)
    {
        $contact = (new ContactMock())->setType($this->type);

        return $populator->populate($array, $contact);
    }
}