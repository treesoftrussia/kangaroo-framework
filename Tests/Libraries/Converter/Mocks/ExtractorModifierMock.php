<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks;

use DateTime;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ExtractorModifierMock
{
    private $created;
    private $enum;

    /**
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     * @return ExtractorModifierMock
     */
    public function setCreated(DateTime $created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return EnumMock
     */
    public function getEnum()
    {
        return $this->enum;
    }

    /**
     * @param EnumMock $enum
     * @return ExtractorModifierMock
     */
    public function setEnum(EnumMock $enum)
    {
        $this->enum = $enum;

        return $this;
    }
}