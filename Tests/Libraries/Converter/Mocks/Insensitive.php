<?php namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks;


/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class Insensitive
{
    private $insensitivefield;

    /**
     * @return mixed
     */
    public function getInsensitivefield()
    {
        return $this->insensitivefield;
    }

    /**
     * @param mixed $insensitivefield
     * @return $this
     */
    public function setInsensitivefield($insensitivefield)
    {
        $this->insensitivefield = $insensitivefield;

        return $this;
    }
}
