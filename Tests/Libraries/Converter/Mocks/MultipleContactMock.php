<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks;

use Mildberry\Kangaroo\Tests\Libraries\Mocks\ContactMock;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class MultipleContactMock
{
    private $borrower;
    private $owner;

    /** @return ContactMock */
    public function getBorrower()
    {
        return $this->borrower;
    }

    public function setBorrower(ContactMock $borrower)
    {
        $this->borrower = $borrower;
        return $this;
    }

    /** @return ContactMock */
    public function getOwner()
    {
        return $this->owner;
    }

    public function setOwner(ContactMock $owner)
    {
        $this->owner = $owner;
        return $this;
    }
}