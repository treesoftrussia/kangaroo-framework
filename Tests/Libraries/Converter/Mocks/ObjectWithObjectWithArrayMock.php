<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks;

use Mildberry\Kangaroo\Tests\Libraries\Mocks\ContactMock;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ObjectWithObjectWithArrayMock
{
    private $testField;
    private $contact;
    private $object;

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    public function setContact(ContactMock $contact)
    {
        $this->contact = $contact;

        return $this;
    }

    public function getTestField()
    {
        return $this->testField;
    }

    public function setTestField($testField)
    {
        $this->testField = $testField;
        return $this;
    }

    /**
     * @return ObjectWithArrayMock
     */
    public function getObject()
    {
        return $this->object;
    }

    public function setObject(ObjectWithArrayMock $object)
    {
        $this->object = $object;

        return $this;
    }
}