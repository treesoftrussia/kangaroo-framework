<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ClientMock
{
    private $fullName;

    public function setFullName($name)
    {
        $this->fullName = $name;
    }

    public function getFullName()
    {
        return $this->fullName;
    }
} 