<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class VectorTestMock
{
    private $values;

    public function setValues(array $values)
    {
        $this->values = $values;
    }

    public function getValues()
    {
        return $this->values;
    }
} 