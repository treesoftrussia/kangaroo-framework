<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks;

use Mildberry\Kangaroo\Tests\Libraries\Mocks\ContactMock;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ExtendedContactMock extends ContactMock
{
    private $extendField;

    public function getExtendField()
    {
        return $this->extendField;
    }

    public function setExtendField($extendField)
    {
        $this->extendField = $extendField;

        return $this;
    }
}