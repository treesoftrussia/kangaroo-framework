<?php
namespace Mildberry\Kangaroo\Tests\Libraries\Converter\Mocks;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ModifierTestMock
{
    private $toInt;
    private $toString;
    private $trimmedString;

    /**
     * @return mixed
     */
    public function getToInt()
    {
        return $this->toInt;
    }

    /**
     * @param mixed $toInt
     * @return ModifierTestMock
     */
    public function setToInt($toInt)
    {
        $this->toInt = $toInt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getToString()
    {
        return $this->toString;
    }

    /**
     * @param mixed $toString
     * @return ModifierTestMock
     */
    public function setToString($toString)
    {
        $this->toString = $toString;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTrimmedString()
    {
        return $this->trimmedString;
    }

    /**
     * @param mixed $trimmedString
     * @return ModifierTestMock
     */
    public function setTrimmedString($trimmedString)
    {
        $this->trimmedString = $trimmedString;
        return $this;
    }
}