<?php
namespace Mildberry\Kangaroo\Tests\Libraries;

use Mildberry\Kangaroo\Tests\Libraries\Mocks\CompanyMock;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\ContactMock;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\ObjectTakeParent;
use Mildberry\Kangaroo\Tests\Libraries\Mocks\ObjectTakeChild;
use Faker\Factory;
use Faker\Generator;
use PHPUnit_Framework_TestCase;
use RuntimeException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class FunctionsTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    public function testIsSetterAndGetter()
    {
        $this->assertTrue(is_getter('isActive'));
        $this->assertTrue(is_getter('hasObject'));
        $this->assertTrue(is_getter('canObject'));
        $this->assertTrue(is_getter('getObject'));
        $this->assertTrue(is_setter('setObject'));

        $this->assertTrue(is_getter('isA'));
        $this->assertTrue(is_getter('hasO'));
        $this->assertTrue(is_getter('getO'));
        $this->assertTrue(is_setter('setO'));
        $this->assertTrue(is_getter('hasO', ['has']));

        $this->assertFalse(is_setter('method'));
        $this->assertFalse(is_getter('isactive'));
        $this->assertFalse(is_getter('hasobject'));
        $this->assertFalse(is_getter('getobject'));
        $this->assertFalse(is_setter('setobject'));
        $this->assertFalse(is_getter('hasO', ['is', 'get']));
    }

    public function testMakeProperty()
    {
        $this->assertEquals('isActive', make_property('isActive'));
        $this->assertEquals('canObject', make_property('canObject'));
        $this->assertEquals('hasObject', make_property('hasObject'));
        $this->assertEquals('object', make_property('hasObject', true));
        $this->assertEquals('object', make_property('getObject'));


        $this->assertEquals('isA', make_property('isA'));
        $this->assertEquals('hasO', make_property('hasO'));
        $this->assertEquals('o', make_property('hasO', true));
        $this->assertEquals('o', make_property('getO'));
    }

    public function testMakeGetterAndSetter()
    {
        $this->assertEquals('getObject', make_getter('object'));
        $this->assertEquals('hasObject', make_getter('hasObject'));
        $this->assertEquals('canObject', make_getter('canObject'));
        $this->assertEquals('isActive', make_getter('isActive'));

        $this->assertEquals('getO', make_getter('o'));
        $this->assertEquals('hasO', make_getter('hasO'));
        $this->assertEquals('isA', make_getter('isA'));


        $this->assertEquals('setActive', make_setter('active'));
        $this->assertEquals('setA', make_setter('a'));
        $this->assertEquals('setPending', make_setter('isPending'));
        $this->assertEquals('setContact', make_setter('hasContact'));
        $this->assertEquals('setContact', make_setter('canContact'));
    }

    public function testShortName()
    {
        $this->assertEquals('class', short_name('class'));
        $this->assertEquals('class', short_name('name\space\long\class'));
    }

    public function testObjectTake()
    {
        $object = new ObjectTakeParent();
        $object->setSubObject((new ObjectTakeChild())->setName('test'));

        $this->assertEquals('test', object_take($object, 'subObject.name', 'null value'));
        $this->assertNull(object_take($object, 'subObject.emptyField', 'null value'));
        $this->assertEquals('null value', object_take($object, 'emptySubObject.empty', 'null value'));
    }

    public function testObjectTakeNotObject()
    {
        $this->setExpectedException(RuntimeException::class, '$object must be object');

        object_take([], 'test');
    }

    public function testObjectDot()
    {
        $company = new CompanyMock();
        $contact = new ContactMock();

        $contact->setFirstName($this->faker->firstName)
            ->setLastName($this->faker->lastName);

        $company->setName($this->faker->company)
            ->setPending(true)
            ->setContact($contact);

        $fields = object_dot($company);

        $this->assertInternalType('array', $fields);
        $arrayForTest = [
            'name' => $company->getName(),
            'pending' => $company->getPending(),
            'isPending' => $company->isPending(),
            'hasContact' => $company->hasContact(),
            'contact.firstName' => $contact->getFirstName(),
            'contact.lastName' => $contact->getLastName(),
            'contact.type' => $contact->getType(),
            'canField' => $company->canField()];

        // I didn't find method nested compare two arrays so
        // I check if both arrays are subset each other
        $this->assertArraySubset($arrayForTest, $fields, true);
        $this->assertArraySubset($fields, $arrayForTest, true);
    }

    public function testArrayEqual()
    {
        $a1 = [10, 402, 12, 10];
        $a2 = [12, 10, 402, 10];

        $this->assertTrue(array_equal($a1, $a2));
        $this->assertTrue(array_equal($a2, $a1));

        $a1 = [10, 402, 12, 10, 12];
        $a2 = [12, 10, 402, 10];

        $this->assertFalse(array_equal($a1, $a2));
        $this->assertFalse(array_equal($a2, $a1));

        $a1 = [
            'rwrgssdsdgsADAFSFD',
            0,
            '22423!@$@%@$!)(*&^%$#!',
            20,
            0,
            '22423!@$@%@$!)(*&^%$#!',
            '%@^#%$^*%^%$#@!~@!#$^fgxcvbfjrfdhgf^$^%dfdfgdhaSFadasdas@#*sdf',
            'sdfjsldfsldkfjskldfjsdklf',
            20
        ];

        $a2 = [
            0,
            'rwrgssdsdgsADAFSFD',
            0,
            '22423!@$@%@$!)(*&^%$#!',
            '22423!@$@%@$!)(*&^%$#!',
            20,
            'sdfjsldfsldkfjskldfjsdklf',
            20,
            '%@^#%$^*%^%$#@!~@!#$^fgxcvbfjrfdhgf^$^%dfdfgdhaSFadasdas@#*sdf'
        ];

        $this->assertTrue(array_equal($a1, $a2));
        $this->assertTrue(array_equal($a2, $a1));
    }

    public function testStringEqual()
    {
        $this->assertTrue(string_equal('test', 'test'));
        $this->assertTrue(string_equal('TEst', 'test'));
        $this->assertTrue(string_equal('test', 'TeST'));
        $this->assertFalse(string_equal('test', 'test1'));

        $this->assertTrue(string_equal('TEST', ['test', 'test1', 'testy']));
        $this->assertTrue(string_equal('test', ['blah', 'TEsT', 'testy']));
        $this->assertFalse(string_equal('test', ['blah', 'wow', 'testy']));
    }

    /**
     * @param array $keys
     * @param array $array
     */
    protected function assertArrayHasKeys(array $keys, array $array)
    {
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $array);
        }
    }

    public function testEmptyDate()
    {
        $this->assertTrue(empty_date(null));
        $this->assertTrue(empty_date('0000-00-00 00:00:00'));
        $this->assertTrue(empty_date('0000-00-00'));
        $this->assertTrue(empty_date(''));
        $this->assertFalse(empty_date('2015-06-01 12:10:10'));
        $this->assertFalse(empty_date('2015-06-01'));
    }

    public function testArraySmash()
    {
        $array = [
            'levels' => [
                'level1' => [
                    'level2' => [
                        'data' => [103, 52, 193]
                    ]
                ]
            ],
            'some' => [
                'other' => [
                    'data' => [
                        'here' => 'hello'
                    ]
                ]
            ]
        ];

        $smashed = array_smash($array);

        $this->assertEquals('hello', $smashed['some.other.data.here']);

        $this->assertTrue(array_equal($smashed['levels.level1.level2.data'], [103, 52, 193]));
    }

    public function testArrayFromDot()
    {
        $dottedArray = [
            'test' => $this->faker->text,
            'test2.nested' => $this->faker->text,
            'test2.double.nested' => $this->faker->text
        ];

        $array = array_unsmash($dottedArray);
        $expectedArray = [
            'test' => $dottedArray['test'],
            'test2' => [
                'nested' => $dottedArray['test2.nested'],
                'double' => [
                    'nested' => $dottedArray['test2.double.nested']
                ]
            ]
        ];

        $this->assertArraySubset($array, $expectedArray, true);
        $this->assertArraySubset($expectedArray, $array, true);
    }

    public function testIsVector()
    {
        $this->assertTrue(is_vector([1, 3, 'asdsad', null, true, false, 12.2]));
        $this->assertTrue(is_vector([291, new \stdClass(), [23432], 12312, 'qwqwe'], false));

        $this->assertFalse(is_vector([
            [
                'field1' => '12312',
                'field2' => '21312'
            ],
            [
                'field1' => '12312',
                'field2' => '21312'
            ]
        ]));

        $this->assertFalse(is_vector([1 => 12, 2 => 123]));
        $this->assertFalse(is_vector([291, new \stdClass(), [23432, 131123], 12312, 'qwqwe']));
        $this->assertFalse(is_vector('wrong value'));
    }

    public function testObjectsVector()
    {
        $companies = [];

        for ($i = 0; $i < 5; $i++) {
            $companies[] = (new CompanyMock())->setName($this->faker->company);
        }

        $values = objects_vector($companies, 'name');

        $countCompanies = count($companies);

        $this->assertEquals($countCompanies, count($values));

        foreach ($companies as $i => $company) {
            /**
             * @var CompanyMock $company
             */
            $this->assertSame($company->getName(), $values[$i]);
        }
    }

    public function testCutString()
    {
        $this->assertEquals('String', cut_string_left('UnwantedString', 'Unwanted'));
        $this->assertEquals('Appraiser', cut_string_right('AppraiserController', 'Controller'));
    }

    public function testArrayDotMerge()
    {
        $array1 = [
            'key' => $this->faker->text,
            'level1.level2.level3' => $this->faker->text,
            'level1.level2.level3_1' => $this->faker->text,
            'level1.level2.level3_2' => $this->faker->text
        ];

        $array2 = [
            'level1.level2' => $this->faker->text,
            'key2' => $this->faker->text
        ];

        $merged = dotted_array_merge($array1, $array2);

        $expected = [
            'key' => $array1['key'],
            'key2' => $array2['key2'],
            'level1.level2' => $array2['level1.level2']
        ];

        $this->assertTrue(array_equal($merged, $expected));
    }
}
