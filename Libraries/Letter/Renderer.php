<?php
namespace Mildberry\Kangaroo\Libraries\Letter;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class Renderer
{
    /**
     * @var string|array
     */
    private $template;

    /**
     * @var array
     */
    private $data;

    /**
     * @return array|string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param array|string $template
     * @return Renderer
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return Renderer
     */
    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }
}