<?php
namespace Mildberry\Kangaroo\Libraries\Letter;

use Illuminate\Mail\Message;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
interface LetterInterface
{
    /**
     * @param Renderer $renderer
     */
    public function render(Renderer $renderer);

    /**
     * Prepare letter before it's sent
     * @param Message $message
     */
    public function message(Message $message);
}