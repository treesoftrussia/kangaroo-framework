<?php
namespace Mildberry\Kangaroo\Libraries\Letter;

use Illuminate\Mail\Mailer as IlluminateMailer;
use Illuminate\Mail\Message;
use RuntimeException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Mailer
{
    /**
     * @var IlluminateMailer
     */
    private $mailer;

    /**
     * @param IlluminateMailer $mailer
     */
    public function __construct(IlluminateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Prepares and sends the letter
     * @param LetterInterface $letter
     * @throws RuntimeException
     */
    public function send(LetterInterface $letter)
    {
        $callback = function (Message $message) use ($letter) {
            $letter->message($message);
        };

        $renderer = new Renderer();

        $letter->render($renderer);
        // TODO: Change to add to queue (send is used only for tests)
        $this->mailer->send($renderer->getTemplate(), $renderer->getData(), $callback);
    }
} 