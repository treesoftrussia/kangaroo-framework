<?php
namespace Mildberry\Kangaroo\Libraries\Resource;

use Illuminate\Support\ServiceProvider as Provider;
use Illuminate\Contracts\Debug\ExceptionHandler as ExceptionHandlerInterface;
use Mildberry\Kangaroo\Libraries\Resource\Integrations\Transformer;
use Mildberry\Kangaroo\Libraries\Resource\Response\JsonResponseFactory;
use Mildberry\Kangaroo\Libraries\Resource\Response\ResponseFactoryInterface;

/**
 * The service provider do the following:
 *  - binds implementations to the interfaces
 *  - registers ExceptionHandler
 *  - registers config provider
 *  - setups Paginator
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ServiceProvider extends Provider
{
    public function register()
    {
        $this->app->singleton(TransformableInterface::class, Transformer::class);
        $this->app->singleton(ResponseFactoryInterface::class, JsonResponseFactory::class);
        $this->app->singleton(ExceptionHandlerInterface::class, ExceptionHandler::class);
        $this->app->singleton(ErrorInterface::class, Error::class);
    }

    public function boot()
    {
    }
}