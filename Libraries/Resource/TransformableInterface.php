<?php
namespace Mildberry\Kangaroo\Libraries\Resource;

/**
 * The interface required to be implemented in order to integrate a third party library to handle data transformation process whenever required by Kangaroo.
 *
 * See Mildberry\Kangaroo\Libraries\Kangaroo\Integrations\Transformer to learn more.
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
interface TransformableInterface
{
    /**
     * Transforms a single item
     *
     * @param object $item
     * @param object $handler
     * @return array
     */
    public function transform($item, $handler);

    /**
     * Transforms a collection of items
     *
     * @param array|\Traversable $collection
     * @param object $handler
     * @return array|\Traversable
     */
    public function transformCollection($collection, $handler);
} 