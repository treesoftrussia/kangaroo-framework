<?php

namespace Mildberry\Kangaroo\Libraries\Resource;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class RequestExceptionData
{
    private $exception;

    private $request;

    /**
     * @return mixed
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * @param mixed $exception
     * @return RequestExceptionData
     */
    public function setException($exception)
    {
        $this->exception = $exception;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     * @return RequestExceptionData
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }
}