<?php
namespace Mildberry\Kangaroo\Libraries\Resource;

use Illuminate\Http\Response;
use Illuminate\View\View;
use Mildberry\Kangaroo\Libraries\Resource\Response\ResponseFactoryInterface;
use ReflectionClass;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class Error implements ErrorInterface
{
    private static $constants;

    const INVALID_SCOPES = 1;
    const INVALID_CREDENTIALS = 2;
    const PERMISSION_DENIED = 4;
    const ROUTE_NOT_FOUND = 5;
    const INVALID_CLIENT = 6;
    const UNKNOWN_ERROR = 7;
    const INTERNAL_ERROR = 8;
    const INVALID_GRANT_TYPE = 9;
    const METHOD_NOT_ALLOWED = 10;
    const TOO_MANY_REQUESTS = 11;

    const SMS_PUSHER_MANY_REQUESTS = 20;
    const INVALID_ACTIVATION_CODE = 21;

    const VALIDATION_BODY_ERROR = 22;
    const VALIDATION_OPTIONS_ERROR = 23;
    const VALIDATION_ROUTE_ERROR = 24;
    
    /**
     * @var ResponseFactoryInterface
     */
    private $responseFactory;
    
    /**
     * @param ResponseFactoryInterface $responseFactory
     */
    public function __construct(ResponseFactoryInterface $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param $data
     * @param $status
     * @param $httpCode
     * @return Response
     */
    public function write($data, $status, $httpCode)
    {
        if($data instanceof View){
            try{
                $data->render();
            } catch(\Exception $e){
                dd($e);
            }

            return new Response($data->render(), $httpCode, ['Content-Type' => 'text/html']);
        }

        $content = [
            'errorData' => $data,
            'errorCode' => $status
        ];

        return $this->responseFactory->create($content, $httpCode);
    }

    /**
     * @return string
     */
    public static function toJSONObject() {
        return json_encode(self::getConstants());
    }

    /**
     * @return array
     */
    public static function getConstants() {
        if(!self::$constants){
            $class = new ReflectionClass(self::class);
            self::$constants = $class->getConstants();

            return self::$constants;
        }

        return self::$constants;
    }
}