<?php
namespace Mildberry\Kangaroo\Libraries\Resource\Integrations;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Pagination\PaginationEntityInterface;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Mildberry\Kangaroo\Libraries\Resource\TransformableInterface;
use Mildberry\Kangaroo\Libraries\Specification\Support\PaginationResponseSpecification;
use Mildberry\Kangaroo\Libraries\Specification\Support\PaginationSpecificationFactory;
use Mildberry\Kangaroo\Libraries\Specification\Support\SpecificationFactory;
use Mildberry\Kangaroo\Libraries\Specification\Types\Collection\CollectionType;
use Mildberry\Kangaroo\Libraries\Transformer\TransformRulesInterface;
use Traversable;

/**
 * Implements the TransformableInterface in order to integrate "Fractal" transformers to Kangaroo
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Transformer implements TransformableInterface
{
    /**
     * @param TransformRulesInterface|string $handler
     * @return array
     */
    private function extractSpecificationClassData($handler)
    {
        if ($handler instanceof TransformRulesInterface) {

            return [$handler->getSpecificationClassName(), [
                'resolvers' => $handler->getResolvers()
            ]];
        } else {
            if (!class_exists($handler)) {
                throw new RuntimeException('Class of handler must be a string or instance of TransformRulesInterface.');
            }

            return [$handler, null];
        }
    }

    /**
     * @param object $item
     * @param TransformRulesInterface|string $handler
     * @return mixed
     */
    public function transform($item, $handler)
    {
        $handlerData = $this->extractSpecificationClassData($handler);

        $specification = SpecificationFactory::make($handlerData[0]);
        return $specification->extract($item, $handlerData[1]);
    }

    /**
     * @param $options
     * @param bool $withPagination
     * @return array
     */
    private function restructureCollectionOptions($options, $withPagination = false)
    {
        if(empty($options['resolvers'])){
            return null;
        }

        if ($withPagination) {
            return [
                'resolvers' => [
                    'items.*' => $options['resolvers']
                ]
            ];
        } else {
            return [
                'resolvers' => [
                    '*' => $options['resolvers']
                ]
            ];
        }
    }

    /**
     * @param $collection
     * @param TransformRulesInterface|string $handler
     * @return array|Traversable
     */
    public function transformCollection($collection, $handler)
    {
        $handlerData = $this->extractSpecificationClassData($handler);

        if ($collection instanceof PaginationEntityInterface) {
            $options = $this->restructureCollectionOptions($handlerData[1], true);
            $specification = SpecificationFactory::make(PaginationResponseSpecification::class, [$handlerData[0]]);
        } else if ($collection instanceof Collection) {
            $options = $this->restructureCollectionOptions($handlerData[1]);
            $className = $handlerData[0];
            $specification = SpecificationFactory::make(CollectionType::class, [function () use ($className) {
                return (new $className)->getBody();
            }]);
        } else {
            throw new RuntimeException('Class of handler is not supported.');
        }

        return $specification->extract($collection, $options);
    }
}