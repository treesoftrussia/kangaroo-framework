<?php
namespace Mildberry\Kangaroo\Libraries\Resource\Resource;

use Illuminate\Support\Facades\Facade as AbstractFacade;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Resource extends AbstractFacade
{
    protected static function getFacadeAccessor()
    {
        return Manager::class;
    }
} 