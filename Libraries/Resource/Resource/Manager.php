<?php
namespace Mildberry\Kangaroo\Libraries\Resource\Resource;

use Mildberry\Kangaroo\Libraries\Resource\Error;
use Mildberry\Kangaroo\Libraries\Resource\Response\ResponseFactoryInterface;
use Mildberry\Kangaroo\Libraries\Resource\TransformableInterface;
use Illuminate\Http\Response;
use Mildberry\Kangaroo\Pagination\PaginationEntityInterface;
use Traversable;
use RuntimeException;

/**
 * Provides functionality/methods to the resource facade.
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 * @author Andrey Vorobiov <andrew.sprw@gmail.com>
 *
 */
class Manager
{
    /**
     * @var TransformableInterface
     */
    private $transformer;

    /**
     * @var ResponseFactoryInterface
     */
    private $responseFactory;

    /**
     * @var Error
     */
    private $error;

    public function __construct(
        TransformableInterface $transformer,
        ResponseFactoryInterface $responseFactory,
        Error $error
    )
    {
        $this->transformer = $transformer;
        $this->responseFactory = $responseFactory;
        $this->error = $error;
    }

    /**
     * Creates a resource based on the provided item and the transformer handler
     *
     * @param object | array $item
     * @param object $handler
     * @param $HTTP_OK
     * @return Response
     */
    public function make($item, $handler, $HTTP_OK = true)
    {
        $content = $this->transformer->transform($item, $handler);
        return $this->responseFactory->create($content, $HTTP_OK ? Response::HTTP_OK : Response::HTTP_CREATED);
    }

    /**
     * Creates a collection of resources based on the provided items and the transformer handler
     *
     * @param PaginationEntityInterface $collection | Traversable $collection
     * @param $handler
     * @return Response
     * @throws RuntimeException
     */
    public function makeAll($collection, $handler)
    {
        $content = $this->transformer->transformCollection($collection, $handler);
        return $this->responseFactory->create($content, Response::HTTP_OK);
    }

    /**
     * Creates an empty resource
     *
     * @return Response
     */
    public function blank()
    {
        return $this->responseFactory->create(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Provides access to the error object to output http errors.
     *
     * @return Error
     */
    public function error()
    {
        return $this->error;
    }
} 