<?php

namespace Mildberry\Kangaroo\Libraries\Resource\ExceptionMiddleware;

use Closure;
use Mildberry\Kangaroo\Libraries\Resource\Error;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\AccessDeniedException;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\InvalidInputException;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\ExceptionInterface;
use Mildberry\Kangaroo\Libraries\Resource\RequestExceptionData;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class DefaultExceptionMiddleware
{

    private $error;

    public function __construct(Error $error)
    {
        $this->error = $error;
    }

    /**
     * @param RequestExceptionData $data
     * @param Closure $next
     * @return \Illuminate\Http\Response
     */
    public function handle($data, Closure $next)
    {
        if ($data->getException() instanceof InvalidInputException) {
            return $this->error->write($data->getException()->getErrors(), $data->getException()->getInternalCode(), $data->getException()->getHTTPCode());
        } else if ($data->getException() instanceof ExceptionInterface) {
            if($data->getException() instanceof AccessDeniedException){
                if (!$data->getRequest()->isJson()) {
                    return redirect('/');
                }
            }

            return $this->error->write($data->getException()->getData(), $data->getException()->getInternalCode(), $data->getException()->getHTTPCode());
        }
        
        return $next($data);
    }
}
