<?php
namespace Mildberry\Kangaroo\Libraries\Resource\Exceptions;

use \RuntimeException as DefaultRuntimeException;
use Mildberry\Kangaroo\Libraries\Resource\Error;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class RuntimeException extends DefaultRuntimeException implements ExceptionInterface
{

    public function getHTTPCode(){
        return Response::HTTP_INTERNAL_SERVER_ERROR;
    }

    public function getInternalCode(){
        return Error::INTERNAL_ERROR;
    }

    public function __construct($message = 'Internal server error.')
    {
        parent::__construct($message);
    }

    public function getData()
    {
        return $this->getMessage();
    }
}