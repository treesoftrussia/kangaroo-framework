<?php
namespace Mildberry\Kangaroo\Libraries\Resource\Exceptions;

use \RuntimeException as DefaultRuntimeException;
use Mildberry\Kangaroo\Libraries\Resource\Error;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class BadRequestException extends DefaultRuntimeException implements ExceptionInterface
{

    public function getHTTPCode(){
        return Response::HTTP_BAD_REQUEST;
    }

    public function getInternalCode(){
        return Error::INTERNAL_ERROR;
    }

    public function __construct($message = 'Bad request.')
    {
        parent::__construct($message);
    }

    public function getData()
    {
        return $this->getMessage();
    }
}