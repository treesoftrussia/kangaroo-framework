<?php
namespace Mildberry\Kangaroo\Libraries\Resource\Exceptions;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
interface ExceptionInterface
{
    public function getHTTPCode();
    public function getInternalCode();
    public function getData();
}