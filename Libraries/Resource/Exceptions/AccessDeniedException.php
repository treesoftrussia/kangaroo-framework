<?php
namespace Mildberry\Kangaroo\Libraries\Resource\Exceptions;

use Mildberry\Kangaroo\Libraries\Resource\Error;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class AccessDeniedException extends AccessDeniedHttpException implements ExceptionInterface
{

    public function getHTTPCode(){
        return Response::HTTP_UNAUTHORIZED;
    }

    public function getInternalCode(){
        return Error::PERMISSION_DENIED;
    }

    public function __construct($message = 'Access denied for requested resource.')
    {
        parent::__construct($message);
    }

    public function getData()
    {
        return $this->getMessage();
    }
}