<?php
namespace Mildberry\Kangaroo\Libraries\Resource\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Mildberry\Kangaroo\Libraries\Resource\Error;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class ResourceNotFoundException extends NotFoundHttpException implements ExceptionInterface
{
    public function getHTTPCode(){
        return Response::HTTP_NOT_FOUND;
    }

    public function getInternalCode(){
        return Error::ROUTE_NOT_FOUND;
    }

    public function __construct($message = 'Resource not found error.')
    {
        parent::__construct($message);
    }

    public function getData()
    {
        return $this->getMessage();
    }
}