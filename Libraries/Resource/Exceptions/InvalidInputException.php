<?php

namespace Mildberry\Kangaroo\Libraries\Resource\Exceptions;

use Mildberry\Kangaroo\Libraries\Resource\Error;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class InvalidInputException extends RuntimeException implements ExceptionInterface
{
    /**
     * @var array
     */
    private $errors;

    /**
     * @return int
     */
    public function getHTTPCode()
    {
        return Response::HTTP_BAD_REQUEST;
    }

    /**
     * @return mixed
     */
    public function getInternalCode()
    {
        return Error::VALIDATION_BODY_ERROR;
    }

    /**
     * InvalidInputException constructor.
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        parent::__construct('Invalid input error.');

        $this->setErrors($errors);
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->getMessage();
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     *
     * @return $this
     */
    public function setErrors(array $errors)
    {
        if (empty($errors)) {
            return $this;
        }

        foreach ($errors as $key => $error) {
            $this->errors[$key] = (is_array($error)) ? $error : [$error];
        }

        return $this;
    }
}
