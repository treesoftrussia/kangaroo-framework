<?php
namespace Mildberry\Kangaroo\Libraries\Resource\Response;

use Illuminate\Http\Response;

/**
 * A common interface for response objects factories
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
interface ResponseFactoryInterface
{
    /**
     * Creates a response object
     *
     * @param $content
     * @param $status
     * @return Response
     */
    public function create($content, $status);
} 