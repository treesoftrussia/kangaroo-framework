<?php
namespace Mildberry\Kangaroo\Libraries\Resource\Response;

use Illuminate\Http\Response;

/**
 * Factory to create json response objects
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class JsonResponseFactory implements ResponseFactoryInterface
{
    /**
     * Creates a new json response object
     *
     * @param mixed $content
     * @param int $status
     * @return Response
     */
    public function create($content, $status)
    {
        return new Response(json_encode($content, JSON_UNESCAPED_SLASHES), $status, ['Content-Type' => 'application/json']);
    }
}