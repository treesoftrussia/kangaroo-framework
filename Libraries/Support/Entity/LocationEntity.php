<?php

namespace Mildberry\Kangaroo\Libraries\Support\Entity;

use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */
class LocationEntity
{
    /**
     * @var CoordinatesEntity
     */
    private $coordinates;
    /**
     * @var string
     */
    private $address;

    /**
     * @return CoordinatesEntity
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param CoordinatesEntity $coordinates
     *
     * @return LocationEntity
     */
    public function setCoordinates(CoordinatesEntity $coordinates)
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return LocationEntity
     */
    public function setAddress($address)
    {
        $this->address = Cast::string($address);

        return $this;
    }
}
