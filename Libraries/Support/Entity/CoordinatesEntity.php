<?php

namespace Mildberry\Kangaroo\Libraries\Support\Entity;

/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */
class CoordinatesEntity
{
    /**
     * @var float
     */
    private $lng;

    /**
     * @var float
     */
    private $ltd;

    /**
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param float $lng
     *
     * @return CoordinatesEntity
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * @return float
     */
    public function getLtd()
    {
        return $this->ltd;
    }

    /**
     * @param float $ltd
     *
     * @return CoordinatesEntity
     */
    public function setLtd($ltd)
    {
        $this->ltd = $ltd;

        return $this;
    }
}
