<?php namespace Mildberry\Kangaroo\Libraries\Support\Entity;

class ImageEntity
{
    /**
     * @var string
     */
    private $originalURL;

    /**
     * @var string
     */
    private $thumbnailURL;

    /**
     * @return mixed
     */
    public function getOriginalURL()
    {
        return $this->originalURL;
    }

    /**
     * @param mixed $originalURL
     *
     * @return ImageEntity
     */
    public function setOriginalURL($originalURL)
    {
        $this->originalURL = $originalURL;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getThumbnailURL()
    {
        return $this->thumbnailURL;
    }

    /**
     * @param mixed $thumbnailURL
     *
     * @return ImageEntity
     */
    public function setThumbnailURL($thumbnailURL)
    {
        $this->thumbnailURL = $thumbnailURL;

        return $this;
    }
}
