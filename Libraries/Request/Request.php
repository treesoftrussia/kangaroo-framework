<?php

namespace Mildberry\Kangaroo\Libraries\Request;

use Illuminate\Foundation\Http\FormRequest;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Populator;
use Mildberry\Kangaroo\Libraries\Specification\Support\IlluminateRequestDataAdapter;
use Mildberry\Kangaroo\Libraries\Specification\Support\RequestDataChecker;
use Mildberry\Kangaroo\Libraries\Specification\Support\SpecificationFactory;

class Request extends FormRequest
{
    /**
     * @var string
     */
    protected $specificationClassName = null;

    /**
     * Extract input data to the entity object.
     *
     * @param $data
     * @param $entityToExtract
     * @param array $options
     *
     * @return object
     */
    protected function extract($data, $entityToExtract, $options = [])
    {
        $populator = new Populator($options);

        return $populator->populate($data, $entityToExtract);
    }

    /**
     * Validate the class instance.
     */
    public function validate()
    {
        if ($this->specificationClassName) {
            return (new RequestDataChecker())->checkRequest($this->getRequestDataAdapterInstance(), SpecificationFactory::make($this->specificationClassName));
        }

        return true;
    }

    protected function getRequestDataAdapterInstance(){
        return new IlluminateRequestDataAdapter($this);
    }
}
