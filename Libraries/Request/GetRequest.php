<?php

namespace Mildberry\Kangaroo\Libraries\Request;
use Mildberry\Kangaroo\Libraries\Specification\Request\GetRequestSpecification;

/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

class GetRequest extends APIRequest
{
    /**
     * @var string
     */
    protected $specificationClassName = GetRequestSpecification::class;

    public function getEntityId()
    {
        return $this->route('id');
    }
}
