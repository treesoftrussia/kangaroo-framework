<?php

namespace Mildberry\Kangaroo\Libraries\Request;

use Mildberry\Kangaroo\Libraries\Specification\Support\IlluminateRequestDataAdapter;
use RuntimeException;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
abstract class APIRequest extends Request
{
    use RequestOptionsTrait;

    /**
     *
     * Return entity built using request data.
     *
     * @return object
     */
    public function getEntity()
    {
        $method = $this->method();

        if ($method == 'GET' || $method == 'DELETE') {
            throw new RuntimeException('Entity creation through Request instance not possible with GET/DELETE methods.');
        }

        if (method_exists($this, 'createEntity')) {
            return call_user_func([$this, 'createEntity']);
        } else {
            throw new RuntimeException('CreateEntity method not implemented.');
        }
    }

    /**
     * @return array
     */
    protected function getBodyData()
    {
        return (new IlluminateRequestDataAdapter($this))->getBodyData();
    }
}
