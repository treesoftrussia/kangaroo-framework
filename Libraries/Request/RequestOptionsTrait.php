<?php

namespace Mildberry\Kangaroo\Libraries\Request;

use Mildberry\Kangaroo\Libraries\Options\AbstractOptionsInterface;
use Mildberry\Kangaroo\Libraries\Specification\Support\IlluminateRequestDataAdapter;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
trait RequestOptionsTrait
{
    /**
     * @return AbstractOptionsInterface
     */
    public function getOptions()
    {
        if (method_exists($this, 'createOptions')) {
            $options = call_user_func([$this, 'createOptions']);
            $options->initDefaultValues();
            return $options;
        } else {
            return null;
        }
    }

    /**
     * @return array
     */
    protected function getOptionsData()
    {
        return $this->getRequestDataAdapterInstance($this)->getOptionsData();
    }
}