<?php

namespace Mildberry\Kangaroo\Libraries\Request;
use Mildberry\Kangaroo\Libraries\Specification\Request\DeleteRequestSpecification;

    /**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

class DeleteRequest extends APIRequest
{
    /**
     * @var string
     */
    protected $specificationClassName = DeleteRequestSpecification::class;

    public function getEntityId()
    {
        return $this->route('id');
    }
}
