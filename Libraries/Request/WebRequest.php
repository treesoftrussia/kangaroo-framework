<?php

namespace Mildberry\Kangaroo\Libraries\Request;

use Mildberry\Kangaroo\Libraries\PageBuilder\PageData;
use Mildberry\Kangaroo\Libraries\Service\UserServiceInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class WebRequest extends Request
{
    use RequestOptionsTrait;

    /**
     * @var PageData
     */
    private $pageData;

    /**
     * @return PageData
     */
    public function getPageData()
    {
        $userService = $this->container->make(UserServiceInterface::class);

        $this->pageData = new PageData();
        $this->pageData->setUser($userService->getCurrentUser());
        $this->pageData->setOptions($this->getOptions());

        return $this->pageData;
    }
}
