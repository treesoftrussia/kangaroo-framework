<?php

namespace Mildberry\Kangaroo\Libraries\Traits\Request;

use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
trait FiltersTrait
{
    /**
     * @var bool
     */
    protected $filters = false;

    public function getFilters()
    {
        if (!method_exists($this, 'createFilters')) {
            throw new RuntimeException('createFiltersEntity method not implemented.');
        }

        $filters = $this->getFiltersData();

        if (!empty($filters)) {
            $filtersEntity = $this->extract($filters, $this->container->call([$this, 'createFilters']));

            return $filtersEntity;
        };

        return;
    }
}
