<?php

namespace Mildberry\Kangaroo\Libraries\Traits\Request;

use Mildberry\Kangaroo\Libraries\Options\Objects\PaginationOptions;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
trait PaginationTrait
{
    /**
     * @var bool
     */
    protected $pagination = false;

    /**
     * @return null|PaginationOptions
     */
    public function getPagination()
    {
        if (!$this->pagination) {
            return;
        } else {
            $requestOptions = $this->getOptionsData();

            return $this->buildPagination($requestOptions);
        }
    }

    protected function buildPagination(array $data)
    {
        return $this->extract($data, new PaginationOptions(), []);
    }
}
