<?php

namespace Mildberry\Kangaroo\Libraries\Traits\Request;

use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
trait OptionsTrait
{
    public function getOptions()
    {
        if (method_exists($this, 'createOptions')) {
            return call_user_func([$this, 'createOptions']);
        } else {
            throw new RuntimeException('CreateOptions method not implemented.');
        }
    }
}
