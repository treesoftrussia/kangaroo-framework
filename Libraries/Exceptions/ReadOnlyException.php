<?php
namespace Mildberry\Kangaroo\Libraries\Exceptions;

use Exception;

/**
 * The exception is used to indicate that value of a property is read-only and cannot be changed
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ReadOnlyException extends Exception
{
    public function __construct($method)
    {
        parent::__construct('Set can\'t be used with a read-only property "' . $method . '".');
    }
} 