<?php namespace Mildberry\Kangaroo\Libraries\Specification;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
abstract class ACompositeType extends AType
{
    use Validatable;

    protected $structure;
    private $resolvers = [];

    private $emptyAllowed;

    public function __construct($structure = null, $emptyAllowed = true, $nullable = false, $optional = false)
    {
        parent::__construct($nullable, $optional);

        $this->structure = $structure;
        $this->emptyAllowed = $emptyAllowed;
    }

    public function structure()
    {
        return $this->structure;
    }

    public function getErrorMessages()
    {
        return $this->errors;
    }

    protected function bubbleEnvironment($checkerItem)
    {
        $this->bubbleContainer($checkerItem);
        $this->bubbleValidator($checkerItem);
        $this->bubbleIsNullableAllowed($checkerItem);
        $this->bubbleScope($checkerItem);
    }

    protected function bubbleScope($checkerItem)
    {
        $checkerItem->setScope($this->getScope());
    }

    protected function bubbleIsNullableAllowed($checkerItem)
    {
        $checkerItem->setIsNullableAllowed($this->isNullableAllowed());
    }

    protected function bubbleContainer($checkerItem)
    {
        $checkerItem->setContainer($this->container);
    }

    protected function handleArrayValue($object)
    {
        if (!is_array($object)) {
            throw new ValidationException("Checking object is not an array.");
        }

        //$object = $this->specificationType == self::REQUEST_SPECIFICATION ? skip_empty($object, [null]) : $object;

        if(empty($object) && !$this->emptyAllowed){
            throw new ValidationException(["Empty value is not allowed."]);
        }

        return $object;
    }

    protected function handleOptions($options)
    {
        if (isset($options['resolvers'])) {
            $this->resolvers = array_unsmash($options['resolvers']);
        }
    }

    protected function setResolvers($resolvers)
    {
        $this->resolvers = $resolvers;
    }

    protected function isResolvable($structureKey)
    {
        if (!empty($this->resolvers[$structureKey]) && is_callable($this->resolvers[$structureKey])) {
            return true;
        }
        return false;
    }

    protected function resolve($structureKey, $entity)
    {
        return $this->resolvers[$structureKey]($entity);
    }

    protected function bubbleResolvers($structureKey, $structureItem)
    {
        if (method_exists($structureItem, 'setResolvers') && isset($this->resolvers[$structureKey]) && !is_callable($this->resolvers[$structureKey])) {
            $structureItem->setResolvers($this->resolvers[$structureKey]);
        }
    }

}