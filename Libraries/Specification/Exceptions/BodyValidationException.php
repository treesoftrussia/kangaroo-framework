<?php
namespace Mildberry\Kangaroo\Libraries\Specification\Exceptions;

use Mildberry\Kangaroo\Libraries\Resource\Error;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\InvalidInputException;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class BodyValidationException extends InvalidInputException
{

    public function getInternalCode(){
        return Error::VALIDATION_BODY_ERROR;
    }

}