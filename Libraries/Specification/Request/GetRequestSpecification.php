<?php

namespace Mildberry\Kangaroo\Libraries\Specification\Request;

use Mildberry\Kangaroo\Libraries\Specification\ASpecification;
use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;

/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

class GetRequestSpecification extends ASpecification
{
    /**
     * @var int
     */
    protected $specificationType = ASpecification::REQUEST_SPECIFICATION;

    /**
     * @return ObjectType
     */
    public function route()
    {
        return new ObjectType([
            'id' => new IntegerType(),
        ]);
    }
}
