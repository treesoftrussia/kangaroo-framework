<?php
namespace Mildberry\Kangaroo\Libraries\Specification;

use Illuminate\Http\Response;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class ValidationException extends RuntimeException
{
    private $errors;

    public function __construct($errors)
    {
        if(is_array($errors)){
            $this->errors = $errors;
        } else {
            $this->errors[] = $errors;
        }
    }

    public function getErrors(){
        return $this->errors;
    }

    public function getHTTPCode(){
        return Response::HTTP_BAD_REQUEST;
    }
}