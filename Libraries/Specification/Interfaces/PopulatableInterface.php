<?php
namespace Mildberry\Kangaroo\Libraries\Specification\Interfaces;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
interface PopulatableInterface
{
    public function populate(array $data, $object = null, $options = []);
}