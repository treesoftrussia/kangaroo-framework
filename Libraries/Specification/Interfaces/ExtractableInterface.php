<?php
namespace Mildberry\Kangaroo\Libraries\Specification\Interfaces;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
interface ExtractableInterface
{
    public function extract($object, $options = []);
}