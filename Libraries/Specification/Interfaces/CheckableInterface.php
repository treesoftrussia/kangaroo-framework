<?php
namespace Mildberry\Kangaroo\Libraries\Specification\Interfaces;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
interface CheckableInterface
{
    public function check($object);
}