<?php namespace Mildberry\Kangaroo\Libraries\Specification;

use Illuminate\Contracts\Container\Container;
use Mildberry\Kangaroo\Libraries\Converter\Support\Scope;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Mildberry\Kangaroo\Libraries\Specification\Interfaces\CheckableInterface;
use Mildberry\Kangaroo\Libraries\Specification\Interfaces\ErrorableInterface;
use Mildberry\Kangaroo\Libraries\Specification\Interfaces\ExtractableInterface;
use Mildberry\Kangaroo\Libraries\Specification\Interfaces\PopulatableInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
abstract class AType implements CheckableInterface, ExtractableInterface, ErrorableInterface
{
    protected $errors = [];
    public $container;

    protected $nullable;
    protected $scope;
    protected $optional;

    protected $isNullableAllowed = false;

    public function __construct($nullable = false, $optional = false)
    {
        $this->nullable = $nullable;
        $this->optional = $optional;
    }

    /**
     * @return Scope
     */
    protected function getScope()
    {
        if (!$this->scope) {
            $this->scope = new Scope();
        }
        return $this->scope;
    }

    /**
     * @param $scope
     * @return $this
     */
    protected function setScope($scope)
    {
        $this->scope = $scope;
        return $this;
    }

    /**
     * @param Container $container
     * @return $this
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;
        return $this;
    }

    /**
     * @return $this
     */
    public function nullable(){
        $this->nullable = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function optional(){
        $this->optional = true;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isNullableAllowed()
    {
        return $this->isNullableAllowed;
    }

    /**
     * @param boolean $isNullableAllowed
     * @return $this
     */
    public function setIsNullableAllowed($isNullableAllowed)
    {
        $this->isNullableAllowed = $isNullableAllowed;
        return $this;
    }

    protected function isOptional(){
        return $this->optional;
    }

    private function skipIfNullable($object)
    {
        if($this->nullable && !$this->isNullableAllowed){
            throw new RuntimeException('Nullable types are not allowed for REQUEST specifications.');
        }

        if(is_null($object) && !$this->nullable){
            throw new ValidationException('Value can not be null');
        } else if(is_null($object)){
            return true;
        }

        return false;
    }


    public function check($object)
    {
        if($this->skipIfNullable($object)){
            return true;
        }

        try {
            $this->findErrors($object);
        } catch (ValidationException $e) {
            $currentScope = $this->getScope()->get(null, true);
            if(empty($currentScope)){
                $this->errors = $e->getErrors();
            } else {
                $this->errors[$currentScope] = $e->getErrors();
                $this->getScope()->drop();
            }
        };

        return empty($this->errors);
    }

    protected abstract function findErrors($object);

    public abstract function extract($object, $options = []);

    public function getErrorMessages()
    {
        return $this->errors;
    }
}