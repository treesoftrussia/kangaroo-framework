<?php

namespace Mildberry\Kangaroo\Libraries\Specification;

use Illuminate\Contracts\Container\Container;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Mildberry\Kangaroo\Libraries\Specification\Interfaces\CheckableInterface;
use Mildberry\Kangaroo\Libraries\Specification\Interfaces\ErrorableInterface;
use Mildberry\Kangaroo\Libraries\Specification\Interfaces\ExtractableInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
abstract class ASpecification implements CheckableInterface, ExtractableInterface, ErrorableInterface
{
    const REQUEST_SPECIFICATION = 1;
    const RESPONSE_SPECIFICATION = 2;

    protected $specificationType = ASpecification::RESPONSE_SPECIFICATION;

    protected $cachedStructureObjects;

    protected $container;

    /**
     * ASpecification constructor.
     * @param Container $container
     */
    public function __construct(Container $container = null)
    {
        $this->container = $container;
        $this->cacheStructureObjects();
    }

    protected function cacheStructureObjects()
    {
        $this->cachedStructureObjects['route'] = $this->route();
        $this->cachedStructureObjects['options'] = $this->options();
        $this->cachedStructureObjects['body'] = $this->body();
    }

    /**
     * @return array | null
     */
    protected function route()
    {
        return null;
    }

    /**
     * @return AType | null
     */
    protected function options()
    {
        return null;
    }

    /**
     * @return AType
     */
    protected function body()
    {
        return null;
    }

    public function getBody()
    {
        return $this->cachedStructureObjects['body'];
    }

    public function getRoute()
    {
        return $this->cachedStructureObjects['route'];
    }

    public function getOptions()
    {
        return $this->cachedStructureObjects['options'];
    }

    /**
     * @param array $data
     * @return bool
     */
    public function checkBody(array $data)
    {
        $body = $this->getBody();

        if (is_null($body)) {
            return true;
        }

        return $body->check($data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function checkRoute(array $data)
    {
        $route = $this->getRoute();

        if (is_null($route)) {
            return true;
        }

        foreach ($data as $k => $item){
            if(is_numeric($item)){
                $data[$k] = intval($item);
            }
        }

        return $route->check($data);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function checkOptions(array $data)
    {
        $options = $this->getOptions();

        if (is_null($options)) {
            return true;
        }

        return $options->check($data);
    }

    /**
     * @param $data
     * @return bool
     */
    public function check($data)
    {
        $body = $this->getDefaultStructureSource();

        if (is_null($body)) {
            return true;
        }
        return $body->check($data);

    }

    /**
     * @param $entity
     * @param array $options
     * @return array
     */
    public function extract($entity, $options = [])
    {
        $body = $this->getDefaultStructureSource();

        if (is_null($body)) {
            throw new RuntimeException("Structure is not set");
        }

        return $body->extract($entity, $options);
    }

    /**
     * @return $this
     */
    public function setStructureObjectsConfiguration()
    {
        if ($route = $this->getRoute()) {
            $route->isNullableAllowed(false);
            $route->setContainer($this->container);
        }

        if ($body = $this->getBody()) {
            //TODO think about nullable properties in request
            $body->setIsNullableAllowed($this->specificationType == ASpecification::RESPONSE_SPECIFICATION);
            $body->setContainer($this->container);
        }

        if ($options = $this->getOptions()) {
            $options->setIsNullableAllowed(false);
            $options->setContainer($this->container);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        $errors = [];
        foreach ($this->cachedStructureObjects as $k => $object) {
            if (!is_null($object)) {
                $errors = $errors + $object->getErrorMessages();
            }
        }
        return $errors;
    }

    protected function getDefaultStructureSource()
    {
        return $this->getBody();
    }

    /**
     * @return int
     */
    public function getSpecificationType()
    {
        return $this->specificationType;
    }

}