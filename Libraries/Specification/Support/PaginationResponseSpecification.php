<?php namespace Mildberry\Kangaroo\Libraries\Specification\Support;

use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Mildberry\Kangaroo\Libraries\Specification\ASpecification;
use Mildberry\Kangaroo\Libraries\Specification\AType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Collection\CollectionType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;
use ReflectionClass;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class PaginationResponseSpecification extends ASpecification
{

    protected $specificationType = ASpecification::RESPONSE_SPECIFICATION;

    private $baseTypeClassName;
    private $baseClassArgs;

    public function __construct($container = null, $baseTypeClassName = "", $args = [])
    {
        if(empty($baseTypeClassName)){
            throw new RuntimeException('BaseTypeClassName was not set');
        }

        $this->baseTypeClassName = $baseTypeClassName;
        $this->baseClassArgs = $args;

        parent::__construct($container);
    }

    /**
     * @param $baseTypeClassName
     * @return object
     */
    private function buildBaseStructureObject($baseTypeClassName){
        if(is_subclass_of($baseTypeClassName, AType::class)){
            $specification = new $baseTypeClassName;
        } else if(is_subclass_of($baseTypeClassName, ASpecification::class)){
            $specification = (new ReflectionClass($baseTypeClassName))->newInstanceArgs([$this->container])->getBody();
        } else {
            throw new RuntimeException('Wrong class name of specification object.');
        }

        return $specification;
    }

    /**
     * @return AType
     */
    public function body()
    {
        $baseTypeClassName = $this->baseTypeClassName;

        $context = $this;

        return new ObjectType(['items' => new CollectionType(function () use ($baseTypeClassName, $context) {
            return $context->buildBaseStructureObject($baseTypeClassName);
        }, true, false),
            'pagination' => new ObjectType([
                    "totalEntries" => new IntegerType(),
                    "entriesOnCurrentPage" => new IntegerType(),
                    "entriesPerPageRequested" => new IntegerType(),
                    "currentPage" => new IntegerType(),
                    "totalPages" => new IntegerType()
                ]
            )]);
    }
}