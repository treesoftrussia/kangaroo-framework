<?php

namespace Mildberry\Kangaroo\Libraries\Specification\Support;
use Illuminate\Http\Request;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class IlluminateRequestDataAdapter implements RequestDataInterface
{

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function getRouteData()
    {
        return $this->request->route()->parameters();
    }

    /**
     * @return array
     */
    public function getBodyData()
    {
        return $this->request->all();
    }

    /**
     * @return array
     */
    public function getOptionsData()
    {
        $options = array_take($this->request->query->all(), 'options', []);

        if(empty($options)){
            return [];
        }

        return $options;
    }
}