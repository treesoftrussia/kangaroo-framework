<?php

namespace Mildberry\Kangaroo\Libraries\Specification\Support;

use Mildberry\Kangaroo\Libraries\Specification\ASpecification;
use Mildberry\Kangaroo\Libraries\Specification\Exceptions\BodyValidationException;
use Mildberry\Kangaroo\Libraries\Specification\Exceptions\OptionsValidationException;
use Mildberry\Kangaroo\Libraries\Specification\Exceptions\RouteValidationException;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class RequestDataChecker
{
    /**
     * @param RequestDataInterface $requestData
     * @param ASpecification $specification
     * @throws RouteValidationException
     * @throws OptionsValidationException
     * @throws BodyValidationException
     * @return bool
     */
    public function checkRequest(RequestDataInterface $requestData, ASpecification $specification)
    {
        if (!$specification->checkRoute($requestData->getRouteData())) {
            throw new RouteValidationException($specification->getErrorMessages());
        }

        if (!$specification->checkOptions($requestData->getOptionsData())) {
            throw new OptionsValidationException($specification->getErrorMessages());
        }

        if (!$specification->checkBody($requestData->getBodyData())) {
            throw new BodyValidationException($specification->getErrorMessages());
        }

        return true;
    }

}