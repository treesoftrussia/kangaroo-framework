<?php

namespace Mildberry\Kangaroo\Libraries\Specification\Support;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
interface RequestDataInterface
{

    /**
     * @return mixed
     */
    public function getRouteData();

    /**
     * @return array
     */
    public function getBodyData();

    /**
     * @return array
     */
    public function getOptionsData();
}