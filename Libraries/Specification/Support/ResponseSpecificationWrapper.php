<?php namespace Mildberry\Kangaroo\Libraries\Specification\Support;

use Illuminate\Contracts\Container\Container;
use Mildberry\Kangaroo\Libraries\Specification\ASpecification;
use Mildberry\Kangaroo\Libraries\Specification\AType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class ResponseSpecificationWrapper extends ASpecification
{
    /**
     * ResponseSpecificationWrapper constructor.
     * @param Container $container
     * @param AType $body
     * @param int $specificationType
     */
    public function __construct(Container $container = null, AType $body = null, $specificationType = ASpecification::RESPONSE_SPECIFICATION)
    {
        $this->specificationType = $specificationType;
        $this->container = $container;

        $this->cachedStructureObjects['body'] = $body;
        $this->cachedStructureObjects['options'] = null;
        $this->cachedStructureObjects['route'] = null;
    }
}