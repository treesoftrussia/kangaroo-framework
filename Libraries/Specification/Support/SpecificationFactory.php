<?php namespace Mildberry\Kangaroo\Libraries\Specification\Support;

use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Mildberry\Kangaroo\Libraries\Specification\ASpecification;
use Mildberry\Kangaroo\Libraries\Specification\AType;
use ReflectionClass;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class SpecificationFactory
{
    public static function make($className, $args = [], $specificationType = ASpecification::RESPONSE_SPECIFICATION)
    {
        if(is_subclass_of($className, AType::class)){
            $specification = new ResponseSpecificationWrapper(app(), (new ReflectionClass($className))->newInstanceArgs($args), $specificationType);
        } else if(is_subclass_of($className, ASpecification::class)){
            array_unshift($args, app());

            $specification = (new ReflectionClass($className))->newInstanceArgs($args);
        } else {
            throw new RuntimeException('Wrong class name of specification object.');
        }

        $specification->setStructureObjectsConfiguration();

        return $specification;
    }
}