<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/08/16
 * Time: 12:09
 */

namespace Mildberry\Kangaroo\Libraries\Specification;

use Illuminate\Validation\Validator;
use Illuminate\Contracts\Validation\Factory as FactoryContract;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
trait Validatable
{
    /**
     * @var Validator $validator
     */
    protected $validator;

    public function setValidator($validator){
        $this->validator = $validator;
    }

    public function getValidator(){
        if($this->validator){
            return $this->validator;
        }

        $factory = $this->container->make(FactoryContract::class);
        $this->validator = $factory->make([], []);
        return $this->validator;
    }

    protected function bubbleValidator($checkerItem){
        if(method_exists($checkerItem, 'setValidator')){
            $checkerItem->setValidator($this->getValidator());
        }
    }

}