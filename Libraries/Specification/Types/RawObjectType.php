<?php namespace Mildberry\Kangaroo\Libraries\Specification\Types;

use Mildberry\Kangaroo\Libraries\Specification\ALeafType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class RawObjectType extends ALeafType
{

    protected function findErrors($object){}

    public function extract($entity, $options = [])
    {
        if(is_null($entity) && $this->nullable){
            return null;
        }

        return $entity;
    }
}