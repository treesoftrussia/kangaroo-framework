<?php namespace Mildberry\Kangaroo\Libraries\Specification\Types\Object;

use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Mildberry\Kangaroo\Libraries\Specification\ACompositeType;
use Mildberry\Kangaroo\Libraries\Specification\AType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Collection\CollectionType;
use Mildberry\Kangaroo\Libraries\Specification\ValidationException;
use ReflectionClass;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class ObjectType extends ACompositeType
{

    private $ignoredKeys = [];

    public function __construct($structure = null, $nullable = false, $optional = false, $ignoredKeys = [])
    {
        parent::__construct($structure, true, $nullable, $optional);
        $this->ignoredKeys = $ignoredKeys;
    }

    private function checkExtraKeys($structure, $inputArray)
    {
        $diff = array_diff(array_keys($inputArray), array_keys($structure));
        if (!empty($diff)) {
            foreach ($diff as $diffItem) {
                if (is_integer($diffItem)) {
                    $this->errors[$this->getScope()->get($diffItem, true)][] = "Integer keys is not allowed (or no key detected)";
                } else {
                    $this->errors[$this->getScope()->get($diffItem, true)][] = "Key is not allowed";
                }
            }
        }
    }

    public function setIgnoredKeys($ignoredKeys){
        $this->ignoredKeys = $ignoredKeys;
        return $this;
    }

    protected function findErrors($object)
    {
        $object = $this->handleArrayValue($object);

        $structure = is_callable($this->structure) ? call_user_func($this->structure()) : $this->structure();
        $this->checkExtraKeys($structure, $object);

        /**
         * @var AType $checkerItem
         */
        foreach ($structure as $key => $checkerItem) {
            $this->bubbleEnvironment($checkerItem);

            if(in_array($key, $this->ignoredKeys)){
                continue;
            }

            $this->getScope()->create($key);

            if (!array_key_exists($key, $object)) {

                if (!$checkerItem->isOptional()) {
                    throw new ValidationException('Value is required');
                }

                if($checkerItem->isOptional() && $checkerItem->isNullableAllowed){
                    throw new RuntimeException('Optional types are not allowed for RESPONSE specifications.');
                }

                $this->getScope()->drop();

                continue;
            }


            if (!$checkerItem->check($object[$key])) {
                $this->errors = array_merge($checkerItem->getErrorMessages(), $this->errors);
            }

            $this->getScope()->drop();
        }
    }


    protected function tryToGetFromGetter($structureKey, $entity)
    {
        $getter = make_getter($structureKey);

        if (method_exists($entity, $getter)) {
            return $entity->$getter();
        } else {
            throw new RuntimeException("Method " . $getter . " doesn't exists in object " . get_class($entity));
        }
    }

    public function getStructure(){
        return is_callable($this->structure) ? call_user_func($this->structure()) : $this->structure();
    }

    public function extract($entity, $options = [])
    {
        if(is_null($entity) && $this->nullable){
            return null;
        }
        $this->handleOptions($options);

        $result = [];
        $structure = $this->getStructure();

        foreach ($structure as $structureKey => $structureItem) {

            if ($this->isResolvable($structureKey, $entity)) {
                $extractedEntity = $this->resolve($structureKey, $entity);
            } else {
                $extractedEntity = $this->tryToGetFromGetter($structureKey, $entity);
            }

            $this->bubbleResolvers($structureKey, $structureItem);

            $result[$structureKey] = $structureItem->extract($extractedEntity);
        }
        return $result;
    }


    public function populate(array $data, $object = null, $options = [])
    {

        $structure = is_callable($this->structure) ? call_user_func($this->structure()) : $this->structure();

        $objectReflection = new ReflectionClass($object);

        foreach ($structure as $structureKey => $structureItem) {
            if(!array_key_exists($structureKey, $data)){
                continue;
            }

            $setter =  make_setter($structureKey);
            if(!method_exists($object, $setter)){
                continue;
            }

            if($structureItem instanceof ACompositeType){
                $setterReflection = $objectReflection->getMethod($setter);
                $setterClass = $setterReflection->getParameters()[0]->getClass();
                //@TODO add options
                $value = $structureItem->populate($data[$structureKey], new $setterClass(), null);
            } else {
                $value = $structureItem->populate($data[$structureKey], null, null);
            }

            $object->{$setter}($value);

            return $object;
        }

    }
//
//    public function fake($options = []){
//        $result = [];
//        $structure = $this->structure();
//
//        foreach ($structure as $structureKey => $structureItem){
//
//            $result[$structureKey] = $structureItem->extract($extractedEntity);
//        }
//        return $result;
//    }
}