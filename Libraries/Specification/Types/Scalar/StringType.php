<?php namespace Mildberry\Kangaroo\Libraries\Specification\Types\Scalar;

use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class StringType extends AScalarType
{
    protected function checkType($object){
        return is_string($object);
    }

    protected function failedCheckValueMessage()
    {
        return "Value is not a string.";
    }

    public function extract($entity, $options = [])
    {
        return Cast::string($entity);
    }
}