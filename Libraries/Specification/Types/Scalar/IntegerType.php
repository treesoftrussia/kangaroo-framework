<?php namespace Mildberry\Kangaroo\Libraries\Specification\Types\Scalar;
use Mildberry\Kangaroo\Libraries\Cast\Cast;


/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class IntegerType extends AScalarType
{
    protected function checkType($object){
        return filter_var($object, FILTER_VALIDATE_INT) !== false;
    }

    protected function failedCheckValueMessage()
    {
        return "Value is not an integer.";
    }

    public function extract($entity, $options = [])
    {
        return Cast::int($entity);
    }
}