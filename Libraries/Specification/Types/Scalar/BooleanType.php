<?php namespace Mildberry\Kangaroo\Libraries\Specification\Types\Scalar;
use Mildberry\Kangaroo\Libraries\Cast\Cast;


/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class BooleanType extends AScalarType
{
    protected function checkType($object){
        if(is_bool($object)){
            return true;
        }

        if(in_array($object, ['true', 'false'])){
            return true;
        }

        return false;
    }

    protected function failedCheckValueMessage()
    {
        return "Value is not a boolean.";
    }

    public function extract($entity, $options = [])
    {
        return Cast::bool($entity);
    }
}