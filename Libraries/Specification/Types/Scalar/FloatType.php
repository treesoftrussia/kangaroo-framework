<?php namespace Mildberry\Kangaroo\Libraries\Specification\Types\Scalar;
use Mildberry\Kangaroo\Libraries\Cast\Cast;


/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class FloatType extends AScalarType
{
    protected function checkType($object){
        return filter_var($object, FILTER_VALIDATE_FLOAT) !== false;
    }

    protected function failedCheckValueMessage()
    {
        return "Value is not a float.";
    }

    public function extract($entity, $options = [])
    {
        return Cast::float($entity);
    }
}