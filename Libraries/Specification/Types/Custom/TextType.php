<?php namespace Mildberry\Kangaroo\Libraries\Specification\Types\Custom;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class TextType extends CustomType
{
    /**
     * @var int
     */
    protected $type = self::STRING_TYPE;

    /**
     * @var int
     */

    private $minLength;
    /**
     * @var int
     */
    private $maxLength;

    /**
     * @param int $minLength
     * @param int $maxLength
     * @param array $rules
     * @param bool $nullable
     * @param bool $optional
     */
    public function __construct($minLength = 1, $maxLength = 255, $rules = [], $nullable = false, $optional = false)
    {
        parent::__construct($rules, $nullable, $optional);

        $this->minLength = $minLength;
        $this->maxLength = $maxLength;
    }

    /**
     * @return array
     */
    public function getRules()
    {
        return ['required','string', 'min:'.$this->minLength, 'max:'.$this->maxLength];
    }
}
