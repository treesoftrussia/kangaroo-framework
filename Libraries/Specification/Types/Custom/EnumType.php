<?php namespace Mildberry\Kangaroo\Libraries\Specification\Types\Custom;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class EnumType extends CustomType
{

    /**
     * @var int
     */
    protected $type = self::STRING_TYPE;

    /**
     * @var array
     */
    protected $whiteList;

    /**
     * EnumType constructor.
     * @param array $whiteList
     * @param bool $nullable
     * @param bool $optional
     */
    public function __construct($whiteList = [], $nullable = false, $optional = false)
    {
        parent::__construct([], $nullable, $optional);

        $this->whiteList = $whiteList;
    }

    /**
     * @return array
     */
    public function getRules()
    {
        return ['in:'.implode(',', $this->whiteList)];
    }
}