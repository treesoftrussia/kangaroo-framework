<?php namespace Mildberry\Kangaroo\Libraries\Specification\Types\Custom;

use Mildberry\Kangaroo\Libraries\Cast\Cast;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Mildberry\Kangaroo\Libraries\Specification\ALeafType;
use Mildberry\Kangaroo\Libraries\Specification\Validatable;
use Mildberry\Kangaroo\Libraries\Specification\ValidationException;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class CustomType extends ALeafType
{
    use Validatable;

    protected $rules;
    protected $type;

    const INTEGER_TYPE = 1;
    const STRING_TYPE = 2;
    const FLOAT_TYPE = 3;
    const BOOLEAN_TYPE = 4;

    public function __construct($rules = [], $nullable = false, $optional = false)
    {
        parent::__construct($nullable, $optional);
        $this->rules = $rules;
    }

    protected function getRules()
    {
        return $this->rules;
    }

    protected function findErrors($object)
    {
        $rules = $this->getRules();

        if (empty($rules)) {
            throw new RuntimeException('Custom type rules was not set.');
        }

        $this->validator = $this->getValidator();

        if (!is_scalar($object)) {
            throw new ValidationException('Value is not a scalar.');
        }

        $this->validator->setRules(['object' => $rules]);
        $this->validator->setData(['object' => $object]);

        if (!$this->validator->passes()) {
            throw new ValidationException($this->validator->getMessageBag()->toArray()['object']);
        }
    }

    public function extract($entity, $options = [])
    {
        if(is_null($entity) && $this->nullable){
            return null;
        }

        if (!isset($this->type)) {
            throw new RuntimeException('Custom value type not set.');
        }

        switch ($this->type) {
            case self::INTEGER_TYPE:
                return Cast::int($entity);
            case self::STRING_TYPE:
                return Cast::string($entity);
            case self::FLOAT_TYPE:
                return Cast::float($entity);
            case self::BOOLEAN_TYPE:
                return Cast::bool($entity);
        }

        throw new RuntimeException('Custom value type is not supported - ' . $this->type);
    }
}