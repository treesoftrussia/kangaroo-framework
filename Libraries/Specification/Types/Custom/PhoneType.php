<?php

namespace Mildberry\Kangaroo\Libraries\Specification\Types\Custom;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class PhoneType extends CustomType
{
    /**
     * @var int
     */
    protected $type = self::STRING_TYPE;

    /**
     * PhoneType constructor.
     *
     * @param bool $nullable
     * @param bool $optional
     */
    public function __construct($nullable = false, $optional = false)
    {
        parent::__construct(['string', 'min:6', 'max:12'], $nullable, $optional);
    }
}
