<?php

namespace Mildberry\Kangaroo\Libraries\Specification\Types\Custom;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class EmailType extends CustomType
{
    /**
     * @var int
     */
    protected $type = self::STRING_TYPE;

    /**
     * EmailType constructor.
     *
     * @param bool $nullable
     * @param bool $optional
     */
    public function __construct($nullable = false, $optional = false)
    {
        parent::__construct(['email'], $nullable, $optional);
    }
}
