<?php
namespace Mildberry\Kangaroo\Libraries\Permissions;

use Closure;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Routing\Middleware as MiddlewareInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * The middleware checks the permissions based on the requested controller and action.
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Middleware implements MiddlewareInterface
{
    private $container;
    private $manager;

    public function __construct(Container $container, Manager $manager)
    {
        $this->container = $container;
        $this->manager = $manager;
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  Closure $next
     * @return Response
     * @throws PermissionsException
     * @throws AccessDeniedHttpException
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route();
        $options = $route->getAction();

        $details = $this->explode($options['uses']);

        $permissionsClass = $this->getPermissionsClass($details);

        if (!class_exists($permissionsClass)) {
            throw new PermissionsException('The permissions class "' . $permissionsClass . '" has not been found.');
        }

        $permissions = $this->container->make($permissionsClass);

        if (!$permissions instanceof AbstractPermissions) {
            throw new PermissionsException('The permissions class "' . $permissionsClass . '" must be instance of AbstractPermissions.');
        }

        $permissions->setRequest($request);
        $permissions->setRoute($route);

        if (!$permissions->has($details['action'])) {
            throw new AccessDeniedHttpException();
        }

        $this->manager->setPermissions($permissions);

        return $next($request);
    }

    /**
     * Prepares permissions object class
     *
     * @param array $details
     * @return string
     */
    private function getPermissionsClass(array $details)
    {
        return $details['controller']['namespace'] . '\Permissions\\' . $details['controller']['name'] . 'Permissions';
    }

    /**
     * Prepares array with detailed information about the requested controller including the requested action name
     *
     * @param string $raw
     * @return array
     */
    private function explode($raw)
    {
        list($controller, $action) = explode('@', $raw);

        $class = substr($controller, strrpos($controller, '\\') + 1);
        $namespace = substr($controller, 0, strrpos($controller, '\\'));
        $name = substr($class, 0, -1 * strlen('controller'));

        return [
            'action' => $action,
            'controller' => [
                'full' => $controller,
                'namespace' => $namespace,
                'name' => $name,
                'class' => $class
            ]
        ];
    }

}