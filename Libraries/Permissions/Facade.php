<?php
namespace Mildberry\Kangaroo\Libraries\Permissions;

use Illuminate\Support\Facades\Facade as AbstractFacade;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Facade extends AbstractFacade
{
    protected static function getFacadeAccessor()
    {
        return Manager::class;
    }
} 