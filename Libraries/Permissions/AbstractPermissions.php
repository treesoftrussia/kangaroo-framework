<?php
namespace Mildberry\Kangaroo\Libraries\Permissions;

/**
 * Abstract class for all permissions classes.
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
abstract class AbstractPermissions
{
    use AppectableTrait;

    /**
     * The method is used to check whether the requested resource is allowed to be accessed
     *
     * @return array
     */
    abstract protected function permissions();

    public function has($action)
    {
        $permissions = $this->permissions();

        foreach ($permissions as $method => $protectors) {
            if ($action === $method) {
                return $this->allow($protectors);
            }
        }

        return false;
    }

    /**
     * Executes the provided protectors to check whether the requested resource is allowed
     *
     * @param array|string $protectors
     * @return bool
     * @throws PermissionsException
     */
    public function allow($protectors)
    {
        if (!is_array($protectors)) {
            $protectors = [$protectors];
        }

        foreach ($protectors as $protector) {
            $method = 'protector' . studly_case($protector);

            if (!method_exists($this, $method)) {
                throw new PermissionsException('The method to handle the "' . $protector . '" protector does not exist.');
            }

            if (call_user_func([$this, $method])) {
                return true;
            }
        }

        return false;
    }
} 