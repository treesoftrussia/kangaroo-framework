<?php
namespace Mildberry\Kangaroo\Libraries\Permissions;

use RuntimeException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class PermissionsException extends RuntimeException
{

} 