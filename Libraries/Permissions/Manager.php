<?php
namespace Mildberry\Kangaroo\Libraries\Permissions;

/**
 * Provides access to the permissions layer within controllers
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Manager
{
    /**
     * @var AbstractPermissions
     */
    private $permissions;

    public function setPermissions(AbstractPermissions $permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * Checks whether the current request has permissions
     *
     * @param $protectors
     * @return bool
     */
    public function has($protectors)
    {
        return $this->permissions->allow($protectors);
    }
} 