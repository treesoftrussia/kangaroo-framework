<?php
namespace Mildberry\Kangaroo\Libraries\Permissions;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
trait AppectableTrait
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Route
     */
    private $route;

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param Route $route
     */
    public function setRoute(Route $route)
    {
        $this->route = $route;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Route
     */
    public function getRoute()
    {
        return $this->route;
    }
} 