<?php
namespace Mildberry\Kangaroo\Libraries\Permissions;

use Illuminate\Routing\Controller;
use Illuminate\Support\ServiceProvider as Provider;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ServiceProvider extends Provider
{
    public function boot()
    {
        $this->app->resolving(function (Controller $controller) {
            $controller->middleware(Middleware::class);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}