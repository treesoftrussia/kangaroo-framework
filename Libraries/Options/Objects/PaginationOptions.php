<?php
namespace Mildberry\Kangaroo\Libraries\Options\Objects;

use Mildberry\Kangaroo\Libraries\Cast\Cast;
use Mildberry\Kangaroo\Libraries\Options\AbstractOptionsInterface;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class PaginationOptions
{
    public function __construct()
    {
        $this->perPage = config('params.pagination_per_page', 10);
    }

    /**
     * @var
     */
    private $perPage;

    /**
     * @var
     */
    private $pageNum = 1;

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @return int
     */
    public function getPageNum()
    {
        return $this->pageNum;
    }

    /**
     * @param int $perPage
     *
     * @return $this
     */
    public function setPerPage($perPage)
    {
        $this->perPage = Cast::int($perPage);
        $this->perPage = $this->perPage < 1 ? 1 : $this->perPage;

        return $this;
    }

    /**
     * @param int $pageNum
     *
     * @return $this
     */
    public function setPageNum($pageNum)
    {
        $pageNum = Cast::int($pageNum);
        $this->pageNum = $pageNum < 1 ? 1 : $pageNum;

        return $this;
    }
}
