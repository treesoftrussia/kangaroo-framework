<?php
namespace Mildberry\Kangaroo\Libraries\Options\Objects;

use Mildberry\Kangaroo\Libraries\Options\AbstractOptionsInterface;
use Mildberry\Kangaroo\Libraries\Options\Traits\PaginationOptionsTrait;

class DefaultPaginationOptions implements AbstractOptionsInterface
{
    use PaginationOptionsTrait;
}
