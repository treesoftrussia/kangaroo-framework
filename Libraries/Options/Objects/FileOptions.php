<?php

namespace Mildberry\Kangaroo\Libraries\Options\Objects;

/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */
class FileOptions
{
    /**
     * @var string
     */
    private $type;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }


}
