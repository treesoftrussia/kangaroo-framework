<?php

namespace Mildberry\Kangaroo\Libraries\Options\Traits;


trait SearchingOptionsTrait
{
    private $searchPhrase;

    /**
     * @return mixed
     */
    public function getSearchPhrase()
    {
        return $this->searchPhrase;
    }

    /**
     * @param $searchPhrase
     * @return $this
     */
    public function setSearchPhrase($searchPhrase)
    {
        $this->searchPhrase = $searchPhrase;
        return $this;
    }
}