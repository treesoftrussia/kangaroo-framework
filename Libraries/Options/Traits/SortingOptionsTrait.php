<?php

namespace Mildberry\Kangaroo\Libraries\Options\Traits;
use Mildberry\Kangaroo\Libraries\Options\Objects\SortingOptions;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
trait SortingOptionsTrait
{
    /**
     * @var bool
     */
    private $useSorting = false;

    /**
     * @var SortingOptions
     */
    private $sorting;

    /**
     * @param null $useSorting
     * @return $this|bool
     */
    public function useSorting($useSorting = null){

        if(is_null($useSorting)){
            return $this->useSorting;
        }

        $this->useSorting = $useSorting;

        return $this;
    }

    /**
     * @param SortingOptions $sorting
     * @return $this
     */
    public function setSorting(SortingOptions $sorting)
    {
        $this->sorting = $sorting;

        return $this;
    }

    /**
     * @return SortingOptions
     */
    public function getSorting()
    {
        return $this->sorting;
    }
}
