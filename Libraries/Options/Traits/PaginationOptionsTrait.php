<?php

namespace Mildberry\Kangaroo\Libraries\Options\Traits;

use Mildberry\Kangaroo\Libraries\Options\Objects\PaginationOptions;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
trait PaginationOptionsTrait
{
    /**
     * @var
     */
    private $pagination;
    /**
     * @param PaginationOptions $pagination
     * @return $this
     */
    public function setPagination(PaginationOptions $pagination)
    {
        $this->pagination = $pagination;

        return $this;
    }

    /**
     * @return PaginationOptions
     */
    public function getPagination()
    {
        return $this->pagination;
    }

    public function setDefaultPagination(){
        if(!$this->pagination){
            $this->pagination = new PaginationOptions();
        }
        return $this;
    }
}
