<?php

namespace Mildberry\Kangaroo\Libraries\Options;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
interface AbstractOptionsInterface
{
    public function initDefaultValues();
}
