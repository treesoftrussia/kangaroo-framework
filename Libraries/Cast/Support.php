<?php

namespace Mildberry\Kangaroo\Libraries\Cast;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Support 
{
    public static function isNull($value)
    {
        if ($value instanceof ValueProviderInterface){
            $value = $value->getValue();
        }

        return $value === null;
    }

    /**
     * Separates sign from the actual number
     *
     * @param int $value
     * @return array
     */
    public static function getNumberParts($value)
    {
        $minus = '';

        if (substr($value, 0, 1) === '-'){
            $minus = '-';
            $value = cut_string_left($value, '-');
        }

        return [$minus, $value];
    }
} 