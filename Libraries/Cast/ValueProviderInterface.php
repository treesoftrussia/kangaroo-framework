<?php

namespace Mildberry\Kangaroo\Libraries\Cast;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
interface ValueProviderInterface
{
    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @param mixed $value
     */
    public function setValue($value);
} 