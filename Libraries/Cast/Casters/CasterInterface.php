<?php

namespace Mildberry\Kangaroo\Libraries\Cast\Casters;

use Mildberry\Kangaroo\Libraries\Cast\CastException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
interface CasterInterface
{
    /**
     * @param mixed $value
     * @return mixed
     * @throws CastException
     */
    public function cast($value);
} 