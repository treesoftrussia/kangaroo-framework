<?php

namespace Mildberry\Kangaroo\Libraries\Cast\Casters;

use Mildberry\Kangaroo\Libraries\Cast\CastException;
use Mildberry\Kangaroo\Libraries\Cast\Support;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class FloatCaster implements CasterInterface
{
    /**
     * @param mixed $value
     * @return float
     * @throws CastException
     */
    public function cast($value)
    {
        if (is_float($value)) {
            return $value;
        }

        return floatval($value);
    }
} 