<?php

namespace Mildberry\Kangaroo\Libraries\Cast\Casters;

use Mildberry\Kangaroo\Libraries\Cast\CastException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class BoolCaster implements CasterInterface
{
    /**
     * @param mixed $value
     * @return bool
     * @throws CastException
     */
    public function cast($value)
    {
        if (is_bool($value)) {
            return $value;
        }

        if ($value === 'false'){
            return false;
        }

        return boolval($value);
    }
} 