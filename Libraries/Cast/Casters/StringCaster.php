<?php

namespace Mildberry\Kangaroo\Libraries\Cast\Casters;

use Mildberry\Kangaroo\Libraries\Cast\CastException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class StringCaster implements CasterInterface
{
    /**
     * @param mixed $value
     * @return string
     * @throws CastException
     */
    public function cast($value)
    {
        if (is_string($value)) {
            return $value;
        }

        return strval($value);
    }
} 