<?php

namespace Mildberry\Kangaroo\Libraries\Cast\Casters;

use Mildberry\Kangaroo\Libraries\Cast\CastException;
use Traversable;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class CollectionCaster implements CasterInterface
{
    /**
     * @param mixed $value
     * @return mixed[]
     * @throws CastException
     */
    public function cast($value)
    {
        if (is_array($value) || $value instanceof Traversable) {
            return $value;
        }

        throw new CastException('Value must be array on instance of Traversable');
    }
} 