<?php

namespace Mildberry\Kangaroo\Libraries\Cast\Casters;

use Mildberry\Kangaroo\Libraries\Cast\CastException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ObjectCaster implements CasterInterface
{
    /**
     * @var string
     */
    private $class;

    /**
     * @param string $class
     */
    public function __construct($class)
    {
        $this->class = $class;
    }

    /**
     * @param mixed $value
     * @return object
     * @throws CastException
     */
    public function cast($value)
    {
        if (!$value instanceof $this->class){
            throw new CastException('The value must be instance of "'.$this->class.'"');
        }

        return $value;
    }
} 