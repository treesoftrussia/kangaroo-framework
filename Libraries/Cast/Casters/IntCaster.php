<?php

namespace Mildberry\Kangaroo\Libraries\Cast\Casters;

use Mildberry\Kangaroo\Libraries\Cast\CastException;
use Mildberry\Kangaroo\Libraries\Cast\Support;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class IntCaster implements CasterInterface
{
    /**
     * @param mixed $value
     * @return int
     * @throws CastException
     */
    public function cast($value)
    {
        if (is_int($value)) {
            return $value;
        }

        return intval($value);
    }
} 