<?php

namespace Mildberry\Kangaroo\Libraries\Cast;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class CastException extends \RuntimeException
{
} 