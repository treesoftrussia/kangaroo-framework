<?php

namespace Mildberry\Kangaroo\Libraries\Cast;

use Mildberry\Kangaroo\Libraries\Cast\Casters\BoolCaster;
use Mildberry\Kangaroo\Libraries\Cast\Casters\CasterInterface;
use Mildberry\Kangaroo\Libraries\Cast\Casters\FloatCaster;
use Mildberry\Kangaroo\Libraries\Cast\Casters\IntCaster;
use Mildberry\Kangaroo\Libraries\Cast\Casters\StringCaster;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class Cast
{
    /**
     * @param mixed $value
     * @return int|ValueProviderInterface
     * @throws CastException
     */
    public static function int($value)
    {
        return static::cast($value,  new IntCaster());
    }

    /**
     * @param mixed $value
     * @return float|ValueProviderInterface
     * @throws CastException
     */
    public static function float($value)
    {
        return static::cast($value,  new FloatCaster());
    }

    /**
     * @param mixed $value
     * @return bool|ValueProviderInterface
     * @throws CastException
     */
    public static function bool($value)
    {
        return static::cast($value,  new BoolCaster());
    }

    /**
     * @param mixed $value
     * @return string|ValueProviderInterface
     * @throws CastException
     */
    public static function string($value)
    {
        return static::cast($value,  new StringCaster());
    }

    /**
     * @param mixed $value
     * @param CasterInterface $caster
     * @return mixed|ValueProviderInterface
     */
    private static function cast($value, CasterInterface $caster)
    {
        if ($value === null){
            return $value;
        }

        if (!is_scalar($value)) {
            throw new CastException('Only scalar types can be converted through the cast methods');
        }

        return $caster->cast($value);
    }
} 