<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Interfaces;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSArticleEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCategoryEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCourseEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTagEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTestEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSUserEntity;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface CollectorClientInterface
{
    /**
     * @param int $guestId
     * @param int $userId
     * @return void
     */
    public function attachGuestIdToUserId($guestId, $userId);

    /**
     * @param RSTagEntity $entity
     * @return RSTagEntity
     */
    public function tagAdd(RSTagEntity $entity);

    /**
     * @param int $id
     * @param RSTagEntity $entity
     * @return RSTagEntity
     */
    public function tagUpdate($id, RSTagEntity $entity);

    /**
     * @param int $id
     */
    public function tagDelete($id);

    /**
     * @param RSCategoryEntity $entity
     * @return RSCategoryEntity
     */
    public function categoryAdd(RSCategoryEntity $entity);

    /**
     * @param int $id
     * @param RSCategoryEntity $entity
     * @return RSCategoryEntity
     */
    public function categoryUpdate($id, $entity);
    
    /**
     * @param $categoryId
     * @param Collection $tagIds
     * @return mixed
     */
    public function categoryTagsUpdate($categoryId, Collection $tagIds);

    /**
     * @param int $id
     */
    public function categoryDelete($id);

    /**
     * @param $id
     * @param Collection $contentIds
     * @return mixed
     */
    public function articleRelatedContentUpdate($id, Collection $contentIds);

    /**
     * @param RSUserEntity $entity
     * @return RSUserEntity
     */
    public function userAdd(RSUserEntity $entity);

    /**
     * @param int $id
     * @param RSUserEntity $entity
     * @return RSUserEntity
     */
    public function userUpdate($id, RSUserEntity $entity);

    /**
     * @param int $id
     */
    public function userDelete($id);

    /**
     * @param RSArticleEntity $entity
     * @return RSArticleEntity
     */
    public function articleAdd(RSArticleEntity $entity);

    /**
     * @param int $id
     * @param RSArticleEntity $entity
     * @return RSArticleEntity
     */
    public function articleUpdate($id, RSArticleEntity $entity);

    /**
     * @param int $id
     */
    public function articleDelete($id);

    /**
     * @param RSTestEntity $entity
     * @return RSTestEntity
     */
    public function testAdd(RSTestEntity $entity);

    /**
     * @param int $id
     * @param RSTestEntity $entity
     * @return RSTestEntity
     */
    public function testUpdate($id, RSTestEntity $entity);

    /**
     * @param int $id
     */
    public function testDelete($id);

    /**
     * @param RSCourseEntity $entity
     * @return RSCourseEntity
     */
    public function courseAdd(RSCourseEntity $entity);

    /**
     * @param int $id
     * @param RSCourseEntity $entity
     * @return RSCourseEntity
     */
    public function courseUpdate($id, RSCourseEntity $entity);

    /**
     * @param int $id
     */
    public function courseDelete($id);
}
