<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Interfaces;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCustomer;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Option\RSGetContentOption;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Option\RSGetInterestsOption;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface RecommenderClientInterface
{
    /**
     * @param RSGetContentOption $option
     * @param RSCustomer $customer
     * @return Collection
     */
    public function getContent(RSGetContentOption $option, RSCustomer $customer);

    /**
     * @param RSGetInterestsOption $option
     * @param RSCustomer $customer
     * @return Collection
     */
    public function getInterests(RSGetInterestsOption $option, RSCustomer $customer);
}