<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Specification\ObjectType;

use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSCategoryObjectType extends ObjectType
{
    /**
     * @return array
     */
    public function structure()
    {
        return [
            'id' => new IntegerType(false, true),
            'name' => new StringType(),
        ];
    }
}
