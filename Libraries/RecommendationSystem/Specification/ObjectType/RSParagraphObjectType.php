<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Specification\ObjectType;

use Mildberry\Kangaroo\Libraries\Specification\Types\Collection\CollectionType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSParagraphObjectType extends ObjectType
{
    /**
     * @return array
     */
    public function structure()
    {
        return [
            'id' => new IntegerType(),
            'tags' => new CollectionType(function () {
                return new RSTagObjectType();
            }),
        ];
    }
}
