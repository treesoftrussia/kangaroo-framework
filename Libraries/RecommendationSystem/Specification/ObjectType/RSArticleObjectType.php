<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Specification\ObjectType;

use Mildberry\Kangaroo\Libraries\Specification\Types\Collection\CollectionType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSArticleObjectType extends ObjectType
{
    /**
     * @return array
     */
    public function structure()
    {
        return [
            'id' => new IntegerType(false, true),
            'title' => new StringType(),
            'isChapterOfCourse' => new IntegerType(),
            'description' => new StringType(true),
            'author' => new StringType(true),
            'createdAt' => new IntegerType(),
            'paragraphs' => new CollectionType(function () {
                return new RSParagraphObjectType();
            }, true, true),
        ];
    }
}
