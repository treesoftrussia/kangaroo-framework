<?php
namespace Mildberry\Kangaroo\Libraries\RecommendationSystem;

use Illuminate\Contracts\Container\Container;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Interfaces\CollectorClientInterface;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Interfaces\RecommenderClientInterface;

/**
 * @author Andrey Vorobiov <andrew.sprw@gmail.com>
 */
class RecommendationSystemManager
{

    /**
     * @var Container
     */
    protected $container;

    /**
     * RecommendationSystemManager constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function recommender(){
        return $this->container->make(RecommenderClientInterface::class);
    }

    public function collector(){
        return $this->container->make(CollectorClientInterface::class);
    }
}