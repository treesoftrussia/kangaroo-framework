<?php
namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Facades;

use Illuminate\Support\Facades\Facade as AbstractFacade;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemManager;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class RecommendationSystem extends AbstractFacade
{
    protected static function getFacadeAccessor()
    {
        return RecommendationSystemManager::class;
    }
} 