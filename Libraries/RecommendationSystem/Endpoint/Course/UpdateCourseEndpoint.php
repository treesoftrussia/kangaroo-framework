<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Course;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCourseEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\RSCourseFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class UpdateCourseEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_PUT;

    /**
     * AddCourse constructor.
     * @param int $id
     * @param RSCourseEntity $entity
     */
    public function __construct($id, RSCourseEntity $entity)
    {
        $this
            ->setUri('/course/'.$id)
            ->setRequestData((new RSCourseFlattenAdapter())->transform($entity))
        ;
    }
}
