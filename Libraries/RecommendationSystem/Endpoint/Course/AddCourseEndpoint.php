<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Course;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCourseEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\RSCourseFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class AddCourseEndpoint extends RecommendationSystemEndpoint
{
    protected $validResponseStatusCode = Response::HTTP_CREATED;

    protected $method = Request::METHOD_POST;

    /**
     * AddCourse constructor.
     * @param RSCourseEntity $entity
     */
    public function __construct(RSCourseEntity $entity)
    {
        $this
            ->setUri('/course')
            ->setRequestData((new RSCourseFlattenAdapter())->transform($entity))
        ;
    }
}
