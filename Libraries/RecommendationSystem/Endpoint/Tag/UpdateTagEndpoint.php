<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Tag;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTagEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\RSTagFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class UpdateTagEndpoint extends RecommendationSystemEndpoint
{
    protected $method = 'PUT';

    /**
     * UpdateTag constructor.
     * @param int $id
     * @param RSTagEntity $entity
     */
    public function __construct($id, RSTagEntity $entity)
    {
        $this
            ->setUri('/tag/'.$id)
            ->setRequestData((new RSTagFlattenAdapter())->transform($entity))
        ;
    }
}
