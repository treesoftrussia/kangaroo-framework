<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Tag;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTagEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\RSTagFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class AddTagEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_POST;

    protected $validResponseStatusCode = Response::HTTP_CREATED;

    /**
     * AddTagEndpoint constructor.
     * @param RSTagEntity $entity
     */
    public function __construct(RSTagEntity $entity)
    {
        $this
            ->setUri('/tag')
            ->setRequestData((new RSTagFlattenAdapter())->transform($entity))
        ;
    }
}
