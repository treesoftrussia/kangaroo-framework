<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\User;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCustomer;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\Options\RSGetInterestsFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Option\RSGetInterestsOption;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class GetUserInterestsEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_GET;

    protected $validResponseStatusCode = Response::HTTP_OK;

    /**
     * @param RSGetInterestsOption $option
     * @param RSCustomer $customer
     */
    public function __construct(RSGetInterestsOption $option, RSCustomer $customer)
    {
        $this
            ->setUri('/user/'.$customer->getId().'/interest')
            ->setRequestOptions((new RSGetInterestsFlattenAdapter())->transform($option))
        ;
    }
}
