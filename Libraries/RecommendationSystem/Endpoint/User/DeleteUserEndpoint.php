<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\User;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class DeleteUserEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_DELETE;

    protected $validResponseStatusCode = Response::HTTP_NO_CONTENT;

    /**
     * DeleteUser constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        $this->setUri('/user/'.$id);
    }
}
