<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\User;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSAttachUserGuestIdEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\RSAttachUserGuestIdFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class AttachUserGuestIdEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_POST;

    protected $validResponseStatusCode = Response::HTTP_OK;

    /**
     * @param RSAttachUserGuestIdEntity $entity
     */
    public function __construct(RSAttachUserGuestIdEntity $entity)
    {
        $this
            ->setUri('/user/'.$entity->getUserId().'/guest-id')
            ->setRequestData((new RSAttachUserGuestIdFlattenAdapter())->transform($entity))
        ;
    }
}
