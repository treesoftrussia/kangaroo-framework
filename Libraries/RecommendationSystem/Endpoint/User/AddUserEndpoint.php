<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\User;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSUserEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\RSUserFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class AddUserEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_POST;

    protected $validResponseStatusCode = Response::HTTP_CREATED;

    /**
     * AddUserEndpoint constructor.
     * @param RSUserEntity $entity
     */
    public function __construct(RSUserEntity $entity)
    {
        $this
            ->setUri('/user')
            ->setRequestData((new RSUserFlattenAdapter())->transform($entity))
        ;
    }
}
