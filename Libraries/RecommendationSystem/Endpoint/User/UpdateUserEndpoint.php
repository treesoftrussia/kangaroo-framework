<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\User;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSUserEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\RSUserFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class UpdateUserEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_PATCH;

    /**
     * UpdateUser constructor.
     * @param int $id
     * @param RSUserEntity $entity
     */
    public function __construct($id, RSUserEntity $entity)
    {
        $this
            ->setUri('/user/'.$id)
            ->setRequestData((new RSUserFlattenAdapter())->transform($entity))
        ;
    }
}
