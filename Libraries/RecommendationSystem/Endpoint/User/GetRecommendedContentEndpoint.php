<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\User;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCustomer;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Option\RSGetContentOption;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\Options\RSGetContentFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class GetRecommendedContentEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_GET;

    protected $validResponseStatusCode = Response::HTTP_OK;

    /**
     * @param RSGetContentOption $option
     * @param RSCustomer $customer
     */
    public function __construct(RSGetContentOption $option, RSCustomer $customer)
    {
        $this
            ->setUri('/'.(($customer->isGuest()) ? 'guest' : 'user').'/'.$customer->getId().'/recommendation')
            ->setRequestOptions((new RSGetContentFlattenAdapter())->transform($option))
        ;
    }
}
