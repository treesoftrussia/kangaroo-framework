<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Article;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSArticleEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\RSArticleFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class AddArticleEndpoint extends RecommendationSystemEndpoint
{
    protected $validResponseStatusCode = Response::HTTP_CREATED;

    protected $method = Request::METHOD_POST;

    /**
     * AddArticle constructor.
     * @param RSArticleEntity $entity
     */
    public function __construct(RSArticleEntity $entity)
    {
        $this
            ->setUri('/article')
            ->setRequestData((new RSArticleFlattenAdapter())->transform($entity))
        ;
    }
}
