<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Article;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSArticleEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\RSArticleFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class UpdateArticleEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_PUT;

    /**
     * AddArticle constructor.
     * @param int $id
     * @param RSArticleEntity $entity
     */
    public function __construct($id, RSArticleEntity $entity)
    {
        $this
            ->setUri('/article/'.$id)
            ->setRequestData((new RSArticleFlattenAdapter())->transform($entity))
        ;
    }
}
