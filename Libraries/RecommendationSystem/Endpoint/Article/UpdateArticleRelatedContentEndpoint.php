<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Article;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class UpdateArticleRelatedContentEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_PUT;

    /**
     * UpdateArticleRelatedContentEndpoint constructor.
     * @param $id
     * @param Collection $contentIds
     */
    public function __construct($id, Collection $contentIds)
    {
        $this
            ->setUri('/content/'.$id.'/related-content')
            ->setRequestData($contentIds->toArray())
        ;
    }
}
