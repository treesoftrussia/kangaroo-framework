<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Statistic;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\Options\RSClickByDayOptionFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Option\RSClickByDayStatisticOption;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Andrew Sparrow <a.vorobyev@mildberry.com>
 */
class GetClickByDayStatisticEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_GET;

    protected $validResponseStatusCode = Response::HTTP_OK;

    /**
     * GetClickByDayStatisticEndpoint constructor.
     * @param RSClickByDayStatisticOption $options
     */
    public function __construct(RSClickByDayStatisticOption $options)
    {
        $this
            ->setUri('/statistic/click-by-day')
            ->setRequestOptions((new RSClickByDayOptionFlattenAdapter())->transform($options))
        ;
    }
}
