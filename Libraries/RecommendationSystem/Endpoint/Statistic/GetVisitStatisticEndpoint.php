<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Statistic;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Andrew Sparrow <a.vorobyev@mildberry.com>
 */
class GetVisitStatisticEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_GET;

    protected $validResponseStatusCode = Response::HTTP_OK;

    /**
     * GetLocationStatisticEndpoint constructor.
     */
    public function __construct()
    {
        $this
            ->setUri('/statistic/visit')
        ;
    }
}
