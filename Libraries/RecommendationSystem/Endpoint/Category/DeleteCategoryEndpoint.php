<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Category;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class DeleteCategoryEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_DELETE;

    protected $validResponseStatusCode = Response::HTTP_NO_CONTENT;

    /**
     * DeleteCategory constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        $this->setUri('/category/'.$id);
    }
}
