<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Category;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCategoryEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\RSCategoryFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class AddCategoryEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_POST;

    protected $validResponseStatusCode = Response::HTTP_CREATED;

    /**
     * AddCategoryEndpoint constructor.
     * @param RSCategoryEntity $entity
     */
    public function __construct(RSCategoryEntity $entity)
    {
        $this
            ->setUri('/category')
            ->setRequestData((new RSCategoryFlattenAdapter())->transform($entity))
        ;
    }
}
