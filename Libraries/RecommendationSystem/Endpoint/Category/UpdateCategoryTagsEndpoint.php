<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Category;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class UpdateCategoryTagsEndpoint extends RecommendationSystemEndpoint
{
    protected $method = 'PUT';

    /**
     * UpdateCategoryTagsEndpoint constructor.
     * @param $categoryId
     * @param Collection $tags
     */
    public function __construct($categoryId, Collection $tags)
    {
        $this
            ->setUri('/category/'.$categoryId.'/tags')
            ->setRequestData($tags->toArray())
        ;
    }
}
