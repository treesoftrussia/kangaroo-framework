<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Category;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCategoryEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\RSCategoryFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class UpdateCategoryEndpoint extends RecommendationSystemEndpoint
{
    protected $method = 'PUT';

    /**
     * UpdateCategory constructor.
     * @param int $tagId
     * @param RSCategoryEntity $entity
     */
    public function __construct($tagId, RSCategoryEntity $entity)
    {
        $this
            ->setUri('/category/'.$tagId)
            ->setRequestData((new RSCategoryFlattenAdapter())->transform($entity))
        ;
    }
}
