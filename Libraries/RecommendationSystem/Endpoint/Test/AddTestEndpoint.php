<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Test;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTestEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\RSTestFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class AddTestEndpoint extends RecommendationSystemEndpoint
{
    protected $validResponseStatusCode = Response::HTTP_CREATED;

    protected $method = Request::METHOD_POST;

    /**
     * AddTest constructor.
     * @param RSTestEntity $entity
     */
    public function __construct(RSTestEntity $entity)
    {
        $this
            ->setUri('/test')
            ->setRequestData((new RSTestFlattenAdapter())->transform($entity))
        ;
    }
}
