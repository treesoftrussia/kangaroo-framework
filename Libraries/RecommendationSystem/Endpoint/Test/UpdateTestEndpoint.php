<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Test;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTestEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\RSTestFlattenAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class UpdateTestEndpoint extends RecommendationSystemEndpoint
{
    protected $method = Request::METHOD_PUT;

    /**
     * AddTest constructor.
     * @param int $id
     * @param RSTestEntity $entity
     */
    public function __construct($id, RSTestEntity $entity)
    {
        $this
            ->setUri('/test/'.$id)
            ->setRequestData((new RSTestFlattenAdapter())->transform($entity))
        ;
    }
}
