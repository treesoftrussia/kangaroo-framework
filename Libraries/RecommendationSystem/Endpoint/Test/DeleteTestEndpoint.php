<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Test;

use Mildberry\Kangaroo\Libraries\RecommendationSystem\RecommendationSystemEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class DeleteTestEndpoint extends RecommendationSystemEndpoint
{
    protected $validResponseStatusCode = Response::HTTP_NO_CONTENT;

    protected $method = Request::METHOD_DELETE;

    /**
     * AddTest constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        $this
            ->setUri('/test/'.$id)
        ;
    }
}
