<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem;

use Illuminate\Log\Writer;
use Illuminate\Support\ServiceProvider as Provider;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Interfaces\CollectorClientInterface;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Interfaces\RecommenderClientInterface;
use Monolog\Logger;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class ServiceProvider extends Provider
{
    public function register()
    {
        $this->app->singleton(RecommenderClientInterface::class, function () {
            $options = [
                'host' => $this->app['config']->get('params.recommendationSystem.host'),
                'port' => $this->app['config']->get('params.recommendationSystem.port'),
                'protocol' => $this->app['config']->get('params.recommendationSystem.protocol'),
            ];

            return new RecommenderClient($options, $this->createRSLogger($this->app['config']->get('params.recommendationSystem.logFile'), $this->app['config']->get('params.recommendationSystem.logLevel')));
        });

        //Recommendation System
        if (!$this->app['config']->get('params.recommendationSystem.useCollectorClientMock')) {
            $app = $this->app;
            $this->app->singleton(CollectorClientInterface::class, function () use ($app) {
                $object = new CollectorClient([
                    'host' => $this->app['config']->get('params.recommendationSystem.host'),
                    'port' => $this->app['config']->get('params.recommendationSystem.port'),
                    'protocol' => $this->app['config']->get('params.recommendationSystem.protocol'),
                ], $this->createRSLogger($this->app['config']->get('params.recommendationSystem.logFile'), $this->app['config']->get('params.recommendationSystem.logLevel')));

                //$object->setLogger();

                return $object;
            });
        }
    }

    public function boot()
    {
    }

    /**
     * @param string $file
     * @param string $level
     * @return Writer
     */
    private function createRSLogger($file, $level)
    {
        $writer = new Writer(new Logger('Recommendation system'));
        $writer->useFiles($file, $level);

        return $writer;
    }
}