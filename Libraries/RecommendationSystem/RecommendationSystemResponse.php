<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem;

use Psr\Http\Message\ResponseInterface;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RecommendationSystemResponse
{
    /**
     * @var int
     */
    private $responseStatusCode;

    /**
     * @var object
     */
    private $data;

    /**
     * @var int
     */
    private $errorCode;

    /**
     * @var array
     */
    private $errorData;

    /**
     * @param ResponseInterface $response
     * @return static
     */
    public static function makeFromResponse(ResponseInterface $response)
    {
        /** @var RecommendationSystemResponse $entity */
        $entity = new static();

        $data = json_decode($response->getBody());

        $entity
            ->setResponseStatusCode($response->getStatusCode())
            ->setData($data)
            ->setErrorCode(!empty($data->errorCode) ? $data->errorCode : 0)
            ->setErrorData(!empty($data->errorData) ? (is_array($data->errorData) ? $data->errorData : [$data->errorData]) : [])
        ;

        return $entity;
    }

    /**
     * @return int
     */
    public function getResponseStatusCode()
    {
        return $this->responseStatusCode;
    }

    /**
     * @param int $responseStatusCode
     *
     * @return $this
     */
    public function setResponseStatusCode($responseStatusCode)
    {
        $this->responseStatusCode = $responseStatusCode;

        return $this;
    }

    /**
     * @return int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param int $errorCode
     *
     * @return $this
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    /**
     * @return array
     */
    public function getErrorData()
    {
        return $this->errorData;
    }

    /**
     * @param array $errorData
     *
     * @return $this
     */
    public function setErrorData($errorData)
    {
        $this->errorData = $errorData;

        return $this;
    }

    /**
     * @return object
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param object $data
     *
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }
}
