<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Log\Writer;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Exceptions\InvalidRequestDataValidationException;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Exceptions\InvalidRequestMethodException;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Exceptions\InvalidResponseDataValidationException;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Exceptions\InvalidResponseStatusCodeException;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RecommendationSystemClient
{
    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * @var string
     */
    private $recommendationSystemHost;

    /**
     * @var int
     */
    private $recommendationSystemPort;

    /**
     * @var string
     */
    private $protocol;

    /**
     * @param array $options
     * @param Writer $logger
     */
    public function __construct($options = [], Writer $logger)
    {
        $this->setOptions($options);

        $this->httpClient = new Client($this->options['guzzleOptions']);

        $this->recommendationSystemHost = $this->options['host'];
        $this->recommendationSystemPort = $this->options['port'];
        $this->protocol = $this->options['protocol'];

        $this->logger = $logger;
    }

    /**
     * @param RecommendationSystemEndpoint $endpoint
     * @return RecommendationSystemResponse
     * @throws Exception
     */
    public function makeRequestToEndpoint(RecommendationSystemEndpoint $endpoint)
    {
        $requestData = $endpoint->getRequestData();

        $this->logger->debug('Making request '.$endpoint->getMethod().' to the '.$endpoint->getUri()."\n".print_r($requestData, true));

        //TODO: костыль
        if($endpoint->getMethod() == 'PUT'){
            unset($requestData['id']);
        }

        try {
            if (!in_array($endpoint->getMethod(), ['GET', 'DELETE', 'POST', 'PUT', 'PATCH'])) {
                throw new InvalidRequestMethodException('Endpoint request method "'.$endpoint->getMethod().'" is not valid!');
            }

            if ($endpoint->getRequestSpecification() && !$this->isValidDataBySpecification($requestData, $endpoint->getRequestSpecification())) {
                throw new InvalidRequestDataValidationException();
            }

            try {
                $response = RecommendationSystemResponse::makeFromResponse($this->httpClient->request($endpoint->getMethod(), $this->createURLFromEndpoint($endpoint), ['json' => $requestData]));
            } catch (ClientException $e) {
                $response = RecommendationSystemResponse::makeFromResponse($e->getResponse());
            }

            if ($response->getResponseStatusCode() <> $endpoint->getValidResponseStatusCode()) {
                throw new InvalidResponseStatusCodeException('Expected response status code '.$endpoint->getValidResponseStatusCode().', actual '.$response->getResponseStatusCode().'\n'
                . print_r($response->getData(), true));
            }

            if ($endpoint->getResponseSpecification() && !$this->isValidDataBySpecification($response->getData(), $endpoint->getResponseSpecification())) {
                throw new InvalidResponseDataValidationException();
            }
        } catch (Exception $e){
            $this->logger->error($e->getMessage());
            throw $e;
        }

        $this->logger->debug('Success! Response status is '.$response->getResponseStatusCode()."\n".print_r($response->getData(), true));

        return $response;
    }

    /**
     * @param array $data
     * @param string $specificationClass
     * @return bool
     */
    private function isValidDataBySpecification($data, $specificationClass)
    {
        //TODO: сделать проверку спецификации
        return true;
    }

    /**
     * @param RecommendationSystemEndpoint $endpoint
     * @return string
     */
    private function createURLFromEndpoint(RecommendationSystemEndpoint $endpoint)
    {
        return $this->protocol.'://'.$this->recommendationSystemHost.':'.$this->recommendationSystemPort.$endpoint->getUri().(($endpoint->getRequestOptions()) ? '?'.http_build_query($endpoint->getRequestOptions()) : '');
    }

    /**
     * @param array $options
     */
    private function setOptions($options = [])
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'demo' => true,
            'host' => 'localhost',
            'port' => 80,
            'protocol' => 'http',
            'guzzleOptions' => [],
        ]);

        $this->options = $resolver->resolve($options);
    }
}
