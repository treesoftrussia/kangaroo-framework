<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter\RSArticleAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter\RSAttachUserGuestIdAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter\RSCategoryAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter\RSCourseAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter\RSTagAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter\RSTestAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter\RSUserAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Article\AddArticleEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Article\DeleteArticleEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Article\UpdateArticleEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Article\UpdateArticleRelatedContentEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Category\AddCategoryEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Category\DeleteCategoryEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Category\UpdateCategoryEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Category\UpdateCategoryTagsEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Course\AddCourseEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Course\DeleteCourseEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Course\UpdateCourseEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Tag\AddTagEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Tag\DeleteTagEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Tag\UpdateTagEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Test\AddTestEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Test\DeleteTestEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Test\UpdateTestEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\User\AddUserEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\User\AttachUserGuestIdEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\User\DeleteUserEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\User\UpdateUserEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSArticleEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSAttachUserGuestIdEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCategoryEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCourseEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTagEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTestEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSUserEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Interfaces\CollectorClientInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class CollectorClient extends RecommendationSystemClient implements CollectorClientInterface
{
    /**
     * @param int $guestId
     * @param int $userId
     * @return RSAttachUserGuestIdEntity
     */
    public function attachGuestIdToUserId($guestId, $userId)
    {
        $entity = (new RSAttachUserGuestIdEntity())
            ->setGuestId($guestId)
            ->setUserId($userId)
        ;

        return (new RSAttachUserGuestIdAdapter())->transform($this->makeRequestToEndpoint(new AttachUserGuestIdEndpoint($entity))->getData());
    }

    /**
     * @param RSTagEntity $entity
     * @return RSTagEntity
     */
    public function tagAdd(RSTagEntity $entity)
    {
        return (new RSTagAdapter())->transform($this->makeRequestToEndpoint(new AddTagEndpoint($entity))->getData());
    }

    /**
     * @param int $id
     * @param RSTagEntity $entity
     * @return RSTagEntity
     */
    public function tagUpdate($id, RSTagEntity $entity)
    {
        return (new RSTagAdapter())->transform($this->makeRequestToEndpoint(new UpdateTagEndpoint($id, $entity))->getData());
    }

    /**
     * @param int $id
     */
    public function tagDelete($id)
    {
        $this->makeRequestToEndpoint(new DeleteTagEndpoint($id));
    }

    /**
     * @param RSCategoryEntity $entity
     * @return RSCategoryEntity
     */
    public function categoryAdd(RSCategoryEntity $entity)
    {
        return (new RSCategoryAdapter())->transform($this->makeRequestToEndpoint(new AddCategoryEndpoint($entity))->getData());
    }

    /**
     * @param int $id
     * @param RSCategoryEntity $entity
     * @return RSCategoryEntity
     */
    public function categoryUpdate($id, $entity)
    {
        return (new RSCategoryAdapter())->transform($this->makeRequestToEndpoint(new UpdateCategoryEndpoint($id, $entity))->getData());
    }

    /**
     * @param $categoryId
     * @param Collection $tagIds
     * @return RSCategoryEntity
     */
    public function categoryTagsUpdate($categoryId, Collection $tagIds)
    {
        return $this->makeRequestToEndpoint(new UpdateCategoryTagsEndpoint($categoryId, $tagIds))->getData();
    }

    /**
     * @param int $id
     */
    public function categoryDelete($id)
    {
        $this->makeRequestToEndpoint(new DeleteCategoryEndpoint($id));
    }

    /**
     * @param RSUserEntity $entity
     * @return RSUserEntity
     */
    public function userAdd(RSUserEntity $entity)
    {
        return (new RSUserAdapter())->transform($this->makeRequestToEndpoint(new AddUserEndpoint($entity))->getData());
    }

    /**
     * @param int $id
     * @param RSUserEntity $entity
     * @return RSUserEntity
     */
    public function userUpdate($id, RSUserEntity $entity)
    {
        return (new RSUserAdapter())->transform($this->makeRequestToEndpoint(new UpdateUserEndpoint($id, $entity))->getData());
    }

    /**
     * @param int $id
     */
    public function userDelete($id)
    {
        $this->makeRequestToEndpoint(new DeleteUserEndpoint($id));
    }

    /**
     * @param RSArticleEntity $entity
     * @return RSArticleEntity
     */
    public function articleAdd(RSArticleEntity $entity)
    {
        return (new RSArticleAdapter())->transform($this->makeRequestToEndpoint(new AddArticleEndpoint($entity))->getData());
    }

    /**
     * @param int $id
     * @param RSArticleEntity $entity
     * @return RSArticleEntity
     */
    public function articleUpdate($id, RSArticleEntity $entity)
    {
        return (new RSArticleAdapter())->transform($this->makeRequestToEndpoint(new UpdateArticleEndpoint($id, $entity))->getData());
    }

    /**
     * @param $id
     * @param Collection $contentIds
     */
    public function articleRelatedContentUpdate($id, Collection $contentIds){
        return $this->makeRequestToEndpoint(new UpdateArticleRelatedContentEndpoint($id, $contentIds))->getData();
    }

    /**
     * @param int $id
     */
    public function articleDelete($id)
    {
        $this->makeRequestToEndpoint(new DeleteArticleEndpoint($id));
    }

    /**
     * @param RSTestEntity $entity
     * @return RSTestEntity
     */
    public function testAdd(RSTestEntity $entity)
    {
        return (new RSTestAdapter())->transform($this->makeRequestToEndpoint(new AddTestEndpoint($entity))->getData());
    }

    /**
     * @param int $id
     * @param RSTestEntity $entity
     * @return RSTestEntity
     */
    public function testUpdate($id, RSTestEntity $entity)
    {
        return (new RSTestAdapter())->transform($this->makeRequestToEndpoint(new UpdateTestEndpoint($id, $entity))->getData());
    }

    /**
     * @param int $id
     */
    public function testDelete($id)
    {
        $this->makeRequestToEndpoint(new DeleteTestEndpoint($id));
    }

    /**
     * @param RSCourseEntity $entity
     * @return RSCourseEntity
     */
    public function courseAdd(RSCourseEntity $entity)
    {
        return (new RSCourseAdapter())->transform($this->makeRequestToEndpoint(new AddCourseEndpoint($entity))->getData());
    }

    /**
     * @param int $id
     * @param RSCourseEntity $entity
     * @return RSCourseEntity
     */
    public function courseUpdate($id, RSCourseEntity $entity)
    {
        return (new RSCourseAdapter())->transform($this->makeRequestToEndpoint(new UpdateCourseEndpoint($id, $entity))->getData());
    }

    /**
     * @param int $id
     */
    public function courseDelete($id)
    {
        $this->makeRequestToEndpoint(new DeleteCourseEndpoint($id));
    }
}
