<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSParagraphEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTagEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSParagraphFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSParagraphEntity $paragraphEntity
     *
     * @return array
     */
    public function transform($paragraphEntity = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return [
            'id' => $sanitizer($paragraphEntity->getId(), 'integer'),
            'tags' => $this->getTagsArray($paragraphEntity->getTags()),
        ];
    }

    private function getTagsArray(Collection $tags)
    {
        $return = [];

        /** @var RSTagEntity $tag */
        foreach ($tags as $tag) {
            $return[] = $tag->getId();
        }

        return $return;
    }

    /**
     * @param Collection $models
     * @return Collection
     */
    public function transformFromCollection(Collection $models)
    {
        $result = array();
        foreach ($models as $model) {
            $result[] = $this->transform($model);
        }

        return new Collection($result);
    }
}
