<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSSocialEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSUserEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSUserFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSUserEntity $user
     *
     * @return array
     */
    public function transform(RSUserEntity $user)
    {
        $sanitizer = $this->sanitizer()->make();

        return [
                'id' => $sanitizer($user->getId(), 'integer'),
                'username' => $sanitizer($user->getUsername(), 'string'),
                'birthday' => $sanitizer($user->getBirthday(), 'string'),
                'childBirthday' => $sanitizer($user->getChildBirthday(), 'string'),
                'gender' => $sanitizer($user->getGender(), 'integer'),
                'isAvatarExists' => $sanitizer($user->isAvatarExists(), 'integer'),
                'social' => $this->getSocialArray($user->getSocial())
            ];
    }

    /**
     * @param Collection $tags
     * @return array
     */
    private function getSocialArray($socials = null)
    {
        if (!isset($socials)) return null;
        $return = [];

        /** @var RSSocialEntity $social */
        foreach ($socials as $social) {
            $return[] = ['type' => $social->getType(), 'internalId' => $social->getInternalId()];
        }

        return $return;
    }
}
