<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSLocationEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSLocationFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSLocationEntity $locationEntity
     *
     * @return array|null
     */
    public function transform($locationEntity = null)
    {
        if (is_null($locationEntity))
        {
            return null;
        }

        $sanitizer = $this->sanitizer()->make();

        return [
            'address' => $sanitizer($locationEntity->getAddress(), 'string'),
            'coordinates' => ((new RSCoordinatesFlattenAdapter())->transform($locationEntity->getCoordinates())),
        ];
    }
}
