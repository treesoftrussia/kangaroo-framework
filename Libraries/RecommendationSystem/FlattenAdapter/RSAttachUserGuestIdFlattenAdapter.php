<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSAttachUserGuestIdEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSAttachUserGuestIdFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSAttachUserGuestIdEntity $entity
     *
     * @return array
     */
    public function transform(RSAttachUserGuestIdEntity $entity)
    {
        $sanitizer = $this->sanitizer()->make();

        return [
                'guestId' => $sanitizer($entity->getGuestId(), 'string'),
            ];
    }
}
