<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\Options;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Option\RSGetContentOption;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSGetContentFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSGetContentOption $entity
     *
     * @return array
     */
    public function transform(RSGetContentOption $entity = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return [
            'relatedContentId' => $sanitizer($entity->getRelatedContentId(), 'integer'),
            'typeIds' => $sanitizer(implode(',', $entity->getTypes()), 'string'),
            'groupId' => $sanitizer($entity->getGroupId(), 'integer'),
            'categoryId' => $sanitizer($entity->getCategoryId(), 'integer'),
            'tagId' => $sanitizer($entity->getTagId(), 'integer'),
            'itemsNumber' => $sanitizer($entity->getItemsNumber(), 'integer'),
            'screenTypeId' => $sanitizer($entity->getScreenTypeId(), 'integer'),
            'zoneTypeId' => $sanitizer($entity->getZoneTypeId(), 'integer'),
            'sequenceToken' => $sanitizer($entity->getSequenceToken(), 'string'),
        ];
    }
}
