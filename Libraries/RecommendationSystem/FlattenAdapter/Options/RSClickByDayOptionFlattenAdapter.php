<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\Options;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Option\RSClickByDayStatisticOption;

/**
 * @author Andrew Sparrow <a.vorobyev@mildberry.com>
 */
class RSClickByDayOptionFlattenAdapter extends AbstractAdapter
{

    /**
     * @param RSClickByDayStatisticOption|null $entity
     * @return array
     */
    public function transform(RSClickByDayStatisticOption $entity = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return [
            'from' => $sanitizer($entity->getFrom(), 'string'),
            'to' => $sanitizer($entity->getTo(), 'string')
        ];
    }
}
