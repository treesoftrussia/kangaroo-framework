<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter\Options;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Option\RSGetInterestsOption;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSGetInterestsFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSGetInterestsOption $entity
     *
     * @return array
     */
    public function transform(RSGetInterestsOption $entity = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return [
            'itemsNumber' => $sanitizer($entity->getItemsNumber(), 'integer'),
            'sequenceToken' => $sanitizer($entity->getSequenceToken(), 'string'),
        ];
    }
}
