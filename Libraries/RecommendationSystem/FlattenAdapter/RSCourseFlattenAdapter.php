<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCourseEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTagEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSCourseFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSCourseEntity $courseEntity
     *
     * @return array
     */
    public function transform($courseEntity = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return [
            'id' => $sanitizer($courseEntity->getId(), 'integer'),
            'title' => $sanitizer($courseEntity->getTitle(), 'string'),
            'groupId' => $sanitizer($courseEntity->getGroupId(), 'integer'),
            'isContentCompleted' => $sanitizer($courseEntity->isContentCompleted(), 'bool'),
            'location' => $courseEntity->getLocation() ? ((new RSLocationFlattenAdapter())->transform($courseEntity->getLocation())) : null,
            'chapters' => ((new RSChapterFlattenAdapter())->transformFromCollection($courseEntity->getChapters())),
            'isPublished' => $sanitizer($courseEntity->isPublished(), 'bool'),
            'createdAt' => $sanitizer($courseEntity->getCreatedAt(), 'integer'),
            'tags' => $this->getTagsArray($courseEntity->getTags())
        ];
    }

    /**
     * @param Collection $tags
     * @return array
     */
    private function getTagsArray(Collection $tags)
    {
        $return = [];

        /** @var RSTagEntity $tag */
        foreach ($tags as $tag) {
            $return[] = $tag->getId();
        }

        return $return;
    }
}
