<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSSocialEntity;

/**
 * @author Evgeny Novoselov <e.novosyolov@mildberry.com>
 */
class RSSocialFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSSocialEntity $socialEntity
     *
     * @return array
     */
    public function transform($socialEntity = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return [
            'type' => $sanitizer($socialEntity->getType(), 'string'),
            'internalId' => $sanitizer($socialEntity->getInternalId(), 'string'),
        ];
    }
}
