<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTagEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTestEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSTestFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSTestEntity $testEntity
     *
     * @return array
     */
    public function transform($testEntity = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return [
            'id' => $sanitizer($testEntity->getId(), 'integer'),
            'title' => $sanitizer($testEntity->getTitle(), 'string'),
            'isChapterOfCourse' => $sanitizer($testEntity->isChapterOfCourse(), 'integer'),
            'description' => $sanitizer($testEntity->getDescription(), 'string'),
            'author' => $sanitizer($testEntity->getAuthor(), 'string'),
            'isPublished' => $sanitizer($testEntity->isPublished(), 'bool'),
            'createdAt' => $sanitizer($testEntity->getCreatedAt(), 'integer'),
            'tags' => $this->getTagsArray($testEntity->getTags())
        ];
    }

    /**
     * @param Collection $tags
     * @return array
     */
    private function getTagsArray(Collection $tags)
    {
        $return = [];

        /** @var RSTagEntity $tag */
        foreach ($tags as $tag) {
            $return[] = $tag->getId();
        }

        return $return;
    }
}
