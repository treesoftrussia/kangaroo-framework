<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCategoryEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTagEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSTagFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSTagEntity $tagEntity
     *
     * @return array
     */
    public function transform($tagEntity = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return [
            'id' => $sanitizer($tagEntity->getId(), 'integer'),
            'name' => $sanitizer($tagEntity->getName(), 'string'),
            'categories' => $this->getCategoriesArray($tagEntity->getCategories())
        ];
    }

    /**
     * @param Collection $categories
     * @return array
     */
    private function getCategoriesArray($categories)
    {
        if (is_null($categories)) return null;
        $return = [];

        /** @var RSCategoryEntity $tag */
        foreach ($categories as $category) {
            $return[] = $category->getId();
        }

        return $return;
    }
    /**
     * @param Collection $models
     * @return Collection
     */
    public function transformFromCollection(Collection $models)
    {
        $result = array();
        foreach ($models as $model) {
            $result[] = $this->transform($model);
        }

        return new Collection($result);
    }
}
