<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSChapterEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSChapterFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSChapterEntity $chapterEntity
     *
     * @return array
     */
    public function transform($chapterEntity = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return [
            'id' => $sanitizer($chapterEntity->getId(), 'integer'),
            'type' => $sanitizer($chapterEntity->getType(), 'integer'),
        ];
    }

    /**
     * @param Collection $models
     * @return Collection
     */
    public function transformFromCollection(Collection $models)
    {
        $result = array();
        foreach ($models as $model) {
            $result[] = $this->transform($model);
        }

        return new Collection($result);
    }
}
