<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSArticleEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSArticleFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSArticleEntity $articleEntity
     *
     * @return array
     */
    public function transform($articleEntity = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return [
            'id' => $sanitizer($articleEntity->getId(), 'integer'),
            'title' => $sanitizer($articleEntity->getTitle(), 'string'),
            'isChapterOfCourse' => $sanitizer($articleEntity->isChapterOfCourse(), 'integer'),
            'description' => $sanitizer($articleEntity->getDescription(), 'string'),
            'author' => $sanitizer($articleEntity->getAuthor(), 'string'),
            'createdAt' => $sanitizer($articleEntity->getCreatedAt(), 'integer'),
            'tags' => $articleEntity->getTagIds(),
            'paragraphs' => (new RSParagraphFlattenAdapter())->transformFromCollection($articleEntity->getParagraphs()),
            'isPublished' => $sanitizer($articleEntity->isPublished(), 'bool'),
        ];
    }
}
