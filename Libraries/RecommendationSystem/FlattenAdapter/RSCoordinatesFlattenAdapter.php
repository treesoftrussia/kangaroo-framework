<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCoordinatesEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSCoordinatesFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSCoordinatesEntity $coordinatesEntity
     *
     * @return array|null
     */
    public function transform($coordinatesEntity = null)
    {
        if (is_null($coordinatesEntity))
        {
            return [];
        }

        $sanitizer = $this->sanitizer()->make();

        return [
            'lng' => $sanitizer($coordinatesEntity->getLng(), 'float'),
            'ltd' => $sanitizer($coordinatesEntity->getLtd(), 'float'),
        ];
    }
}
