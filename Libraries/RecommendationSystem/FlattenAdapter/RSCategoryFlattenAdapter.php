<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\FlattenAdapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCategoryEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSCategoryFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RSCategoryEntity $entity
     *
     * @return array
     */
    public function transform($entity = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return [
            'id' => $sanitizer($entity->getId(), 'integer'),
            'name' => $sanitizer($entity->getName(), 'string'),
        ];
    }
}
