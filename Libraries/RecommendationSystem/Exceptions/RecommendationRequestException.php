<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Exceptions;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class RecommendationRequestException extends RecommendationSystemException
{
}
