<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Exceptions;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class InvalidResponseStatusCodeException extends RecommendationSystemException
{
}
