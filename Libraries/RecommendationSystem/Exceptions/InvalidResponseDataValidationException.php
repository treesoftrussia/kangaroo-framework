<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Exceptions;
use Exception;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class InvalidResponseDataValidationException extends RecommendationSystemException
{
    /**
     * InvalidResponseDataValidationException constructor.
     * @param string $message
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($message = 'Invalid response received from the Recommendation System', $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
