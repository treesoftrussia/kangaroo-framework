<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;

use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Andrew Sparrow <a.vorobyev@mildberry.com>
 */
class RSVisitStatisticEntity
{

    /**
     * @var integer
     */
    private $total;

    /**
     * @var integer
     */
    private $today;

    /**
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param string $total
     * @return RSVisitStatisticEntity
     */
    public function setTotal($total)
    {
        $this->total = Cast::int($total);
        return $this;
    }

    /**
     * @return string
     */
    public function getToday()
    {
        return $this->today;
    }

    /**
     * @param string $today
     * @return RSVisitStatisticEntity
     */
    public function setToday($today)
    {
        $this->today = Cast::int($today);
        return $this;
    }

}
