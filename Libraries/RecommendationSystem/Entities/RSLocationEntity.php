<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSLocationEntity
{
    /**
     * @var string
     */
    private $address;

    /**
     * @var RSCoordinatesEntity
     */
    private $coordinates;

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return RSCoordinatesEntity
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param RSCoordinatesEntity $coordinates
     *
     * @return $this
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;

        return $this;
    }
}
