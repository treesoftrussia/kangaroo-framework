<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSCoordinatesEntity
{
    /**
     * @var float
     */
    private $lng;

    /**
     * @var float
     */
    private $ltd;

    /**
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param float $lng
     *
     * @return $this
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * @return float
     */
    public function getLtd()
    {
        return $this->ltd;
    }

    /**
     * @param float $ltd
     *
     * @return $this
     */
    public function setLtd($ltd)
    {
        $this->ltd = $ltd;

        return $this;
    }
}
