<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;

/**
 * @author Andrew Sparrow <a.vorobyev@mildberry.com>
 */
class RSLocationStatisticEntity
{
    /**
     * @var integer
     */
    private $userId;

    /**
     * @var string
     */
    private $ltd;

    /**
     * @var string
     */
    private $lng;

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return RSLocationStatisticEntity
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getLtd()
    {
        return $this->ltd;
    }

    /**
     * @param string $ltd
     * @return RSLocationStatisticEntity
     */
    public function setLtd($ltd)
    {
        $this->ltd = $ltd;
        return $this;
    }

    /**
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param string $lng
     * @return RSLocationStatisticEntity
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
        return $this;
    }
}
