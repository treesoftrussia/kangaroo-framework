<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;

use Illuminate\Support\Collection;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSTestEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $isChapterOfCourse;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $author;

    /**
     * @var int
     */
    private $createdAt;

    /**
     * @var Collection
     */
    private $tags;

    /**
     * @var bool
     */
    private $isPublished;

    /**
     * TestEntity constructor.
     */
    public function __construct()
    {
        $this->tags = new Collection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     *
     * @return $this
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param Collection $tags
     *
     * @return $this
     */
    public function setTags(Collection $tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * @return mixed
     */
    public function isChapterOfCourse()
    {
        return $this->isChapterOfCourse;
    }

    /**
     * @param mixed $isChapterOfCourse
     *
     * @return $this
     */
    public function setIsChapterOfCourse($isChapterOfCourse)
    {
        $this->isChapterOfCourse = $isChapterOfCourse;

        return $this;
    }

    /**
     * @param bool $isPublished
     * @return $this
     */
    public function setIsPublished(bool $isPublished)
    {
        $this->isPublished = $isPublished;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->isPublished;
    }
}
