<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;

use Illuminate\Support\Collection;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSParagraphEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Collection
     */
    private $tags;

    /**
     * ParagraphEntity constructor.
     */
    public function __construct()
    {
        $this->tags = new Collection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param Collection $tags
     *
     * @return $this
     */
    public function setTags(Collection $tags)
    {
        $this->tags = $tags;

        return $this;
    }
}