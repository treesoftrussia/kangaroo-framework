<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;
use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Andrew Sparrow <a.vorobyev@mildberry.com>
 */
class RSClickStatisticEntity
{
    /**
     * @var int
     */
    private $total;

    /**
     * @var int
     */
    private $today;

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     * @return RSArticleEntity
     */
    public function setTotal($total)
    {
        $this->total = Cast::int($total);

        return $this;
    }

    /**
     * @return int
     */
    public function getToday()
    {
        return $this->today;
    }

    /**
     * @param int $today
     * @return RSArticleEntity
     */
    public function setToday($today)
    {
        $this->today = Cast::int($today);

        return $this;
    }
}