<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSArticleEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $isChapterOfCourse;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $author;

    /**
     * @var int
     */
    private $createdAt;

    /**
     * @var Collection
     */
    private $paragraphs;

    /**
     * @var bool
     */
    private $isPublished;

    /**
     * @var
     */
    private $tagIds;

    /**
     * ArticleEntity constructor.
     */
    public function __construct()
    {
        $this->paragraphs = new Collection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     *
     * @return $this
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getParagraphs()
    {
        return $this->paragraphs;
    }

    /**
     * @param Collection $paragraphs
     *
     * @return $this
     */
    public function setParagraphs(Collection $paragraphs)
    {
        $this->paragraphs = $paragraphs;

        return $this;
    }

    /**
     * @return mixed
     */
    public function isChapterOfCourse()
    {
        return $this->isChapterOfCourse;
    }

    /**
     * @param mixed $isChapterOfCourse
     *
     * @return $this
     */
    public function setIsChapterOfCourse($isChapterOfCourse)
    {
        $this->isChapterOfCourse = $isChapterOfCourse;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isPublished(): bool
    {
        return $this->isPublished ? true : false;
    }

    /**
     * @param boolean $isPublished
     * @return $this
     */
    public function setIsPublished(bool $isPublished)
    {
        $this->isPublished = Cast::bool($isPublished);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTagIds()
    {
        return $this->tagIds;
    }

    /**
     * @param Collection $tagIds
     * @return $this
     */
    public function setTagIds(Collection $tagIds)
    {
        $this->tagIds = $tagIds;
        return $this;
    }
}