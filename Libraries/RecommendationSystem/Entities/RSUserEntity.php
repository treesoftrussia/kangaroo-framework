<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;
use Illuminate\Support\Collection;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSUserEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $birthday;

    /**
     * @var int
     */
    private $gender;

    /**
     * @var int
     */
    private $isAvatarExists;

    /**
     * @var Collection
     */
    private $social;

    /**
     * @var string
     */
    private $childBirthday;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSocial()
    {
        return $this->social;
    }

    /**
     * @param Collection $social
     *
     * @return $this
     */
    public function setSocial($social)
    {
        $this->social = $social;

        return $this;
    }

    /**
     * @return string
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param string $birthday
     *
     * @return $this
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     *
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return int
     */
    public function isAvatarExists()
    {
        return $this->isAvatarExists;
    }

    /**
     * @param int $isAvatarExists
     *
     * @return $this
     */
    public function setIsAvatarExists($isAvatarExists)
    {
        $this->isAvatarExists = $isAvatarExists;

        return $this;
    }

    /**
     * @param string $childBirthday
     * @return $this
     */
    public function setChildBirthday($childBirthday)
    {
        $this->childBirthday = $childBirthday;
        return $this;
    }

    /**
     * @return string
     */
    public function getChildBirthday()
    {
        return $this->childBirthday;
    }
}
