<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSAttachUserGuestIdEntity
{
    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $guestId;

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     *
     * @return $this
     */
    public function setUserId( $userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return string
     */
    public function getGuestId()
    {
        return $this->guestId;
    }

    /**
     * @param string $guestId
     *
     * @return $this
     */
    public function setGuestId($guestId)
    {
        $this->guestId = $guestId;

        return $this;
    }
}
