<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;

/**
 * @author Evgeny Novoselov <e.novosyolov@mildberry.com>
 */
class RSSocialEntity
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $internalId;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getInternalId()
    {
        return $this->internalId;
    }

    /**
     * @param string $internalId
     */
    public function setInternalId( $internalId)
    {
        $this->internalId = $internalId;
    }
}