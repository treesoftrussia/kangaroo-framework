<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class RSRecommendationEntity
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var float
     */
    protected $rating;

    /**
     * @var int
     */
    protected $typeId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return float
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param float $rating
     *
     * @return $this
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @return int
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param int $typeId
     *
     * @return $this
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;

        return $this;
    }
}
