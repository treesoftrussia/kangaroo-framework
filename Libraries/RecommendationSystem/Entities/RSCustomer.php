<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSCustomer
{
    /**
     * @var boolean
     */
    private $guest;

    /**
     * @var string
     */
    private $id;

    /**
     * @return boolean
     */
    public function isGuest()
    {
        return $this->guest;
    }

    /**
     * @param boolean $guest
     *
     * @return $this
     */
    public function setGuest($guest)
    {
        $this->guest = $guest;

        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
