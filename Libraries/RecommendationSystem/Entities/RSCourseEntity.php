<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;

use Illuminate\Support\Collection;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSCourseEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $groupId;

    /**
     * @var Collection
     */
    private $tags;

    /**
     * @var RSLocationEntity
     */
    private $location;

    /**
     * @var Collection
     */
    private $chapters;

    /**
     * @var bool
     */
    private $isContentCompleted;

    /**
     * @var int
     */
    private $createdAt;

    /**
     * @var bool
     */
    private $isPublished;

    /**
     * CourseEntity constructor.
     */
    public function __construct()
    {
        $this->tags = new Collection();
        $this->chapters = new Collection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param Collection $tags
     *
     * @return $this
     */
    public function setTags(Collection $tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param int $groupId
     *
     * @return $this
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * @return RSLocationEntity
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param RSLocationEntity $location
     *
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getChapters()
    {
        return $this->chapters;
    }

    /**
     * @param Collection $chapters
     *
     * @return $this
     */
    public function setChapters(Collection $chapters)
    {
        $this->chapters = $chapters;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isContentCompleted()
    {
        return $this->isContentCompleted;
    }

    /**
     * @param boolean $isContentCompleted
     *
     * @return $this
     */
    public function setIsContentCompleted($isContentCompleted)
    {
        $this->isContentCompleted = $isContentCompleted;

        return $this;
    }

    /**
     * @param boolean $isPublished
     * @return $this
     */
    public function setIsPublished(bool $isPublished)
    {
        $this->isPublished = $isPublished;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->isPublished;
    }
}
