<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities;

use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Andrew Sparrow <a.vorobyev@mildberry.com>
 */
class RSDayClickStatisticEntity
{
    /**
     * @var string
     */
    private $date;

    /**
     * @var int
     */
    private $count;

    /**
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = Cast::string($date);

        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param $count
     * @return $this
     */
    public function setCount($count)
    {
        $this->count = Cast::int($count);

        return $this;
    }
}