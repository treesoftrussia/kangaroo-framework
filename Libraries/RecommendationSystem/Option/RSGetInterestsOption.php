<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Option;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSGetInterestsOption
{
    /**
     * @var int
     */
    private $itemsNumber;

    /**
     * @var string
     */
    private $sequenceToken;

    /**
     * @return int
     */
    public function getItemsNumber()
    {
        return $this->itemsNumber;
    }

    /**
     * @param int $itemsNumber
     *
     * @return $this
     */
    public function setItemsNumber($itemsNumber)
    {
        $this->itemsNumber = $itemsNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getSequenceToken()
    {
        return $this->sequenceToken;
    }

    /**
     * @param string $sequenceToken
     *
     * @return $this
     */
    public function setSequenceToken($sequenceToken)
    {
        $this->sequenceToken = $sequenceToken;

        return $this;
    }
}
