<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Option;

use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSGetContentOption
{
    /**
     * @var array
     */
    private $types;

    /**
     * @var int
     */
    private $groupId;

    /**
     * @var int
     */
    private $categoryId;

    /**
     * @var int
     */
    private $tagId;

    /**
     * @var int
     */
    private $itemsNumber;

    /**
     * @var int
     */
    private $screenTypeId;

    /**
     * @var int
     */
    private $zoneTypeId;

    /**
     * @var string
     */
    private $sequenceToken;

    /**
     * @var int
     */
    private $relatedContentId;

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @param int $type
     *
     * @return $this
     */
    public function addType($type)
    {
        array_push($this->types, $type);

        return $this;
    }

    /**
     * @param array $types
     *
     * @return $this
     */
    public function setTypes($types)
    {
        $this->types = $types;

        return $this;
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param int $groupId
     *
     * @return $this
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     *
     * @return $this
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * @return int
     */
    public function getTagId()
    {
        return $this->tagId;
    }

    /**
     * @param int $tagId
     *
     * @return $this
     */
    public function setTagId($tagId)
    {
        $this->tagId = $tagId;

        return $this;
    }

    /**
     * @return int
     */
    public function getItemsNumber()
    {
        return $this->itemsNumber;
    }

    /**
     * @param int $itemsNumber
     *
     * @return $this
     */
    public function setItemsNumber($itemsNumber)
    {
        $this->itemsNumber = $itemsNumber;

        return $this;
    }

    /**
     * @return int
     */
    public function getScreenTypeId()
    {
        return $this->screenTypeId;
    }

    /**
     * @param int $screenTypeId
     *
     * @return $this
     */
    public function setScreenTypeId($screenTypeId)
    {
        $this->screenTypeId = $screenTypeId;

        return $this;
    }

    /**
     * @return int
     */
    public function getZoneTypeId()
    {
        return $this->zoneTypeId;
    }

    /**
     * @param int $zoneTypeId
     *
     * @return $this
     */
    public function setZoneTypeId($zoneTypeId)
    {
        $this->zoneTypeId = $zoneTypeId;

        return $this;
    }

    /**
     * @return string
     */
    public function getSequenceToken()
    {
        return $this->sequenceToken;
    }

    /**
     * @param string $sequenceToken
     *
     * @return $this
     */
    public function setSequenceToken($sequenceToken)
    {
        $this->sequenceToken = $sequenceToken;

        return $this;
    }

    /**
     * @return int
     */
    public function getRelatedContentId()
    {
        return $this->relatedContentId;
    }

    /**
     * @param int $relationContentId
     * @return $this
     */
    public function setRelatedContentId($relationContentId)
    {
        $this->relatedContentId = Cast::int($relationContentId);

        return $this;
    }
}
