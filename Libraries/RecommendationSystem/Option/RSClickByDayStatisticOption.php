<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Option;

use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Andrew Sparrow <a.vorobyev@mildberry.com>
 */
class RSClickByDayStatisticOption
{

    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $to;

    /**
     * @return string
     */
    public function getFrom()
    {
        return Cast::string($this->from);
    }

    /**
     * @param string $from
     * @return RSClickByDayStatisticOption
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param string $to
     * @return RSClickByDayStatisticOption
     */
    public function setTo($to)
    {
        $this->to = Cast::string($to);
        return $this;
    }
}
