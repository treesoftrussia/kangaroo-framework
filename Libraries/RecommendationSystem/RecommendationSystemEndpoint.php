<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem;

use Symfony\Component\HttpFoundation\Response;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RecommendationSystemEndpoint
{
    const ENDPOINT_PREFIX = '/api/v1';

    /**
     * @var string
     */
    protected $requestSpecification;

    /**
     * @var string
     */
    protected $responseSpecification;

    /**
     * @var int
     */
    protected $validResponseStatusCode = Response::HTTP_OK;

    /**
     * @var string
     */
    protected $method = 'GET';

    /**
     * @var string
     */
    private $uri;

    /**
     * @var array
     */
    private $requestData = [];

    /**
     * @var array
     */
    private $requestOptions = [];

    /**
     * @return string
     */
    public function getRequestSpecification()
    {
        return $this->requestSpecification;
    }

    /**
     * @param string $requestSpecification
     *
     * @return $this
     */
    public function setRequestSpecification($requestSpecification)
    {
        $this->requestSpecification = $requestSpecification;

        return $this;
    }

    /**
     * @return string
     */
    public function getResponseSpecification()
    {
        return $this->responseSpecification;
    }

    /**
     * @param string $responseSpecification
     *
     * @return $this
     */
    public function setResponseSpecification($responseSpecification)
    {
        $this->responseSpecification = $responseSpecification;

        return $this;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     *
     * @return $this
     */
    public function setUri($uri)
    {
        $this->uri = self::ENDPOINT_PREFIX.$uri;

        return $this;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     *
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return array
     */
    public function getRequestData()
    {
        return $this->requestData;
    }

    /**
     * @param array $requestData
     *
     * @return $this
     */
    public function setRequestData($requestData)
    {
        $this->requestData = $requestData;

        return $this;
    }

    /**
     * @return int
     */
    public function getValidResponseStatusCode()
    {
        return $this->validResponseStatusCode;
    }

    /**
     * @param int $validResponseStatusCode
     *
     * @return $this
     */
    public function setValidResponseStatusCode($validResponseStatusCode)
    {
        $this->validResponseStatusCode = $validResponseStatusCode;

        return $this;
    }

    /**
     * @return array
     */
    public function getRequestOptions()
    {
        return $this->requestOptions;
    }

    /**
     * @param array $requestOptions
     *
     * @return $this
     */
    public function setRequestOptions($requestOptions)
    {
        $this->requestOptions = $requestOptions;

        return $this;
    }
}
