<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter\RSClickByDayStatisticAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter\RSClickStatisticAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter\RSInterestAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter\RSLocationStatisticAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter\RSRecommendationAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter\RSVisitStatisticAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Statistic\GetClickByDayStatisticEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Statistic\GetClickStatisticEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Statistic\GetLocationStatisticEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\User\GetRecommendedContentEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\User\GetUserInterestsEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Endpoint\Statistic\GetVisitStatisticEndpoint;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCustomer;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSVisitStatisticEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Interfaces\RecommenderClientInterface;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Option\RSClickByDayStatisticOption;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Option\RSGetContentOption;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Option\RSGetInterestsOption;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class RecommenderClient extends RecommendationSystemClient implements RecommenderClientInterface
{

    /**
     * @return RSVisitStatisticEntity
     */
    public function getVisitStatistic(){
        $data = $this->makeRequestToEndpoint(new GetVisitStatisticEndpoint())->getData();

        return (new RSVisitStatisticAdapter())->transform($data);
    }

    /**
     * @param RSClickByDayStatisticOption $options
     * @return Collection
     */
    public function getClickByDayStatistic(RSClickByDayStatisticOption $options){
        $data = $this->makeRequestToEndpoint(new GetClickByDayStatisticEndpoint($options))->getData();

        $collection = new Collection();
        foreach ($data as $item) {
            $collection->push((new RSClickByDayStatisticAdapter())->transform($item));
        }

        return $collection;
    }

    /**
     * @return Collection
     */
    public function getLocationStatistic(){
        $data = $this->makeRequestToEndpoint(new GetLocationStatisticEndpoint())->getData();

        $collection = new Collection();
        foreach ($data as $item) {
            $collection->push((new RSLocationStatisticAdapter())->transform($item));
        }

        return $collection;
    }

    /**
     * @return Entities\RSClickStatisticEntity
     */
    public function getClickStatistic(){
        $data = $this->makeRequestToEndpoint(new GetClickStatisticEndpoint())->getData();

        return (new RSClickStatisticAdapter())->transform($data);
    }

    /**
     * @param RSGetContentOption $option
     * @param RSCustomer $customer
     * @return Collection
     */
    public function getContent(RSGetContentOption $option, RSCustomer $customer)
    {
        $data = $this->makeRequestToEndpoint(new GetRecommendedContentEndpoint($option, $customer))->getData();

        $collection = new Collection();
        foreach ($data as $item) {
            $collection->push((new RSRecommendationAdapter())->transform($item));
        }

        return $collection;
    }

    /**
     * @param RSGetInterestsOption $option
     * @param RSCustomer $customer
     * @return Collection
     */
    public function getInterests(RSGetInterestsOption $option, RSCustomer $customer)
    {
        $data = $this->makeRequestToEndpoint(new GetUserInterestsEndpoint($option, $customer))->getData();

        $collection = new Collection();
        foreach ($data as $item) {
            $collection->push((new RSInterestAdapter())->transform($item));
        }

        return $collection;
    }
}