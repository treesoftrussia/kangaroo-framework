<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSChapterEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSChapterAdapter extends AbstractAdapter
{
    /**
     * @param object $model
     *
     * @return RSChapterEntity
     */
    public function transform($model = null)
    {
        $entity = new RSChapterEntity();
        $entity
            ->setId($model->id)
            ->setType($model->type)
        ;

        return $entity;
    }

    /**
     * @param array $models
     * @return Collection
     */
    public function transformFromArray(array $models)
    {
        $result = array();
        foreach ($models as $id) {
            $result[] = (new RSChapterEntity())->setId($id);
        }

        return new Collection($result);
    }
}
