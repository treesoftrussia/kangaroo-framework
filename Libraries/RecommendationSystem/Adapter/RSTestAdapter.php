<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTestEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSTestAdapter extends AbstractAdapter
{
    /**
     * @param object $data
     *
     * @return RSTestEntity
     */
    public function transform($data)
    {
        $entity = new RSTestEntity();

        $entity
            ->setId($data->id)
            ->setTitle($data->title)
            ->setDescription($data->description)
            ->setAuthor($data->author)
            ->setIsChapterOfCourse($data->isChapterOfCourse)
            ->setCreatedAt($data->createdAt)
        ;

        $entity->setTags((new RSTagAdapter())->transformFromArray($data->tags));

        return $entity;
    }
}
