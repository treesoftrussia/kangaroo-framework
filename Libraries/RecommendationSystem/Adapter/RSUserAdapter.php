<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSUserEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSUserAdapter extends AbstractAdapter
{
    /**
     * @param object $model
     *
     * @return RSUserEntity
     */
    public function transform($model)
    {
        $entity = new RSUserEntity();

        $entity
            ->setId($model->id)
            ->setUsername($model->username)
            ->setBirthday($model->birthday)
            ->setIsAvatarExists($model->isAvatarExists)
            ->setGender($model->gender)
            ->setSocial($model->social)
        ;

        return $entity;
    }
}
