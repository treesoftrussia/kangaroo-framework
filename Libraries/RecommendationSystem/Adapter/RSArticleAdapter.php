<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSArticleEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSArticleAdapter extends AbstractAdapter
{
    /**
     * @param object $data
     *
     * @return RSArticleEntity
     */
    public function transform($data)
    {
        $entity = new RSArticleEntity();

        $entity
            ->setId($data->id)
            ->setTitle($data->title)
            ->setDescription($data->description)
            ->setAuthor($data->author)
            ->setIsChapterOfCourse($data->isChapterOfCourse)
            ->setCreatedAt($data->createdAt)
            ->setTagIds(new Collection($data->tags))
        ;

        $entity->setParagraphs((new RSParagraphAdapter())->transformFromArray($data->paragraphs));

        return $entity;
    }
}
