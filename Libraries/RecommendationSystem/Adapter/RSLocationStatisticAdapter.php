<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSLocationEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSLocationStatisticEntity;

/**
 * @author Andrew Sparrow <a.vorobyev@mildberry.com>
 */
class RSLocationStatisticAdapter extends AbstractAdapter
{
    /**
     * @param object $model
     *
     * @return RSLocationEntity
     */
    public function transform($model = null)
    {
        $entity = new RSLocationStatisticEntity();
        $entity
            ->setUserId($model->userId)
            ->setLtd($model->ltd)
            ->setLng($model->lng)
        ;

        return $entity;
    }
}
