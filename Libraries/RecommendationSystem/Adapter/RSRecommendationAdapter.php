<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSRecommendationEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSRecommendationAdapter extends AbstractAdapter
{
    /**
     * @param object $model
     *
     * @return RSRecommendationEntity
     */
    public function transform($model)
    {
        $entity = new RSRecommendationEntity();

        $entity
            ->setId($model->id)
            ->setRating($model->rating)
            ->setTypeId($model->recommendationTypeId)
        ;

        return $entity;
    }
}
