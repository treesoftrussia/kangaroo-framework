<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCategoryEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSCategoryAdapter extends AbstractAdapter
{
    /**
     * @param object $model
     *
     * @return RSCategoryEntity
     */
    public function transform($model)
    {
        $entity = new RSCategoryEntity();

        $entity
            ->setId($model->id)
            ->setName($model->name)
        ;

        return $entity;
    }
}
