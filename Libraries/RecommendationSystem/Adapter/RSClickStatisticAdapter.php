<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSClickStatisticEntity;

/**
 * @author Andrew Sparrow <a.vorobyev@mildberry.com>
 */
class RSClickStatisticAdapter extends AbstractAdapter
{

    /**
     * @param $data
     * @return RSClickStatisticEntity
     */
    public function transform($data)
    {
        $entity = new RSClickStatisticEntity();

        $entity
            ->setToday($data->today)
            ->setTotal($data->total);

        return $entity;
    }
}
