<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSClickStatisticEntity;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSDayClickStatisticEntity;

/**
 * @author Andrew Sparrow <a.vorobyev@mildberry.com>
 */
class RSClickByDayStatisticAdapter extends AbstractAdapter
{

    /**
     * @param $data
     * @return RSClickStatisticEntity
     */
    public function transform($data)
    {
        $entity = new RSDayClickStatisticEntity();

        $entity
            ->setDate($data->date)
            ->setCount($data->count);

        return $entity;
    }
}
