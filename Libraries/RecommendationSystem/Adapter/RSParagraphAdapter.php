<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSParagraphEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSParagraphAdapter extends AbstractAdapter
{
    /**
     * @param object $model
     *
     * @return RSParagraphEntity
     */
    public function transform($model = null)
    {
        $entity = new RSParagraphEntity();

        $entity
            ->setId($model->id)
        ;

        $entity->setTags((new RSTagAdapter())->transformFromArray($model->tags));

        return $entity;
    }

    /**
     * @param array $models
     * @return Collection
     */
    public function transformFromArray(array $models)
    {
        $result = array();
        foreach ($models as $model) {
            $result[] = $this->transform($model);
        }

        return new Collection($result);
    }
}
