<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSLocationEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSLocationAdapter extends AbstractAdapter
{
    /**
     * @param object $model
     *
     * @return RSLocationEntity
     */
    public function transform($model = null)
    {
        $entity = new RSLocationEntity();
        $entity
            ->setAddress($model->address)
            ->setCoordinates((new RSCoordinatesAdapter())->transform($model->coordinates))
        ;

        return $entity;
    }
}
