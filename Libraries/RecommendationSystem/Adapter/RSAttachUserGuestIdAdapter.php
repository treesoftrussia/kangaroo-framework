<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSAttachUserGuestIdEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSAttachUserGuestIdAdapter extends AbstractAdapter
{
    /**
     * @param object $model
     *
     * @return RSAttachUserGuestIdEntity
     */
    public function transform($model)
    {
        $entity = new RSAttachUserGuestIdEntity();

        $entity
            ->setUserId($model->userId)
            ->setGuestId($model->guestId)
        ;

        return $entity;
    }
}
