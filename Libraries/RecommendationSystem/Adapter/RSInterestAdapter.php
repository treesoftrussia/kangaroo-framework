<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSInterestEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSInterestAdapter extends AbstractAdapter
{
    /**
     * @param object $model
     *
     * @return RSInterestEntity
     */
    public function transform($model)
    {
        $entity = new RSInterestEntity();

        $entity
            ->setId($model->id)
            ->setName($model->name)
        ;

        return $entity;
    }
}
