<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCourseEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSCourseAdapter extends AbstractAdapter
{
    /**
     * @param object $data
     *
     * @return RSCourseEntity
     */
    public function transform($data)
    {
        $entity = new RSCourseEntity();

        $entity
            ->setId($data->id)
            ->setTitle($data->title)
            ->setGroupId($data->groupId)
            ->setIsContentCompleted($data->isContentCompleted)
            ->setCreatedAt($data->createdAt)
            ->setChapters((new RSChapterAdapter())->transformFromArray($data->chapters))
            ->setTags((new RSTagAdapter())->transformFromArray($data->tags))
        ;

        return $entity;
    }
}
