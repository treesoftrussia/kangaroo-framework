<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSVisitStatisticEntity;

/**
 * @author Andrew Sparrow <a.vorobyev@mildberry.com>
 */
class RSVisitStatisticAdapter extends AbstractAdapter
{

    /**
     * @param $data
     * @return RSVisitStatisticEntity
     */
    public function transform($data)
    {
        $entity = new RSVisitStatisticEntity();

        $entity
            ->setToday($data->today)
            ->setTotal($data->total);

        return $entity;
    }
}
