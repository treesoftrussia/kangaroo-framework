<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSCoordinatesEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSCoordinatesAdapter extends AbstractAdapter
{
    /**
     * @param object $model
     *
     * @return RSCoordinatesEntity
     */
    public function transform($model = null)
    {
        $entity = new RSCoordinatesEntity();
        $entity
            ->setLng($model->lng)
            ->setLtd($model->ltd)
        ;

        return $entity;
    }
}
