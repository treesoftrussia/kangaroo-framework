<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem\Adapter;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\RecommendationSystem\Entities\RSTagEntity;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RSTagAdapter extends AbstractAdapter
{
    /**
     * @param object $model
     *
     * @return RSTagEntity
     */
    public function transform($model = null)
    {
        $entity = new RSTagEntity();
        $entity
            ->setId($model->id)
            ->setName($model->name)
            ->setCategories($model->categoryId)
        ;

        return $entity;
    }

    /**
     * @param array $models
     * @return Collection
     */
    public function transformFromArray(array $models)
    {
        $result = array();
        foreach ($models as $id) {
            $result[] = (new RSTagEntity())->setId($id);
        }

        return new Collection($result);
    }
}
