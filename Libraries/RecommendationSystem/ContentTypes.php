<?php

namespace Mildberry\Kangaroo\Libraries\RecommendationSystem;
use Mildberry\Kangaroo\Libraries\Enum\Enum;


/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class ContentTypes extends Enum
{
    const ARTICLE = 1;
    const TEST = 2;
    const COURSE = 3;
}