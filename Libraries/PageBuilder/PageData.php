<?php

namespace Mildberry\Kangaroo\Libraries\PageBuilder;

use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;

class PageData
{
    /**
     * State constants.
     */
    const UPDATE = 1;
    const CREATE = 2;

    /**
     * @var
     */
    private $entity;

    /**
     * @var
     */
    private $state;

    /**
     * @var UserEntityInterface
     */
    private $user;

    /**
     * @var
     */
    private $entityName;

    /**
     * @var
     */
    private $options;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     *
     * @return PageData
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $entity
     *
     * @return PageData
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    public function getEntityName()
    {
        return $this->entityName;
    }

    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     *
     * @return PageData
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEditState()
    {
        return $this->state == self::UPDATE;
    }

    /**
     * @return bool
     */
    public function isUpdateState()
    {
        return $this->state == self::UPDATE;
    }

    /**
     * @param $name
     *
     * @return string
     */
    public function getInputValue($name)
    {
        if (!$this->isEditState()) {
            return '';
        }

        $getter = make_getter($name);

        $entity = $this->getEntity();
        if ($entity && method_exists($entity, $getter)) {
            return $entity->{$getter}();
        }
        throw new RuntimeException('Requested input value was not found.');
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param $options
     * @return $this
     */
    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }
}
