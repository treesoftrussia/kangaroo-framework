<?php
namespace Mildberry\Kangaroo\Libraries\Processor;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
abstract class AbstractFilterProcessor extends AbstractProcessor
{
    /**
     * @return array
     */
    public function toArray()
    {
        return array_take(parent::toArray(), 'filter', []);
    }

    /**
     * Indicates whether auto validation is allowed
     *
     * @return bool
     */
    public function validateAutomatically()
    {
        return false;
    }

    /**
     * @param object $object
     * @param array $options
     * @return object
     */
    protected function populate($object, array $options = [])
    {
        $data = $this->toArray();

        $data = $this->filterByAllowable($data);
        $data = $this->filterByRules($data);

        return $this->performPopulation($object, $data, $options);
    }
} 