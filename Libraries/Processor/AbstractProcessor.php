<?php
namespace Mildberry\Kangaroo\Libraries\Processor;

use Mildberry\Kangaroo\Libraries\Converter\Populator\Populator;
use Mildberry\Kangaroo\Libraries\Modifier\ModifierProviderTrait;
use Mildberry\Kangaroo\Libraries\Validator\PostValidationInterface;
use Illuminate\Contracts\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Validation\Factory as ValidatorFactory;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * This is a base class for all
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
abstract class AbstractProcessor
{
    use ModifierProviderTrait;

    /**
     * Keeps cached validation errors
     * @var array
     */
    private $validationErrors;

    /**
     * @var array
     */
    private $dotted;

    /**
     * @var Container
     */
    protected  $container;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var ValidatorFactory
     */
    private $validatorFactory;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        if ($this->request === null){
            $this->request = $this->container->make(Request::class);
        }

        return $this->request;
    }

    /**
     * @return ValidatorFactory
     */
    protected function getValidatorFactory()
    {
        if ($this->validatorFactory === null){
            $this->validatorFactory = $this->container->make(ValidatorFactory::class);
        }

        return $this->validatorFactory;
    }

    /**
     * Rules to validate the retrieved data
     * @return array
     */
    protected function rules()
    {
        return [];
    }

    /**
     * Provides custom message for the validator
     * @return array
     */
    protected function messages()
    {
        return [];
    }

    /**
     * List of request's allowable fields for a populate object
     * @return array
     */
    protected function allowable()
    {
        return [];
    }

    /**
     * Gets the prepared value by the key
     * @param string|array $key
     * @param null|mixed $default
     * @param null|string $modifiers
     * @param bool $validator
     * @return mixed
     * @throws \RuntimeException
     */
    public function get($key, $default = null, $modifiers = null, $validator = false)
    {
        if (is_array($key)) {
            $key = $this->findKey($key);
        }

        $value = $default;

        if ($this->has($key)) {

            if (!$validator || $this->isValid($key)){
                $value = $this->toDotted()[$key];
            }
        }

        return $modifiers ? $this->modify($value, $modifiers) : $value;
    }

    /**
     * Checks whether the field is valid
     *
     * @param string $key
     * @return bool
     */
    private function isValid($key)
    {
        return !in_array($key, array_keys($this->validate()));
    }

    /**
     * Modifiers the provided value
     * @param $value
     * @param array|string $modifiers
     * @return mixed
     */
    protected function modify($value, $modifiers)
    {
        return $this->getModifierManager()->modify($value, $modifiers);
    }

    /**
     * Tries to get a value by key with validation applied
     * @param $key
     * @param string $default
     * @param null $modifiers
     * @return mixed
     */
    public function tryGet($key, $default = null, $modifiers = null)
    {
        return $this->get($key, $default, $modifiers, true);
    }

    /**
     * Searches a key
     * @param array $keys
     * @return mixed
     */
    private function findKey(array $keys)
    {
        foreach ($keys as $key) {
            if ($this->has($key)) {
                return $key;
            }
        }

        return reset($keys);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        if ($this->getRequest()->isMethod(Request::METHOD_DELETE)
            || $this->getRequest()->isMethod(Request::METHOD_GET)
        ) {
            return $this->getRequest()->query->all();
        }

        return $this->getRequest()->json()->all();
    }


    /**
     * Shortcut method to populate the provided object with the provided data
     *
     * @param object $object
     * @param array $options
     * @return object
     * @throws BadRequestHttpException
     */
    protected function populate($object, array $options = [])
    {
        $data = $this->toArray();

        $data = $this->filterByAllowable($data);

        if (!$data) {
            throw new BadRequestHttpException('There\'s no allowable data in the body.');
        }

        return $this->performPopulation($object, $data, $options);
    }

    /**
     * @param object $object
     * @param array $data
     * @param array $options
     * @return object
     * @throws BadRequestHttpException
     */
    protected function performPopulation($object, array $data, array $options = [])
    {
        return (new Populator($options))
            ->setModifierManager($this->getModifierManager())
            ->populate($data, $object);
    }

    /**
     * Filters data according to the validation rules
     *
     * @param array $data
     * @return array
     */
    protected function filterByRules(array $data)
    {
        $filtered = [];

        foreach (array_keys(array_smash($data)) as $field) {
            $value = array_get($data, $field);

            if (!$this->isValid($field)) {
                continue;
            }

            array_set($filtered, $field, $value);
        }

        return $filtered;
    }

    /**
     * Filters data according to the allowable fields
     *
     * @param array $data
     * @return array
     */
    protected function filterByAllowable(array $data)
    {
        $filtered = [];
        $fields = $this->allowable();

        foreach ($fields as $field) {
            $item = array_get($data, $field);

            if ($item === null) {
                continue;
            }

            if (is_array($item) && !is_vector($item)){
                continue ;
            }

            array_set($filtered, $field, $item);
        }

        return $filtered;
    }

    /**
     * Validates the retrieved data
     * @return array
     * @throws \RuntimeException
     */
    public function validate()
    {
        if ($this->validationErrors === null) {

            $before = $rules = $this->rules();
            $after = [];

            if (is_array(array_take($rules, 0))) {
                $before = $rules[0];
                $after = array_take($rules, 1, []);
            }

            $data = array_merge($this->toDotted());

            $validator = $this->getValidatorFactory()
                ->make($data, $before, $this->messages());

            if ($validator instanceof PostValidationInterface) {
                $validator->setAfterRules($after);
            }

            $this->validationErrors = $validator->errors()->toArray();
        }

        return $this->validationErrors;
    }

    /**
     * Indicates whether auto validation is allowed
     *
     * @return bool
     */
    public function validateAutomatically()
    {
        return true;
    }

    /**
     * @return array
     */
    public function toDotted()
    {
        if ($this->dotted === null){
            $this->dotted = array_smash($this->toArray());
        }

        return $this->dotted;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has($key)
    {
        return array_take($this->toDotted(), $key, '') !== '';
    }
}