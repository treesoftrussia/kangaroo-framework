<?php
namespace Mildberry\Kangaroo\Libraries\Processor;

use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class SharedModifiers
{
    /**
     * @param $value
     * @return int|null
     */
    public function int($value)
    {
        return Cast::int($value);
    }

    /**
     * @param $value
     * @return bool|null
     */
    public function bool($value)
    {
        if (in_array($value, [0, '0', 'false'], true)) {
            return false;
        }

        if (in_array($value, [1, '1', 'true'], true)) {
            return true;
        }

        return Cast::bool($value);
    }

    /**
     * @param $value
     * @return float|null
     */
    public function float($value)
    {
        return Cast::float($value);
    }

    /**
     * @param string $value
     * @return array
     */
    public function explode($value)
    {
        $value = Cast::string($value);

        if ($value) {
            return explode(',', $value);
        }

        return [];
    }
}
