<?php
namespace Mildberry\Kangaroo\Libraries\Processor;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
abstract class AbstractOptionsProcessor extends AbstractProcessor
{
    /**
     * @return array
     */
    public function toArray()
    {
        $json = $this->getRequest()->header('Options', '{}');
        return json_decode($json, true);
    }

    /**
     * @param object $object
     * @param array $options
     * @return object
     * @throws BadRequestHttpException
     */
    protected function populate($object, array $options = [])
    {
        $data = $this->filterByAllowable($this->toArray());

        if (!$data){
            return $object;
        }

        return $this->performPopulation($object, $data, $options);
    }
} 