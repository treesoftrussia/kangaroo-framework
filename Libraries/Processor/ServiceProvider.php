<?php
namespace Mildberry\Kangaroo\Libraries\Processor;

use Mildberry\Kangaroo\Libraries\Kangaroo\Exceptions\InvalidInputException;
use Illuminate\Support\ServiceProvider as Provider;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ServiceProvider extends Provider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
        $this->app->afterResolving(function (AbstractProcessor $processor) {

            if (!$processor->validateAutomatically()){
                return ;
            }

            if ($errors = $processor->validate()) {
                throw new InvalidInputException($errors);
            }
        });
    }
}