<?php
namespace Mildberry\Kangaroo\Libraries\SocialNetwork;

use Mildberry\Kangaroo\Libraries\Resource\Error;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\ExceptionInterface;
use RuntimeException as DefaultRuntimeException;
use Symfony\Component\HttpFoundation\Response;

class OAuthServerErrorException extends DefaultRuntimeException implements ExceptionInterface
{
    public function getHTTPCode()
    {
        return Response::HTTP_UNAUTHORIZED;
    }

    public function getInternalCode()
    {
        return Error::INVALID_CREDENTIALS;
    }

    public function __construct($message = 'Auth server don\'t accepted authorization.')
    {
        parent::__construct($message);
    }

    public function getData()
    {
        return $this->getMessage();
    }
}
