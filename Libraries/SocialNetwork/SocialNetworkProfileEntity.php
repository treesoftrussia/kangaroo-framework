<?php
namespace Mildberry\Kangaroo\Libraries\SocialNetwork;


use Mildberry\Kangaroo\Libraries\OAuth\Entities\TokenEntity;

class SocialNetworkProfileEntity
{
    private $id;

    private $userId;

    private $internalId;

    private $type;

    private $tokenData;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return SocialNetworkProfileEntity
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     *
     * @return SocialNetworkProfileEntity
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInternalId()
    {
        return $this->internalId;
    }

    /**
     * @param mixed $internalId
     *
     * @return SocialNetworkProfileEntity
     */
    public function setInternalId($internalId)
    {
        $this->internalId = $internalId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     *
     * @return SocialNetworkProfileEntity
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return TokenEntity $token
     */
    public function getTokenData()
    {
        return $this->tokenData;
    }

    /**
     * @param mixed $tokenData
     *
     * @return SocialNetworkProfileEntity
     */
    public function setTokenData(TokenEntity $tokenData)
    {
        $this->tokenData = $tokenData;

        return $this;
    }
}
