<?php
namespace Mildberry\Kangaroo\Libraries\SocialNetwork;

interface SocialNetworkProviderInterface
{
    public function requestAccess($code);

    public function getUser();
}
