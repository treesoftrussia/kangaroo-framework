<?php

namespace Mildberry\Kangaroo\Libraries\SocialNetwork;
use Mildberry\Kangaroo\Libraries\Options\AbstractOptionsInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class AccessTokenRequestViaSocialNetworkOptions implements AbstractOptionsInterface
{
    /**
     * @var
     */
    private $code;

    /**
     * @var
     */
    private $type;

    /**
     * @var
     */
    private $guestId;

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     * @return AccessTokenRequestViaSocialNetworkOptions
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return AccessTokenRequestViaSocialNetworkOptions
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function initDefaultValues(){}

    /**
     * @return mixed
     */
    public function getGuestId()
    {
        return $this->guestId;
    }

    /**
     * @param $guestId
     * @return $this
     */
    public function setGuestId($guestId)
    {
        $this->guestId = $guestId;
        return $this;
    }


}