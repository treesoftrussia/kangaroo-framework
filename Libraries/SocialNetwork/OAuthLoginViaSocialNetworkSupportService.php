<?php

namespace Mildberry\Kangaroo\Libraries\SocialNetwork;

use LucaDegasperi\OAuth2Server\Authorizer;
use Mildberry\Kangaroo\Libraries\Container\ContainerInterface;
use Mildberry\Kangaroo\Libraries\OAuth\Entities\TokenEntity;
use Mildberry\Kangaroo\Libraries\OAuth\GrantTypes\PasswordGrant;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Mildberry\Kangaroo\Libraries\Service\AbstractService;
use Mildberry\Kangaroo\Libraries\Service\UserServiceInterface;
use Mildberry\Kangaroo\Libraries\SocialNetwork\DAL\SocialNetworkRepository;
use Mildberry\Kangaroo\Libraries\SocialNetwork\Providers\QQSocialNetworkProvider;
use Mildberry\Kangaroo\Libraries\SocialNetwork\Providers\WechatSocialNetworkProvider;
use Mildberry\Kangaroo\Libraries\SocialNetwork\Providers\WeiboSocialNetworkProvider;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserRepositoryInterface;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class OAuthLoginViaSocialNetworkSupportService extends AbstractService
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var
     */
    private $userService;

    /**
     * OAuthLoginViaSocialNetworkSupportService constructor.
     * @param ContainerInterface $container
     * @param UserServiceInterface $userService
     * @param SocialNetworkRepository $userRepository
     */
    public function __construct(ContainerInterface $container, UserServiceInterface $userService, SocialNetworkRepository $userRepository)
    {
        parent::__construct($container);
        $this->userRepository = $userRepository;
        $this->userService = $userService;
    }

    /**
     * @param AccessTokenRequestViaSocialNetworkOptions $options
     * @return TokenEntity
     */
    public function getToken(AccessTokenRequestViaSocialNetworkOptions $options)
    {
        $provider = $this->getProvider($options->getType());

        $socialNetworkProfile = $provider->requestAccess($options->getCode());
        $user = $provider->getUser();

        $userEntity = $this->userRepository->createOrUpdateIfExistsSocialNetworkEntry($user, $socialNetworkProfile);

        /** @var PasswordGrant $passwordGrant */
        $passwordGrant = $this->container->get(Authorizer::class)->getIssuer()->getGrantType('password');
        $clientEntity = $passwordGrant->getClient(config('params.web_client_id'), config('params.web_client_secret'));

        $tokenData = $passwordGrant->generateAccessToken($clientEntity, $userEntity);
        $tokenData['redirectURL'] = '/';
        $tokenData['userId'] = $userEntity->getId();

        $this->userService->setCurrentUser($userEntity);

        return TokenEntity::makeFromArray($tokenData);
    }

    /**
     * @param $type
     * @return object
     */
    private function getProvider($type)
    {
        switch ($type) {
            case 'wechat':
                return $this->container->get(WechatSocialNetworkProvider::class);
            case 'weibo':
                return $this->container->get(WeiboSocialNetworkProvider::class);
            case 'qq':
                return $this->container->get(QQSocialNetworkProvider::class);
            default:
                throw new RuntimeException('Specified login type not supported.');
        }
    }
}
