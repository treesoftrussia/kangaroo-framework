<?php


namespace Mildberry\Kangaroo\Libraries\SocialNetwork\Providers;

use GuzzleHttp\Client;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Facades\Hash;
use Mildberry\Kangaroo\Libraries\OAuth\Entities\TokenEntity;
use Mildberry\Kangaroo\Libraries\SocialNetwork\OAuthServerErrorException;
use Mildberry\Kangaroo\Libraries\SocialNetwork\SocialNetworkProfileEntity;
use Mildberry\Kangaroo\Libraries\SocialNetwork\SocialNetworkProviderInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Entity\Users\UserEntity;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\AvatarEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;


/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */
class QQSocialNetworkProvider implements SocialNetworkProviderInterface
{
    /**
     * @var
     */
    private $profile;

    /**
     * @var Container
     */
    protected $container;

    /**
     * QQSocialNetworkProvider constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * init request
     * https: //graph.qq.com/oauth2.0/authorize?response_type=code&client_id={client_id}&redirect_uri={redirect_uri}&state={CSRF}
     * api http://wiki.connect.qq.com/%E4%BD%BF%E7%94%A8authorization_code%E8%8E%B7%E5%8F%96access_token
     * @param $code
     * @return SocialNetworkProfileEntity
     */
    public function requestAccess($code)
    {

        $clientId = config('social_network.qq.client_id');
        $clientSecret = config('social_network.qq.client_secret');
        $redirectUrl = config('social_network.qq.redirect_uri');

        $socialTokenData = $this->requestAccessToken($code, $clientId, $clientSecret, $redirectUrl);
        if ($socialTokenData && is_object($socialTokenData) && isset($socialTokenData->access_token)) {
            $tokenData = (new TokenEntity())->setAccessToken($socialTokenData->access_token)->setExpiresIn($socialTokenData->expires_in)->setTokenType('Bearer');
            $this->profile = new SocialNetworkProfileEntity();
            $this->profile->setInternalId($this->requestInternalUserId()->OpenID)->setType('qq')->setTokenData($tokenData);

            return $this->profile;
        }

        throw new OAuthServerErrorException(is_object($socialTokenData) && $socialTokenData->errmsg ? sprintf('QQ auth server returned error: %s.', $socialTokenData->errmsg) : 'Unknown qq auth server error.');
    }

    /**
     * @return object
     */
    protected function requestInternalUserId()
    {
        $userOpenIdUrl = config('social_network.qq.user_open_id_url');

        $client = new Client();

        $res = $client->request('GET', sprintf($userOpenIdUrl.'?access_token=%s', $this->profile->getTokenData()->getAccessToken()));
        return json_decode($res->getBody());

    }
    /**
     * @param string $code
     * @param string $clientId
     * @param string $clientSecret
     * @param string $redirectUri
     * @return object
     */
    protected function requestAccessToken($code, $clientId, $clientSecret, $redirectUri)
    {
        $tokenUrl = config('social_network.qq.token_url');

        $client = new Client();
        $res = $client->request('GET', $tokenUrl.'?client_id='.$clientId.'&client_secret='.$clientSecret.'&code='.$code.'&grant_type=authorization_code&redirect_uri='.$redirectUri);

        return json_decode($res->getBody()->getContents());
    }

    /**
     * @return UserEntity
     */
    public function getUser()
    {
        return $this->buildUserEntity($this->requestUserInfo());
    }

    /**
     * @return mixed
     */
    protected function requestUserInfo(){

        $userDataUrl = config('social_network.qq.user_data_url');
        $oauthConsumerKey = config('social_network.qq.oauth_consumer_key');
        $client = new Client();

        $res = $client->request('GET', sprintf($userDataUrl.'?access_token=%s&oauth_consumer_key=%s&openid=%s', $this->profile->getTokenData()->getAccessToken(), $oauthConsumerKey,$this->profile->getInternalId()));

        $socialUserInfo = json_decode($res->getBody());
        if ($res->getStatusCode() == 200 && $socialUserInfo && is_object($socialUserInfo) && isset($socialUserInfo->access_token)) {
            return $socialUserInfo;
        }

        throw new OAuthServerErrorException(is_object($socialUserInfo) && $socialUserInfo->errmsg ? sprintf('Wechat auth server returned error: %s.', $socialUserInfo->errmsg) : 'Unknown wechat auth server error.');
    }

    /**
     * @param $socialUserInfo
     * @return UserEntity
     */
    public function buildUserEntity($socialUserInfo){
        $user = $this->container->make(UserEntityInterface::class);

        $user->setUsername($socialUserInfo->nickname);
        $user->setSex($socialUserInfo->gender);
        $user->setAvatar(!empty($socialUserInfo->figureurl_2) ? ($this->container->make(AvatarEntityInterface::class))->setOriginalURL($socialUserInfo->figureurl_2)->setThumbnailURL($socialUserInfo->figureurl_2) : null);
        $user->setPassword(str_random(40));
        $user->setPasswordHash(Hash::make($user->getPassword()));

        return $user;
    }
}