<?php


namespace Mildberry\Kangaroo\Libraries\SocialNetwork\Providers;

use GuzzleHttp\Client;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Facades\Hash;
use Mildberry\Kangaroo\Libraries\OAuth\Entities\TokenEntity;
use Mildberry\Kangaroo\Libraries\SocialNetwork\OAuthServerErrorException;
use Mildberry\Kangaroo\Libraries\SocialNetwork\SocialNetworkProfileEntity;
use Mildberry\Kangaroo\Libraries\SocialNetwork\SocialNetworkProviderInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Entity\Users\UserEntity;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\AvatarEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;


/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */
class WeiboSocialNetworkProvider implements SocialNetworkProviderInterface
{
    /**
     * @var
     */
    private $profile;

    /**
     * @var Container
     */
    protected $container;

    /**
     * WeiboSocialNetworkProvider constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * init request https://api.weibo.com/oauth2/authorize?client_id={client_id}&redirect_uri={redirect_uri}
     * api http://open.weibo.com/wiki/2/oauth2/authorize/en
     * api https://api.weibo.com/oauth2/access_token
     * @param $code
     * @return SocialNetworkProfileEntity
     */
    public function requestAccess($code)
    {
        $clientId = config('social_network.weibo.client_id');
        $clientSecret = config('social_network.weibo.client_secret');
        $redirectUri = config('social_network.weibo.redirect_uri');

        $socialTokenData = $this->requestAccessToken($code, $clientId, $clientSecret, $redirectUri);

        if ($socialTokenData && is_object($socialTokenData) && isset($socialTokenData->access_token)) {
            $tokenData = (new TokenEntity())->setAccessToken($socialTokenData->access_token)->setExpiresIn($socialTokenData->expires_in)->setTokenType('Bearer');
            $this->profile = new SocialNetworkProfileEntity();
            $this->profile->setInternalId($socialTokenData->uid)->setType('weibo')->setTokenData($tokenData);

            return $this->profile;
        }

        throw new OAuthServerErrorException(is_object($socialTokenData) && $socialTokenData->errmsg ? sprintf('Weibo auth server returned error: %s.', $socialTokenData->errmsg) : 'Unknown weibo auth server error.');
    }

    /**
     * @param string $code
     * @param string $clientId
     * @param string $clientSecret
     * @param string $redirectUri
     * @return object
     */
    protected function requestAccessToken($code, $clientId, $clientSecret, $redirectUri)
    {
        $tokenUrl = config('social_network.weibo.token_url');

        $client = new Client();
        $res = $client->request('GET', $tokenUrl.'?client_id='.$clientId.'&client_secret='.$clientSecret.'&code='.$code.'&grant_type=authorization_code&redirect_uri='.$redirectUri);

        return json_decode($res->getBody()->getContents());
    }

    /**
     * @return UserEntity
     */
    public function getUser()
    {
        return $this->buildUserEntity($this->requestUserInfo());
    }

    /**
     * @return mixed
     */
    protected function requestUserInfo(){
        $client = new Client();
        $userDataUrl = config('social_network.wechat.user_data_url');
        $res = $client->request('GET', sprintf($userDataUrl.'?access_token=%s&uid=%s', $this->profile->getTokenData()->getAccessToken(), $this->profile->getInternalId()));

        $socialUserInfo = json_decode($res->getBody());
        if ($res->getStatusCode() == 200 && $socialUserInfo && is_object($socialUserInfo) && isset($socialUserInfo->access_token)) {
            return $socialUserInfo;
        }

        throw new OAuthServerErrorException(is_object($socialUserInfo) && $socialUserInfo->errmsg ? sprintf('Weibo auth server returned error: %s.', $socialUserInfo->errmsg) : 'Unknown weibo auth server error.');
    }

    /**
     * @param $socialUserInfo
     * @return UserEntity
     */
    public function buildUserEntity($socialUserInfo){
        $user = $this->container->make(UserEntityInterface::class);

        $user->setUsername($socialUserInfo->screen_name);
        $user->setSex($socialUserInfo->gender);
        $user->setAvatar(!empty($socialUserInfo->profile_image_url) ? ($this->container->make(AvatarEntityInterface::class))->setOriginalURL($socialUserInfo->profile_image_url)->setThumbnailURL($socialUserInfo->profile_image_url) : null);
        $user->setPassword(str_random(40));
        $user->setPasswordHash(Hash::make($user->getPassword()));

        return $user;
    }
}