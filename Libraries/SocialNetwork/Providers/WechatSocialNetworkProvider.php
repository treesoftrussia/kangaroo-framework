<?php

namespace Mildberry\Kangaroo\Libraries\SocialNetwork\Providers;

use GuzzleHttp\Client;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Facades\Hash;
use Mildberry\Kangaroo\Libraries\OAuth\Entities\TokenEntity;
use Mildberry\Kangaroo\Libraries\SocialNetwork\OAuthServerErrorException;
use Mildberry\Kangaroo\Libraries\SocialNetwork\SocialNetworkProfileEntity;
use Mildberry\Kangaroo\Libraries\SocialNetwork\SocialNetworkProviderInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Entity\Users\UserEntity;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\AvatarEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;


class WechatSocialNetworkProvider implements SocialNetworkProviderInterface
{
    /**
     * @var
     */
    private $profile;

    /**
     * @var Container
     */
    protected $container;

    /**
     * WechatSocialNetworkProvider constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param $code
     * @return SocialNetworkProfileEntity
     */
    public function requestAccess($code)
    {
        $clientId = config('wechat.client_id');
        $clientSecret = config('wechat.client_secret');

        $client = new Client();
        $res = $client->request('GET', 'https://api.wechat.com/sns/oauth2/access_token?appid='.$clientId.'&secret='.$clientSecret.'&code='.$code.'&grant_type=authorization_code');

        $wechatTokenData = json_decode($res->getBody()->getContents());

        if ($res->getStatusCode() == 200 && $wechatTokenData && is_object($wechatTokenData) && isset($wechatTokenData->access_token)) {
            $tokenData = (new TokenEntity())->setAccessToken($wechatTokenData->access_token)->setExpiresIn($wechatTokenData->expires_in)->setTokenType('Bearer');
            $this->profile = new SocialNetworkProfileEntity();
            $this->profile->setInternalId($wechatTokenData->openid)->setType('wechat')->setTokenData($tokenData);

            return $this->profile;
        }

        throw new OAuthServerErrorException(is_object($wechatTokenData) && $wechatTokenData->errmsg ? sprintf('Wechat auth server returned error: %s.', $wechatTokenData->errmsg) : 'Unknown wechat auth server error.');
    }

    /**
     * @return UserEntity
     */
    public function getUser()
    {
        return $this->buildUserEntity($this->requestUserInfo());
    }

    /**
     * @return mixed
     */
    protected function requestUserInfo(){
        $client = new Client();

        //https://api.wechat.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID
        $res = $client->request('GET', sprintf('https://api.wechat.com/sns/userinfo?access_token=%s&openid=%s', $this->profile->getTokenData()->getAccessToken(), $this->profile->getInternalId()));

        $wechatUserInfo = json_decode($res->getBody());
        if ($res->getStatusCode() == 200 && $wechatUserInfo && is_object($wechatUserInfo)) {
            return $wechatUserInfo;
        }

        throw new OAuthServerErrorException(is_object($wechatUserInfo) && $wechatUserInfo->errmsg ? sprintf('Wechat auth server returned error: %s.', $wechatUserInfo->errmsg) : 'Unknown wechat auth server error.');
    }

    /**
     * @param $wechatUserInfo
     * @return UserEntity
     */
    public function buildUserEntity($wechatUserInfo){
        $user = $this->container->make(UserEntityInterface::class);

        $user->setUsername($wechatUserInfo->nickname);
        $user->setSex($wechatUserInfo->sex);
        $user->setAvatar(!empty($wechatUserInfo->headimgurl) ? ($this->container->make(AvatarEntityInterface::class))->setOriginalURL($wechatUserInfo->headimgurl)->setThumbnailURL($wechatUserInfo->headimgurl) : null);
        $user->setPassword(str_random(40));
        $user->setPasswordHash(Hash::make($user->getPassword()));

        return $user;
    }
}
