<?php

namespace Mildberry\Kangaroo\Libraries\SocialNetwork\DAL;

use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Facades\DB;
use Mildberry\Kangaroo\Libraries\Database\Repository\AbstractRepository;
use Mildberry\Kangaroo\Libraries\SocialNetwork\SocialNetworkProfileEntity;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserRepositoryInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class SocialNetworkRepository extends AbstractRepository
{

    /**
     * @var mixed
     */
    private $userRepository;

    /**
     * SocialNetworkRepository constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->userRepository = $container->make(UserRepositoryInterface::class);
    }

    /**
     * @param SocialNetworkProfileEntity $socialNetworkProfile
     *
     * @return \App\Core\UserManagement\Entity\Users\SocialNetworkProfileEntity
     */
    public function createSocialNetworkProfile(SocialNetworkProfileEntity $socialNetworkProfile)
    {
        $createdSocialNetworkProfileModel = UserSocialNetworkModel::create((new UserSocialNetworkFlattenAdapter())->transform($socialNetworkProfile));

        return (new SocialNetworkProfileAdapter())->transform(UserSocialNetworkModel::find($createdSocialNetworkProfileModel->id));
    }

    /**
     * @param UserEntityInterface $user
     * @param SocialNetworkProfileEntity $socialNetworkProfile
     * @return mixed
     */
    public function createOrUpdateIfExistsSocialNetworkEntry(UserEntityInterface $user, SocialNetworkProfileEntity $socialNetworkProfile)
    {
        $context = $this;



        return DB::transaction(function () use ($context, $user, $socialNetworkProfile) {
            $socialNetworkModel = UserSocialNetworkModel::whereType($socialNetworkProfile->getType())->whereInternalUserId($socialNetworkProfile->getInternalId())->lockForUpdate()->first();

            if (!$socialNetworkModel) {
                $user = $context->userRepository->create($user, ['username']);
                $socialNetworkProfile->setUserId($user->getId());
                $context->createSocialNetworkProfile($socialNetworkProfile);
            } else {
                $user = $context->userRepository->update($socialNetworkModel->user_id, $user, ['username']);
            }

            return $user;
        });
    }
}
