<?php

namespace Mildberry\Kangaroo\Libraries\SocialNetwork\DAL;

use Mildberry\Kangaroo\Libraries\Elegant\Elegant;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class UserSocialNetworkModel extends Elegant
{
    protected $table = 'user_social_networks_data';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
