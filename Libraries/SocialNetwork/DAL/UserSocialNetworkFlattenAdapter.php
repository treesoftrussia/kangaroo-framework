<?php
namespace Mildberry\Kangaroo\Libraries\SocialNetwork\DAL;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\SocialNetwork\SocialNetworkProfileEntity;

/**
 * Class UserSocialNetworkFlattenAdapter.
 */
class UserSocialNetworkFlattenAdapter extends AbstractAdapter
{

    /**
     * @param SocialNetworkProfileEntity $entity
     * @return array
     */
    public function transform(SocialNetworkProfileEntity $entity)
    {
        $sanitizer = $this->sanitizer()->make();

        return skip_empty([
            'internal_user_id' => $sanitizer($entity->getInternalId(), 'string'),
            'type' => $sanitizer($entity->getType(), 'string'),
            'user_id' => $sanitizer($entity->getUserId(), 'integer'),
        ], [null]);
    }
}
