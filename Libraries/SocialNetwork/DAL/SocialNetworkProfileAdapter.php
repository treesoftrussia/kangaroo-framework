<?php

namespace Mildberry\Kangaroo\Libraries\SocialNetwork\DAL;

use App\Core\UserManagement\Entity\Users\SocialNetworkProfileEntity;
use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;

/**
 * Class SocialNetworkProfileAdapter.
 */
class SocialNetworkProfileAdapter extends AbstractAdapter
{

    /**
     * @param UserSocialNetworkModel $model
     *
     * @return SocialNetworkProfileEntity
     */
    public function transform(UserSocialNetworkModel $model)
    {
        $entity = new SocialNetworkProfileEntity();
        $entity
            ->setId($model->id)
            ->setInternalId($model->user_internal_id)
            ->setType($model->type)
            ->setUserId($model->user_id)
        ;

        return $entity;
    }
}
