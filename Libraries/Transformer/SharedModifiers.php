<?php
namespace Mildberry\Kangaroo\Libraries\Transformer;
use Carbon\Carbon;
use DateTime;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class SharedModifiers 
{
    /**
     * Converts the provided date to the ISO8601 format
     *
     * @param DateTime $datetime
     * @return null|string
     */
    public function datetime(DateTime $datetime = null)
    {
        if ($datetime === null) {
            return null;
        }

        if (!$datetime instanceof Carbon) {
            $datetime = Carbon::instance($datetime);
        }

        return $datetime->toIso8601String();
    }
} 