<?php

namespace Mildberry\Kangaroo\Libraries\Transformer;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
interface TransformRulesInterface
{
    public function getResolvers();

    public function getSpecificationClassName();
}