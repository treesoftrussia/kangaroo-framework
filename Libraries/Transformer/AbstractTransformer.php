<?php namespace Mildberry\Kangaroo\Libraries\Transformer;

use Mildberry\Kangaroo\Libraries\Converter\Extractor\Extractor;
use Mildberry\Kangaroo\Libraries\Modifier\ModifierProviderTrait;
use League\Fractal\TransformerAbstract;

/**
 * This class is an extension to League transformers that gives extended functionality to transformers
 *
 * Class BaseTransformer
 */
abstract class AbstractTransformer extends TransformerAbstract
{
    use ModifierProviderTrait;

    /**
     * The shortcut method to extract data from the provided object
     *
     * @param $object
     * @param array $options
     * @return array
     */
    protected function extract($object, array $options = [])
    {
        return (new Extractor($options))
            ->setModifierManager($this->getModifierManager())
            ->extract($object);
    }
}