<?php
use Mildberry\Kangaroo\Libraries\Converter\Extractor\Extractor;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Cloner\VarCloner;
/**
 * Takes the value from the array or returns default if the value is not set
 * @param array|ArrayAccess $array
 * @param string $key
 * @param null $default
 * @return mixed
 * @throws RuntimeException
 */
function array_take($array, $key, $default = null)
{

    if (!is_array($array) && !$array instanceof ArrayAccess) {
        throw new RuntimeException('$array must be array or instance of ArrayAccess');
    }

    return isset($array[$key]) ? $array[$key] : $default;
}

/**
 * Takes the value from the object or returns default if the value is not set
 * @param object $object
 * @param string $key
 * @param null $default
 * @return mixed|null
 * @throws RuntimeException
 */
function object_take($object, $key, $default = null)
{
    if ($object === null) {
        return $default;
    }

    if (!is_object($object)) {
        throw new RuntimeException('$object must be object');
    }

    $keys = explode('.', $key);
    $currentKey = array_shift($keys);
    $getter = make_getter($currentKey);

    if (!method_exists($object, $getter)) {
        throw new  RuntimeException("Method '{$getter}' doesn't exists in object");
    }

    $value = call_user_func([$object, $getter]);

    if ((count($keys) > 0)) {
        if ($value === null) {
            return $default;
        }

        return object_take($value, implode('.', $keys), $default);
    }

    return $value;
}

/**
 * Removes empty values from array
 * @param array $array
 * @param array $emptiness - sets the values considered as empty.
 * By default, "null" and empty string is considered as an empty value.
 *
 * @return array
 */
function skip_empty(array $array, $emptiness = [null, ''])
{

    $data = [];

    foreach ($array as $key => $value) {

        if (in_array($value, $emptiness, true)) {
            continue;
        }

        $data[$key] = $value;
    }
    return $data;
}


/**
 * Function recursive converts all keys to lower and returns converted array.
 * @param $array
 * @return array
 */
function keystolower($array)
{
    $preparedArray = [];

    foreach ($array as $key => $value) {
        $key = strtolower($key);

        $preparedArray[$key] = is_array($value) ? (keystolower($value)) : ($value);
    }

    return $preparedArray;
}

/**
 * Function recursive converts all keys to camel case.
 * @param $array
 * @return array
 */
function keystocamelcase($array)
{
    $preparedArray = [];

    foreach ($array as $key => $value) {
        $key = camel_case($key);

        $preparedArray[$key] = is_array($value) ? (keystocamelcase($value)) : ($value);
    }

    return $preparedArray;
}

/**
 * The function determines whether the method is getter
 *
 * @param string $method
 * @param array $types
 * @return bool
 */
function is_getter($method, $types = ['get', 'is', 'has', 'can'])
{
    $typesStr = implode('|', $types);

    return (bool)preg_match("/^({$typesStr})[A-Z0-9]/", $method);
}


/**
 * The function determines whether the method is setter
 *
 * @param string $method
 * @return bool
 */
function is_setter($method)
{
    return (bool)preg_match('/^set[A-Z0-9]/', $method);
}


/**
 * The function resolves the property name from getter or setter
 *
 * @param $method
 * @param bool $real - if it is "true" the prefix "is" and "has" will be dropped
 * @return string
 * @throws RuntimeException
 */
function make_property($method, $real = false)
{
    $matches = [];

    if (!preg_match('/^(set|get|is|has|can)([A-Z0-9].*)/', $method, $matches)) {
        throw new RuntimeException('The method "' . $method . '" must be either getter or setter.');
    }

    if (!$real && in_array($matches[1], ['is', 'has', 'can'])) {
        return $method;
    }

    return lcfirst($matches[2]);
}

/**
 * Creates getter name from property name
 *
 * @param string $property
 * @return string
 * @throws RuntimeException
 */
function make_getter($property)
{
    if (preg_match('/^(is|has|can)[A-Z0-9]/', $property)) {
        return $property;
    }

    return 'get' . ucfirst($property);
}


/**
 * Creates setter name from property name
 *
 * @param string $property
 * @return string
 * @throws RuntimeException
 */
function make_setter($property)
{
    $matches = [];

    if (preg_match('/^(?:is|has|can)([A-Z0-9].*)/', $property, $matches)) {
        return 'set' . ucfirst($matches[1]);
    }

    return 'set' . ucfirst($property);
}


/**
 * Drops the namespace from class, method, function name
 *
 * @param string $longname
 * @return string
 */
function short_name($longname)
{
    if (strpos($longname, '\\') === false) {
        return $longname;
    }

    $longname = explode('\\', $longname);
    return array_pop($longname);
}

/**
 * Flattens a nested object into a single level array that uses "dot" notation to indicate depth
 * @param object $object
 * @return array
 */
function object_dot($object)
{
    return array_dot((new Extractor())->extract($object));
}


/**
 * Checks if arrays are equal
 *
 * @param array $array1
 * @param array $array2
 * @return bool
 */
function array_equal(array $array1, array $array2)
{
    sort($array1, SORT_STRING);
    sort($array2, SORT_STRING);
    return $array1 == $array2;
}

/**
 * Compares first string with the second one or with any of strings in an array
 *
 * @param string $string1
 * @param array|string $string2
 * @return bool
 */
function string_equal($string1, $string2)
{
    $string1 = strtolower($string1);

    if (is_string($string2)) {
        $string2 = [$string2];
    }

    $string2 = array_map('strtolower', $string2);

    return in_array($string1, $string2);
}

/**
 * Check if provided date string empty.
 *
 * @param string $string
 * @return bool
 */
function empty_date($string)
{
    return ($string == '0000-00-00 00:00:00' ||
        $string === null ||
        $string == '' ||
        $string == '0000-00-00');
}

/**
 * Detect if a value is traversable
 * @param $array
 * @return bool
 */
function is_traversable($array)
{
    return is_array($array) || $array instanceof Traversable;
}

/**
 * Detect if a value is array accessible
 *
 * @param $array
 * @return bool
 */
function is_arrayable($array)
{
    return is_array($array) || $array instanceof ArrayAccess;
}

/**
 * Flatten a multi-dimensional associative array with dots.
 * Unlike "array_dot", this  method stops flattening once it meets an array with numeric indexes starting with 0.
 *
 * @param  array $array
 * @param  string $prepend
 * @return array
 */
function array_smash(array $array, $prepend = '')
{
    $results = [];

    foreach ($array as $key => $value) {
        if (is_array($value) && !is_vector($value, false)) {
            $results = array_merge($results, array_smash($value, $prepend . $key . '.'));
        } else {
            $results[$prepend . $key] = $value;
        }
    }

    return $results;
}

/**
 * Creates nested array from array with dot notation.
 *
 * @param array $dotted
 * @return array
 */
function array_unsmash($dotted)
{
    $array = [];

    foreach ($dotted as $key => $value) {
        array_set($array, $key, $value);
    }

    return $array;
}

/**
 * Checks if an array is vector
 *
 * @param $array
 * @param bool $scalar_only
 * @return array|bool
 */
function is_vector($array, $scalar_only = true)
{
    if (!is_array($array)) {
        return false;
    }

    $next = 0;

    foreach ($array as $key => $value) {

        if ($scalar_only && !is_scalar($value) && $value !== null) {
            return false;
        }

        if ($key !== $next) {
            return false;
        }

        $next++;
    }

    return true;
}


/**
 * @param array|Traversable $vector
 * @param string $field
 * @return array
 */
function objects_vector($vector, $field = 'id')
{
    if (!is_traversable($vector)) {
        throw new RuntimeException('$vector should be is array or Traversable');
    }

    $array = [];

    $getter = make_getter($field);

    foreach ($vector as $item) {
        $array[] = $item->{$getter}();
    }

    return $array;
}

/**
 * Cuts off the specified substring from the beginning of the specified string
 *
 * @param string $string
 * @param string $unwanted
 * @return string
 */
function cut_string_left($string, $unwanted)
{
    return substr($string, strlen($unwanted));
}

/**
 * Cuts off the specified substring from the end of the specified string
 *
 * @param string $string
 * @param string $unwanted
 * @return string
 */
function cut_string_right($string, $unwanted)
{
    return substr($string, 0, strlen($string) - strlen($unwanted));
}


/**
 * Merge two dotted array
 * @param array $array1
 * @param array $array2
 * @return array
 */
function dotted_array_merge(array $array1, array $array2)
{
    foreach ($array2 as $key2 => $value2) {
        foreach (array_keys($array1) as $key1) {
            if (starts_with($key1, $key2)) {
                unset($array1[$key1]);
            }
        }

        $array1[$key2] = $value2;
    }

    return $array1;
}

/**
 * Dump the passed variables and end the script.
 *
 * @param  mixed
 * @return void
 */
function ddc()
{

    array_map(function ($x) {
        (new CliDumper)->dump((new VarCloner)->cloneVar($x));
    }, func_get_args());

    die(1);
}