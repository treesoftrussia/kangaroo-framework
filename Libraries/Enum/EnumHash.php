<?php
namespace Mildberry\Kangaroo\Libraries\Enum;

use RuntimeException;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
abstract class EnumHash
{
    /**
     * @var array
     */
    private $hash = [];

    /**
     * @return string
     */
    abstract public function getEnumClass();

    /**
     * @param Enum $key
     * @param mixed $value
     * @return $this
     */
    public function set(Enum $key, $value)
    {
        $this->verifyEnum($key);
        $this->hash[$key->value()] = $value;

        return $this;
    }

    /**
     * @param Enum $key
     * @param mixed $default
     * @return mixed
     */
    public function get(Enum $key, $default = null)
    {
        return $this->has($key) ? $this->hash[$key->value()] : $default;
    }

    /**
     * @param Enum $key
     * @return bool
     */
    public function has(Enum $key)
    {
        $this->verifyEnum($key);
        return array_key_exists($key->value(), $this->hash);
    }

    /**
     * @return Enum[]
     */
    public function keys()
    {
        $keys = array_keys($this->hash);
        $objects = [];
        $enumClass = $this->getEnumClass();

        foreach ($keys as $key) {
            $objects[] = new $enumClass($key);
        }

        return $objects;
    }

    /**
     * @param Enum $key
     * @return bool
     * @throws RuntimeException
     */
    private function verifyEnum(Enum $key)
    {
        $valueClass = get_class($key);
        $enumClass = $this->getEnumClass();

        if (!($key instanceof $enumClass) || !call_user_func([$enumClass, 'has'], $key->value())) {
            throw new RuntimeException("Key should be '{$enumClass}' but received '{$valueClass}'");
        }
    }

}