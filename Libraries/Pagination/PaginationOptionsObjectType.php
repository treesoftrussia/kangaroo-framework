<?php
namespace Mildberry\Kangaroo\Libraries\Pagination;

use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class PaginationOptionsObjectType extends ObjectType
{
    /**
     * @return array
     */
    public function structure()
    {
        return [
            'pageNum' => new IntegerType(false, true),
            'perPage' => new IntegerType(),

        ];
    }
}