<?php
namespace Mildberry\Kangaroo\Libraries\Pagination;

interface PaginationInterface
{
    public function getTotalEntries();

    public function getEntriesOnCurrentPage();

    public function getEntriesPerPageRequested();

    public function getCurrentPage();

    public function getTotalPages();
}
