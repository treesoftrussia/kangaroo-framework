<?php
namespace Mildberry\Kangaroo\Libraries\Pagination;

class Pagination implements PaginationInterface
{
    /**
     * @var
     */
    private $totalEntries;

    /**
     * @var
     */
    private $entriesOnCurrentPage;

    /**
     * @var
     */
    private $entriesPerPageRequested;

    /**
     * @var
     */
    private $currentPage;

    /**
     * @var
     */
    private $totalPages;

    /**
     * @return mixed
     */
    public function getTotalEntries()
    {
        return $this->totalEntries;
    }

    /**
     * @param $totalEntries
     *
     * @return $this
     */
    public function setTotalEntries($totalEntries)
    {
        $this->totalEntries = $totalEntries;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntriesOnCurrentPage()
    {
        return $this->entriesOnCurrentPage;
    }

    /**
     * @param $total
     * @param $current
     * @param $entriesPerPage
     *
     * @return $this
     */
    public function setEntriesOnCurrentPage($total, $current, $entriesPerPage, $totalPages)
    {
        if ($total <= $entriesPerPage && $current <= $totalPages) {
            $this->entriesOnCurrentPage = $total;

            return $this;
        }
        if ($current * $entriesPerPage <= $total) {
            $this->entriesOnCurrentPage = $entriesPerPage;

            return $this;
        }
        if ($total < $entriesPerPage * ($current - 1)) {
            $this->entriesOnCurrentPage = 0;

            return $this;
        }
        $this->entriesOnCurrentPage = $entriesPerPage - ($current * $entriesPerPage - $total);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntriesPerPageRequested()
    {
        return $this->entriesPerPageRequested;
    }

    /**
     * @param $entriesPerPageRequested
     *
     * @return $this
     */
    public function setEntriesPerPageRequested($entriesPerPageRequested)
    {
        $this->entriesPerPageRequested = $entriesPerPageRequested;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @param $currentPage
     *
     * @return $this
     */
    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPages()
    {
        return $this->totalPages;
    }

    /**
     * @param $totalPages
     *
     * @return $this
     */
    public function setTotalPages($totalPages)
    {
        $this->totalPages = $totalPages;

        return $this;
    }
}
