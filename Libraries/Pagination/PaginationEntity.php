<?php

namespace Mildberry\Kangaroo\Libraries\Pagination;

use Illuminate\Support\Collection;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class PaginationEntity implements PaginationEntityInterface
{
    /**
     * @var Collection
     */
    private $items;

    /**
     * @var Pagination
     */
    private $pagination;

    public function getItems()
    {
        return $this->items;
    }

    public function setItems(Collection $items)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return Pagination
     */
    public function getPagination()
    {
        return $this->pagination;
    }

    /**
     * @param Pagination $pagination
     *
     * @return $this
     */
    public function setPagination(Pagination $pagination)
    {
        $this->pagination = $pagination;

        return $this;
    }
}
