<?php
namespace Mildberry\Kangaroo\Libraries\Pagination;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
interface PaginationEntityInterface
{
    public function getItems();

    public function getPagination();
}
