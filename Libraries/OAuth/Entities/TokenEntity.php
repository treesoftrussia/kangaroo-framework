<?php

namespace Mildberry\Kangaroo\Libraries\OAuth\Entities;

use Mildberry\Kangaroo\Libraries\Cast\Cast;

class TokenEntity
{
    /**
     * @var
     */
    private $userId;
    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var int
     */
    private $expiresIn;

    /**
     * @var string
     */
    private $tokenType;

    /**
     * @var string
     */
    private $redirectURL;

    /**
     * @param array $tokenData
     *
     * @return TokenEntity
     */
    public static function makeFromArray(array $tokenData)
    {
        $entity = new self();
        $entity
            ->setAccessToken($tokenData['access_token'])
            ->setTokenType($tokenData['token_type'])
            ->setExpiresIn($tokenData['expires_in'])
            ->setUserId($tokenData['userId'])
        ;

        if (isset($tokenData['redirectURL'])) {
            $entity->setRedirectURL($tokenData['redirectURL']);
        }

        return $entity;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     *
     * @return TokenEntity
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = Cast::string($accessToken);

        return $this;
    }

    /**
     * @return int
     */
    public function getExpiresIn()
    {
        return $this->expiresIn;
    }

    /**
     * @param int $expiresIn
     *
     * @return $this
     */
    public function setExpiresIn($expiresIn)
    {
        $this->expiresIn = Cast::int($expiresIn);

        return $this;
    }

    /**
     * @return string
     */
    public function getTokenType()
    {
        return $this->tokenType;
    }

    /**
     * @param string $tokenType
     *
     * @return TokenEntity
     */
    public function setTokenType($tokenType)
    {
        $this->tokenType = Cast::string($tokenType);

        return $this;
    }

    /**
     * @return string
     */
    public function getRedirectURL()
    {
        return $this->redirectURL;
    }

    /**
     * @param string $redirectURL
     *
     * @return $this
     */
    public function setRedirectURL($redirectURL)
    {
        $this->redirectURL = Cast::string($redirectURL);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }
}
