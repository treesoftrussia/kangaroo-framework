<?php

namespace Mildberry\Kangaroo\Libraries\OAuth\Entities;

class WebLogoutResponseEntity
{
    /**
     * @var
     */
    private $redirectUrl;

    /**
     * @return mixed
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * @param mixed $redirectUrl
     *
     * @return WebLogoutResponseEntity
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;

        return $this;
    }
}
