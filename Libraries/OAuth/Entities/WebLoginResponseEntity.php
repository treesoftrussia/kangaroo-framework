<?php

namespace Mildberry\Kangaroo\Libraries\OAuth\Entities;

use Mildberry\Kangaroo\Libraries\Cast\Cast;

class WebLoginResponseEntity
{
    /**
     * @var
     */
    private $status;

    /**
     * @var
     */
    private $redirectUrl;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     *
     * @return WebLoginResponseEntity
     */
    public function setStatus($status)
    {
        $this->status = Cast::int($status);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * @param mixed $redirectUrl
     *
     * @return WebLoginResponseEntity
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;

        return $this;
    }
}
