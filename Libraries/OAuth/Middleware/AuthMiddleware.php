<?php

namespace Mildberry\Kangaroo\Libraries\OAuth\Middleware;

use Closure;
use Illuminate\Contracts\Container\Container;
use League\OAuth2\Server\Exception\InvalidRequestException;
use LucaDegasperi\OAuth2Server\Authorizer;
use LucaDegasperi\OAuth2Server\Middleware\OAuthMiddleware;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\AccessDeniedException;
use Mildberry\Kangaroo\Libraries\Service\UserServiceInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Entity\Users\GuestUserEntity;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class AuthMiddleware extends OAuthMiddleware
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * AuthMiddleware constructor.
     * @param Container $container
     * @param Authorizer $authorizer
     * @param bool $httpHeadersOnly
     */
    public function __construct(Container $container, Authorizer $authorizer, $httpHeadersOnly = false)
    {
        parent::__construct($authorizer, $httpHeadersOnly);

        $this->container = $container;
    }

    /**
     * @param $request
     * @return GuestUserEntity
     */
    protected function buildGuestUser($request)
    {
        $guestUser = new GuestUserEntity();
        return $guestUser;
    }

    /**
     * @return bool|string
     */
    protected function determineAccessToken()
    {
        try {
            return $this->authorizer->getChecker()->determineAccessToken($this->httpHeadersOnly);
        } catch (InvalidRequestException $e) {
            return false;
        }
    }

    /**
     * @param $request
     * @return array
     */
    protected function getRouteScopes($request)
    {
        $action = $request->route()->getAction();
        $scopes = [];
        if (isset($action['scopes'])) {
            $scopes = (is_array($action['scopes'])) ? $action['scopes'] : explode('+', $action['scopes']);
        }

        return $scopes;
    }

    /**
     * @param $token
     * @return bool
     */
    protected function isTokenValid($token)
    {
        try {
            return $this->authorizer->getChecker()->isValidRequest($this->httpHeadersOnly, $token);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param null $scopesString
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next, $scopesString = null)
    {
        /**
         * @var UserServiceInterface $userService
         */
        $userService = $this->container->make(UserServiceInterface::class);

        //if already set then skip all checks (useful for PHPUNIT tests)
        if ($userService->getCurrentUser()) {
            return $next($request);
        }

        $this->authorizer->setRequest($request);
        $token = $this->determineAccessToken();

        //If token exists, try to validate it
        if ($token) {
            //If valid and not expired
            if ($this->isTokenValid($token)) {
                //If restricted by routes for authentificated users then throw access denied
                if (!empty($action['restrictedForAuthorized'])) {
                    throw new AccessDeniedException('Endpoint is not allowed for authorized users.');
                }

                //Retrieve User by Resource Owner Id from database
                $user = $userService->getUserById($this->authorizer->getResourceOwnerId());
            } else {
                //if not valid token but route unprotected then login user as guest
                if (empty($this->getRouteScopes($request))) {
                    $userService->setCurrentUser($this->tryToBuildGuestUser($request));
                    return $next($request);
                }
                //else throw denied exception
                throw new AccessDeniedException();
            }
        } else {
            //retrieve current user as Guest
            $user = $this->tryToBuildGuestUser($request);
        }

        //Validate token for scopes existence required by route
        $this->validateScopes($this->getRouteScopes($request));

        $userService->setCurrentUser($user);

        return $next($request);
    }

    /**
     * @param $request
     * @return GuestUserEntity
     */
    protected function tryToBuildGuestUser($request)
    {
        $action = $request->route()->getAction();
        if (!empty($action['restrictedForGuests'])) {
            throw new AccessDeniedException('Endpoint is not allowed for guests');
        }

        return $this->buildGuestUser($request);
    }
}
