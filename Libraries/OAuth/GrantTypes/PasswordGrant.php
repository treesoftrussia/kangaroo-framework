<?php
namespace Mildberry\Kangaroo\Libraries\OAuth\GrantTypes;

use League\OAuth2\Server\Grant\PasswordGrant as BasePasswordGrant;
use League\OAuth2\Server\Exception;
use League\OAuth2\Server\Event;
use League\OAuth2\Server\Entity\ClientEntity;
use League\OAuth2\Server\Entity\SessionEntity;
use League\OAuth2\Server\Entity\AccessTokenEntity;
use League\OAuth2\Server\Util\SecureKey;
use League\OAuth2\Server\Entity\RefreshTokenEntity;
use Mildberry\Kangaroo\Libraries\Service\UserServiceInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;

/**
 * Password grant class.
 */
class PasswordGrant extends BasePasswordGrant
{
    private $userService;

    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Complete the password grant
     *
     * @return array
     *
     * @throws
     */
    public function completeFlow()
    {
        // Get the required params
        $clientId = $this->server->getRequest()->request->get('client_id', $this->server->getRequest()->getUser());
        if (is_null($clientId)) {
            throw new Exception\InvalidRequestException('client_id');
        }

        $clientSecret = $this->server->getRequest()->request->get('client_secret', $this->server->getRequest()->getPassword());
        if (is_null($clientSecret)) {
            throw new Exception\InvalidRequestException('client_secret');
        }

        $login = $this->server->getRequest()->request->get('login', null);
        if (is_null($login)) {
            throw new Exception\InvalidRequestException('login');
        }

        $password = $this->server->getRequest()->request->get('password', null);
        if (is_null($password)) {
            throw new Exception\InvalidRequestException('password');
        }

        // Check if user's username and password are correct
        $user = $this->userService->getUserByCredentials($login, $password);

        if (is_null($user)) {
            $this->server->getEventEmitter()->emit(new Event\UserAuthenticationFailedEvent($this->server->getRequest()));
            throw new Exception\InvalidCredentialsException();
        }

        $client = $this->getClient($clientId, $clientSecret);

        // Validate any scopes that are in the request
        $scopeParam = $this->server->getRequest()->request->get('scope', '');


        return $this->generateAccessToken($client, $user, $scopeParam);
    }

    /**
     * @param $clientId
     * @param $clientSecret
     * @return ClientEntity|null
     * @throws Exception\InvalidClientException
     */
    public function getClient($clientId, $clientSecret)
    {
        // Validate client ID and client secret
        $client = $this->server->getClientStorage()->get(
            $clientId,
            $clientSecret,
            null,
            $this->getIdentifier()
        );

        if (($client instanceof ClientEntity) === false) {
            $this->server->getEventEmitter()->emit(new Event\ClientAuthenticationFailedEvent($this->server->getRequest()));
            throw new Exception\InvalidClientException();
        }

        return $client;
    }

    /**
     * @param ClientEntity $client
     * @param UserEntityInterface $user
     * @param array $scopes
     * @return array
     */
    public function generateAccessToken(ClientEntity $client, UserEntityInterface $user, $scopeParam = '')
    {
        // Set current user for scope storage
        $this->server->getScopeStorage()->setCurrentUser($user);

        // Set default scopes if was not defined in request
        $this->server->requireScopeParam(false);
        $this->server->setDefaultScope($user->getScopeIds());

        $scopes = $this->validateScopes($scopeParam, $client);

        // Create a new session
        $session = new SessionEntity($this->server);
        $session->setOwner('user', $user->getId());
        $session->associateClient($client);

        // Generate an access token
        $accessToken = new AccessTokenEntity($this->server);
        $accessToken->setId(SecureKey::generate());
        $accessToken->setExpireTime($this->getAccessTokenTTL() + time());

        // Associate scopes with the session and access token
        foreach ($scopes as $scope) {
            $session->associateScope($scope);
        }

        foreach ($session->getScopes() as $scope) {
            $accessToken->associateScope($scope);
        }

        $this->server->getTokenType()->setSession($session);
        $this->server->getTokenType()->setParam('access_token', $accessToken->getId());
        $this->server->getTokenType()->setParam('expires_in', $this->getAccessTokenTTL());

        // Associate a refresh token if set
        if ($this->server->hasGrantType('refresh_token')) {
            $refreshToken = new RefreshTokenEntity($this->server);
            $refreshToken->setId(SecureKey::generate());
            $refreshToken->setExpireTime($this->server->getGrantType('refresh_token')->getRefreshTokenTTL() + time());
            $this->server->getTokenType()->setParam('refresh_token', $refreshToken->getId());
        }

        // Save everything
        $session->save();
        $accessToken->setSession($session);
        $accessToken->save();

        if ($this->server->hasGrantType('refresh_token')) {
            $refreshToken->setAccessToken($accessToken);
            $refreshToken->save();
        }

        return $this->server->getTokenType()->generateResponse();
    }
}
