<?php
namespace Mildberry\Kangaroo\Libraries\OAuth\ExceptionMiddleware;

use Closure;
use League\OAuth2\Server\Exception\OAuthException;
use Illuminate\Http\Response;
use LucaDegasperi\OAuth2Server\Exceptions\NoActiveAccessTokenException;
use Mildberry\Kangaroo\Libraries\Resource\Error;
use Mildberry\Kangaroo\Libraries\Resource\RequestExceptionData;
use Mildberry\Resolvers\Traits\ConfigAwareTrait;

class OAuth
{

    private $error;

    public function __construct(Error $error)
    {
        $this->error = $error;
    }

    /**
     * @param RequestExceptionData $data
     * @param Closure              $next
     *
     * @return Response
     */
    public function handle($data, Closure $next)
    {
        if ($data->getException() instanceof OAuthException) {
            switch ($data->getException()->errorType) {
                case 'invalid_request':
                    if (!$data->getRequest()->isJson()) {
                        return redirect(config('oauth2.redirect_unauthorized_uri', '/'));
                    }
                    $code = Error::PERMISSION_DENIED;

                    return $this->error->write($data->getException()->getMessage(), $code, Response::HTTP_UNAUTHORIZED);
                case 'invalid_credentials':
                    $code = Error::INVALID_CREDENTIALS;

                    return $this->error->write($data->getException()->getMessage(), $code, Response::HTTP_UNAUTHORIZED);
                case 'invalid_client':
                    $code = Error::INVALID_CLIENT;

                    return $this->error->write($data->getException()->getMessage(), $code, Response::HTTP_UNAUTHORIZED);
                case 'invalid_scope':
                    $code = Error::VALIDATION_BODY_ERROR;

                    return $this->error->write($data->getException()->getMessage(), $code, Response::HTTP_UNAUTHORIZED);
                case 'unsupported_grant_type':
                    $code = Error::INVALID_GRANT_TYPE;

                    return $this->error->write($data->getException()->getMessage(), $code, Response::HTTP_UNAUTHORIZED);
                case 'access_denied':
                    if (!$data->getRequest()->isJson()) {
                        return redirect(config('oauth2.redirect_unauthorized_uri', '/'));
                    }
                    $code = Error::PERMISSION_DENIED;

                    return $this->error->write($data->getException()->getMessage(), $code, Response::HTTP_UNAUTHORIZED);
            }
        }

        if ($data->getException() instanceof NoActiveAccessTokenException) {
            if (!$data->getRequest()->isJson()) {
                return redirect(config('oauth2.redirect_unauthorized_uri', '/'));
            }
            $code = Error::PERMISSION_DENIED;

            return $this->error->write($data->getException()->getMessage(), $code, Response::HTTP_UNAUTHORIZED);
        }

        return $next($data);
    }
}
