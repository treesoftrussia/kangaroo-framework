<?php
namespace Mildberry\Kangaroo\Libraries\OAuth\Core;

use League\OAuth2\Server\ResourceServer as LeagueResourceServer;
use League\OAuth2\Server\Exception\InvalidRequestException;
use League\OAuth2\Server\TokenType\MAC;

class ResourceServer extends LeagueResourceServer
{
    public function determineAccessToken($headerOnly = false)
    {
        if ($this->getRequest()->headers->get('Authorization') !== null) {
            $accessToken = $this->getTokenType()->determineAccessTokenInHeader($this->getRequest());
        } else {
            $accessToken = $this->getRequest()->cookies->get('jwtToken');
        }

        if (empty($accessToken)) {
            throw new InvalidRequestException('access token');
        }

        return $accessToken;
    }
}