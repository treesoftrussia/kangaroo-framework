<?php
namespace Mildberry\Kangaroo\Libraries\OAuth\Core;

use League\OAuth2\Server\Entity\ScopeEntity;
use LucaDegasperi\OAuth2Server\Storage\FluentScope as BaseFluentScope;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;

/**
 * This is the fluent scope class.
 *
 * @author Luca Degasperi <packages@lucadegasperi.com>
 */
class FluentScope extends BaseFluentScope
{
    /**
     * @var
     */
    private $currentUser;

    /**
     * @param UserEntityInterface $user
     */
    public function setCurrentUser(UserEntityInterface $user)
    {
        $this->currentUser = $user;
    }

    /**
     * Return information about a scope.
     *
     * Example SQL query:
     *
     * <code>
     * SELECT * FROM oauth_scopes WHERE scope = :scope
     * </code>
     *
     * @param string $scope     The scope
     * @param string $grantType The grant type used in the request (default = "null")
     * @param string $clientId  The client id used for the request (default = "null")
     *
     * @return \League\OAuth2\Server\Entity\ScopeEntity|null If the scope doesn't exist return false
     */
    public function get($scope, $grantType = null, $clientId = null)
    {
        $query = $this->getConnection()->table('oauth_scopes')
                    ->select('oauth_scopes.id as id', 'oauth_scopes.description as description')
                    ->where('oauth_scopes.id', $scope);

        if ($this->limitClientsToScopes === true && !is_null($clientId)) {
            $query = $query->join('oauth_client_scopes', 'oauth_scopes.id', '=', 'oauth_client_scopes.scope_id')
                           ->where('oauth_client_scopes.client_id', $clientId);
        }

        if ($this->limitScopesToGrants === true && !is_null($grantType)) {
            $query = $query->join('oauth_grant_scopes', 'oauth_scopes.id', '=', 'oauth_grant_scopes.scope_id')
                           ->join('oauth_grants', 'oauth_grants.id', '=', 'oauth_grant_scopes.grant_id')
                           ->where('oauth_grants.id', $grantType);
        }

        if (!is_null($this->currentUser)) {
            $query = $query->join('role_oauth_scopes', 'oauth_scopes.id', '=', 'role_oauth_scopes.oauth_scope_id')
                ->join('user_roles', 'role_oauth_scopes.role_id', '=', 'user_roles.role_id')
                ->where('user_roles.user_id', $this->currentUser->getId());
        }

        $result = $query->first();

        if (is_null($result)) {
            return;
        }

        $scope = new ScopeEntity($this->getServer());
        $scope->hydrate([
            'id' => $result->id,
            'description' => $result->description,
        ]);

        return $scope;
    }
}
