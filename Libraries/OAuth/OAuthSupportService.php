<?php

namespace Mildberry\Kangaroo\Libraries\OAuth;

use Illuminate\Http\Request;
use League\OAuth2\Server\Exception\InvalidCredentialsException;
use Mildberry\Kangaroo\Libraries\OAuth\Entities\TokenEntity;
use Mildberry\Kangaroo\Libraries\OAuth\Entities\WebLoginEntity;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Symfony\Component\HttpFoundation\ParameterBag;

class OAuthSupportService
{
    /**
     * @param WebLoginEntity $webLoginEntity
     *
     * @return TokenEntity
     *
     * @throws InvalidCredentialsException
     */
    public function getTokenByPasswordGrantType(WebLoginEntity $webLoginEntity)
    {
        $requestData = [
            'grant_type' => 'password',
            'client_id' => config('params.web_client_id'),
            'client_secret' => config('params.web_client_secret'),
            'login' => $webLoginEntity->getLogin(),
            'password' => $webLoginEntity->getPassword(),
        ];

        return $this->makeTokenEntity((array) $this->requestToken($requestData));
    }

    public function getTokenByActivationCodeGrantType($credentials)
    {
        $requestData = [
            'grant_type' => 'activation_code',
            'client_id' => config('params.mobile_client_id'),
            'client_secret' => config('params.mobile_client_secret'),
            'phone' => $credentials['phone'],
            'activation_code' => $credentials['activation_code'],
        ];

        return $this->makeTokenEntity((array) $this->requestToken($requestData));
    }

    private function requestToken($requestData)
    {
        $oauthRequest = Request::create('/oauth/access_token', 'POST', $requestData);
        $oauthRequest->request = new ParameterBag($requestData);

        $response = app()->handle($oauthRequest);

        $responseData = json_decode($response->getContent());

        if (empty($responseData->access_token)) {
            throw new InvalidCredentialsException('The user credentials were incorrect');
        }

        return $responseData;
    }

    public function getTokenByGrantType($credentialsEntity, $grantType = 'password')
    {
        switch ($grantType) {
            case 'password':
                return $this->getTokenByPasswordGrantType($credentialsEntity);
            case 'activation_code':
                return $this->getTokenByActivationCodeGrantType($credentialsEntity);
        }
        throw new RuntimeException('Wrong grant type.');
    }

    private function makeTokenEntity(array $tokenData)
    {
        $tokenEntity = new TokenEntity();
        $tokenEntity->setAccessToken($tokenData['access_token']);
        $tokenEntity->setExpiresIn($tokenData['expires_in']);
        $tokenEntity->setTokenType($tokenData['token_type']);

        return $tokenEntity;
    }
}
