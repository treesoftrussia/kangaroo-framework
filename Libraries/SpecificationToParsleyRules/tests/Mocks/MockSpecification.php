<?php
namespace Mildberry\Kangaroo\Libraries\SpecificationToParsleyRules\tests\Mocks;
use Mildberry\Kangaroo\Libraries\Specification\ASpecification;
use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class MockSpecification extends ASpecification
{

    public function body(){
        return new ObjectType([
            'a' => new StringType(false, true),
            'b' => new IntegerType(false, true),
        ], false, true);
    }

}