<?php
namespace Mildberry\Kangaroo\Libraries\SpecificationToParsleyRules\tests;

use Mildberry\Kangaroo\Libraries\SpecificationToParsleyRules\ParsleyRules;
use Mildberry\Kangaroo\Libraries\SpecificationToParsleyRules\tests\Mocks\MockSpecification;
use Mildberry\Kangaroo\Tests\Libraries\Processor\TranslatorMock;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Foundation\Application;
use Illuminate\Validation\Factory as ValidationFactory;
use Illuminate\Contracts\Validation\Factory as FactoryContract;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class BaseTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    private $app;

    public function setUp()
    {
        parent::setUp();

        $this->app = new Application();

        $this->faker = Factory::create();
    }

    private function createContainer()
    {
        $container = new Container();
        $container->bind(FactoryContract::class, ValidationFactory::class);
        $container->bind(TranslatorInterface::class, TranslatorMock::class);

        Container::setInstance($container);
    }

    public function testExtractRulesFrom()
    {

        ParsleyRules::makeFromSpecification(new MockSpecification($this->createContainer()));
    }
}