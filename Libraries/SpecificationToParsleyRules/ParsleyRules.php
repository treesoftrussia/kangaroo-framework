<?php namespace Mildberry\Kangaroo\Libraries\SpecificationToParsleyRules;

use Mildberry\Kangaroo\Libraries\Converter\Support\Scope;
use Mildberry\Kangaroo\Libraries\Specification\ASpecification;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class ParsleyRules
{
    public static function makeFromSpecification(ASpecification $specification){
        $body = $specification->getBody();

        $scope = new Scope();

        $structure = $body->getStructure();

        foreach ($structure as $key => $item){
            $scope->create($key);


        }

    }
}