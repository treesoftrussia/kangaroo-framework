<?php
namespace Mildberry\Kangaroo\Libraries\Elegant;

use Illuminate\Database\Eloquent\Collection;
use Mildberry\Kangaroo\Libraries\Elegant\Separator\Separator;
use Mildberry\Kangaroo\Libraries\Elegant\Sorting\AbstractSortSpecification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Traversable;
use Mildberry\Kangaroo\Libraries\Elegant\Collection as ElegantCollection;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
abstract class Elegant extends Model implements CollectionAwareInterface
{
    use CollectionAwareTrait;

    protected $dateFormat = 'U';

    const DB_FALSE = 0;
    const DB_TRUE = 1;

    const SORT_ASC = 'asc';
    const SORT_DESC = 'desc';

    /**
     * By default we do not require the fields be protected from mass-assignment.
     * However, it's up to a developer to cleanup the fields before sending to the database.
     * @var array
     */
    protected $guarded = [];

    /**
     * Remove duplicated join statements from query
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeDropEqualJoins(Builder $query)
    {
        $joins = (new JoinsNormalizer($query->getQuery()->joins))->process();

        if (count($joins) != count($query->getQuery()->joins)) {
            $query->getQuery()->joins = $joins;
        }

        return $query;
    }

    /**
     * Extend Builder with custom method to apply filters
     *
     * @param Builder $query
     * @param AbstractModifier $modifier
     * @return Builder
     */
    public function scopeModify(Builder $query, AbstractModifier $modifier)
    {
        return $modifier->modify($query);
    }

    /**
     * @param Builder $query
     * @param AbstractSortSpecification $specification
     * @param string $sortBy
     * @param string $sortDirection
     * @return Builder
     */
    public function scopeSort(Builder $query, AbstractSortSpecification $specification, $sortBy, $sortDirection = self::SORT_ASC)
    {
        return $specification->sort($query, $sortBy, $sortDirection);
    }

    /**
     * Updates an entry by  primary key
     * @param $id
     * @param array $data
     * @return mixed
     */
    public static function refresh($id, array $data)
    {
        $model = new static();
        return $model->where($model->getKeyName(), $id)->update($data);
    }

    /**
     * Deletes an entry by primary key
     * @param $id
     */
    public static function wipeout($id)
    {
        $model = new static();
        $model->where($model->getKeyName(), $id)->delete();
    }

    /**
     * @param array|Traversable $data
     * @param array $shared
     * @return bool
     */
    public static function insertAll($data, $shared = [])
    {
        foreach ($data as $key => $row) {
            $data[$key] = array_merge($row, $shared);
        }

        return static::insert($data);
    }

    /**
     * Checks whether an entry exists by primary key
     *
     * @param int $id
     * @return bool
     */
    public static function available($id)
    {
        $model = new static();
        return $model->where($model->getKeyName(), $id)->exists();
    }


    /**
     * Creates the collection
     *
     * @param array $models
     * @return Collection
     */
    public function newCollection(array $models = [])
    {
        return new Collection($models);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function __get($key)
    {
        if (method_exists($this, $key)
            && $this->hasCollection()
            && !$this->getCollection()->loaded($key)
        ) {
            $this->getCollection()->load($key);
        }

        return parent::__get($key);
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        // TODO: Should be cached for table
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->table);
    }

    /**
     * @param mixed $query
     * @param array $config can receive 'ignore' and 'map' fields
     * @return Elegant
     */
    public static function fromMixed($query, $config = [])
    {
        return (new Separator($config))->separate($query, get_called_class());
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getCreatedAtAttribute($value)
    {
        return $value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getUpdatedAtAttribute($value)
    {
        return $value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getDeletedAtAttribute($value)
    {
        return $value;
    }

    /**
     * @param $key
     * @return bool
     */
    public function relationLoadedAndExists($key){
        return $this->relationLoaded($key) && $this->getRelationValue($key);
    }

    /**
     * @param $key
     * @param bool $collectionAsEmptyValue
     * @return Collection|mixed|null
     */
    public function loadedRelationData($key, $collectionAsEmptyValue = true){

        if($this->relationLoaded($key)){
            return $this->getRelationValue($key);
        }

        return $collectionAsEmptyValue ? new Collection() : null;
    }
}
