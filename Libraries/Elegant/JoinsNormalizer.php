<?php
namespace Mildberry\Kangaroo\Libraries\Elegant;

use Illuminate\Database\Query\JoinClause;

/**
 * Clear duplicated join statements
 *
 * Class JoinsNormalizer
 * @package Mildberry\Kangaroo\Libraries\Elegant
 */
class JoinsNormalizer
{
    /**
     * @var array
     */
    private $joins;

    /**
     * @param $joins
     */
    public function __construct($joins)
    {
        $this->joins = $joins;
    }

    /**
     * Init process of duplicates finding
     * @return array
     */
    public function process()
    {
        if($this->isOperationRequired())
        {
            $this->compareHashes();
            if($this->isOperationRequired())
            {
                $this->compareObjects();
            }
        }
        return $this->joins;
    }

    /**
     * Check if there is possibility to find duplicates
     * @return bool
     */
    private function isOperationRequired()
    {
        if(count($this->joins) < 2)
        {
            return false;
        }

        $joinedTables = [];
        foreach($this->joins as $joinClause)
        {
            $joinedTables[$joinClause->table] = true;
        }

        if(count($this->joins) == count($joinedTables))
        {
            return false;
        }

        return true;
    }

    /**
     * Fast and rough cleaning basing on objects md5 hash.
     * Removing only exactly the same JoinClause objects.
     */
    private function compareHashes()
    {
        $hashes = [];
        $joins = $this->joins;
        if ($joins != null) {
            foreach($joins as $i => $join)
            {
                $hash = md5(serialize($join));
                if(!in_array($hash, $hashes))
                {
                    $hashes[] = $hash;
                }
                else
                {
                    unset($this->joins[$i]);
                }
            }
        }
    }

    /**
     * Remove all equal JoinClause objects even if they have an different hash sum.
     */
    private function compareObjects()
    {
        $uniqueJoins = [];
        $joins = $this->joins;
        $uniqueJoins[] = $this->applySort($joins[0]);

        for($i = 1; $i < count($joins); $i++)
        {
            $join = $this->applySort($joins[$i]);
            $isUnique = true;
            foreach($uniqueJoins as $uJoin)
            {
                if($uJoin == $join)
                {
                    $isUnique = false;
                    break;
                }
            }
            if($isUnique)
            {
                $uniqueJoins[] = $join;
            }
        }
        $this->joins = $uniqueJoins;
    }

    /**
     * Apply sort to every Object clause to exclude possible
     * conflicts of 'first' and 'second' fields
     *
     * @param JoinClause $join
     * @return JoinClause
     */
    private function applySort(JoinClause &$join)
    {
        foreach($join->clauses as $i => $clause)
        {
            if(strcmp($clause['first'], $clause['second']) < 0)
            {
                $first = $clause['first'];
                $clause['first'] = $clause['second'];
                $clause['second'] = $first;
                $join->clauses[$i] = $clause;
            }
        }
        $clauses = $join->clauses;

        usort($clauses, function($a, $b)
            {
                return strcmp($a['first'], $b['first']);
            }
        );
        $join->clauses = $clauses;

        return $join;
    }

    /**
     * @param array $joins
     * @return JoinsNormalizer
     */
    public function setJoins(array $joins)
    {
        $this->joins = $joins;
        return $this;
    }
}