<?php
namespace Mildberry\Kangaroo\Libraries\Elegant;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
trait CollectionAwareTrait
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * @param Collection $collection
     */
    public function setCollection(Collection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @return bool
     */
    public function hasCollection()
    {
        return $this->collection !== null;
    }
} 