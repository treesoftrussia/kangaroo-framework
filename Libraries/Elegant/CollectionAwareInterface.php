<?php
namespace Mildberry\Kangaroo\Libraries\Elegant;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
interface CollectionAwareInterface
{
    /**
     * @param Collection $collection
     */
    public function setCollection(Collection $collection);

    /**
     * @return bool
     */
    public function hasCollection();

    /**
     * @return Collection
     */
    public function getCollection();
} 