<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Support;

use Illuminate\Database\Eloquent\Model;
use RuntimeException;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
trait VerifyModelTrait
{
    /**
     * @return string|string[]
     */
    abstract protected function getSupportedModelClasses();

    /**
     * Verified correctness of the provided model
     *
     * @param Model $model
     * @throws RuntimeException
     */
    private function verifyModel(Model $model)
    {
        $classes = $this->getSupportedModelClasses();

        if (!is_array($classes)) {
            $classes = [$classes];
        }

        foreach ($classes as $class) {
            if (is_a($model, $class)) {
                return;
            }
        }

        throw new RuntimeException('The model must be instance of one of the specified model classes in the "'
            . get_called_class() . '" modifier. The instance of "' . get_class($model) . '" has been given.');
    }
}