<?php
namespace Mildberry\Kangaroo\Libraries\Elegant;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Collection extends EloquentCollection
{
    /**
     * Contains relations that have been loaded already
     * @var array
     */
    private $loaded = [];

    /**
     * @param array $items
     */
    public function __construct($items = array())
    {
        $this->tryAppectCollection($items);

        parent::__construct($items);
    }

    /**
     * @param  mixed  $item
     * @return $this
     */
    public function add($item)
    {
        $this->tryAppectCollection($item);

        return parent::add($item);
    }

    /**
     * Tries to Appect a collection
     *
     * @param $items
     */
    private function tryAppectCollection($items)
    {
        if (!is_array($items)){
            $items = [$items];
        }

        foreach ($items as $item){
            if ($item instanceof CollectionAwareInterface){
                $item->setCollection($this);
            }
        }
    }

    /**
     * Checks whether the relation has been loaded already
     *
     * @param string $relation
     * @return bool
     */
    public function loaded($relation)
    {
        return isset($this->loaded[$relation]);
    }

    /**
     * @param string|array $relations
     * @return $this
     */
    public function load($relations)
    {
        parent::load($relations);

        if (is_string($relations)) {
            $relations = func_get_args();
        }

        foreach ($relations as $relation) {
            $this->loaded[$relation] = true;
        }

        return $this;
    }
} 