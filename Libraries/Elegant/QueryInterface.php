<?php
namespace Mildberry\Kangaroo\Libraries\Elegant;

/**
 * Provides a common interface for all query objects
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
interface QueryInterface
{
    /**
     * Prepares the query
     * @param object $query
     * @return object
     */
    public function prepare($query);
} 