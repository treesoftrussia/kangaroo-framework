<?php
namespace Mildberry\Kangaroo\Libraries\Elegant;

use Mildberry\Kangaroo\Libraries\Elegant\Support\VerifyModelTrait;
use Illuminate\Database\Eloquent\Builder;
use ReflectionObject;
use RuntimeException;
use ReflectionMethod;

/**
 * Update provided query to filter and order values
 *
 * @author Maxim Sergeev<raincoven@gmail.com>
 */
abstract class AbstractModifier
{
    use VerifyModelTrait;

    /**
     * @var ReflectionObject
     */
    private $reflection;

    /**
     * @var object
     */
    private $filter;

    /**
     * @param object $filter
     */
    public function __construct($filter)
    {
        $this->filter = $filter;
        $this->reflection = new ReflectionObject($filter);
    }

    /**
     * @param Builder $query
     * @return Builder
     * @throws RuntimeException
     */
    public function modify(Builder $query)
    {
        $this->verifyModel($query->getModel());

        $this->initialize($query);

        $methods = $this->reflection->getMethods(ReflectionMethod::IS_PUBLIC);

        foreach ($methods as $method){

            $name = $method->getName();

            if (!is_getter($name)){
                continue ;
            }

            $result = $method->invoke($this->filter);

            if ($result === null){
                continue ;
            }

            $apply = 'apply'.ucfirst(make_property($name));

            if (!method_exists($this, $apply)){
                throw new RuntimeException('The handler for the "'.$name.'" filter has not been found.');
            }

            call_user_func([$this, $apply], $query, $result);
        }

        return $query;
    }

    /**
     * @param Builder $query
     */
    protected function initialize(Builder $query) {}
}