<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Sorting;

use Mildberry\Kangaroo\Libraries\Elegant\Elegant;
use Mildberry\Kangaroo\Libraries\Elegant\Sorting\Resolvers\AbstractResolver;
use Mildberry\Kangaroo\Libraries\Elegant\Sorting\Resolvers\CallableResolver;
use Mildberry\Kangaroo\Libraries\Elegant\Sorting\Resolvers\NestedResolver;
use Mildberry\Kangaroo\Libraries\Elegant\Sorting\Resolvers\StringResolver;
use Mildberry\Kangaroo\Libraries\Elegant\Support\VerifyModelTrait;
use Illuminate\Database\Eloquent\Builder;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
abstract class AbstractSortSpecification
{
    use VerifyModelTrait;

    /**
     * @var AbstractResolver[]
     */
    private $resolvers;

    /**
     * @return array
     */
    abstract public function specification();

    /**
     * @param Builder $query
     * @param string $sortKey
     * @param string $direction
     * @return Builder
     * @throws SortSpecificationException
     */
    public function sort(Builder $query, $sortKey, $direction)
    {
        $this->verifyModel($query->getModel());

        $breaker = new Breaker($sortKey, $this->specification());

        $specificationKey = $breaker->getSpecificationKey();

        if (!$specificationKey) {
            $this->throwUnsupportedKeyException($sortKey);
        }

        if (!$this->checkDirection($direction)) {
            throw new SortSpecificationException('The direction \'' . $direction . '\' is unsupported.');
        }

        $nestedKey = $breaker->getSubSpecificationKey();

        $value = $this->specification()[$specificationKey];

        foreach ($this->getResolvers() as $resolver) {
            if ($resolver->canResolve($value, $nestedKey)) {
                $resolver->resolve($query, $value, $direction, $nestedKey);

                return $query;
            }
        }

        $this->throwUnsupportedKeyException($sortKey, $specificationKey);

        return $query;
    }


    /**
     * Check if direction allowed
     * @param string $direction
     * @return bool
     */
    private function checkDirection($direction)
    {
        $allowed = [Elegant::SORT_ASC, Elegant::SORT_DESC];

        return in_array(strtolower($direction), $allowed);
    }

    /**
     * Throws an exception with message
     *
     * @param $sortKey
     * @param null $specificationKey
     * @throws SortSpecificationException
     */
    private function throwUnsupportedKeyException($sortKey, $specificationKey = null)
    {
        $defaultMessage = "The key '{$sortKey}' is unsupported.";
        $specificationMessage = 'No specification key has been matched.';

        if ($specificationKey){
            $specificationMessage = "The '{$specificationKey}' specification key has been matched.";
        }

        throw new SortSpecificationException($defaultMessage.' '.$specificationMessage);
    }

    /**
     * @return AbstractResolver[]
     */
    private function getResolvers()
    {
        if ($this->resolvers === null) {
            $this->resolvers = [
                new StringResolver($this),
                new CallableResolver($this),
                new NestedResolver($this)
            ];
        }

        return $this->resolvers;
    }
}