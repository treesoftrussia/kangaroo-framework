<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Sorting;

use Exception;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class SortSpecificationException extends Exception
{

}