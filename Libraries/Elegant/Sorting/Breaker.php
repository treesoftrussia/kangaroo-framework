<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Sorting;
use Mildberry\Kangaroo\QA\Endpoints\Specification\SpecificationException;

/**
 * Breaks the key into 2 pieces where the first piece matches the key from the specification
 * and the second piece matches the rest of the key
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Breaker 
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var array
     */
    private $specification;

    /**
     * @var string
     */
    private $specificationKey;

    /**
     * @var bool
     */
    private $matchedAlready = false;

    /**
     * @param string $key
     * @param array $specification
     */
    public function __construct($key, array $specification)
    {
        $this->key = $key;
        $this->specification = $specification;
    }

    /**
     * Returns the 1st part
     *
     * @return null|string
     * @throws SpecificationException
     */
    public function getSpecificationKey()
    {
        if ($this->matchedAlready === false){

            $this->specificationKey = $this->matchSpecificationKey(explode('.', $this->key));
            $this->matchedAlready = true;
        }

        return $this->specificationKey;
    }

    /**
     * Matches the 1st part
     *
     * @param array $keys
     * @return string
     */
    private function matchSpecificationKey(array $keys)
    {
        $path = '';

        foreach ($keys as $key) {

            $path .= $key;

            if (array_key_exists($path, $this->specification)) {
                return $path;
            }

            $path .= '.';
        }

        return null;
    }

    /**
     * Returns the 2nd part
     *
     * @return null|string
     */
    public function getSubSpecificationKey()
    {
        $specificationKey = $this->getSpecificationKey();

        if ($this->key === $specificationKey){
            return null;
        }

        return cut_string_left($this->key, "{$specificationKey}.");
    }
} 