<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Sorting\Resolvers;

use Mildberry\Kangaroo\Libraries\Elegant\Sorting\AbstractSortSpecification;
use Illuminate\Database\Eloquent\Builder;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class NestedResolver extends AbstractResolver
{
    /**
     * @param mixed $value
     * @param string $nestedKey
     * @return bool
     */
    public function canResolve($value, $nestedKey = null)
    {
        return $value instanceof AbstractSortSpecification && $nestedKey !== null;
    }

    /**
     * @param Builder $query
     * @param mixed $value
     * @param string $direction
     * @param string $nestedKey
     */
    public function resolve(Builder $query, $value, $direction, $nestedKey = null)
    {
        $value->sort($query, $nestedKey, $direction);
    }
}