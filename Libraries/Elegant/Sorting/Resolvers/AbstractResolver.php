<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Sorting\Resolvers;

use Mildberry\Kangaroo\Libraries\Elegant\Sorting\AbstractSortSpecification;
use Illuminate\Database\Eloquent\Builder;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
abstract class AbstractResolver
{
    /**
     * @var AbstractSortSpecification
     */
    private $specification;

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @param AbstractSortSpecification $specification
     * @param array $config
     */
    public function __construct(AbstractSortSpecification $specification,
                                array $config = [])
    {
        $this->specification = $specification;
        $this->config = $config;
    }

    /**
     * @param mixed $value
     * @param null|string $nestedKey
     * @return bool
     */
    abstract public function canResolve($value, $nestedKey = null);

    /**
     * @param Builder $query
     * @param mixed $value
     * @param null|string $nestedKey
     * @param string $direction
     * @return
     */
    abstract public function resolve(Builder $query, $value, $direction, $nestedKey = null);
}