<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Sorting\Resolvers;

use Illuminate\Database\Eloquent\Builder;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class StringResolver extends AbstractResolver
{
    /**
     * @param mixed $value
     * @param null $nestedKey
     * @return bool
     */
    public function canResolve($value, $nestedKey = null)
    {
        return is_string($value) && $nestedKey === null;
    }

    /**
     * @param Builder $query
     * @param mixed $value
     * @param string $direction
     * @param null|string $nestedKey
     */
    public function resolve(Builder $query, $value, $direction, $nestedKey = null)
    {
        $query->orderBy($value, $direction);
    }
}