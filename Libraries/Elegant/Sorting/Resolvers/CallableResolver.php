<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Sorting\Resolvers;

use Illuminate\Database\Eloquent\Builder;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class CallableResolver extends AbstractResolver
{

    /**
     * @param $nestedKey
     * @param mixed $value
     * @return bool
     * @internal param string $field
     */
    public function canResolve($value, $nestedKey = null)
    {
        return is_callable($value);
    }

    /**
     * @param Builder $query
     * @param callable $value
     * @param string $direction
     * @param null|string $nestedKey
     */
    public function resolve(Builder $query, $value, $direction, $nestedKey = null)
    {
        $value($query, $direction, $nestedKey);
    }
}