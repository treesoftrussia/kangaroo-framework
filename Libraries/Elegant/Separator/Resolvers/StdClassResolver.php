<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Separator\Resolvers;

use Generator;
use Illuminate\Database\Eloquent\Collection;
use stdClass;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class StdClassResolver extends AbstractResolver
{
    /**
     * @param mixed $query
     * @return Generator|null
     */
    public function resolve($query)
    {
        yield (array)$query;
    }

    /**
     * @param Collection $collection
     * @return mixed
     */
    public function value(Collection $collection)
    {
        return $collection->first();
    }

    /**
     * @param mixed $query
     * @return bool
     */
    public function canResolve($query)
    {
        return $query instanceof stdClass;
    }
}