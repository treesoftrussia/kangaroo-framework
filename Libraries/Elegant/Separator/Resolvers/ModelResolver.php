<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Separator\Resolvers;

use Generator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ModelResolver extends AbstractResolver
{
    /**
     * @param Collection $collection
     * @return mixed
     */
    public function value(Collection $collection)
    {
        return $collection->first();
    }

    /**
     * @param mixed $query
     * @return bool
     */
    public function canResolve($query)
    {
        return $query instanceof Model;
    }

    /**
     * @param mixed $query
     * @return Generator|null
     */
    public function resolve($query)
    {
        yield $query->toArray();
    }
}