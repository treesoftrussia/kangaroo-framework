<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Separator\Resolvers;

use Generator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class CollectionResolver extends AbstractResolver
{
    /**
     * @param mixed $query
     * @return Generator|null
     */
    public function resolve($query)
    {
        foreach ($query as $item) {
            /**
             * @var Model $item
             */
            yield $item->toArray();
        }
    }

    /**
     * @param mixed $query
     * @return bool
     */
    public function canResolve($query)
    {
        return $query instanceof Collection;
    }
}