<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Separator\Resolvers;

use Generator;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class TraversableResolver extends AbstractResolver
{
    /**
     * @param mixed $query
     * @return Generator|null
     */
    public function resolve($query)
    {
        foreach ($query as $row) {
            yield (array)$row;
        }
    }

    /**
     * @param mixed $query
     * @return bool
     */
    public function canResolve($query)
    {
        return is_traversable($query);
    }
}