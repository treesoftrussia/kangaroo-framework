<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Separator\Resolvers;

use Illuminate\Database\Eloquent\Collection;
use Generator;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
abstract class AbstractResolver
{
    /**
     * @param mixed $query
     * @return bool
     */
    abstract public function canResolve($query);

    /**
     * @param mixed $query
     * @return Generator|null
     */
    abstract public function resolve($query);

    /**
     * @param Collection $collection
     * @return mixed
     */
    public function value(Collection $collection)
    {
        return $collection;
    }
}