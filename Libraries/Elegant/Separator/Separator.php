<?php
namespace Mildberry\Kangaroo\Libraries\Elegant\Separator;

use Mildberry\Kangaroo\Libraries\Elegant\Elegant;
use Mildberry\Kangaroo\Libraries\Elegant\Separator\Resolvers\AbstractResolver;
use Mildberry\Kangaroo\Libraries\Elegant\Separator\Resolvers\CollectionResolver;
use Mildberry\Kangaroo\Libraries\Elegant\Separator\Resolvers\ModelResolver;
use Mildberry\Kangaroo\Libraries\Elegant\Separator\Resolvers\StdClassResolver;
use Mildberry\Kangaroo\Libraries\Elegant\Separator\Resolvers\TraversableResolver;
use Illuminate\Database\Eloquent\Collection;
use RuntimeException;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class Separator
{
    /**
     * @var array
     */
    private $map = [];

    /**
     * @var array
     */
    private $ignore = [];

    /**
     * @var array
     */
    private $resolvers = [
        ModelResolver::class,
        CollectionResolver::class,
        StdClassResolver::class,
        TraversableResolver::class
    ];

    /**
     * @param $config
     */
    public function __construct($config)
    {
        $this->ignore = array_take($config, 'ignore', []);
        $this->map = array_take($config, 'map', []);
    }

    /**
     * @param mixed $query
     * @param string $classModel
     * @return Elegant|Collection
     */
    public function separate($query, $classModel)
    {
        $resolver = $this->findResolver($query);
        $collection = new Collection();

        foreach ($resolver->resolve($query) as $item) {
            /**
             * @var Elegant $row
             */
            $row = new $classModel();

            $columns = $row->getColumns();

            foreach ($item as $key => $value) {
                if (in_array($key, $this->ignore)) {
                    continue;
                }

                $key = array_take($this->map, $key, $key);

                if (in_array($key, $columns)) {
                    $row->setAttribute($key, $value);
                }
            }

            $collection->push($row);
        }

        return $resolver->value($collection);
    }

    /**
     * @param $query
     * @return AbstractResolver
     */
    private function findResolver($query)
    {
        /**
         * @var AbstractResolver $resolver
         */
        foreach ($this->resolvers as $className) {
            $resolver = new $className();

            if ($resolver->canResolve($query)) {
                return $resolver;
            }
        }

        throw new RuntimeException('Cannot determine source query.');
    }
}