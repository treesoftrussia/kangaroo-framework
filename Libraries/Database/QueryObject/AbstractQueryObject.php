<?php

namespace Mildberry\Kangaroo\Libraries\Database\QueryObject;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
abstract class AbstractQueryObject
{
    /**
     * @var mixed
     */
    public $query;

    public function __construct($query = null)
    {
        if(!is_null($query)){
            $this->query = $query;
        } else {
            $this->query = $this->getQuery();
        }
    }

    /**
     * @return mixed
     */
    protected abstract function getQuery();

    /**
     * @return string
     */
    protected function selectFieldsExpression(){
        return '*';
    }

    /**
     * @var array
     */
    protected $sortingFieldMap = [];

}