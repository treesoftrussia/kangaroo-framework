<?php
namespace Mildberry\Kangaroo\Libraries\Database\QueryObject\Modifiers;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
trait PaginationModifyTrait
{

    protected function applyPagination($options){
        if ($options->getPagination()) {
            $this->query->forPage($options->getPagination()->getPageNum(), $options->getPagination()->getPerPage());
        }

        return $this;
    }
}