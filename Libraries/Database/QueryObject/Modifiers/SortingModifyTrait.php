<?php
namespace Mildberry\Kangaroo\Libraries\Database\QueryObject\Modifiers;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
trait SortingModifyTrait
{
    /**
     * @param $options
     * @return $this
     */
    protected function applySorting($options){

        if($options->getSorting()) {
            $askedField = $options->getSorting()->getField();
            $field = array_key_exists($askedField, $this->sortingFieldMap)?$this->sortingFieldMap[$askedField]:$askedField;
            $this->query->orderBy($field, $options->getSorting()->getDirection());
        }

        return $this;
    }


}