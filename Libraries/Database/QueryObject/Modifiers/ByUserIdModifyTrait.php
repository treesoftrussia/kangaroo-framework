<?php
namespace Mildberry\Kangaroo\Libraries\Database\QueryObject\Modifiers;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
trait ByUserIdModifyTrait
{
    /**
     * @var $int
     */
    protected $byUserId;

    /**
     * @param $userId
     * @return $this
     */
    public function byUserId($userId){
        $this->byUserId = $userId;
        return $this;
    }

    protected function applyByUserId(){
        if($this->byUserId){
            $this->query->where('user_id', $this->byUserId);
        }

        return $this;
    }
}