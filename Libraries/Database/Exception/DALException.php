<?php

namespace Mildberry\Kangaroo\Libraries\Database\Exception;

use Exception;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class DALException extends Exception
{

}