<?php

namespace Mildberry\Kangaroo\Libraries\Database\Exception;


/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class EntityNotFoundDALException extends DALException
{

}