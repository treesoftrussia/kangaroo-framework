<?php

namespace Mildberry\Kangaroo\Libraries\Database\Exception;

use Exception;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class FieldNotUniqueDALException extends DALException
{

    /**
     * @var string
     */
    private $fieldName;

    /**
     * FieldNotUniqueDALException constructor.
     * @param string $fieldName
     * @param string $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($fieldName, $message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct('Field '.$fieldName.' is already in use', $code, $previous);

        $this->fieldName = $fieldName;
    }

    /**
     * @return string
     */
    public function getFieldName(){
        return $this->fieldName;
    }

}