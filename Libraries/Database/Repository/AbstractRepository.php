<?php

namespace Mildberry\Kangaroo\Libraries\Database\Repository;
use Illuminate\Contracts\Container\Container;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
abstract class AbstractRepository
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * AbstractRepository constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }
}