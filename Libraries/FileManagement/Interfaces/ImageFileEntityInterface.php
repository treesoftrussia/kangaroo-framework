<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\Interfaces;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface ImageFileEntityInterface extends FileEntityInterface
{
    /**
     * @return string
     */
    public function getOriginalURL();

    /**
     * @param mixed $originalURL
     *
     * @return $this
     */
    public function setOriginalURL($originalURL);

    /**
     * @return mixed
     */
    public function getThumbnailURL();

    /**
     * @param mixed $thumbnailURL
     *
     * @return $this
     */
    public function setThumbnailURL($thumbnailURL);
}
