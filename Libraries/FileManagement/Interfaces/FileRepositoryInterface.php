<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\Interfaces;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Database\Exception\EntityNotFoundDALException;
use Mildberry\Kangaroo\Libraries\Options\Objects\FileOptions;

/**
 * @author Evgeny Novoselov <e.novosyolov@mildberry.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface FileRepositoryInterface
{
    /**
     * @param FileOptions $options
     * @return Collection
     */
    public function getAll(FileOptions $options = null);
    
    /**
     * @param int $id
     *
     * @return FileEntityInterface
     * @throws EntityNotFoundDALException
     */
    public function get($id);

    /**
     * @param FileEntityInterface $object
     *
     * @return FileEntityInterface
     */
    public function create(FileEntityInterface $object);

    /**
     * @param int        $id
     * @param FileEntityInterface $object
     *
     * @return FileEntityInterface
     * @throws EntityNotFoundDALException
     */
    public function update($id, FileEntityInterface $object);

    /**
     * @param int $id
     *
     * @return int
     */
    public function delete($id);
}
