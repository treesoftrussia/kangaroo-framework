<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\Interfaces;

use Mildberry\Kangaroo\Libraries\FileManagement\Exception\MakeThumbnailFileException;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface ImageFileServiceInterface extends FileServiceInterface
{
    /**
     * @param string $imageFileName
     * @param int $width
     * @param int $height
     * @return string
     * @throws MakeThumbnailFileException
     */
    public function getThumbnailContent($imageFileName, $width, $height);

    /**
     * @param string $path
     * @return bool
     */
    public function existsThumbnail($path);
}
