<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\Interfaces;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface FileEntityInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getFilename();

    /**
     * @param string $filename
     *
     * @return $this
     */
    public function setFilename($filename);

    /**
     * @return string
     */
    public function getPublicURL();

    /**
     * @param string $publicURL
     *
     * @return $this
     */
    public function setPublicURL($publicURL);

    /**
     * @return string
     */
    public function getDiskName();

    /**
     * @param string $storageName
     *
     * @return $this
     */
    public function setDiskName($storageName);

    /**
     * @return boolean
     */
    public function isRemote();

    /**
     * @param boolean $isRemote
     *
     * @return $this
     */
    public function setRemote($isRemote);

    /**
     * @return array
     */
    public function getMetaData();

    /**
     * @param array $metaData
     *
     * @return $this
     */
    public function setMetaData($metaData);

    /**
     * @return string
     */
    public function getType();

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType($type);
}
