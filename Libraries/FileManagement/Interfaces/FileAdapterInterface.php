<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\Interfaces;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface FileAdapterInterface
{
    /**
     * @param FileModelInterface $model
     *
     * @return FileEntityInterface
     */
    public function transform($model = null);

    /**
     * @param EloquentCollection $collection
     *
     * @return Collection
     */
    public function transformCollection(EloquentCollection $collection);
}
