<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\Interfaces;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface FileFlattenAdapterInterface
{
    /**
     * @param FileEntityInterface $fileEntity
     *
     * @return array
     */
    public function transform($fileEntity = null);
}
