<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\Interfaces;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\FileManagement\Exception\UploadFileException;
use Mildberry\Kangaroo\Libraries\Options\Objects\FileOptions;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface FileServiceInterface
{
    /**
     * @param FileOptions $options
     * @return Collection
     */
    public function getAll(FileOptions $options = null);

    /**
     * @param $id
     *
     * @return FileEntityInterface
     */
    public function get($id);

    /**
     * @param FileEntityInterface $fileEntity
     *
     * @return FileEntityInterface
     */
    public function create(FileEntityInterface $fileEntity);

    /**
     * @param $id
     * @param FileEntityInterface $fileEntity
     *
     * @return FileEntityInterface
     */
    public function update($id, FileEntityInterface $fileEntity);

    /**
     * @param $id
     *
     * @return bool
     */
    public function delete($id);

    /**
     * @param UploadedFile $file
     * @return FileEntityInterface
     * @throws UploadFileException
     */
    public function uploadFile(UploadedFile $file = null);

    /**
     * @param string $url
     * @return FileEntityInterface
     */
    public function uploadFileFromURL($url);

    /**
     * @param string $path
     * @return bool
     */
    public function exists($path);
}
