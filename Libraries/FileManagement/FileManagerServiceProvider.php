<?php


namespace Mildberry\Kangaroo\Libraries\FileManagement;

use Illuminate\Support\ServiceProvider;
use Mildberry\Kangaroo\Libraries\FileManagement\DAL\Adapter\BaseFileAdapter;
use Mildberry\Kangaroo\Libraries\FileManagement\DAL\FlattenAdapter\BaseFileFlattenAdapter;
use Mildberry\Kangaroo\Libraries\FileManagement\DAL\Model\BaseFileModel;
use Mildberry\Kangaroo\Libraries\FileManagement\DAL\Repository\BaseFileRepository;
use Mildberry\Kangaroo\Libraries\FileManagement\Entity\BaseFileEntity;
use Mildberry\Kangaroo\Libraries\FileManagement\Entity\BaseImageFileEntity;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileAdapterInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileEntityInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileFlattenAdapterInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileModelInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileRepositoryInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileServiceInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\ImageFileEntityInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\ImageFileServiceInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Service\BaseFileService;
use Mildberry\Kangaroo\Libraries\FileManagement\Service\BaseImageFileService;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class FileManagerServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FileAdapterInterface::class, BaseFileAdapter::class);
        $this->app->bind(FileEntityInterface::class, BaseFileEntity::class);
        $this->app->bind(FileFlattenAdapterInterface::class, BaseFileFlattenAdapter::class);
        $this->app->bind(FileModelInterface::class, BaseFileModel::class);
        $this->app->singleton(FileRepositoryInterface::class, BaseFileRepository::class);
        $this->app->singleton(FileServiceInterface::class, BaseFileService::class);
        $this->app->bind(ImageFileEntityInterface::class, BaseImageFileEntity::class);
        $this->app->singleton(ImageFileServiceInterface::class, BaseImageFileService::class);
    }

    /**
     * @return array
     */
    public function provides()
    {
        //TODO: Не работает, разобраться по чему
        return [
            //ImageServiceProvider::class,
        ];
    }
}
