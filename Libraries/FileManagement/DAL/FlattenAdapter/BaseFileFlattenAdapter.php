<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\DAL\FlattenAdapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileEntityInterface;

/**
 * @author Evgeny Novoselov <e.novosyolov@mildberry.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class BaseFileFlattenAdapter extends AbstractAdapter
{
    /**
     * @param FileEntityInterface $fileEntity
     *
     * @return array
     */
    public function transform($fileEntity = null)
    {
        $sanitizer = $this->sanitizer()->make();

        return skip_empty([
            'filename' => $sanitizer($fileEntity->getFilename(), 'string'),
            'public_url' => $sanitizer($fileEntity->getPublicURL(), 'string'),
            'is_remote' => $sanitizer($fileEntity->isRemote(), 'bool'),
            'disk_name' => $sanitizer($fileEntity->getDiskName(), 'string'),
            'meta_data' => $sanitizer(json_encode($fileEntity->getMetaData(), JSON_UNESCAPED_SLASHES), 'string'),
            'type' => $sanitizer($fileEntity->getType(), 'string'),
        ], [null]);
    }
}
