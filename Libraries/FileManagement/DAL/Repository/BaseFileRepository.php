<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\DAL\Repository;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Database\Repository\AbstractRepository;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileAdapterInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileEntityInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileFlattenAdapterInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileModelInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Mildberry\Kangaroo\Libraries\Database\Exception\EntityNotFoundDALException;
use Mildberry\Kangaroo\Libraries\Options\Objects\FileOptions;

/**
 * @author Evgeny Novoselov <e.novosyolov@mildberry.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class BaseFileRepository extends AbstractRepository implements FileRepositoryInterface
{
    /**
     * @param FileOptions $options
     * @return Collection
     */
    public function getAll(FileOptions $options = null)
    {
        $model = $this->createFileModel();
        $query = $model::query();

        if($options->getType()) {
            $query->where('type', $options->getType());
        }

        return $this->createFileAdapter()->transformCollection($query->get());
    }

    /**
     * @param int $id
     * @return mixed
     * @throws EntityNotFoundDALException
     */
    public function get($id)
    {
        $model = $this->createFileModel();
        $file = $model::find($id);

        if (!$file) {
            throw new EntityNotFoundDALException('File not found');
        }

        return $this->createFileAdapter()->transform($file);
    }

    /**
     * @param FileEntityInterface $fileEntity
     *
     * @return FileEntityInterface
     */
    public function create(FileEntityInterface $fileEntity)
    {
        $model = $this->createFileModel();
        $fileModel = DB::transaction(function () use ($model, $fileEntity) {
            return $model::create($this->createFileFlattenAdapter()->transform($fileEntity));
        });

        return $this->createFileAdapter()->transform($fileModel);
    }

    /**
     * @param int        $id
     * @param FileEntityInterface $fileEntity
     *
     * @return FileEntityInterface
     *
     * @throws EntityNotFoundDALException
     */
    public function update($id, FileEntityInterface $fileEntity)
    {
        DB::transaction(function () use ($id, $fileEntity) {
            $model = $this->createFileModel();
            $fileModel = $model::whereId($id)->lockForUpdate()->first();
            if (!$fileModel) {
                throw new EntityNotFoundDALException('File not found');
            }

            $fileModel->update($this->createFileFlattenAdapter()->transform($fileEntity));
        });

        return $this->get($id);
    }

    /**
     * @param int $id
     * @return int
     */
    public function delete($id)
    {
        DB::transaction(function () use ($id) {
            $model = $this->createFileModel();
            $fileModel = $model::whereId($id)->lockForUpdate()->first();
            if (!$fileModel) {
                throw new EntityNotFoundDALException('File not found');
            }

            $fileModel->delete();
        });
    }

    /**
     * @return FileAdapterInterface
     */
    protected function createFileAdapter()
    {
        return $this->container->make(FileAdapterInterface::class);
    }

    /**
     * @return FileFlattenAdapterInterface
     */
    protected function createFileFlattenAdapter()
    {
        return $this->container->make(FileFlattenAdapterInterface::class);
    }

    /**
     * @return FileEntityInterface
     */
    protected function createFileEntity()
    {
        return $this->container->make(FileEntityInterface::class);
    }

    /**
     * @return FileModelInterface
     */
    protected function createFileModel()
    {
        return $this->container->make(FileModelInterface::class);
    }
}
