<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\DAL\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileEntityInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileModelInterface;

/**
 * @author Evgeny Novoselov <e.novosyolov@mildberry.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class BaseFileAdapter extends AbstractAdapter
{
    /**
     * @param FileModelInterface $model
     *
     * @return FileEntityInterface
     */
    public function transform($model = null)
    {
        /** @var FileEntityInterface $entity */
        $entity = $this->getContainer()->make(FileEntityInterface::class);

        $entity
            ->setId($model->id)
            ->setType($model->type)
            ->setFilename($model->filename)
            ->setPublicURL($model->public_url)
            ->setRemote($model->is_remote)
            ->setDiskName($model->disk_name)
            ->setMetaData(json_decode($model->meta_data, true))
        ;

        return $entity;
    }
}
