<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\DAL\Model;

use Mildberry\Kangaroo\Libraries\Elegant\Elegant;

/**
 * @author Evgeny Novoselov <e.novosyolov@mildberry.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class BaseFileModel extends Elegant
{
    /**
     * @var string
     */
    protected $table = 'files';

    /**
     * @var string
     */
    protected $primaryKey = 'id';
}
