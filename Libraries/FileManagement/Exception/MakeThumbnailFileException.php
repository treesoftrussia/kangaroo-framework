<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\Exception;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class MakeThumbnailFileException extends FileException
{
}
