<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\Exception;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class UploadFileException extends FileException
{
}
