<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\Entity;

use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class BaseImageFileEntity extends BaseFileEntity
{
    /**
     * @var string
     */
    protected $type = 'image';

    /**
     * @return string
     */
    public function getOriginalURL()
    {
        return $this->getPublicURL();
    }

    /**
     * @param mixed $originalURL
     *
     * @return $this
     */
    public function setOriginalURL($originalURL)
    {
        $this->setPublicURL($originalURL);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getThumbnailURL()
    {
        return (!empty($this->metaData['thumbnailURL']) ? $this->metaData['thumbnailURL'] : null);
    }

    /**
     * @param mixed $thumbnailURL
     *
     * @return $this
     */
    public function setThumbnailURL($thumbnailURL)
    {
        $this->metaData['thumbnailURL'] = Cast::string($thumbnailURL);

        return $this;
    }
}
