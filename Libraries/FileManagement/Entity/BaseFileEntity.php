<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\Entity;

use Mildberry\Kangaroo\Libraries\Cast\Cast;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileEntityInterface;

/**
 * @author Evgeny Novoselov <e.novosyolov@mildberry.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class BaseFileEntity implements FileEntityInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $filename;

    /**
     * @var string
     */
    protected $publicURL;

    /**
     * @var string
     */
    protected $diskName;

    /**
     * @var bool
     */
    protected $isRemote;

    /**
     * @var string
     */
    protected $type = 'file';

    /**
     * @var array
     */
    protected $metaData;

    /**
     * @param string $publicURL
     * @param string $filename
     * @param string $diskName
     * @param array $metadata
     * @param bool $isRemote
     * @return static
     */
    public static function make($publicURL, $filename, $diskName, $metadata = [], $isRemote = false)
    {
        $entity = new static();

        $entity
            ->setPublicURL($publicURL)
            ->setFilename($filename)
            ->setDiskName($diskName)
            ->setMetaData($metadata)
            ->setRemote($isRemote)
        ;

        return $entity;
    }

    /**
     * @param BaseFileEntity $fileEntity
     * @return static
     */
    public static function makeFromFileEntity($fileEntity)
    {
        $entity = new static();

        $entity
            ->setId($fileEntity->getId())
            ->setPublicURL($fileEntity->getPublicURL())
            ->setFilename($fileEntity->getFilename())
            ->setDiskName($fileEntity->getDiskName())
            ->setMetaData($fileEntity->getMetaData())
            ->setRemote($fileEntity->isRemote())
        ;

        return $entity;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = Cast::int($id);

        return $this;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     *
     * @return $this
     */
    public function setFilename($filename)
    {
        $this->filename = Cast::string($filename);

        return $this;
    }

    /**
     * @return string
     */
    public function getPublicURL()
    {
        return $this->publicURL;
    }

    /**
     * @param string $publicURL
     *
     * @return $this
     */
    public function setPublicURL($publicURL)
    {
        $this->publicURL = Cast::string($publicURL);

        return $this;
    }

    /**
     * @return string
     */
    public function getDiskName()
    {
        return $this->diskName;
    }

    /**
     * @param string $diskName
     *
     * @return $this
     */
    public function setDiskName($diskName)
    {
        $this->diskName = Cast::string($diskName);

        return $this;
    }

    /**
     * @return boolean
     */
    public function isRemote()
    {
        return $this->isRemote;
    }

    /**
     * @param boolean $isRemote
     *
     * @return $this
     */
    public function setRemote($isRemote)
    {
        $this->isRemote = Cast::bool($isRemote);

        return $this;
    }

    /**
     * @return array
     */
    public function getMetaData()
    {
        return $this->metaData;
    }

    /**
     * @param array $metaData
     *
     * @return $this
     */
    public function setMetaData($metaData)
    {
        $this->metaData = $metaData;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = Cast::string($type);

        return $this;
    }

    /**
     * @param string $className
     * @return mixed
     */
    protected function convertToObject($className) {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($className),
            $className,
            strstr(strstr(serialize($this), '"'), ':')
        ));
    }
}
