<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\Service;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Mildberry\Kangaroo\Libraries\FileManagement\Exception\UploadFileException;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileEntityInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileRepositoryInterface;
use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Container\ContainerInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileServiceInterface;
use Mildberry\Kangaroo\Libraries\Options\Objects\FileOptions;
use Mildberry\Kangaroo\Libraries\Service\AbstractService;
use Mildberry\Kangaroo\Libraries\Specification\Exceptions\BodyValidationException;

/**
 * @author Evgeny Novoselov <e.novosyolov@mildberry.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class BaseFileService extends AbstractService implements FileServiceInterface
{
    /**
     * @var FileRepositoryInterface
     */
    private $repository;

    /**
     * @var FilesystemManager
     */
    private $fsManager;

    /**
     * @var string
     */
    protected $fileEntityInterface = FileEntityInterface::class;

    /**
     * @var string
     */
    protected $diskName = null;

    /**
     * @var string
     */
    protected $pathPrefix = '';

    /**
     * @var string
     */
    protected $fileValidatorRules = 'required|file';

    /**
     * @param FileRepositoryInterface $repository
     * @param ContainerInterface $container
     * @param FilesystemManager $fsManager
     */
    public function __construct(FileRepositoryInterface $repository, ContainerInterface $container, FilesystemManager $fsManager)
    {
        parent::__construct($container);

        $this->repository = $repository;
        $this->fsManager = $fsManager;
    }

    /**
     * @param FileOptions $options
     *
     * @return Collection
     */
    public function getAll(FileOptions $options = null)
    {
        return $this->repository->getAll($options);
    }

    /**
     * @param $id
     *
     * @return FileEntityInterface
     */
    public function get($id)
    {
        $entity = $this->createFileEntity();

        return $entity::makeFromFileEntity($this->repository->get($id));
    }

    /**
     * @param FileEntityInterface $fileEntity
     *
     * @return FileEntityInterface
     */
    public function create(FileEntityInterface $fileEntity)
    {
        $entity = $this->createFileEntity();

        return $entity::makeFromFileEntity($this->repository->create($fileEntity));
    }

    /**
     * @param $id
     * @param FileEntityInterface $fileEntity
     *
     * @return FileEntityInterface
     */
    public function update($id, FileEntityInterface $fileEntity)
    {
        $entity = $this->createFileEntity();

        return $entity::makeFromFileEntity($this->repository->update($id, $fileEntity));
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function delete($id)
    {
        return $this->repository->delete($id);
    }

    /**
     * @param UploadedFile $file
     * @return FileEntityInterface
     * @throws UploadFileException
     */
    public function uploadFile(UploadedFile $file = null)
    {
        $this->tryToThrowBodyValidationException($file);

        $fileName = $this->generateUniqueFileName($file->getClientOriginalExtension());

        if (!$this->getFS()->put($fileName, File::get($file))) {
            throw new UploadFileException('Error on upload file :'.$file->getClientOriginalName());
        }

        $fileEntity = $this->createFileEntity();

        return $this->create($fileEntity::make(asset($this->getUrl($fileName)), $fileName, $this->diskName, [], false));
    }

    /**
     * @param string $url
     * @return FileEntityInterface
     */
    public function uploadFileFromURL($url)
    {
        $fileName = basename($url); //TODO: сделать проверку если у файла нет расширения
        $filePath = sys_get_temp_dir().DIRECTORY_SEPARATOR.time().rand(1, 9999).$fileName;
        file_put_contents($filePath, file_get_contents($url));
        $fileEntity = $this->uploadFile((new UploadedFile($filePath, $fileName, null, null, null, true)));
        unlink($filePath);

        return $fileEntity;
    }

    /**
     * @param string $path
     * @return bool
     */
    public function exists($path)
    {
        return $this->getFS()->exists($this->pathPrefix.$path);
    }

    /**
     * @param string $fileExtension
     * @return string
     */
    protected function generateUniqueFileName($fileExtension)
    {
        $fileName = $this->pathPrefix.uniqid().'.'.$fileExtension;

        while ($this->getFS()->exists($fileName)) {
            $fileName = $this->pathPrefix.uniqid().'.'.$fileExtension;
        }

        return $fileName;
    }

    /**
     * @return Filesystem
     */
    protected function getFS()
    {
        return $this->fsManager->disk($this->diskName);
    }

    /**
     * @param string $fileName
     * @return string
     */
    protected function getUrl($fileName)
    {
        return asset($this->getFS()->url($fileName));
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    protected function getFileExtensionByFileName($fileName)
    {
        return pathinfo($fileName, PATHINFO_EXTENSION);
    }

    /**
     * @return FileEntityInterface
     */
    private function createFileEntity()
    {
        return $this->container->get($this->fileEntityInterface);
    }

    /**
     * @param UploadedFile $file
     */
    private function tryToThrowBodyValidationException(UploadedFile $file = null)
    {
        /** @var \Illuminate\Validation\Validator $validator */
        $validator = Validator::make(['file' => $file], ['file' => $this->fileValidatorRules]);

        if ($validator->fails()){
            throw new BodyValidationException($validator->getMessageBag()->getMessages());
        }
    }
}
