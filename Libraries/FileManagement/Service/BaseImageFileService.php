<?php

namespace Mildberry\Kangaroo\Libraries\FileManagement\Service;

use Folklore\Image\Facades\Image;
use Illuminate\Http\UploadedFile;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Exception\MakeThumbnailFileException;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\ImageFileEntityInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\ImageFileServiceInterface;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class BaseImageFileService extends BaseFileService implements ImageFileServiceInterface
{
    const THUMBNAIL_DIRECTORY_NAME = 'thumbnail';

    /**
     * @var string
     */
    protected $fileEntityInterface = ImageFileEntityInterface::class;

    /**
     * @var string
     */
    protected $pathPrefix = 'images/';

    /**
     * @var string
     */
    protected $diskName = 'public';

    /**
     * @var int
     */
    protected $thumbnailWidth = 300;

    /**
     * @var int
     */
    protected $thumbnailHeight = 300;

    /**
     * @var string
     */
    protected $fileValidatorRules = 'required|file|image';

    /**
     * @param UploadedFile $file
     * @return ImageFileEntityInterface
     * @throws MakeThumbnailFileException
     */
    public function uploadFile(UploadedFile $file = null)
    {
        /** @var ImageFileEntityInterface $fileEntity */
        $fileEntity = parent::uploadFile($file);

        $sourceFileName = $this->getFS()->getDriver()->getAdapter()->getPathPrefix().$fileEntity->getFilename();
        $fileName = $this->pathPrefix.self::THUMBNAIL_DIRECTORY_NAME.DIRECTORY_SEPARATOR.basename($fileEntity->getFilename());

        if (!$this->getFS()->put($fileName, $this->getThumbnailContent($sourceFileName, $this->thumbnailWidth, $this->thumbnailHeight))) {
            throw new MakeThumbnailFileException('Error on save thumbnail file '.$fileName);
        }

        $fileEntity
            ->setThumbnailURL($this->getUrl($fileName))
            ->setType('image')
        ;

        return $this->update($fileEntity->getId(), $fileEntity);
    }

    /**
     * @param string $imageFileName
     * @param int $width
     * @param int $height
     * @return string
     * @throws MakeThumbnailFileException
     */
    public function getThumbnailContent($imageFileName, $width, $height)
    {
        try {
            return Image::open($imageFileName)->thumbnail(new Box($width, $height), ImageInterface::THUMBNAIL_OUTBOUND)->get($this->getFileExtensionByFileName($imageFileName));
        } catch (\Exception $e) {
            throw new MakeThumbnailFileException('Error on make thumbnail image from file '.$imageFileName.': '.$e->getMessage());
        }
    }

    /**
     * @param string $path
     * @return bool
     */
    public function existsThumbnail($path)
    {
        return $this->getFS()->exists($this->pathPrefix.self::THUMBNAIL_DIRECTORY_NAME.DIRECTORY_SEPARATOR.$path);
    }

    /**
     * @return int
     */
    public function getThumbnailWidth()
    {
        return $this->thumbnailWidth;
    }

    /**
     * @param $thumbnailWidth
     * @return $this
     */
    public function setThumbnailWidth($thumbnailWidth)
    {
        $this->thumbnailWidth = $thumbnailWidth;
        return $this;
    }

    /**
     * @return int
     */
    public function getThumbnailHeight()
    {
        return $this->thumbnailHeight;
    }

    /**
     * @param $thumbnailHeight
     * @return $this
     */
    public function setThumbnailHeight($thumbnailHeight)
    {
        $this->thumbnailHeight = $thumbnailHeight;
        return $this;
    }

    /**
     * @return string
     */
    public function getFileValidatorRules()
    {
        return $this->fileValidatorRules;
    }

    /**
     * @param $fileValidatorRules
     * @return $this
     */
    public function setFileValidatorRules($fileValidatorRules)
    {
        $this->fileValidatorRules = $fileValidatorRules;
        return $this;
    }
}
