<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Interfaces;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface UserAdapterInterface
{
    /**
     * @param UserModelInterface $model
     * @return UserEntityInterface
     */
    public function transform(UserModelInterface $model);
}
