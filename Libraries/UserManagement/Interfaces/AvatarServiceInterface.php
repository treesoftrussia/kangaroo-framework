<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Interfaces;

use Illuminate\Http\UploadedFile;
use Mildberry\Kangaroo\Libraries\FileManagement\Exception\MakeThumbnailFileException;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\ImageFileServiceInterface;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface AvatarServiceInterface extends ImageFileServiceInterface
{
}
