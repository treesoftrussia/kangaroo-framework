<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Interfaces;

interface UserEntityInterface
{
    /**
     * @return mixed
     */
    public function getScopes();

    /**
     * @param $scope
     *
     * @return mixed
     */
    public function hasScope($scope);

    /**
     * @return mixed
     */
    public function getScopeIds();
}
