<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Interfaces;

use Mildberry\Kangaroo\Libraries\Database\Exception\EntityNotFoundDALException;

interface UserRepositoryInterface
{
    /**
     * @param $id
     *
     * @return mixed
     */
    public function get($id);

    /**
     * @param int $id
     *
     * @throws EntityNotFoundDALException
     */
    public function delete($id);

    /**
     * @param UserEntityInterface $user
     * @param array $fields
     * @param null $id
     * @return mixed
     */
    public function checkUserOnExistByFields(UserEntityInterface $user, array $fields, $id = null);

    /**
     * @param UserEntityInterface $userEntity
     * @param array $roleIds
     *
     * @return UserEntityInterface|bool
     */
    public function setRoles(UserEntityInterface $userEntity, array $roleIds);

    /**
     * @param $userId
     * @param $roleId
     */
    public function assignRoleForUser($userId, $roleId);

    /**
     * @param UserEntityInterface $object
     * @param array $fields
     * @return mixed
     */
    public function create(UserEntityInterface $object, array $fields);

    /**
     * @param $id
     * @param UserEntityInterface $object
     * @param array $fields
     * @return mixed
     */
    public function update($id, UserEntityInterface $object, array $fields);

    /**
     * @param UserListOptionsInterface $options
     *
     * @return mixed
     */
    public function getAll(UserListOptionsInterface $options);

    /**
     * @param UserListOptionsInterface $options
     *
     * @return mixed
     */
    public function getTotal(UserListOptionsInterface $options);

    /**
     * @param $identificator
     * @param array $fields
     * @return mixed
     */
    public function getUserByField($identificator, array $fields);
}
