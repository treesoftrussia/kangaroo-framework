<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Interfaces;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Collection;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface UserModelInterface
{
    /**
     * @param $key
     * @param bool $collectionAsEmptyValue
     * @return Collection|mixed|null
     */
    public function loadedRelationData($key, $collectionAsEmptyValue = true);

    /**
     * @return BelongsToMany
     */
    public function roles();

    /**
     * @return BelongsTo
     */
    public function avatar();
}
