<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Interfaces;

use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\ImageFileEntityInterface;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface AvatarEntityInterface extends ImageFileEntityInterface
{
}
