<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Interfaces;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\UserManagement\Options\ScopeListOptions;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface ScopeRepositoryInterface
{
    /**
     * @param ScopeListOptions $options
     *
     * @return Collection
     */
    public function getAll(ScopeListOptions $options);

    /**
     * @param ScopeListOptions $options
     *
     * @return int
     */
    public function getTotal(ScopeListOptions $options);
}
