<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Interfaces;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Database\Exception\EntityNotFoundDALException;
use Mildberry\Kangaroo\Libraries\Database\Exception\FieldNotUniqueDALException;
use Mildberry\Kangaroo\Libraries\UserManagement\Entity\Rights\RoleEntity;
use Mildberry\Kangaroo\Libraries\UserManagement\Options\RoleListOptions;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface RoleRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return RoleEntity
     *
     * @throws EntityNotFoundDALException
     */
    public function get($id);

    /**
     * @param RoleListOptions $options
     *
     * @return Collection
     */
    public function getAll(RoleListOptions $options);

    /**
     * @param RoleListOptions $options
     *
     * @return int
     */
    public function getTotal(RoleListOptions $options);

    /**
     * @param RoleEntity $entity
     *
     * @return RoleEntity
     *
     * @throws FieldNotUniqueDALException
     */
    public function create(RoleEntity $entity);

    /**
     * @param int $id
     * @param RoleEntity $entity
     *
     * @return RoleEntity
     *
     * @throws FieldNotUniqueDALException
     * @throws EntityNotFoundDALException
     */
    public function update($id, RoleEntity $entity);

    /**
     * @param int $id
     *
     * @return void
     *
     * @throws EntityNotFoundDALException
     */
    public function delete($id);

    /**
     * @param int        $id
     * @param Collection $scopes
     *
     * @return Collection
     *
     * @throws EntityNotFoundDALException
     */
    public function updateScopes($id, Collection $scopes);

    /**
     * @param int        $id
     * @param Collection $users
     *
     * @return Collection
     *
     * @throws EntityNotFoundDALException
     */
    public function updateUsers($id, Collection $users);
}
