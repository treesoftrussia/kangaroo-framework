<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Interfaces;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
interface UserFlattenAdapterInterface
{
    /**
     * @param UserEntityInterface $entity
     * @return array
     */
    public function transform(UserEntityInterface $entity);
}
