<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Options;

use Mildberry\Kangaroo\Libraries\Options\AbstractOptionsInterface;
use Mildberry\Kangaroo\Libraries\Options\Traits\PaginationOptionsTrait;
use Mildberry\Kangaroo\Libraries\Options\Traits\SearchingOptionsTrait;
use Mildberry\Kangaroo\Libraries\Options\Traits\SortingOptionsTrait;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserListOptionsInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class UserListOptions implements AbstractOptionsInterface, UserListOptionsInterface
{
    use PaginationOptionsTrait;
    use SortingOptionsTrait;
    use SearchingOptionsTrait;

    /**
     * @var int
     */
    private $roleId;

    /**
     * @var int
     */
    private $exclusiveRoleId;

    /**
     * @return $this
     */
    public function initDefaultValues()
    {
        $this->setDefaultPagination();

        return $this;
    }

    /**
     * @return int
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /**
     * @param int $roleId
     *
     * @return $this
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;

        return $this;
    }

    /**
     * @return int
     */
    public function getExclusiveRoleId()
    {
        return $this->exclusiveRoleId;
    }

    /**
     * @param int $exclusiveRoleId
     *
     * @return $this
     */
    public function setExclusiveRoleId($exclusiveRoleId)
    {
        $this->exclusiveRoleId = $exclusiveRoleId;

        return $this;
    }
}
