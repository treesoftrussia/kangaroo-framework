<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Options;

use Mildberry\Kangaroo\Libraries\Options\AbstractOptionsInterface;
use Mildberry\Kangaroo\Libraries\Options\Traits\PaginationOptionsTrait;
use Mildberry\Kangaroo\Libraries\Options\Traits\SearchingOptionsTrait;
use Mildberry\Kangaroo\Libraries\Options\Traits\SortingOptionsTrait;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RoleListOptions implements AbstractOptionsInterface
{
    use PaginationOptionsTrait;
    use SortingOptionsTrait;
    use SearchingOptionsTrait;

    /**
     * @return $this
     */
    public function initDefaultValues()
    {
        $this->setDefaultPagination();

        return $this;
    }
}
