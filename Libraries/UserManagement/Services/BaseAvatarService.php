<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Services;

use GuzzleHttp\Client;
use Illuminate\Http\UploadedFile;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileEntityInterface;
use Mildberry\Kangaroo\Libraries\FileManagement\Service\BaseImageFileService;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\AvatarEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\AvatarServiceInterface;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class BaseAvatarService extends BaseImageFileService implements AvatarServiceInterface
{
    /**
     * @var string
     */
    protected $fileEntityInterface = AvatarEntityInterface::class;

    /**
     * @var int
     */
    protected $thumbnailWidth = 90;

    /**
     * @var int
     */
    protected $thumbnailHeight = 90;

    /**
     * @var string
     */
    protected $pathPrefix = 'images/avatar/';

    /**
     * @var string
     */
    protected $fileValidatorRules = 'required|file|image|max:1024|mimes:jpg,jpeg,gif,png|dimensions:max_width=5000,max_height=5000|dimensions:min_width=90,min_height=90';

    /**
     * @param string $url
     *
     * @return FileEntityInterface
     */
    public function uploadFileFromURL($url)
    {
        //TODO: refactoring
        $fileName = basename($url);

        if(str_contains($fileName, '.')) {
            return parent::uploadFileFromURL($url);
        }

        $client = new Client();
        $resp = $client->get($url);
        $fileName .= '.'.explode('/',$resp->getHeader('Content-type')[0])[1];

        $filePath = sys_get_temp_dir().DIRECTORY_SEPARATOR.time().rand(1, 9999).$fileName;
        file_put_contents($filePath, $resp->getBody()->getContents());
        $fileEntity = $this->uploadFile((new UploadedFile($filePath, $fileName, null, null, null, true)));
        unlink($filePath);

        return $fileEntity;
    }
}
