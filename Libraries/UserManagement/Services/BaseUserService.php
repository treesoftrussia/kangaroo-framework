<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Services;

use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Container\ContainerInterface;
use Mildberry\Kangaroo\Libraries\Database\Exception\DALException;
use Mildberry\Kangaroo\Libraries\Database\Exception\EntityNotFoundDALException;
use Mildberry\Kangaroo\Libraries\Database\Exception\FieldNotUniqueDALException;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\AccessDeniedException;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\BadRequestException;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\ResourceNotFoundException;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Mildberry\Kangaroo\Libraries\Service\AbstractService;
use Mildberry\Kangaroo\Libraries\Service\UserServiceInterface;
use Mildberry\Kangaroo\Libraries\Specification\Exceptions\BodyValidationException;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\AvatarEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\AvatarServiceInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserListOptionsInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserRepositoryInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class BaseUserService extends AbstractService implements UserServiceInterface
{
    /**
     * @var UserRepositoryInterface
     */
    protected $repository;

    /**
     * @var UserEntityInterface
     */
    protected $currentUser;

    /**
     * @var AvatarServiceInterface
     */
    private $avatarService;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $repository
     * @param ContainerInterface $container
     * @param AvatarServiceInterface $avatarService
     */
    public function __construct(UserRepositoryInterface $repository, ContainerInterface $container, AvatarServiceInterface $avatarService)
    {
        parent::__construct($container);
        $this->repository = $repository;
        $this->avatarService = $avatarService;
    }

    /**
     * @param int $id
     *
     * @return UserEntityInterface
     *
     * @throws ResourceNotFoundException
     */
    public function get($id)
    {
        try {
            return $this->repository->get($id);
        } catch (EntityNotFoundDALException $e) {
            throw new ResourceNotFoundException('User not found');
        }
    }

    /**
     * @param UserEntityInterface $entity
     *
     * @return UserEntityInterface
     *
     * @throws BadRequestException
     */
    public function create(UserEntityInterface $entity)
    {
        try {
            return $this->repository->create($entity, $this->getLoginFields());
        } catch (FieldNotUniqueDALException $e) {
            throw new BadRequestException($e->getMessage());
        }
    }

    /**
     * @param int        $id
     * @param UserEntityInterface $entity
     *
     * @return UserEntityInterface
     *
     * @throws BadRequestException
     * @throws ResourceNotFoundException
     */
    public function update($id, UserEntityInterface $entity)
    {
        try {
            return $this->repository->update($id, $entity, $this->getLoginFields());
        } catch (DALException $e) {
            if ($e instanceof EntityNotFoundDALException) {
                throw new ResourceNotFoundException('User not found');
            } elseif ($e instanceof FieldNotUniqueDALException) {
                throw new BadRequestException($e->getMessage());
            }
        }
    }

    /**
     * @param int $id
     *
     * @throws ResourceNotFoundException
     */
    public function delete($id)
    {
        try {
            $this->repository->delete($id);
        } catch (EntityNotFoundDALException $e) {
            throw new ResourceNotFoundException('User not found');
        }
    }

    /**
     * @return UserEntityInterface
     */
    public function getCurrentUser()
    {
        return $this->currentUser;
    }

    /**
     * @return UserEntityInterface
     */
    public function getUser()
    {
        return $this->getCurrentUser();
    }

    /**
     * @param UserEntityInterface $userEntity
     */
    public function setCurrentUser(UserEntityInterface $userEntity)
    {
        $this->currentUser = $userEntity;
    }

    /**
     * @param $id
     *
     * @return UserEntityInterface
     * @throws ResourceNotFoundException
     */
    public function getUserById($id)
    {
        try {
            return $this->repository->get($id);
        } catch (EntityNotFoundDALException $e) {
            throw new ResourceNotFoundException('User not found');
        }
    }

    /**
     * @param $login
     * @param $password
     * @return mixed|void
     */
    public function getUserByCredentials($login, $password)
    {
        $user = $this->repository->getUserByField($login, $this->getLoginFields());

        $hasher = $this->container->get(Hasher::class);

        if (!is_null($user) && $hasher->check($password, $user->getPasswordHash())) {
            return $user;
        }

        throw new AccessDeniedException();
    }

    /**
     * @param $options
     *
     * @return Collection
     */
    public function getAll(UserListOptionsInterface $options)
    {
        return $this->repository->getAll($options);
    }

    /**
     * @param $options
     *
     * @return integer
     */
    public function getTotal(UserListOptionsInterface $options)
    {
        return $this->repository->getTotal($options);
    }

    /**
     * Sets to null current user value
     */
    public function purgeCurrentUser(){
        $this->currentUser = null;
    }

    /**
     * @return array
     */
    public function getLoginFields()
    {
        return ['email', 'phone'];
    }

    /**
     * @param $login
     * @return mixed
     */
    public function getUserByLogin($login)
    {
        return $this->repository->getUserByField($login, $this->getLoginFields());
    }

    /**
     * @param $login
     * @param $activationCode
     */
    public function checkOnActivityAndExistenceActivationCode($login, $activationCode)
    {
        // TODO: Implement checkOnActivityAndExistenceActivationCode() method.
        throw new RuntimeException('Not implemented yet.');
    }

    /**
     * @param int $id
     * @param UploadedFile $file
     * @return AvatarEntityInterface
     */
    public function updateAvatar($id, UploadedFile $file = null)
    {
        $user = $this->getUserById($id);

        try {
            $user->setAvatar($this->avatarService->uploadFile($file));
        }
        catch (BodyValidationException $e) {
            throw $e;
        }
        catch (RuntimeException $e) {
            throw new BadRequestException($e->getMessage());
        }

        $user = $this->update($id, $user);

        return $user->getAvatar();
    }
}
