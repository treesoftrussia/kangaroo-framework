<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Services;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Container\ContainerInterface;
use Mildberry\Kangaroo\Libraries\Database\Exception\EntityNotFoundDALException;
use Mildberry\Kangaroo\Libraries\Database\Exception\FieldNotUniqueDALException;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\BadRequestException;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\ResourceNotFoundException;
use Mildberry\Kangaroo\Libraries\Service\AbstractService;
use Mildberry\Kangaroo\Libraries\UserManagement\Entity\Rights\RoleEntity;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\RoleRepositoryInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Options\RoleListOptions;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RoleService extends AbstractService
{
    /**
     * @var RoleRepositoryInterface
     */
    private $repository;

    /**
     * RoleService constructor.
     *
     * @param ContainerInterface      $container
     * @param RoleRepositoryInterface $repository
     */
    public function __construct(ContainerInterface $container, RoleRepositoryInterface $repository)
    {
        parent::__construct($container);

        $this->repository = $repository;
    }

    /**
     * @param RoleListOptions $options
     *
     * @return Collection
     */
    public function getAll(RoleListOptions $options)
    {
        return $this->repository->getAll($options);
    }

    /**
     * @param int $id
     *
     * @return RoleEntity
     */
    public function get($id)
    {
        try {
            $entity = $this->repository->get($id);
        } catch (EntityNotFoundDALException $e) {
            throw new ResourceNotFoundException('Role not found');
        }

        return $entity;
    }

    /**
     * @param RoleListOptions $options
     *
     * @return int
     */
    public function getTotal(RoleListOptions $options)
    {
        return $this->repository->getTotal($options);
    }

    /**
     * @param RoleEntity $entity
     *
     * @return RoleEntity
     */
    public function create(RoleEntity $entity)
    {
        try {
            $entity = $this->repository->create($entity);
        } catch (FieldNotUniqueDALException $e) {
            throw new BadRequestException('Role with name '.$entity->getMachineName().' already exists');
        }

        return $entity;
    }

    /**
     * @param int        $id
     * @param RoleEntity $entity
     *
     * @return RoleEntity
     */
    public function update($id, RoleEntity $entity)
    {
        try {
            $entity = $this->repository->update($id, $entity);
        } catch (FieldNotUniqueDALException $e) {
            throw new BadRequestException('Role with name '.$entity->getMachineName().' already exists');
        } catch (EntityNotFoundDALException $e) {
            throw new ResourceNotFoundException('Role not found');
        }

        return $entity;
    }

    /**
     * @param int $id
     */
    public function delete($id)
    {
        try {
            $this->repository->delete($id);
        } catch (EntityNotFoundDALException $e) {
            throw new ResourceNotFoundException('Role not found');
        }
    }

    /**
     * @param $id
     * @param Collection $scopes
     *
     * @return Collection
     */
    public function updateScopes($id, Collection $scopes)
    {
        $updatedTagIds = $this->repository->updateScopes($id, $scopes);

        return $updatedTagIds;
    }

    /**
     * @param $id
     * @param Collection $users
     *
     * @return Collection
     */
    public function updateUsers($id, Collection $users)
    {
        $updatedTagIds = $this->repository->updateUsers($id, $users);

        return $updatedTagIds;
    }
}
