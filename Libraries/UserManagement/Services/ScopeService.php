<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Services;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Container\ContainerInterface;
use Mildberry\Kangaroo\Libraries\Service\AbstractService;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\ScopeRepositoryInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Options\ScopeListOptions;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class ScopeService extends AbstractService
{
    /**
     * @var ScopeRepositoryInterface
     */
    private $repository;

    /**
     * RoleService constructor.
     *
     * @param ContainerInterface       $container
     * @param ScopeRepositoryInterface $repository
     */
    public function __construct(ContainerInterface $container, ScopeRepositoryInterface $repository)
    {
        parent::__construct($container);

        $this->repository = $repository;
    }

    /**
     * @param ScopeListOptions $options
     *
     * @return Collection
     */
    public function getAll(ScopeListOptions $options)
    {
        return $this->repository->getAll($options);
    }

    /**
     * @param ScopeListOptions $options
     *
     * @return int
     */
    public function getTotal(ScopeListOptions $options)
    {
        return $this->repository->getTotal($options);
    }
}
