<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\QueryObject;

use Illuminate\Database\Eloquent\Builder;
use Mildberry\Kangaroo\Libraries\Database\QueryObject\AbstractQueryObject;
use Mildberry\Kangaroo\Libraries\Database\QueryObject\Modifiers\PaginationModifyTrait;
use Mildberry\Kangaroo\Libraries\Database\QueryObject\Modifiers\SortingModifyTrait;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Model\ScopeModel;
use Mildberry\Kangaroo\Libraries\UserManagement\Options\ScopeListOptions;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class ScopeQueryObject extends AbstractQueryObject
{
    use PaginationModifyTrait;
    use SortingModifyTrait;

    /**
     * @param ScopeListOptions $options
     * @return Builder
     */
    public function build(ScopeListOptions $options)
    {
        if ($options->getSearchPhrase()) {
            $phrase = $options->getSearchPhrase();
            $this->query->where(function ($query) use ($phrase) {
                $query
                    ->where('id', 'LIKE', '%'.$phrase.'%')
                ;
            });
        }

        $this->applyPagination($options)->applySorting($options);
        $this->applyRoleId($options->getRoleId());
        $this->applyExclusiveRoleId($options->getExclusiveRoleId());

        return $this->query;
    }

    /**
     * @param ScopeListOptions $options
     *
     * @return mixed
     */
    public function buildTotal(ScopeListOptions $options)
    {
        if ($options->getSearchPhrase()) {
            $phrase = $options->getSearchPhrase();
            $this->query->where(function ($query) use ($phrase) {
                $query
                    ->where('id', 'LIKE', '%'.$phrase.'%')
                ;
            });
        }

        $this->applyRoleId($options->getRoleId());


        return $this->query;
    }

    /**
     * @return mixed
     */
    protected function getQuery()
    {
        return ScopeModel::query();
    }

    /**
     * @param int $roleId
     */
    protected function applyRoleId($roleId)
    {
        if ($roleId) {
            $this->query
                ->leftJoin('role_oauth_scopes', 'oauth_scopes.id', '=', 'role_oauth_scopes.oauth_scope_id')
                ->where('role_oauth_scopes.role_id', $roleId)
            ;
        }
    }

    /**
     * @param int $roleId
     */
    protected function applyExclusiveRoleId($roleId)
    {
        if ($roleId) {
            $this->query
                ->whereRaw('oauth_scopes.id not in (select oauth_scope_id from role_oauth_scopes where role_oauth_scopes.role_id = '.$roleId.')')
            ;
        }
    }
}
