<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\QueryObject;

use App\DAL\Model\UserModel;
use Illuminate\Database\Eloquent\Builder;
use Mildberry\Kangaroo\Libraries\Database\QueryObject\AbstractQueryObject;
use Mildberry\Kangaroo\Libraries\Database\QueryObject\Modifiers\PaginationModifyTrait;
use Mildberry\Kangaroo\Libraries\Database\QueryObject\Modifiers\SortingModifyTrait;
use Mildberry\Kangaroo\Libraries\UserManagement\Options\UserListOptions;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class AllUsersQueryObject extends AbstractQueryObject
{
    use PaginationModifyTrait;
    use SortingModifyTrait;

    /**
     * @param UserListOptions $options
     *
     * @return Builder
     */
    public function build(UserListOptions $options)
    {
        if ($options->getSearchPhrase()) {
            $phrase = $options->getSearchPhrase();
            $this->query->where(function ($query) use ($phrase) {
                $query
                    ->where('id', '=', $phrase)
                    ->orWhere('username', 'LIKE', '%'.$phrase.'%')
                    ->orWhere('phone', 'LIKE', '%'.$phrase.'%')
                    ->orWhere('email', 'LIKE', '%'.$phrase.'%')
                ;
            });
        }

        $this->applyPagination($options)->applySorting($options);
        $this->applyRoleId($options->getRoleId());
        $this->applyExclusiveRoleId($options->getExclusiveRoleId());

        return $this->query;
    }

    /**
     * @param UserListOptions $options
     *
     * @return mixed
     */
    public function buildTotal(UserListOptions $options)
    {
        if ($options->getSearchPhrase()) {
            $phrase = $options->getSearchPhrase();
            $this->query->where(function ($query) use ($phrase) {
                $query
                    ->where('id', '=', $phrase)
                    ->orWhere('username', 'LIKE', '%'.$phrase.'%')
                    ->orWhere('phone', 'LIKE', '%'.$phrase.'%')
                    ->orWhere('email', 'LIKE', '%'.$phrase.'%')
                ;
            });
        }

        $this->applyRoleId($options->getRoleId());
        $this->applyExclusiveRoleId($options->getExclusiveRoleId());

        return $this->query;
    }

    /**
     * @return Builder
     */
    protected function getQuery()
    {
        //TODO: Костыль
        return UserModel::query();
    }

    /**
     * @param int $roleId
     */
    protected function applyRoleId($roleId)
    {
        if ($roleId) {
            $this->query
                ->leftJoin('user_roles', 'users.id', '=', 'user_roles.user_id')
                ->where('user_roles.role_id', $roleId)
            ;
        }
    }

    /**
     * @param int $roleId
     */
    protected function applyExclusiveRoleId($roleId)
    {
        if ($roleId) {
            $this->query
                ->whereRaw('users.id not in (select user_id from user_roles where user_roles.role_id = '.$roleId.')')
            ;
        }
    }
}
