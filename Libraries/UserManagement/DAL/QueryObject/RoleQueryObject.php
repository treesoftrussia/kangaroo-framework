<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\QueryObject;

use Illuminate\Database\Eloquent\Builder;
use Mildberry\Kangaroo\Libraries\Database\QueryObject\AbstractQueryObject;
use Mildberry\Kangaroo\Libraries\Database\QueryObject\Modifiers\PaginationModifyTrait;
use Mildberry\Kangaroo\Libraries\Database\QueryObject\Modifiers\SortingModifyTrait;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Model\RoleModel;
use Mildberry\Kangaroo\Libraries\UserManagement\Options\RoleListOptions;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RoleQueryObject extends AbstractQueryObject
{
    use PaginationModifyTrait;
    use SortingModifyTrait;

    /**
     * @param RoleListOptions $options
     * @return Builder
     */
    public function build(RoleListOptions $options)
    {
        if ($options->getSearchPhrase()) {
            $phrase = $options->getSearchPhrase();
            $this->query->where(function ($query) use ($phrase) {
                $query
                    ->where('id', '=', $phrase)
                    ->orWhere('machine_name', 'LIKE', '%'.$phrase.'%')
                ;
            });
        }

        $this->applyPagination($options)->applySorting($options);

        return $this->query;
    }

    /**
     * @param RoleListOptions $options
     *
     * @return mixed
     */
    public function buildTotal(RoleListOptions $options)
    {
        if ($options->getSearchPhrase()) {
            $phrase = $options->getSearchPhrase();
            $this->query->where(function ($query) use ($phrase) {
                $query
                    ->where('id', '=', $phrase)
                    ->orWhere('machine_name', 'LIKE', '%'.$phrase.'%')
                ;
            });
        }

        return $this->query;
    }

    /**
     * @return mixed
     */
    protected function getQuery()
    {
        return RoleModel::query();
    }
}
