<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\FileManagement\Interfaces\FileAdapterInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Entity\Users\UserEntity;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\AvatarEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserAdapterInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserModelInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class BaseUserAdapter extends AbstractAdapter implements UserAdapterInterface
{
    /**
     * @param UserModelInterface $model
     * @return UserEntityInterface
     */
    public function transform(UserModelInterface $model)
    {
        /** @var UserEntity $entity */
        $entity = $this->getContainer()->make(UserEntityInterface::class);

        $entity
            ->setId($model->id)
            ->setEmail($model->email)
            ->setUserName($model->username)
            ->setPhone($model->phone)
            ->setPasswordHash($model->password)
            ->setRoles((new RoleAdapter())->transformCollection($model->roles))
            ->setCreatedAt($model->created_at)
            ->setUpdatedAt($model->updated_at)
        ;


        $avatarModel = $model->loadedRelationData('avatar', false);
        $avatarEntity = $this->getContainer()->make(AvatarEntityInterface::class);
        $avatar = (!is_null($avatarModel)) ? $avatarEntity::makeFromFileEntity($this->getContainer()->make(FileAdapterInterface::class)->transform($avatarModel)) : $avatarEntity;

        $entity->setAvatar($avatar);

        return $entity;
    }
}
