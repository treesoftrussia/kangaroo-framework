<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Model\ScopeModel;
use Mildberry\Kangaroo\Libraries\UserManagement\Entity\Rights\ScopeEntity;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class ScopeAdapter extends AbstractAdapter
{
    /**
     * @param ScopeModel $model
     * @return ScopeEntity
     */
    public function transform(ScopeModel $model){
        $entity = new ScopeEntity();

        $entity
            ->setId($model->id)
            ->setDescription($model->description)
        ;

        return $entity;
    }
}
