<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\Adapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Model\RoleModel;
use Mildberry\Kangaroo\Libraries\UserManagement\Entity\Rights\RoleEntity;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class RoleAdapter extends AbstractAdapter
{
    /**
     * @param RoleModel $model
     * @return RoleEntity
     */
    public function transform(RoleModel $model){
        $entity = new RoleEntity();

        $entity
            ->setId($model->id)
            ->setMachineName($model->machine_name)
            ->setScopes((new ScopeAdapter())->transformCollection($model->scopes))
        ;

        return $entity;
    }
}
