<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\Model;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Mildberry\Kangaroo\Libraries\Elegant\Elegant;
use Illuminate\Auth\Authenticatable;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserModelInterface;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class BaseUserModel extends Elegant implements AuthenticatableContract, UserModelInterface
{
    use Authenticatable;

    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(RoleModel::class, 'user_roles', 'user_id', 'role_id');
    }

    /**
     * @return BelongsTo
     */
    public function avatar()
    {
        return $this->belongsTo(AvatarModel::class, 'avatar_file_id');
    }
}
