<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\Model;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Mildberry\Kangaroo\Libraries\Elegant\Elegant;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class ScopeModel extends Elegant
{
    /**
     * @var string
     */
    protected $table = 'oauth_scopes';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(RoleModel::class, 'role_oauth_scopes', 'oauth_scope_id', 'role_id');
    }

    /**
     * @return BelongsToMany
     */
    public function oauthClients()
    {
        return $this->belongsToMany(OauthClientModel::class, 'oauth_client_scopes', 'scope_id', 'client_id');
    }
}
