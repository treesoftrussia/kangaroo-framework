<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\Model;

use App\DAL\Model\UserModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Mildberry\Kangaroo\Libraries\Elegant\Elegant;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class RoleModel extends Elegant
{
    /**
     * @var string
     */
    protected $table = 'roles';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return BelongsToMany
     */
    public function users()
    {
        //TODO: Костыль
        return $this->belongsToMany(UserModel::class, 'user_roles', 'role_id', 'user_id');
    }

    /**
     * @return BelongsToMany
     */
    public function scopes()
    {
        return $this->belongsToMany(ScopeModel::class, 'role_oauth_scopes', 'role_id', 'oauth_scope_id');
    }
}
