<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\FlattenAdapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\UserManagement\Entity\Rights\RoleEntity;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RoleFlattenAdapter extends AbstractAdapter
{
    /**
     * @param RoleEntity $entity
     * @return array
     */
    public function transform(RoleEntity $entity)
    {
        $sanitizer = $this->sanitizer()->make();

        return skip_empty([
            'machine_name' => $sanitizer($entity->getMachineName(), 'string'),
        ], [null]);
    }
}
