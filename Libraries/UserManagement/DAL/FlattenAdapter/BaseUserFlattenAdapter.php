<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\FlattenAdapter;

use Mildberry\Kangaroo\Libraries\Adapter\AbstractAdapter;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class BaseUserFlattenAdapter extends AbstractAdapter
{
    /**
     * @param UserEntityInterface $user
     * @return array
     */
    public function transform(UserEntityInterface $user)
    {
        $sanitizer = $this->sanitizer()->make();

        return skip_empty([
            'email' => $sanitizer($user->getEmail(), 'string'),
            'username' => $sanitizer($user->getUsername(), 'string'),
            'phone' => $sanitizer($user->getPhone(), 'string'),
            'password' => $sanitizer($user->getPasswordHash(), 'string'),
            'avatar_file_id' => ((!is_null($user->getAvatar())) && intval($user->getAvatar()->getId()) > 0) ? $user->getAvatar()->getId() : null,
        ], [null]);
    }
}
