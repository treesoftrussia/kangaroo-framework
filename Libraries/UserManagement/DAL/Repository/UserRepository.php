<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\Repository;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Mildberry\Kangaroo\Libraries\Database\Exception\EntityNotFoundDALException;
use Mildberry\Kangaroo\Libraries\Database\Exception\FieldNotUniqueDALException;
use Mildberry\Kangaroo\Libraries\Database\Repository\AbstractRepository;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Adapter\RoleAdapter;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Model\RoleModel;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\QueryObject\AllUsersQueryObject;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserAdapterInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserFlattenAdapterInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserListOptionsInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserModelInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserRepositoryInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class UserRepository extends AbstractRepository implements UserRepositoryInterface
{

    /**
     * @return UserAdapterInterface
     */
    protected function createUserAdapter()
    {
        return $this->container->make(UserAdapterInterface::class);
    }

    /**
     * @return UserAdapterInterface
     */
    protected function createUserFlattenAdapter()
    {
        return $this->container->make(UserFlattenAdapterInterface::class);
    }

    /**
     * @return UserEntityInterface
     */
    protected function createUserEntity()
    {
        return $this->container->make(UserEntityInterface::class);
    }

    /**
     * @return UserModelInterface
     */
    protected function createUserModel()
    {
        return $this->container->make(UserModelInterface::class);
    }

    /**
     * @param $id
     *
     * @return UserEntityInterface
     * @throws EntityNotFoundDALException
     */
    public function get($id)
    {
        $model = $this->createUserModel();
        $userEntry = $model::where('id', $id)->with(['roles', 'avatar'])->first();

        if (!$userEntry) {
            throw new EntityNotFoundDALException('User not found');
        }

        return $this->createUserAdapter()->transform($userEntry);
    }

    /**
     * @param int $id
     *
     * @throws EntityNotFoundDALException
     */
    public function delete($id)
    {
        $model = $this->createUserModel();
        DB::transaction(function () use ($id, $model) {
            $user = $model::where('id', $id)->lockForUpdate()->first();

            if (!$user) {
                throw new EntityNotFoundDALException('User not found');
            }

            $user->delete();
        });
    }

    /**
     * @param UserEntityInterface $user
     * @param array $fields
     * @param null $id
     * @return mixed
     */
    public function checkUserOnExistByFields(UserEntityInterface $user, array $fields, $id = null)
    {
        $model = $this->createUserModel();

        $query = $model::getQuery();

        $query->where(function($query) use ($user, $fields) {
            foreach ($fields as $field){
                $getter = make_getter($field);
                $value = $user->{$getter}();
                if(!is_null($value)){
                    $query->orWhere($field, $user->{$getter}());
                }
            }
        });

        if(!is_null($id)){
            $query->where('id', '!=', $id);
        }

        return $query->exists();
    }

    /**
     * @param UserEntityInterface $user
     * @param array $fields
     * @return mixed
     * @throws FieldNotUniqueDALException
     */
    public function create(UserEntityInterface $user, array $fields)
    {
        if ($this->checkUserOnExistByFields($user, $fields)) {
            throw new FieldNotUniqueDALException(implode(',', $fields).' not unique');
        }
        $model = $this->createUserModel();
        $model = $model::create($this->createUserFlattenAdapter()->transform($user));

//        //TODO: Не понятно зачем это
//        $userEntry = UserModel::find($createdUser->id);
//        $userEntity = (new BaseUserAdapter())->transform($userEntry);
//        $userEntity->setPassword($user->getPassword());

        return $this->get($model->id);
    }

    /**
     * @param UserEntityInterface $userEntity
     * @param array $roleIds
     *
     * @return UserEntityInterface|bool
     */
    public function setRoles(UserEntityInterface $userEntity, array $roleIds)
    {
        $roleEntries = RoleModel::whereIn('machine_name', $roleIds)->get();
        if (!empty($roleEntries)) {
            $model = $this->createUserModel();
            $userEntry = $model::find($userEntity->getId());
            $userEntry->roles()->attach(array_pluck($roleEntries, 'id'));

            $userEntity->setRoles((new RoleAdapter())->transformCollection($roleEntries));

            return $userEntity;
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @param UserEntityInterface $user
     * @param array $fields
     * @return UserEntityInterface
     * @throws FieldNotUniqueDALException
     */
    public function update($id, UserEntityInterface $user, array $fields)
    {
        if ($user->getEmail() || $user->getPhone()) {
            if ($this->checkUserOnExistByFields($user, $fields, $id)) {
                throw new FieldNotUniqueDALException(implode(',', $fields) . ' not unique');
            }
        }

        $model = $this->createUserModel();

        DB::transaction(function () use ($id, $user, $model) {
            $userModel = $model::where('id', $id)->lockForUpdate()->first();

            if (!$userModel) {
                throw new EntityNotFoundDALException('User not found');
            }

            $userModel->update($this->createUserFlattenAdapter()->transform($user));
        });


        return $this->get($id);
    }

    /**
     * @param UserListOptionsInterface $options
     *
     * @return Collection
     */
    public function getAll(UserListOptionsInterface $options)
    {
        $query = (new AllUsersQueryObject())->build($options)->with(['avatar']);

        $usersEntries = $query->get();

        return $this->createUserAdapter()->transformCollection($usersEntries);
    }

    /**
     * @param UserListOptionsInterface $options
     *
     * @return integer
     */
    public function getTotal(UserListOptionsInterface $options)
    {
        $total = (new AllUsersQueryObject())->build($options)->count();

        return $total;
    }

    /**
     * @param $name
     *
     * @return integer
     */
    public function getRoleIdByName($name)
    {
        return RoleModel::where('machine_name', '=', $name)->value('id');
    }

    /**
     * @param $userId
     * @param $roleId
     */
    public function assignRoleForUser($userId, $roleId)
    {
        $model = $this->createUserModel();
        //TODO: Обернуть в транзакцию и сделать блокировку
        $user = $model::find($userId);

        $exists = $user->roles->contains($roleId);
        if (!$exists) {
            $user->roles()->attach($roleId);
        }
    }

    /**
     * @param $identificator
     * @param array $fields
     * @return UserEntityInterface
     * @throws EntityNotFoundDALException
     */
    public function getUserByField($identificator, array $fields)
    {
        $model = $this->createUserModel();
        $query = $model::with(['roles', 'avatar']);

        $query->where(function($query) use ($fields, $identificator) {
            foreach ($fields as $field){
                $query->orWhereRaw('(`'.$field.'` is not null and `'.$field.'` = ?)', [$identificator]);
            }
        });

        $model = $query->first();

        if (!$model) {
            throw new EntityNotFoundDALException('User not found');
        }

        return $this->createUserAdapter()->transform($model);
    }
}
