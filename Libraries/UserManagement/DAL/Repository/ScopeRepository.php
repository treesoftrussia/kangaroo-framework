<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\Repository;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Adapter\ScopeAdapter;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\QueryObject\ScopeQueryObject;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\ScopeRepositoryInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Options\ScopeListOptions;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class ScopeRepository implements ScopeRepositoryInterface
{
    /**
     * @param ScopeListOptions $options
     *
     * @return Collection
     */
    public function getAll(ScopeListOptions $options)
    {
        $query = (new ScopeQueryObject())->build($options);

        return (new ScopeAdapter())->transformCollection($query->get());
    }

    /**
     * @param ScopeListOptions $options
     *
     * @return int
     */
    public function getTotal(ScopeListOptions $options)
    {
        return (new ScopeQueryObject())->build($options)->count();
    }
}
