<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\DAL\Repository;

use App\DAL\Model\UserModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Mildberry\Kangaroo\Libraries\Database\Exception\EntityNotFoundDALException;
use Mildberry\Kangaroo\Libraries\Database\Exception\FieldNotUniqueDALException;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Adapter\RoleAdapter;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\FlattenAdapter\RoleFlattenAdapter;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Model\RoleModel;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Model\ScopeModel;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\QueryObject\RoleQueryObject;
use Mildberry\Kangaroo\Libraries\UserManagement\Entity\Rights\RoleEntity;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\RoleRepositoryInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Options\RoleListOptions;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class RoleRepository implements RoleRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return RoleEntity
     *
     * @throws EntityNotFoundDALException
     */
    public function get($id)
    {
        $model = RoleModel::find($id);

        if (!$model) {
            throw new EntityNotFoundDALException('Role not found');
        }

        return (new RoleAdapter())->transform($model);
    }

    /**
     * @param RoleListOptions $options
     *
     * @return Collection
     */
    public function getAll(RoleListOptions $options)
    {
        $query = (new RoleQueryObject())->build($options);

        return (new RoleAdapter())->transformCollection($query->get());
    }

    /**
     * @param RoleListOptions $options
     *
     * @return int
     */
    public function getTotal(RoleListOptions $options)
    {
        return (new RoleQueryObject())->build($options)->count();
    }

    /**
     * @param RoleEntity $entity
     *
     * @return RoleEntity
     *
     * @throws FieldNotUniqueDALException
     */
    public function create(RoleEntity $entity)
    {
        $id = DB::transaction(function () use ($entity) {
            if (RoleModel::where('machine_name', '=', $entity->getMachineName())->lockForUpdate()->first()) {
                throw new FieldNotUniqueDALException('machine_name');
            }

            $model = RoleModel::create((new RoleFlattenAdapter())->transform($entity));

            return $model->id;
        });

        return $this->get($id);
    }

    /**
     * @param int $id
     * @param RoleEntity $entity
     *
     * @return RoleEntity
     *
     * @throws FieldNotUniqueDALException
     * @throws EntityNotFoundDALException
     */
    public function update($id, RoleEntity $entity)
    {
        DB::transaction(function () use ($id, $entity) {
            $model = RoleModel::whereId($id)->lockForUpdate()->first();

            if (!$model) {
                throw new EntityNotFoundDALException('Role not found');
            }

            if ($entity->getMachineName() != $model->machine_name && RoleModel::where('machine_name', '=', $entity->getMachineName())->lockForUpdate()->first()) {
                throw new FieldNotUniqueDALException('machine_name');
            }

            $model->update((new RoleFlattenAdapter())->transform($entity));
        });

        return $this->get($id);
    }

    /**
     * @param int $id
     *
     * @return void
     *
     * @throws EntityNotFoundDALException
     */
    public function delete($id)
    {
        $model = RoleModel::find($id);

        if (!$model) {
            throw new EntityNotFoundDALException();
        }

        $model->delete();
    }

    /**
     * @param int        $id
     * @param Collection $scopes
     *
     * @return Collection
     *
     * @throws EntityNotFoundDALException
     */
    public function updateScopes($id, Collection $scopes)
    {
        DB::transaction(function () use ($id, $scopes) {
            $model = RoleModel::where('id', $id)->sharedLock()->first();

            if (!$model) {
                throw new EntityNotFoundDALException('Role not found');
            }

            $newScopesIds = $scopes->map(function ($item) {
                return $item->getId();
            })->all();

            ScopeModel::whereIn('id', $newScopesIds)->lockForUpdate()->get();

            return $model->scopes()->sync($newScopesIds);
        });

        return RoleModel::where('id', $id)->first()->scopes->pluck('id');
    }

    /**
     * @param int        $id
     * @param Collection $users
     *
     * @return Collection
     *
     * @throws EntityNotFoundDALException
     */
    public function updateUsers($id, Collection $users)
    {
        DB::transaction(function () use ($id, $users) {
            $model = RoleModel::where('id', $id)->sharedLock()->first();

            if (!$model) {
                throw new EntityNotFoundDALException('Role not found');
            }

            $newUsersIds = $users->map(function ($item) {
                return $item->getId();
            })->all();
            UserModel::whereIn('id', $newUsersIds)->lockForUpdate()->get();

            return $model->users()->sync($newUsersIds);
        });

        return RoleModel::where('id', $id)->first()->users->pluck('id');
    }
}
