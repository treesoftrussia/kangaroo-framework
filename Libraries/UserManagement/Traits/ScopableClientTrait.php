<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Traits;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;

trait ScopableClientTrait
{
    /**
     * @var Collection
     */
    private $scopes;

    public function setScopes(Collection $scopesCollection)
    {
        $this->scopes = $scopesCollection;
    }

    /**
     * @return array
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @return array
     */
    public function getScopeIds()
    {
        return array_map(function ($scope) {
            return $scope->getId();
        }, $this->scopes);
    }

    /**
     * @param $scopeId
     *
     * @return mixed
     *
     * @throws RuntimeException
     */
    public function hasScope($scopeId)
    {
        if (is_array($scopeId)) {
            throw new RuntimeException('Scope arrays not supported yet.');
        } else {
            return in_array($scopeId, $this->getScopeIds());
        }
    }
}
