<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Entity\Rights;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Cast\Cast;

class RoleEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $machineName;

    /**
     * @var
     */
    private $scopes;

    /**
     * @var
     */
    private $users;

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = Cast::int($id);

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $machineName
     *
     * @return $this
     */
    public function setMachineName($machineName)
    {
        $this->machineName = $machineName;

        return $this;
    }

    /**
     * @return string
     */
    public function getMachineName()
    {
        return $this->machineName;
    }

    /**
     * @param Collection $scopes
     *
     * @return $this
     */
    public function setScopes(Collection $scopes)
    {
        $this->scopes = $scopes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param Collection $users
     *
     * @return $this
     */
    public function setUsers(Collection $users)
    {
        $this->users = $users;

        return $this;
    }
}
