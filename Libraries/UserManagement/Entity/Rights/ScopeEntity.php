<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Entity\Rights;

use Illuminate\Support\Collection;

class ScopeEntity
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var Collection
     */
    private $roles;

    /**
     * @var Collection
     */
    private $oauthClients;

    /**
     * @param $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param Collection $roles
     *
     * @return $this
     */
    public function setRoles(Collection $roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOauthClients()
    {
        return $this->oauthClients;
    }

    /**
     * @param Collection $oauthClients
     *
     * @return $this
     */
    public function setOauthClients(Collection $oauthClients)
    {
        $this->oauthClients = $oauthClients;

        return $this;
    }
}
