<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Entity\Users;

use Mildberry\Kangaroo\Libraries\FileManagement\Entity\BaseImageFileEntity;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\AvatarEntityInterface;

/**
 * @author Egor Zyuskin <e.zyuskin@mildberry.com>
 */
class BaseAvatarEntity extends BaseImageFileEntity implements AvatarEntityInterface
{
}
