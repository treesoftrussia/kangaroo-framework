<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Entity\Users;

use Mildberry\Kangaroo\Libraries\Support\Behaviours\TimesTrait;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;

abstract class BaseUserEntity implements UserEntityInterface
{
    use TimesTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public abstract function isGuest();
}
