<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement\Entity\Users;

class GuestUserEntity extends BaseUserEntity
{

    /**
     * @var
     */
    private $guestId;

    /**
     * @return bool
     */
    public function isGuest()
    {
        return true;
    }

    /**
     * @return mixed
     */
    public function getScopes()
    {
        // TODO: Implement getScopes() method.
    }

    /**
     * @param $scope
     *
     * @return mixed
     */
    public function hasScope($scope)
    {
        // TODO: Implement hasScope() method.
    }

    /**
     * @return mixed
     */
    public function getScopeIds()
    {
        // TODO: Implement getScopeIds() method.
    }

    /**
     * @return mixed
     */
    public function getGuestId(){
        return $this->guestId;
    }

    /**
     * @param $guestId
     * @return $this
     */
    public function setGuestId($guestId){
        $this->guestId = $guestId;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasGuestId(){
        return !empty($this->guestId);
    }
}
