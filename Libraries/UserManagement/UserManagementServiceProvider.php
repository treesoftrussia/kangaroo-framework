<?php

namespace Mildberry\Kangaroo\Libraries\UserManagement;

use Illuminate\Support\ServiceProvider as Provider;
use Mildberry\Kangaroo\Libraries\Service\UserServiceInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Adapter\BaseUserAdapter;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\FlattenAdapter\BaseUserFlattenAdapter;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Model\BaseUserModel;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Repository\RoleRepository;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Repository\ScopeRepository;
use Mildberry\Kangaroo\Libraries\UserManagement\DAL\Repository\UserRepository;
use Mildberry\Kangaroo\Libraries\UserManagement\Entity\Users\BaseAvatarEntity;
use Mildberry\Kangaroo\Libraries\UserManagement\Entity\Users\UserEntity;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\AvatarEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\AvatarServiceInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\RoleRepositoryInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\ScopeRepositoryInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserAdapterInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserFlattenAdapterInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserModelInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserRepositoryInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Services\BaseAvatarService;
use Mildberry\Kangaroo\Libraries\UserManagement\Services\BaseUserService;

/**
 * @author Andrew Sparrow<andrew.sprw@gmail.com>
 */
class UserManagementServiceProvider extends Provider
{
    public function register()
    {
        $this->app->singleton(RoleRepositoryInterface::class, RoleRepository::class);
        $this->app->singleton(ScopeRepositoryInterface::class, ScopeRepository::class);

        $this->app->singleton(UserServiceInterface::class, BaseUserService::class);
        $this->app->alias(BaseUserService::class, UserServiceInterface::class);
        $this->app->singleton(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(UserModelInterface::class, BaseUserModel::class);
        $this->app->bind(UserEntityInterface::class, UserEntity::class);
        $this->app->bind(UserAdapterInterface::class, BaseUserAdapter::class);
        $this->app->bind(UserFlattenAdapterInterface::class, BaseUserFlattenAdapter::class);

        $this->app->bind(AvatarEntityInterface::class, BaseAvatarEntity::class);
        $this->app->singleton(AvatarServiceInterface::class, BaseAvatarService::class);
    }

    public function boot()
    {
    }
}
