<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Support;

use RuntimeException;

/**
 * The object that represents scope
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Scope
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * Creates scope
     *
     * @param string $key
     * @throws RuntimeException
     */
    public function create($key)
    {
        $this->data[] = $key;
    }

    /**
     * Drops previously created scope
     */
    public function drop()
    {
        array_pop($this->data);
    }

    /**
     * Gets current scope or possible current scope if the key is provided
     *
     * @param $key
     * @param bool $sensitive
     * @return string
     */
    public function get($key = null, $sensitive = false)
    {
        if ($key === null) {
            return $this->toString($this->data, $sensitive);
        }

        $data = $this->data;
        $data[] = $key;

        return $this->toString($data, $sensitive);
    }

    /**
     * @param string $key
     * @return string
     */
    public function getSensitive($key = null)
    {
        return $this->get($key, true);
    }

    /**
     * @param array $data
     * @param bool $sensitive
     * @return string
     */
    private function toString(array $data, $sensitive = false)
    {
        $string = implode('.', $data);

        if (!$sensitive) {
            $string = strtolower($string);
        }

        return $string;
    }
} 