<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Support;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
trait IgnoreTrait
{
    /**
     * Keeps config for ignoring
     *
     * @var array
     */
    protected $ignores = [];

    /**
     * Sets list ignored fields
     * @param array $fields
     * @return $this
     */
    public function ignore(array $fields)
    {
        $this->ignores = array_merge($this->ignores, array_map('strtolower', $fields));
        return $this;
    }

    /**
     * Return true if field in ignored list
     * @param string $field
     * @return bool
     */
    public function isIgnored($field)
    {
        return in_array($field, $this->ignores);
    }
}