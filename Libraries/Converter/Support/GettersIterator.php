<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Support;

use FilterIterator;
use ArrayIterator;
use ReflectionMethod;
use ReflectionObject;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class GettersIterator extends FilterIterator
{
    /**
     * @param object $object
     */
    public function __construct($object)
    {
        $reflection = new ReflectionObject($object);
        $methods = $reflection->getMethods(ReflectionMethod::IS_PUBLIC);

        parent::__construct(new ArrayIterator($methods));
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Check whether the current element of the iterator is acceptable
     * @link http://php.net/manual/en/filteriterator.accept.php
     * @return bool true if the current element is acceptable, otherwise false.
     */
    public function accept()
    {
        /**
         * @var ReflectionMethod $method
         */
        $method = $this->getInnerIterator()->current();
        $name = $method->getName();

        return is_getter($name) &&
        (count($method->getParameters()) == 0);
    }
}