<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Support;

/**
 * Adds functions for mapping fields
 * @author Sergei Melnikov <me@rnr.name>
 */
trait MapTrait
{
    /**
     * Keeps mapping
     *
     * @var array
     */
    protected $maps = [];

    protected $originalMaps = [];

    /**
     * @param array $rules
     * @return $this
     */
    public function map(array $rules)
    {
        $this->originalMaps = array_merge($this->maps, $rules);
        $this->maps = array_merge($this->maps, keystolower($rules));

        return $this;
    }

    /**
     * Return mapped name if it exists in map
     * @param string $field
     * @param string $key
     * @return mixed
     */
    public function getMappedName($field, $key = null)
    {
        if ($key === null) {
            $key = $field;
        }

        return array_take($this->maps, strtolower($field), $key);
    }
}