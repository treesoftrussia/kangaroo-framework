<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Support;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
trait DefaultTrait
{
    /**
     * @var array
     */
    protected $defaults = [];

    /**
     * Sets default values for empty field.
     * @param $rules
     * @return $this
     */
    public function defaults($rules)
    {
        $this->defaults = $rules;

        return $this;
    }
}