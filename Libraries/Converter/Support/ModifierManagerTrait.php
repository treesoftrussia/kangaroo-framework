<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Support;

use Mildberry\Kangaroo\Libraries\Modifier\Manager;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
trait ModifierManagerTrait
{

    /**
     * @var Manager
     */
    private $modifierManager;

    /**
     * Keeps config for modifiers
     *
     * @var array
     */
    private $modifiers = ['before' => [], 'after' => []];

    /**
     * Modifies value before resolution
     *
     * @param mixed $value
     * @param string $field
     * @return mixed
     */
    protected function modifyBefore($value, $field)
    {
        return $this->modifyValue('before', $value, $field);
    }

    /**
     * Modifies value after resolution
     *
     * @param mixed $value
     * @param string $field
     * @return mixed
     */
    protected function modifyAfter($value, $field)
    {
        return $this->modifyValue('after', $value, $field);
    }

    /**
     * @param string $block
     * @param mixed $value
     * @param string $field
     * @return mixed
     */
    protected function modifyValue($block, $value, $field)
    {
        if (isset($this->modifiers[$block][$field])) {
            return $this->modifierManager->modify($value, $this->modifiers[$block][$field]);
        }

        return $value;
    }

    /**
     * @param string $type
     * @param string $field
     * @param mixed $modifiers
     * @return $this
     */
    public function modify($type, $field, $modifiers)
    {
        $field = strtolower($field);
        $this->modifiers[$type][$field] = $modifiers;

        return $this;
    }

    /**
     * @param Manager $manager
     * @return $this
     */
    public function setModifierManager(Manager $manager)
    {
        $this->modifierManager = $manager;

        return $this;
    }

    /**
     * @return Manager
     */
    public function getModifierManager()
    {
        return $this->modifierManager;
    }
}