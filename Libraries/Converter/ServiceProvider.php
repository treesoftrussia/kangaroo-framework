<?php
namespace Mildberry\Kangaroo\Libraries\Converter;

use Illuminate\Support\ServiceProvider as Provider;
use Mildberry\Kangaroo\Libraries\Converter\Extractor\Extractor;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Populator;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ServiceProvider extends Provider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {
    }
}