<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Extractor;

use Exception;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ExtractorException extends Exception
{
}