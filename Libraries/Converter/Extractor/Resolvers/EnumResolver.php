<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Extractor\Resolvers;

use Mildberry\Kangaroo\Libraries\Enum\Enum;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class EnumResolver extends AbstractResolver
{
    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @return bool
     */
    public function canResolve($field, $value)
    {
        return $value instanceof Enum;
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param Enum $value
     * @return mixed
     */
    public function resolve($field, $value)
    {
        return $value->value();
    }
}