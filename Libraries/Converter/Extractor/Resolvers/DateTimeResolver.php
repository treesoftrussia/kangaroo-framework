<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Extractor\Resolvers;

use DateTime;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class DateTimeResolver extends AbstractResolver
{

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @return bool
     */
    public function canResolve($field, $value)
    {
        return $value instanceof DateTime;
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param DateTime $value
     * @return mixed
     */
    public function resolve($field, $value)
    {
        $modifierManager = $this->extractor->getModifierManager();

        return $modifierManager ? $modifierManager->modify($value, 'datetime') :
            $value->format('Y-m-d H:i:s');
    }
}