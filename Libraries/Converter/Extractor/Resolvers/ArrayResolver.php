<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Extractor\Resolvers;

use Traversable;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ArrayResolver extends AbstractResolver
{

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @return bool
     */
    public function canResolve($field, $value)
    {
        return is_array($value) || ($value instanceof Traversable);
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param array|Traversable $value
     * @return array
     */
    public function resolve($field, $value)
    {
        $array = [];

        foreach ($value as $item) {
            $array[] = $this->extractor->extract($item);
        }

        return $array;
    }
}