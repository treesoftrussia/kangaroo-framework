<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Extractor\Resolvers;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class SimpleResolver extends AbstractResolver
{

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @return bool
     */
    public function canResolve($field, $value)
    {
        return true;
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param mixed $value
     * @return mixed
     */
    public function resolve($field, $value)
    {
        return $value;
    }
}