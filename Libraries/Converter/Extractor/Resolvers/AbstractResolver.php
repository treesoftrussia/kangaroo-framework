<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Extractor\Resolvers;

use Mildberry\Kangaroo\Libraries\Converter\Extractor\Extractor;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Populator;
use ReflectionClass;

/**
 * The abstract class for all resolvers
 *
 * @author Sergei Melnikov <me@rnr.name>
 */
abstract class AbstractResolver
{
    /**
     * @var Extractor
     */
    protected $extractor;

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @param Extractor $extractor
     * @param array $config
     */
    public function __construct(Extractor $extractor, array $config = [])
    {
        $this->extractor = $extractor;
        $this->config = $config;
    }

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @return bool
     */
    abstract public function canResolve($field, $value);

    /**
     * Resolves a value
     *
     * @param string $field
     * @param mixed $value
     * @return mixed
     */
    abstract public function resolve($field, $value);
}