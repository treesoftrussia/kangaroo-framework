<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Extractor\Resolvers;

use Mildberry\Kangaroo\Libraries\Enum\EnumHash;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class EnumHashResolver extends AbstractResolver
{

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @return bool
     */
    public function canResolve($field, $value)
    {
        return $value instanceof EnumHash;
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param EnumHash $value
     * @return mixed
     */
    public function resolve($field, $value)
    {
        $hash = [];
        $keys = $value->keys();

        foreach ($keys as $key) {
            $hash[$key->value()] = $this->extractor->extract($value->get($key));
        }

        return $hash;
    }
}