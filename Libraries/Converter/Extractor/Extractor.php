<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Extractor;

use Mildberry\Kangaroo\Libraries\Converter\Extractor\Resolvers\AbstractResolver;
use Mildberry\Kangaroo\Libraries\Converter\Extractor\Resolvers\ArrayResolver;
use Mildberry\Kangaroo\Libraries\Converter\Extractor\Resolvers\DateTimeResolver;
use Mildberry\Kangaroo\Libraries\Converter\Extractor\Resolvers\EnumHashResolver;
use Mildberry\Kangaroo\Libraries\Converter\Extractor\Resolvers\EnumResolver;
use Mildberry\Kangaroo\Libraries\Converter\Extractor\Resolvers\InstanceResolver;
use Mildberry\Kangaroo\Libraries\Converter\Extractor\Resolvers\SimpleResolver;
use Mildberry\Kangaroo\Libraries\Converter\Support\IgnoreTrait;
use Mildberry\Kangaroo\Libraries\Converter\Support\MapTrait;
use Mildberry\Kangaroo\Libraries\Converter\Support\ModifierManagerTrait;
use Mildberry\Kangaroo\Libraries\Converter\Support\Scope;
use ReflectionClass;
use ReflectionMethod;

/**
 * Class Extractor converts an Object to array using getters of class (is*, get*, and has* methods)
 * @package Mildberry\Kangaroo\Libraries\Converter
 *
 * @usage
 * You can see usage Populator in test class \App\Tests\Libraries\Converter\ExtractorTest
 * @author Sergei Melnikov <me@rnr.name>
 */
class Extractor
{
    use IgnoreTrait;
    use MapTrait;
    use ModifierManagerTrait;

    /**
     * The current scope
     *
     * @var Scope
     */
    private $scope;


    private $allow;

    /**
     * @param array $config has keys:
     * - 'ignore' - list of ignored fields
     * - 'map' - list of aliases for fields
     */
    public function __construct($config = [])
    {
        $this->scope = new Scope();

        $this->ignore(array_take($config, 'ignore', []));
        $this->map(array_take($config, 'map', []));

        $this->allow = array_take($config, 'allow', []);


        foreach (array_take($config, 'modify', []) as $type => $block) {
            foreach ($block as $field => $modifiers) {
                $this->modify($type, $field, $modifiers);
            }
        }
    }

    /**
     * For each method of $object starts with is*, get* and has* extract method checks type
     * of the variable that returned from a method.
     * If variable is class then method is called recursive for this object.
     * If variable is Enum then value derived from Enum added to array.
     * Elsewhere variable added to array.
     *
     * @param object $object
     * @return array
     */
    public function extract($object)
    {
        if (!is_object($object)) {
            $value = $this->modifyBefore($object, '');
            $value = $this->resolve($value);
            $value = $this->modifyAfter($value, '');

            return $value;
        }

        $transformedObject = [];

        $objectReflection = new ReflectionClass($object);

        foreach ($objectReflection->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            $key = $this->tryGetKey($method);

            if (!$key) {
                continue;
            }

            $this->scope->create($key);
            $scopeField = $this->scope->get();

            $value = $this->getValue($object, $method);

            $value = $this->modifyBefore($value, $scopeField);
            $value = $this->resolve($value);
            $value = $this->modifyAfter($value, $scopeField);

            $transformedObject[$this->getFieldName($key)] = $value;

            $this->scope->drop();
        }

        return $transformedObject;
    }

    protected function isKeyAllowed($key){
        return empty($this->allow) ? true : in_array($key, $this->allow);
    }

    /**
     * @param ReflectionMethod $method
     * @return null|string
     */
    protected function tryGetKey(ReflectionMethod $method)
    {
        if (!is_getter($method->name)) {
            return null;
        }

        if (count($method->getParameters()) != 0) {
            return null;
        }

        $key = make_property($method->name);

        if ($this->isIgnored($this->scope->get($key))) {
            return null;
        }

        if (!$this->isKeyAllowed($this->scope->get($key))){
            return null;
        }

        return $key;
    }

    /**
     * @param object $object
     * @param ReflectionMethod $method
     * @return mixed
     */
    protected function getValue($object, ReflectionMethod $method)
    {
        return $method->invoke($object);
    }

    /**
     * @param string $key
     * @return string
     */
    private function getFieldName($key)
    {
        return $this->getMappedName($this->scope->get(), $key);
    }

    /**
     * @param mixed $value
     * @return mixed
     * @throws ExtractorException
     */
    protected function resolve($value)
    {
        $resolvers = $this->createResolvers();
        $scope = $this->scope->get();

        foreach ($resolvers as $resolver) {
            $canResolve = $resolver->canResolve($scope, $value);

            if ($canResolve) {
                return $resolver->resolve($scope, $value);
            }
        }

        throw new ExtractorException(
            "Can't extract object key '{$scope}'.");
    }

    /**
     * @return AbstractResolver[]
     */
    protected function createResolvers()
    {
        return [
            new ArrayResolver($this),
            new EnumHashResolver($this),
            new EnumResolver($this),
            new DateTimeResolver($this),
            new InstanceResolver($this),
            new SimpleResolver($this)
        ];
    }
}
