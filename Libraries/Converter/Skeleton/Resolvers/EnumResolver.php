<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Skeleton\Resolvers;

use ReflectionClass;
use Mildberry\Kangaroo\Libraries\Enum\Enum;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class EnumResolver extends AbstractResolver
{

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param ReflectionClass $class
     * @return bool
     */
    public function canResolve($field, ReflectionClass $class = null)
    {
        return $class && $class->isSubclassOf(Enum::class);
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param ReflectionClass $class
     * @return mixed
     */
    public function resolve($field, ReflectionClass $class = null)
    {
        return null;
    }
}