<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Skeleton\Resolvers;

use ReflectionClass;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class HintResolver extends AbstractResolver
{
    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param ReflectionClass $class
     * @return bool
     */
    public function canResolve($field, ReflectionClass $class = null)
    {
        return isset($this->config[$field]);
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param ReflectionClass $class
     * @return mixed
     */
    public function resolve($field, ReflectionClass $class = null)
    {
        return $this->config[$field];
    }
}