<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Skeleton\Resolvers;

use Mildberry\Kangaroo\Libraries\Converter\Skeleton\Skeleton;
use ReflectionClass;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
abstract class AbstractResolver
{
    /**
     * @var Skeleton
     */
    protected $skeleton;

    /**
     * @var array
     */
    protected $config = [];

    /**
     * AbstractResolver constructor.
     * @param Skeleton $skeleton
     * @param array $config
     */
    public function __construct(Skeleton $skeleton, array $config = [])
    {
        $this->skeleton = $skeleton;
        $this->config = $config;
    }

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param ReflectionClass $class
     * @return bool
     */
    abstract public function canResolve($field, ReflectionClass $class = null);

    /**
     * Resolves a value
     *
     * @param string $field
     * @param ReflectionClass $class
     * @return mixed
     */
    abstract public function resolve($field, ReflectionClass $class = null);
}