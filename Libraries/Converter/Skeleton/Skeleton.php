<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Skeleton;

use Mildberry\Kangaroo\Libraries\Converter\Support\IgnoreTrait;
use Mildberry\Kangaroo\Libraries\Converter\Support\MapTrait;
use Mildberry\Kangaroo\Libraries\Converter\Support\Scope;
use Mildberry\Kangaroo\Libraries\Converter\Skeleton\Resolvers\AbstractResolver;
use Mildberry\Kangaroo\Libraries\Converter\Skeleton\Resolvers\HintResolver;
use Mildberry\Kangaroo\Libraries\Converter\Skeleton\Resolvers\EnumResolver;
use Mildberry\Kangaroo\Libraries\Converter\Skeleton\Resolvers\InstanceResolver;
use Mildberry\Kangaroo\Libraries\Converter\Skeleton\Resolvers\SimpleResolver;
use ReflectionClass;
use ReflectionMethod;

/**
 * Class Skeleton provides information about class structure
 * @author Sergei Melnikov <me@rnr.name>
 */
class Skeleton
{
    use IgnoreTrait;
    use MapTrait;

    /**
     * The current scope
     *
     * @var Scope
     */
    private $scope;

    /**
     * Keeps config for HintResolver
     *
     * @var array
     */
    private $hints = [];

    /**
     * @param array $config has keys:
     * - 'ignore' - list of ignored fields
     * - 'map' - list of aliases for fields
     * - 'hint' - list hinters for arrays
     */
    public function __construct($config = [])
    {
        $this->scope = new Scope();

        foreach (array_take($config, 'hint', []) as $field => $class) {
            $this->hint($field, $class);
        }

        $this->ignore(array_take($config, 'ignore', []));
        $this->map(array_take($config, 'map', []));
    }

    /**
     * @param string $field
     * @param string $className
     * @return $this
     */
    public function hint($field, $className)
    {
        $this->hints[strtolower($field)] = $className;

        return $this;
    }

    /**
     * Gets information about all nested keys in class in dot notation.
     * @param string $className
     * @return array
     * @throws SkeletonException
     */
    public function flatten($className)
    {
        $fields = [];

        $reflectionClass = new ReflectionClass($className);
        $methods = $reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC);

        foreach ($methods as $method) {
            if (!$this->passes($method)) {
                continue;
            }

            $field = $this->getFieldName($method);
            $dottedKey = $this->scope->getSensitive(
                $this->getMappedName($this->scope->get($field), $field));

            $this->scope->create($field);

            $type = $this->getTypeValue($reflectionClass, $field);

            if (!$type) {
                $fields[] = $dottedKey;
            } else {
                $fields = array_merge($fields, $this->flatten($type));
            }

            $this->scope->drop();
        }

        return $fields;
    }

    /**
     * @param ReflectionMethod $getter
     * @return bool
     */
    private function passes(ReflectionMethod $getter)
    {
        if (!is_getter($getter->name)) {
            return false;
        }

        $field = $this->getFieldName($getter);

        if ($this->isIgnored($this->scope->get($field))) {
            return false;
        }

        if (count($getter->getParameters()) != 0) {
            return false;
        }

        return true;
    }

    /**
     * Creates field name from the getter reflection
     *
     * @param ReflectionMethod $getter
     * @return string
     */
    private function getFieldName(ReflectionMethod $getter)
    {
        return make_property($getter->name);
    }

    /**
     * @param ReflectionClass $reflectionClass
     * @param string $field
     * @return string|null
     * @throws SkeletonException
     */
    private function getTypeValue(ReflectionClass $reflectionClass, $field)
    {
        $setter = $this->getSetter($reflectionClass, $field);

        if ($setter === null) {
            return null;
        }

        $scope = $this->scope->get();
        $class = $setter->getParameters()[0]->getClass();
        $resolvers = $this->createResolvers();

        foreach ($resolvers as $resolver) {
            $canResolve = $resolver->canResolve($scope, $class);

            if ($canResolve) {
                return $resolver->resolve($scope, $class);
            }
        }

        throw new SkeletonException(
            "Can't get object key '{$scope}' type.");
    }

    /**
     * @param ReflectionClass $reflectionClass
     * @param string $field
     * @return null|ReflectionMethod
     */
    private function getSetter(ReflectionClass $reflectionClass, $field)
    {
        if (preg_match('/^(is|has)[A-Z0-9]/', $field)) {
            return null;
        }

        $setterName = make_setter($field);

        if (!$reflectionClass->hasMethod($setterName)) {
            return null;
        }

        $setter = $reflectionClass->getMethod($setterName);

        if (count($setter->getParameters()) != 1) {
            return null;
        }

        return $setter;
    }

    /**
     * @return AbstractResolver[]
     */
    private function createResolvers()
    {
        return [
            new HintResolver($this, $this->hints),
            new EnumResolver($this),
            new InstanceResolver($this),
            new SimpleResolver($this)
        ];
    }
}