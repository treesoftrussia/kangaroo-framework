<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Transferer;

use Mildberry\Kangaroo\Libraries\Converter\Extractor\Extractor;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Populator;
use Mildberry\Kangaroo\Libraries\Converter\Support\DefaultTrait;
use Mildberry\Kangaroo\Libraries\Converter\Support\IgnoreTrait;
use Mildberry\Kangaroo\Libraries\Converter\Support\MapTrait;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class Transferer
{
    use IgnoreTrait;
    use MapTrait;
    use DefaultTrait;

    /**
     * @param array $config - can receive 'ignore', 'map', 'defaults' fields
     */
    public function __construct($config = [])
    {
        $this->ignore(array_take($config, 'ignore', []));
        $this->map(array_take($config, 'map', []));
        $this->defaults(array_take($config, 'defaults', []));
    }

    /**
     * @param object $srcObject
     * @param object $destObject
     * @return object
     */
    public function transfer($srcObject, $destObject)
    {
        $data = (new Extractor([
            'ignore' => $this->ignores
        ]))->extract($srcObject);

        $flatten = array_smash($data);
        $transferredArray = dotted_array_merge($flatten, $this->getMappedValues($flatten));

        return (new Populator([
            'defaults' => $this->defaults
        ]))->populate(array_unsmash($transferredArray), $destObject);
    }

    /**
     * @param array $srcObject
     * @return array
     */
    private function getMappedValues(array $srcObject)
    {
        $data = [];

        foreach ($this->originalMaps as $key => $value) {
            if (array_key_exists($key, $srcObject)) {
                $data[$value] = $srcObject[$key];
            }
        }

        return $data;
    }
}