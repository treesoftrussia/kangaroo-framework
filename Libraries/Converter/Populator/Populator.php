<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator;

use Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers\DateTimeResolver;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers\EnumHashResolver;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers\VectorResolver;
use Mildberry\Kangaroo\Libraries\Converter\Support\DefaultTrait;
use Mildberry\Kangaroo\Libraries\Converter\Support\IgnoreTrait;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers\AggregateResolver;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers\AllowResolver;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers\AbstractResolver;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers\EnumResolver;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers\HintResolver;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers\InstanceResolver;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers\SimpleResolver;
use Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers\HintCollectionResolver;
use Mildberry\Kangaroo\Libraries\Converter\Support\MapTrait;
use Mildberry\Kangaroo\Libraries\Converter\Support\ModifierManagerTrait;
use Mildberry\Kangaroo\Libraries\Converter\Support\Scope;
use Illuminate\Support\Collection;
use ReflectionClass;
use ReflectionMethod;

/**
 * Class Populator converts an array to Object using setters of class
 * @package Mildberry\Kangaroo\Libraries\Converter
 *
 * @usage
 * You can see usage Populator in test class \App\Tests\Libraries\Converter\PopulatorTest
 * @author Sergei Melnikov <me@rnr.name>
 */
class Populator
{
    use IgnoreTrait;
    use MapTrait;
    use DefaultTrait;
    use ModifierManagerTrait;

    /**
     * Keeps config for AggregateResolver
     *
     * @var array
     */
    private $aggregates = [];

    /**
     * Keeps config for HintResolver
     *
     * @var array
     */
    private $hints = [];

    /**
     * Keeps config for HintCollectionResolver
     *
     * @var array
     */
    private $hintsCollection = [];

    /**
     * Keeps config for AllowResolver
     *
     * @var array
     */
    private $allows = [];

    /**
     * The current scope
     *
     * @var Scope
     */
    private $scope;

    /**
     * @param array $config has keys:
     * - 'ignore' - list of ignored fields
     * - 'allow' - list of fields that can take array
     * - 'hint' - hash of fields/class that can take array of class
     * - 'resolver' - list of resolves for fields
     */
    public function __construct($config = [])
    {
        $this->scope = new Scope();

        foreach (array_take($config, 'hint', []) as $field => $class) {
            $this->hint($field, $class);
        }

        foreach (array_take($config, 'resolver', []) as $field => $callback) {
            $this->resolver($field, $callback);
        }

        $this->allow(array_take($config, 'allow', []));
        $this->ignore(array_take($config, 'ignore', []));
        $this->map(array_take($config, 'map', []));
        $this->defaults(array_take($config, 'defaults', []));

        foreach (array_take($config, 'modify', []) as $type => $block) {
            foreach ($block as $field => $modifiers) {
                $this->modify($type, $field, $modifiers);
            }
        }
    }

    /**
     * @param $data
     * @param null $object
     * @return array|bool
     */
    protected function tryToMakeRootCollection($data, $object = null){
        if(!empty($this->hintsCollection['']) && is_null($object)){
            $collectionResolver = new HintCollectionResolver($this, $this->hintsCollection);
            return $collectionResolver->resolve('', $data);
        }
        return false;
    }

    /**
     * The method populates an object with data from array with resolvers/modifiers applied
     *
     * @param array $data
     * @param object $object
     * @return object - returns the same object as $object
     * @throws PopulatorException
     */
    public function populate(array $data, $object = null)
    {
        if($collection = $this->tryToMakeRootCollection($data, $object)){
            return $collection;
        }

        $objectReflection = new ReflectionClass($object);

        $data = $this->transferDefaultsOnce($data);
        $dataWithLowerKeys = keystolower($data);


        foreach ($data as $field => $value) {
            $method = $this->tryGetSetterName($dataWithLowerKeys, $field, $objectReflection);

            if (!$method) {
                continue;
            }

            $this->scope->create($field);
            $scopeField = $this->scope->get();

            $value = $this->getValue($dataWithLowerKeys, $field);

            $value = $this->modifyBefore($value, $scopeField);

            $value = $this->resolve($value, $method);

            $value = $this->modifyAfter($value, $scopeField);

            $method->invoke($object, $value);

            $this->scope->drop();
        }

        return $object;
    }

    /**
     * @param array $data
     * @return array mixed
     */
    protected function transferDefaultsOnce($data)
    {
        foreach ($this->defaults as $key => $value) {
            $fieldValue = array_get($data, $key);

            if ($fieldValue === null) {
                array_set($data, $key, $value);
            }
        }

        $this->defaults = [];

        return $data;
    }

    /**
     * @param array $data
     * @param string $field
     * @return mixed
     */
    private function getValue(array $data, $field)
    {
        return $data[strtolower($field)];
    }

    /**
     * @param array $data
     * @param string $field
     * @return bool
     */
    private function hasValue(array $data, $field)
    {
        return isset($data[strtolower($field)]);
    }

    /**
     * Creates field name from the setter reflection
     *
     * @param string $field
     * @return string
     */
    private function getFieldName($field)
    {
        return $this->getMappedName($this->scope->get($field), $field);
    }

    /**
     * Checks whether the setter can be processed
     *
     * @param array $data
     * @param string $field
     * @param ReflectionClass $objectReflection
     * @return ReflectionMethod|null
     */
    private function tryGetSetterName(array $data, $field, ReflectionClass $objectReflection)
    {
        if (is_getter($field, ['has'])) {
            return null;
        }

        $methodName = make_setter($this->getFieldName($field));

        if (!$objectReflection->hasMethod($methodName)) {
            return null;
        }

        $setter = $objectReflection->getMethod($methodName);

        if (!$setter->isPublic()) {
            return null;
        }

        if (!is_setter($setter->name)) {
            return null;
        }

        if (!$this->hasValue($data, $field)) {
            return null;
        }

        if ($this->isIgnored($this->scope->get($field))) {
            return null;
        }

        if (count($setter->getParameters()) != 1) {
            return null;
        }

        if (!$this->isKeyAllowed($this->scope->get($field))){
            return null;
        }

        return $setter;
    }

    protected function isKeyAllowed($key){
        return empty($this->allows) ? true : in_array($key, $this->allows);
    }


    /**
     * Resolves the value
     *
     * @param mixed $value
     * @param ReflectionMethod $setter
     * @return mixed
     * @throws PopulatorException
     */
    private function resolve($value, ReflectionMethod $setter)
    {
        $resolvers = $this->createResolvers();

        $scope = $this->scope->get();
        $class = $setter->getParameters()[0]->getClass();

        foreach ($resolvers as $resolver) {
            $canResolve = $resolver->canResolve($scope, $value, $class);

            if ($canResolve) {
                return $resolver->resolve($scope, $value, $class);
            }
        }

        throw new PopulatorException(
            "Can't populate object key '{$scope}' has not allowed array.");
    }

    /**
     * Creates a list of resolvers
     *
     * @return AbstractResolver[]
     */
    private function createResolvers()
    {
        return [
            new AggregateResolver($this, $this->aggregates),
            new EnumHashResolver($this, $this->hints),
            new HintResolver($this, $this->hints),
            new HintCollectionResolver($this, $this->hintsCollection),
            new VectorResolver($this),
            new DateTimeResolver($this),
            new EnumResolver($this),
            new InstanceResolver($this),
            new SimpleResolver($this)
        ];
    }

    /**
     * Sets list allowed fields that contains arrays
     * @param array $fields
     * @return $this
     */
    public function allow(array $fields)
    {
        $this->allows = array_merge($this->allows, array_map('strtolower', $fields));
        return $this;
    }

    /**
     * Gives hint about array type to populator
     * @param string $field
     * @param $className
     * @return $this
     */
    public function hint($field, $className)
    {
        $matches = [];

        if (preg_match('/^collection:(.*)/', $className, $matches)) {
            $this->hintsCollection[strtolower($field)] = $matches[1];
        } else {
            $this->hints[strtolower($field)] = $className;
        }

        return $this;
    }

    /**
     * Gives to add resolver for selected field
     * @param $field
     * @param callable $callback
     * @return $this
     */
    public function resolver($field, callable $callback)
    {
        $this->aggregates[strtolower($field)] = $callback;

        return $this;
    }
}
