<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers;

use Mildberry\Kangaroo\Libraries\Enum\Enum;
use ReflectionClass;

/**
 * The resolver creates an Enum instance from the class that is type of an argument of a setter method
 *
 * @author Sergei Melnikov <me@rnr.name>
 */
class EnumResolver extends AbstractResolver
{
    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return object
     */
    public function resolve($field, $value, ReflectionClass $class = null)
    {
        return $class->newInstance($value);
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return bool
     */
    public function canResolve($field, $value, ReflectionClass $class = null)
    {
        return $class && $class->isSubclassOf(Enum::class);
    }
}