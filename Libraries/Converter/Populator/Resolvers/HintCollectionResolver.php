<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers;

use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Converter\Populator\PopulatorException;
use ReflectionClass;

/**
 * The resolver creates a collection of objects of class that is specified in the config for specific fields
 *
 * @author Sergei Melnikov <me@rnr.name>
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class HintCollectionResolver extends AbstractResolver
{
    use HasKeyTrait;

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return array
     * @throws PopulatorException
     */
    public function resolve($field, $value, ReflectionClass $class = null)
    {
        $className = $this->config[$field];


        $array = [];

        foreach ($value as $item) {
            if(is_array($item)){
                $array[] = $this->populator->populate($item, new $className());
            } else {
                $array[] = $item;
            }
        }

        return new Collection($array);
    }
}