<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers;

use DateTime;
use ReflectionClass;

/**
 * The resolver creates an DateTime instance
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class DateTimeResolver extends AbstractResolver
{
    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return object
     */
    public function resolve($field, $value, ReflectionClass $class = null)
    {
        return $class->newInstance($value);
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return bool
     */
    public function canResolve($field, $value, ReflectionClass $class = null)
    {
        return $class && ($class->isSubclassOf(DateTime::class) || $class->getName() === DateTime::class);
    }
}