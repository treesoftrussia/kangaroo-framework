<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers;

use ReflectionClass;

/**
 * The resolver simply passes a value if it's not an array and type of an argument of a setter method is scalar
 *
 * @author Sergei Melnikov <me@rnr.name>
 */
class SimpleResolver extends AbstractResolver
{
    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return mixed
     */
    public function resolve($field, $value, ReflectionClass $class = null)
    {
        return $value;
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return bool
     */
    public function canResolve($field, $value, ReflectionClass $class = null)
    {
        return $class === null && !is_array($value);
    }
}