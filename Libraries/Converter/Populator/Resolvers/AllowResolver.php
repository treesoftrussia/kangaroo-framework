<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers;

use ReflectionClass;

/**
 * The resolver passes thru a value for a specified fields.
 * That is useful, when you want to specify explicitly to what fields the value from the dataset must be just passed
 *
 * @author Sergei Melnikov <me@rnr.name>
 */
class AllowResolver extends AbstractResolver
{

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return bool
     */
    public function canResolve($field, $value, ReflectionClass $class = null)
    {
        return in_array($field, $this->config);
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return mixed
     */
    public function resolve($field, $value, ReflectionClass $class = null)
    {
        return $value;
    }

}