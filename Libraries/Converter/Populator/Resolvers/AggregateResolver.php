<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers;

use ReflectionClass;

/**
 * The resolver allows to set any callable callbacks to resolve a value for any specified field
 *
 * @author Sergei Melnikov <me@rnr.name>
 */
class AggregateResolver extends AbstractResolver
{
    use HasKeyTrait;

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return mixed
     */
    public function resolve($field, $value, ReflectionClass $class = null)
    {
        return $this->config[$field]($this->populator, $value);
    }
}