<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers;
use Mildberry\Kangaroo\Libraries\Enum\EnumHash;
use ReflectionClass;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class EnumHashResolver extends AbstractResolver
{

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return bool
     */
    public function canResolve($field, $value, ReflectionClass $class = null)
    {
        return $class && $class->isSubclassOf(EnumHash::class);
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param EnumHash $value
     * @param ReflectionClass $class
     * @return mixed
     */
    public function resolve($field, $value, ReflectionClass $class = null)
    {
        /**
         * @var EnumHash $hash
         */
        $hash = $class->newInstance();
        $enumName = $hash->getEnumClass();
        $className = array_take($this->config, $field);

        foreach ($value as $key => $item) {
            $item = $className === null ? $item : $this->populator->populate($item, new $className());
            $hash->set(new $enumName($key), $item);
        }

        return $hash;
    }
}