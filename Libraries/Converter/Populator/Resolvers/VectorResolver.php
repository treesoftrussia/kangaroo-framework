<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers;
use ReflectionClass;

/**
 * Resolves a value if it is a vector
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class VectorResolver extends AbstractResolver
{
    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return bool
     */
    public function canResolve($field, $value, ReflectionClass $class = null)
    {
        return is_vector($value);
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return mixed
     */
    public function resolve($field, $value, ReflectionClass $class = null)
    {
        return $value;
    }
}