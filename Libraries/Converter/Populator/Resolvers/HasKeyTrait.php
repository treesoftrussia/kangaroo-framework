<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers;

use ReflectionClass;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
trait HasKeyTrait
{
    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return bool
     */
    public function canResolve($field, $value, ReflectionClass $class = null)
    {
        return isset($this->config[$field]);
    }
}