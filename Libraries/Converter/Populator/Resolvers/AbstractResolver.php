<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers;

use Mildberry\Kangaroo\Libraries\Converter\Populator\Populator;
use ReflectionClass;

/**
 * The abstract class for all resolvers
 *
 * @author Sergei Melnikov <me@rnr.name>
 */
abstract class AbstractResolver
{
    /**
     * @var Populator
     */
    protected $populator;

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @param Populator $populator
     * @param array $config
     */
    public function __construct(Populator $populator, array $config = [])
    {
        $this->populator = $populator;
        $this->config = $config;
    }

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return bool
     */
    abstract public function canResolve($field, $value, ReflectionClass $class = null);

    /**
     * Resolves a value
     *
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return mixed
     */
    abstract public function resolve($field, $value, ReflectionClass $class = null);
}