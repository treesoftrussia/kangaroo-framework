<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers;

use Mildberry\Kangaroo\Libraries\Converter\Populator\PopulatorException;
use ReflectionClass;

/**
 * The resolver creates an object of class that is specified in the config for specific fields
 *
 * @author Sergei Melnikov <me@rnr.name>
 */
class HintResolver extends AbstractResolver
{
    use HasKeyTrait;

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return object
     * @throws PopulatorException
     */
    public function resolve($field, $value, ReflectionClass $class = null)
    {
        $className = $this->config[$field];

        return $this->populator->populate($value, new $className());
    }

}