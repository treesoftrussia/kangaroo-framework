<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator\Resolvers;

use Mildberry\Kangaroo\Libraries\Converter\Populator\PopulatorException;
use ReflectionClass;

/**
 * The resolver creates an instance from the class that is type of an argument in a setter method
 *
 * @author Sergei Melnikov <me@rnr.name>
 */
class InstanceResolver extends AbstractResolver
{
    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return bool
     */
    public function canResolve($field, $value, ReflectionClass $class = null)
    {
        return $class && is_array($value);
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionClass $class
     * @return object
     * @throws PopulatorException
     */
    public function resolve($field, $value, ReflectionClass $class = null)
    {
        return $this->populator->populate($value, $class->newInstance());
    }
}