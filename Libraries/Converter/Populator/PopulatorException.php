<?php
namespace Mildberry\Kangaroo\Libraries\Converter\Populator;

use Exception;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class PopulatorException extends Exception
{
}