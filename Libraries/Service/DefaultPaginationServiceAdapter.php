<?php

namespace Mildberry\Kangaroo\Libraries\Service;

use Mildberry\Kangaroo\Libraries\Options\AbstractOptionsInterface;
use Mildberry\Kangaroo\Libraries\Options\Objects\PaginationOptions;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;
use Mildberry\Kangaroo\Libraries\Pagination\Pagination;
use Mildberry\Kangaroo\Libraries\Pagination\PaginationEntity;
use Mildberry\Kangaroo\Libraries\Pagination\PaginationEntityInterface;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class DefaultPaginationServiceAdapter
{
    /**
     * @var AbstractService
     */
    private $provider;

    /**
     * @param $provider
     *
     * @throws RuntimeException
     */
    public function __construct($provider)
    {
        if (!method_exists($provider, $this->getAllMethodName())) {
            throw new RuntimeException('The provider does not have method "'.$this->getAllMethodName().'".');
        }

        if (!method_exists($provider, $this->getTotalMethodName())) {
            throw new RuntimeException('The provider does not have method "'.$this->getTotalMethodName().'".');
        }

        $this->provider = $provider;
    }

    /**
     * @param $total
     * @param $items
     * @param $current
     * @param $totalPages
     * @param $itemsPerPage
     * @return PaginationEntityInterface
     */
    protected function buildPaginationEntity($total, $items, $current, $totalPages, $itemsPerPage)
    {
        $paginationEntity = new PaginationEntity();

        $paginationEntity->setItems($items);

        $pagination = new Pagination();
        $pagination->setTotalEntries($total);

        $pagination->setEntriesOnCurrentPage($total, $current, $itemsPerPage, $totalPages);
        $pagination->setEntriesPerPageRequested($itemsPerPage);
        $pagination->setCurrentPage($current);
        $pagination->setTotalPages($totalPages);

        $paginationEntity->setPagination($pagination);

        return $paginationEntity;
    }

    /**
     * @param AbstractOptionsInterface $options
     * @return PaginationEntityInterface
     */
    public function getPaginationEntity(AbstractOptionsInterface $options)
    {
        if(!($options instanceof PaginationOptions)){
            $paginationOptions = $options->getPagination();
        } else {
            $paginationOptions = $options;
        }

        $optionsClone = unserialize(serialize($options));

        $params = func_get_args();

        $total = call_user_func_array([$this->provider, $this->getTotalMethodName()], $params);

        //@TODO: костыль, здесь нам нужен полный клон объекта, мы не можем передавать полный клон опций в getAll и getTotal, так каквнутри опции могут изменяться
        $params[0] = $optionsClone;

        $items = call_user_func_array([$this->provider, $this->getAllMethodName()], $params);

        return $this->buildPaginationEntity($total, $items, $paginationOptions->getPageNum(), (int) ceil($total / $paginationOptions->getPerPage()), $paginationOptions->getPerPage());
    }

    /**
     * Returns method name for items retrieving.
     *
     * @return string
     */
    protected function getAllMethodName()
    {
        return 'getAll';
    }

    /**
     * Get method name for retrieving total items.
     *
     * @return string
     */
    protected function getTotalMethodName()
    {
        return 'getTotal';
    }

    /**
     * is triggered when invoking inaccessible methods in an object context.
     *
     * @param $name string
     * @param $arguments array
     *
     * @return mixed
     *
     * @link http://php.net/manual/en/language.oop5.overloading.php#language.oop5.overloading.methods
     */
    public function __call($name, $arguments)
    {
        if ($name == $this->getAllMethodName()) {
            return call_user_func_array([$this, 'getPaginationEntity'], $arguments);
        } else {
            if (method_exists($this->provider, $name)) {
                return call_user_func_array([$this->provider, $name], $arguments);
            } else {
                throw new RuntimeException('Method '.$name.' in '.get_class($this->provider).' not found.');
            }
        }
    }
}
