<?php

namespace Mildberry\Kangaroo\Libraries\Service;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\ResourceNotFoundException;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\AvatarEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserListOptionsInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
interface UserServiceInterface
{
    /**
     * @return UserEntityInterface
     */
    public function getCurrentUser();

    /**
     * @param UserEntityInterface $userEntity
     */
    public function setCurrentUser(UserEntityInterface $userEntity);

    /**
     * @return UserEntityInterface
     */
    public function getUser();

    /**
     * @param $id
     *
     * @return UserEntityInterface
     * @throws ResourceNotFoundException
     */
    public function getUserById($id);

    /**
     * @param $login
     * @param $password
     * @return mixed|void
     */
    public function getUserByCredentials($login, $password);

    /**
     * @param $options
     *
     * @return Collection
     */
    public function getAll(UserListOptionsInterface $options);

    /**
     * Sets to null current user value
     */
    public function purgeCurrentUser();

    /**
     * @return array
     */
    public function getLoginFields();

    /**
     * @param $login
     * @return mixed
     */
    public function getUserByLogin($login);

    /**
     * @param $login
     * @param $activationCode
     */
    public function checkOnActivityAndExistenceActivationCode($login, $activationCode);

    /**
     * @param int $id
     * @param UploadedFile $file
     * @return AvatarEntityInterface
     */
    public function updateAvatar($id, UploadedFile $file = null);
}