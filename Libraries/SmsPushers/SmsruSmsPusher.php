<?php
namespace App\Core\Support\SmsPushers;

class SmsruSmsPusher extends AbstractSmsPusher
{
    public function send($phone, $activationCode)
    {
        $apiId = config('params.sms_pusher_smsru_api_id');
        $responseData = file_get_contents("http://sms.ru/sms/send?api_id=" . $apiId . "&to=" . $phone . "&text=" . $activationCode);
    }
}
