<?php
namespace App\Core\Support\SmsPushers;

use \RuntimeException as DefaultRuntimeException;
use App\Libraries\Kangaroo\Error;
use Symfony\Component\HttpFoundation\Response;
use App\Libraries\Kangaroo\Exceptions\ExceptionInterface;


class InvalidActivationCodeException extends DefaultRuntimeException implements ExceptionInterface
{

    public function getHTTPCode(){
        return Response::HTTP_BAD_REQUEST;
    }

    public function getInternalCode(){
        return Error::INVALID_ACTIVATION_CODE;
    }

    public function __construct($message = 'Activation code is invalid.')
    {
        parent::__construct($message);
    }

    public function getData()
    {
        return $this->getMessage();
    }
}