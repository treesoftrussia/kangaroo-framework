<?php
namespace App\Core\Support\SmsPushers;

use Illuminate\Support\Facades\Mail;

class FakeSmsPusher extends AbstractSmsPusher
{
    public function send($phone, $activationCode)
    {
        Mail::raw($activationCode, function($message) {
            $message->from(config('params.sms_pusher_fake_email'), 'OOS');
            $message->to(config('params.sms_pusher_fake_email'));
            $message->subject('Activation Code');
        });
    }
}
