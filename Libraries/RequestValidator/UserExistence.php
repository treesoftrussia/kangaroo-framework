<?php

namespace Mildberry\Kangaroo\Libraries\RequestsValidator;

use App\Core\User\Services\UserService;
use Mildberry\Kangaroo\Libraries\Validator\UserExistenceInterface;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 * @author Tushar Ambalia <tusharambalia17@gmail.com>
 */
class UserExistence implements UserExistenceInterface
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param string $username
     * @param int    $exclude  - id of the user that has to be excluded from the list
     *
     * @return bool
     */
    public function existsByUsername($username, $exclude = null)
    {
        return $this->userService->existsByUserName($username, $exclude);
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function exists($id)
    {
        return $this->userService->exists($id);
    }

    /**
     * @param int[] $ids
     *
     * @return bool
     */
    public function existAll(array $ids)
    {
        return $this->userService->existAll($ids);
    }
}
