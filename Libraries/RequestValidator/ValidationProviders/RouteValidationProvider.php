<?php

namespace Mildberry\Kangaroo\Libraries\RequestsValidator\ValidationProviders;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class RouteValidationProvider extends AbstractValidationProvider
{
    protected function getRules()
    {
        if (method_exists($this->rulesBuilder, 'route')) {
            return $this->container->call([$this->rulesBuilder, 'route']);
        }

        return [];
    }

    protected function getValidationData()
    {
        return $this->request->route()->parameters();
    }

    protected function validationFailed($validator)
    {
        throw new NotFoundHttpException();
    }
}
