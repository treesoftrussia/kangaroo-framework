<?php

namespace Mildberry\Kangaroo\Libraries\RequestsValidator\ValidationProviders;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\InvalidInputException;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class JsonBodyValidationProvider extends AbstractValidationProvider
{
    protected function getRules()
    {
        if (method_exists($this->rulesBuilder, 'body') && ($this->request->method() == 'POST' || $this->request->method() == 'PUT')) {
            return $this->container->call([$this->rulesBuilder, 'body']);
        }

        return [];
    }

    protected function getValidationData()
    {
        return $this->request->json()->all();
    }

    protected function validationFailed($validator)
    {
        throw new InvalidInputException($this->formatErrors($validator));
    }
}
