<?php

namespace Mildberry\Kangaroo\Libraries\RequestsValidator\ValidationProviders;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\InvalidInputException;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class SortingValidationProvider extends AbstractValidationProvider
{
    protected function getRules()
    {
        if (method_exists($this->rulesBuilder, 'sorting')) {
            return $this->container->call([$this->rulesBuilder, 'sorting']);
        }

        return [];
    }

    protected function getValidationData()
    {
        $query = $this->request->query();
        if (!empty($query['options'])) {
            return $query['options'];
        }

        return [];
    }

    protected function validationFailed($validator)
    {
        throw new InvalidInputException($this->formatErrors($validator));
    }
}
