<?php

namespace Mildberry\Kangaroo\Libraries\RequestsValidator\RuleBuilders;

use Mildberry\Kangaroo\Libraries\RequestsValidator\RuleObjects\RuleObjectCollection;
use Mildberry\Kangaroo\Libraries\RequestsValidator\RuleObjects\TestChildRuleObject;
use Mildberry\Kangaroo\Libraries\RequestsValidator\RuleObjects\TestRuleObject;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestRuleBuilder extends AbstractRuleBuilder
{
    use PaginationRuleBuilderTrait;

    public function route()
    {
        $rules = [];
        switch ($this->method) {
            case 'GET':
            case 'PUT':
            case 'DELETE':
            case 'PATCH':
                $rules['id'] = 'required|numeric';
                break;
        }

        return $rules;
    }

    public function body()
    {
        $rules = [];
        $childrenRulesObject = (new RuleObjectCollection(new TestChildRuleObject()));
        $ruleObject = new TestRuleObject();
        $ruleObject->addChild('children', $childrenRulesObject);
        switch ($this->method) {
            case 'PUT':
                $rules = $ruleObject->build();
                break;
            case 'POST':
                $rules = $ruleObject->build();
                break;
            case 'PATCH':
                $ruleObject->setFieldsRequired(false);
                $rules = $ruleObject->build();
        }

        return $rules;
    }
}
