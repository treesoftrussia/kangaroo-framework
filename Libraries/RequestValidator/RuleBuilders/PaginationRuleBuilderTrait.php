<?php

namespace Mildberry\Kangaroo\Libraries\RequestsValidator\RuleBuilders;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
trait PaginationRuleBuilderTrait
{
    public function pagination()
    {
        return [
            'pageNum' => 'numeric|min:1|required_with:perPage',
            'perPage' => 'numeric|min:1|max:300|required_with:pageNum',
        ];
    }
}
