<?php

namespace Mildberry\Kangaroo\Libraries\RequestsValidator\RuleBuilders;

class PaginationRuleBuilder extends AbstractRuleBuilder
{
    use PaginationRuleBuilderTrait;

    public function route()
    {
        $rules = [];
        switch ($this->method) {
            case 'GET':
            case 'PUT':
            case 'DELETE':
            case 'PATCH':
                $rules['id'] = 'required';
                break;
        }

        return $rules;
    }
}
