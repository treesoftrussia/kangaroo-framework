<?php

namespace Mildberry\Kangaroo\Libraries\RequestsValidator\RuleBuilders;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
abstract class AbstractRuleBuilder
{
    protected $method;

    protected $id;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function __construct($method)
    {
        $this->method = $method;
    }

    public function setMethod($method)
    {
        $this->method = $method;
    }
}
