<?php

namespace Mildberry\Kangaroo\Libraries\RequestsValidator\RuleObjects;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
abstract class AbstractRuleObject
{
    protected $isIdRequired = true;

    protected $children = array();

    protected $rules = array();

    protected $fieldsRequired = true;

    public function addChild($key, AbstractRuleObject $object)
    {
        $this->children[$key] = $object;

        return $this;
    }

    public function build()
    {
        $this->rules = $this->rules();

        foreach ($this->children as $k => $child) {
            $childRules = $child->build();
            foreach ($childRules as $childRuleKey => $childRule) {
                $this->rules[$k.'.'.$childRuleKey] = $childRule;
            }
        }

        return $this->rules;
    }

    abstract protected function rules();

    public function setFieldsRequired($flag)
    {
        $this->fieldsRequired = $flag;
    }
}
