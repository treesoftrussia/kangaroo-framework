<?php

namespace Mildberry\Kangaroo\Libraries\RequestsValidator\RuleObjects;

/**
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class TestChildRuleObject extends AbstractRuleObject
{
    protected function rules()
    {
        $rules['title'][] = 'required';
        $rules['title'][] = 'string';
        $rules['title'][] = 'max:255';

        return $rules;
    }
}
