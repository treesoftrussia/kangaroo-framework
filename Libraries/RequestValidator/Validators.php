<?php

namespace Mildberry\Kangaroo\Libraries\RequestsValidator;

use App\Core\Location\Services\StateService;
use App\Core\User\Objects\Credentials;
use App\Core\User\Services\UserService;
use App\Core\Checklist\Services\ChecklistService;
use App\Core\Client\Branch\Services\BranchService;
use App\Core\Appraisement\Appraiser\Services\AppraiserService;
use App\Core\Appraisement\Branch\Services\BranchService as AppraiserBranchService;
use Illuminate\Log\Writer;
use Log;
use Mildberry\Kangaroo\Libraries\Validator\SharedValidators;
use RuntimeException;

/**
 * A class providing all shared validators in the system.
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Validators extends SharedValidators
{
    /**
     * Validates user's credentials.
     *
     * @param string $attr
     * @param string $value
     *
     * @return bool
     */
    protected function validateCredentials($attr, $value)
    {
        /**
         * @var UserService
         */
        $userService = $this->container->make(UserService::class);

        $credentials = new Credentials();
        $credentials->setUsername($value);
        $credentials->setPassword($this->getData()['password']);

        return $userService->existsByCredentials($credentials);
    }

    /**
     * Validates first/last name.
     *
     * @param string $attr
     * @param mixed  $value
     *
     * @return bool
     */
    protected function validateName($attr, $value)
    {
        if (!$this->validateMax($attr, $value, [30])) {
            return false;
        }

        return $this->validateRegex($attr, $value, [' /^[a-zA-Z\s\'\-]+$/']);
    }

    /**
     * Validates CompanyModel name.
     *
     * @param string $attr
     * @param string $value
     *
     * @return bool
     */
    protected function validateCompanyName($attr, $value)
    {
        if (!$this->validateMax($attr, $value, [45])) {
            return false;
        }

        return $this->validateRegex($attr, $value, [' /^[A-Z]([a-zA-Z0-9- @,\'\.#&!])*$/']);
    }

    /**
     * Validates city.
     *
     * @param string $attr
     * @param mixed  $value
     *
     * @return bool
     */
    protected function validateCity($attr, $value)
    {
        if (!$this->validateMax($attr, $value, [35])) {
            return false;
        }

        return $this->validateRegex($attr, $value, [' /^[a-zA-Z\s\'\-]+$/']);
    }

    /**
     * @param string $attr
     * @param mixed  $value
     *
     * @return bool
     */
    protected function validatePassword($attr, $value)
    {
        if (!$this->validateMin($attr, $value, [6])) {
            return false;
        }

        if (!$this->validateMax($attr, $value, [30])) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attr
     * @param mixed  $value
     *
     * @return bool
     */
    protected function validateUsername($attr, $value)
    {
        if (!$this->validateMin($attr, $value, [2])) {
            return false;
        }

        if (!$this->validateMax($attr, $value, [30])) {
            return false;
        }

        return $this->validateRegex($attr, $value, ['/^[\w\.@\+\-_]+$/']);
    }

    /**
     * Validates that the input is a hex color.
     *
     * #2414cf and #F00 both pass, 2414cf does not
     *
     * @param string $attr  Attribute
     * @param string $value Input value
     *
     * @return bool
     */
    protected function validateHexColor($attr, $value)
    {
        return $this->validateRegex($attr, $value, ['/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/']);
    }

    /**
     * Simple address validation.
     *
     * Since addresses vary so widely, this will simply check for the presence of numbers
     * followed by a space, followed by characters which are not numbers.
     *
     * @param string $attr  Attribute
     * @param string $value Input value
     *
     * @return bool
     */
    protected function validateAddress($attr, $value)
    {
        return $this->validateRegex($attr, $value, ['/^\d+\s+\D+$/']);
    }

    /**
     * Validate against federal tax ID.
     *
     * Format: 00-0000000
     *
     * @param string $attr
     * @param string $value
     *
     * @return bool
     */
    protected function validateTaxId($attr, $value)
    {
        return $this->validateRegex($attr, $value, ['/\d{2}-\d{7}/']);
    }

    /**
     * Validate zip code (either 00000 or 00000-0000 format).
     *
     * @param string $attr
     * @param string $value
     *
     * @return bool
     */
    protected function validateZipCode($attr, $value)
    {
        return $this->validateRegex($attr, $value, ['/^\d{5}([\-]?\d{4})?$/']);
    }

    /**
     * Corrected bool validator.
     *
     * @param string $attribute
     * @param mixed  $value
     * @param array  $params
     *
     * @return bool
     */
    protected function validateBoolean($attribute, $value, array $params = [])
    {
        $isSoft = (bool) array_take($params, 0, false);

        if ($isSoft) {
            return in_array($value, [true, false, 'true', 'false'], true);
        }

        return is_bool($value);
    }

    /**
     * Corrected bool validator.
     *
     * @param string $attribute
     * @param mixed  $value
     * @param array  $params
     *
     * @return bool
     */
    protected function validateInteger($attribute, $value, array $params = [])
    {
        $isSoft = (bool) array_take($params, 0, false);

        if ($isSoft) {
            return parent::validateInteger($attribute, $value);
        }

        return is_int($value);
    }

    /**
     * Validates a float value.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    protected function validateFloat($attribute, $value)
    {
        return is_int($value) || is_float($value);
    }

    /**
     * @return UserExistenceInterface
     *
     * @throws RuntimeException
     */
    protected function getUserExistence()
    {
        return new UserExistence($this->container->make(UserService::class));
    }

    /**
     * @return StateExistenceInterface
     *
     * @throws RuntimeException
     */
    protected function getStateExistence()
    {
        return new StateExistence($this->container->make(StateService::class));
    }

    /**
     * @return Writer
     */
    protected function getLogger()
    {
        return $this->container->make('log');
    }

    /**
     * @param string $attr
     * @param mixed  $value
     *
     * @return bool
     */
    protected function validateChecklistExists($attr, $value)
    {
        $checklistService = $this->container->make(ChecklistService::class);

        return $checklistService->exists($value);
    }

    /**
     * @param string $attr
     * @param array  $value
     * @param array  $params
     *
     * @return bool
     */
    public function validateHasEmployees($attr, $value, $params)
    {
        $branchService = $this->container->make(BranchService::class);

        return $branchService->hasEmployees($params[0], $value);
    }

    /**
     * @param string $attr
     * @param array  $value
     *
     * @return bool
     */
    public function validateHasAppraisers($attr, $value)
    {
        $appraiserService = $this->container->make(AppraiserService::class);

        if (is_string($value) && str_contains($value, ',')) {
            $value = explode(',', $value);
        }

        return $appraiserService->exists($value);
    }

    /**
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    protected function validatePhone($attribute, $value)
    {
        return $this->validateDigits($attribute, $value, [10]);
    }

    /**
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    protected function validatePhoneExt($attribute, $value)
    {
        return $this->validateDigits($attribute, $value, [5]);
    }

    /**
     * @param string $attr
     * @param int    $value
     *
     * @return bool
     */
    protected function validateBranchExists($attr, $value)
    {
        $branchService = $this->container->make(AppraiserBranchService::class);

        return $branchService->exists($value);
    }

    /**
     * @param string $attr
     * @param int    $value
     *
     * @return bool
     */
    protected function validateClientBranchExists($attr, $value)
    {
        /**
         * @var BranchService
         */
        $branchService = $this->container->make(BranchService::class);

        return $branchService->exists($value);
    }
}
