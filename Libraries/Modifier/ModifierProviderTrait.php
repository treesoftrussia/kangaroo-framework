<?php
namespace Mildberry\Kangaroo\Libraries\Modifier;

/**
 * Trait provides modifier manager and register shared modifiers
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
trait ModifierProviderTrait
{
    /**
     * @var Manager|Manager[]
     */
    private $modifierManager;

    /**
     * @var object|object[]
     */
    private static $sharedModifiersProvider;


    /**
     * @param null $name
     * @return Manager
     */
    protected function getModifierManager($name = null)
    {
        if (!$this->hasCachedModifierManager($name)){

            $manager = new Manager();

            $method = 'modifiers';

            if ($name){
                $method = 'register'.ucfirst(camel_case($name));
            }

            if (method_exists($this, $method)){
                call_user_func([$this, $method], $manager);
            }

            if (static::hasSharedModifiersProvider($name)){
                $manager->registerProvider(static::getSharedModifiersProvider($name));
            }

            $this->setCachedModifierManager($name, $manager);
        }

        return $this->getCachedModifierManager($name);;
    }

    /**
     * @param null|string $name
     * @return bool
     */
    private function hasCachedModifierManager($name = null)
    {
        if ($name){
            return isset($this->modifierManager[$name]);
        }

        return $this->modifierManager !== null;
    }

    /**
     * @param null|string $name
     * @return Manager|Manager[]
     */
    private function getCachedModifierManager($name = null)
    {
        if ($name){
            return $this->modifierManager[$name];
        }

        return $this->modifierManager;
    }

    /**
     * @param null|string $name
     * @param Manager $manager
     */
    private function setCachedModifierManager($name = null, Manager $manager)
    {
        if ($name){
            $this->modifierManager[$name] = $manager;
        } else {
            $this->modifierManager = $manager;
        }
    }

    /**
     * @param null|string $name
     * @return bool
     */
    public static function hasSharedModifiersProvider($name = null)
    {
        if ($name){
            return isset(self::$sharedModifiersProvider[$name]);
        }

        return self::$sharedModifiersProvider !== null;
    }

    /**
     * @param string|null $name
     * @return object
     */
    public static function getSharedModifiersProvider($name = null)
    {
        if ($name){
            return self::$sharedModifiersProvider[$name];
        }

        return self::$sharedModifiersProvider;
    }

    /**
     * @param string|object $nameOrProvider
     * @param null|object $provider
     */
    public static function setSharedModifiersProvider($nameOrProvider, $provider = null)
    {
        if (is_string($nameOrProvider)){
            self::$sharedModifiersProvider[$nameOrProvider] = $provider;
        } else {
            self::$sharedModifiersProvider = $nameOrProvider;
        }
    }
} 