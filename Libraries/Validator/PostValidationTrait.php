<?php
namespace Mildberry\Kangaroo\Libraries\Validator;

use Illuminate\Support\MessageBag;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
trait PostValidationTrait
{
    private $afterRules = [];

    public function setAfterRules(array $rules)
    {
        $this->afterRules = $this->explodeRules($rules);
        return $this;
    }

    /**
     * Overrides a default behaviour to allow post validation
     * @return bool
     */
    public function passes()
    {
        $this->messages = new MessageBag();

        foreach ($this->rules as $attribute => $rules) {
            foreach ($rules as $rule) {
                $this->validate($attribute, $rule);
            }
        }

        $after = array_except(
            $this->afterRules,
            array_keys($this->messages->toArray())
        );

        foreach ($after as $attribute => $rules) {
            foreach ($rules as $rule) {
                $this->validate($attribute, $rule);
            }
        }

        return count($this->messages->all()) === 0;
    }
} 