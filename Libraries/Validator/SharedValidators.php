<?php
namespace Mildberry\Kangaroo\Libraries\Validator;

use Illuminate\Log\Writer;
use Illuminate\Validation\Validator;
use Symfony\Component\Translation\TranslatorInterface;
use Mildberry\Kangaroo\Libraries\USPS\CityStateLookup;
use Mildberry\Kangaroo\Libraries\USPS\RequestException;
use Mildberry\Kangaroo\Libraries\USPS\ErrorResult;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 * @author Tushar Ambalia <tusharambalia17@gmail.com>
 */
abstract class SharedValidators extends Validator implements PostValidationInterface
{
    use PostValidationTrait;

    public function __construct(
        TranslatorInterface $translator,
        array $data,
        array $rules,
        array $messages = array(),
        array $customAttributes = array()
    )
    {
        parent::__construct($translator, $data, $rules, $messages, $customAttributes);

        $this->setCustomMessages([
            'state_exists' => 'State does not exist',
            'zip' => '', // The message is set within validator method depending on the error type
            'user_not_exists' => 'User already exists',
            'user_exists' => 'User does not exist',
        ]);
    }

    /**
     * @return UserExistenceInterface
     */
    abstract protected function getUserExistence();

    /**
     * @return StateExistenceInterface
     */
    abstract protected function getStateExistence();

    /**
     * @return Writer
     */
    abstract protected function getLogger();

    /**
     * Checks whether the state exists by code or id.
     * Note: it doesn't know anything about the database. It works directly through the model interface
     *
     * Usage:
     * 1) 'state' => 'state_exists:code'
     * 2) 'state' => 'state_exists:id'
     *
     * @param array $attr
     * @param $value
     * @param array $params
     * @return bool
     * @throws \RuntimeException
     */
    public function validateStateExists($attr, $value, $params = [])
    {
        $field = array_take($params, 0, 'code');

        $stateExistence = $this->getStateExistence();

        if (!$stateExistence instanceof StateExistenceInterface) {
            throw new \RuntimeException('State existence must be instance of StateExistenceInterface');
        }

        if ($field == 'id') {
            return $stateExistence->exists($value);
        }

        if ($field == 'code') {
            return $stateExistence->existsByCode($value);
        }

        if ($field == 'name') {
            return $stateExistence->existsByName($value);
        }

        throw new \RuntimeException('Unsupported attribute');
    }

    /**
     * Verifies whether the user does NOT exist by the specified field name
     * (see the opposite validator for more details)
     * @param $attr
     * @param $value
     * @param $params
     * @return bool
     */
    public function validateUserNotExists($attr, $value, $params)
    {
        return !$this->validateUserExists($attr, $value, $params);
    }


    /**
     * Verifies whether the user exists by the specified field name
     *
     * Usage:
     * 1) 'user' => 'user_exists:username'
     * 2) 'user' => 'user_exists:username, 999'
     * 3) 'user' => 'user_exists:id'
     *
     * @param $attr
     * @param $value
     * @param $params - where the 1st param represents the field by which the search will be performed,
     * and the 2nd param (optional) represents the id of the user who should be excluded from the list
     * @return bool
     * @throws \RuntimeException
     */
    public function validateUserExists($attr, $value, $params = [])
    {
        $field = reset($params);

        $userExistence = $this->getUserExistence();

        if (!$userExistence instanceof UserExistenceInterface) {
            throw new \RuntimeException('User existence must be instance of UserExistenceInterface');
        }

        if ($field == 'id' || !$field) {

            if (is_array($value)) {

                return $userExistence->existAll($value);
            }

            return $userExistence->exists($value);
        }

        if ($field == 'username') {
            return $userExistence->existsByUsername($value, array_take($params, 1));
        }

        throw new \RuntimeException('Unsupported field');
    }

    /**
     * Validates zip code by utilizing USPS service
     * @param $attr
     * @param $value
     * @param array $params - first argument represents the state field name against which the zip code is validated.
     * Usage: 'zip' => 'zip:state'
     * @return bool
     * @throws \RuntimeException
     */
    public function validateZip($attr, $value, $params = [])
    {
        /*
         * We don't want to proceed if the field is already failed the validation.
         * It will prevent us from doing unnecessary request to USPS
         */
        if ($this->getMessageBag()->has($attr)) {
            return true;
        }

        $usps = new CityStateLookup($value);

        $state = reset($params);

        if (!$state) {
            throw new \RuntimeException('State field name is missing in Zip validator');
        }

        try {
            $result = $usps->execute();
        } catch (RequestException $ex) {

            $logger = $this->getLogger();

            if (!$logger instanceof Writer) {
                throw new \RuntimeException('Logger must be instance of Writer');
            }

            /*
             * In case of failure we don't want to prevent user update his profile so we just pass this validation.
             * However, the error should be logged in order to investigate the cause.
             */
            $logger->error($ex);

            return true;
        }

        if ($result instanceof ErrorResult) {
            $this->customMessages['zip'] = 'Invalid zip code';
            return false;
        }

        if (strtolower($result->getState()) != strtolower($this->getData()[$state])) {
            $this->customMessages['zip'] = 'Zip code does not belong to the selected state';
            return false;
        }

        return true;
    }

    /**
     * @param $attr
     * @param $value
     * @param array $params
     * @return bool
     */
    protected function validateEnum($attr, $value, $params = [])
    {
        return call_user_func(array_take($params, 0).'::has', $value);
    }
} 