<?php
namespace Mildberry\Kangaroo\Libraries\Validator;

/**
 * Provides interface to the object willing to provide user existence feature
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 * @author Tushar Ambalia <tusharambalia17@gmail.com>
 */
interface UserExistenceInterface
{
    /**
     * @param int $id
     * @return bool
     */
    public function exists($id);

    /**
     * @param int[] $ids
     * @return bool
     */
    public function existAll(array $ids);

    /**
     * @param string $username
     * @param int $exclude - id of the user that has to be excluded from the list
     * @return bool
     */
    public function existsByUsername($username, $exclude = null);
} 