<?php
namespace Mildberry\Kangaroo\Libraries\Validator;

/**
 * Provides interface to the object willing to provide state existence feature
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
interface StateExistenceInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function exists($id);

    public function existsByCode($code);

    public function existsByName($name);
} 