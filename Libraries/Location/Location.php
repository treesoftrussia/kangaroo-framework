<?php
namespace Mildberry\Kangaroo\Libraries\Location;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Location
{
    /**
     * Detect location based on provided Address
     * @param Address $address
     * @return Result|null
     * @throws RequestException
     * @throws ResponseException
     */
    public static function detect(Address $address)
    {
        $address = urlencode($address->getFormattedAddress());

        $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&sensor=false';

        $data = file_get_contents($url);

        if ($data === false) {
            throw new RequestException('Cannot retrieve the data');
        }

        $locations = json_decode($data, true);

        if ($locations === null || !isset($locations['results'])) {
            throw new ResponseException('Unrecognized response format');
        }

        if (count($locations['results']) == 0) {
            return null;
        }

        $loc = reset($locations['results']);

        if (!isset($loc['geometry']['location']['lng'])
            || !isset($loc['geometry']['location']['lat'])
        ) {
            throw new ResponseException('Unrecognized response format');
        }

        $result = new Result();

        $result->setLatitude($loc['geometry']['location']['lat']);
        $result->setLongitude($loc['geometry']['location']['lng']);

        return $result;
    }
} 