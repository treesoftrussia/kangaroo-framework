<?php
namespace Mildberry\Kangaroo\Libraries\Location;

/**
 * Class represent the US address
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Address
{
    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $zip;

    /**
     * @param string $street
     * @param string $city
     * @param string $state
     * @param string $zip
     */
    public function __construct($street, $city, $state, $zip)
    {
        $this->setStreet($street);
        $this->setCity($city);
        $this->setState($state);
        $this->setZip($zip);
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Prepares the formatted address string from the current fields
     * @return string
     */
    public function getFormattedAddress()
    {
        return $this->getStreet() . ', ' . $this->getCity() . ', ' . $this->getState() . ' ' . $this->getZip();
    }
} 