<?php
namespace Mildberry\Kangaroo\Libraries\Sorting;

use Mildberry\Kangaroo\Libraries\Specification\Types\Object\ObjectType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\IntegerType;
use Mildberry\Kangaroo\Libraries\Specification\Types\Scalar\StringType;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
class SortingOptionsObjectType extends ObjectType
{
    /**
     * @return array
     */
    public function structure()
    {
        return [
            'field' => new StringType(),
            'direction' => new StringType(false, true)
        ];
    }
}