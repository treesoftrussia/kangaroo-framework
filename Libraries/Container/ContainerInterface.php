<?php

namespace Mildberry\Kangaroo\Libraries\Container;

/**
 * @author Andrew Sparrow<andrew.sprw@gmail.com>
 */
interface ContainerInterface
{
    /**
     * @param string $abstract
     * @param array  $parameters
     *
     * @return object
     */
    public function get($abstract, $parameters = []);
}
