<?php

namespace Mildberry\Kangaroo\Libraries\Adapter;

use Mildberry\Kangaroo\Libraries\Cast\CastException;
use DateTime;
use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class SharedModifiers
{
    /**
     * @param $datetime
     * @return DateTime
     */
    public function datetime($datetime)
    {
        return new DateTime($datetime);
    }

    /**
     * @param mixed $value
     * @throws CastException
     * @return bool|null
     */
    public function bool($value)
    {
        if (in_array($value, [0, '0', 'false'], true)) {
            return false;
        }

        if (in_array($value, [1, '1', 'true'], true)) {
            return true;
        }

        return Cast::bool($value);
    }
} 