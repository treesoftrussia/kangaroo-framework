<?php

namespace Mildberry\Kangaroo\Libraries\Adapter;

use Mildberry\Kangaroo\Libraries\Cast\CastException;
use DateTime;
use Mildberry\Kangaroo\Libraries\Cast\Cast;

/**
 * @author Andrew Sparrow<andrew.sprw@gmail.com>
 */
class SharedSanitizers
{

    /**
     * @param mixed $integer
     * @return integer
     */
    public function integer($integer){
       return  Cast::int($integer);
    }

    /**
     * @param mixed $float
     * @return float
     */
    public function float($float){
        return Cast::float($float);
    }

    /**
     * @param mixed $string
     * @return string
     */
    public function string($string){
        return Cast::string($string);
    }

    /**
     * @param DateTime $datetime
     * @return string
     */
    public function datetime(DateTime $datetime)
    {
        return $datetime->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $value
     * @throws CastException
     * @return int
     */
    public function bool($value)
    {
        $value = Cast::bool($value);

        return (int)$value;
    }

    /**
     * @param string $value
     * @return string
     */
    public function password($value)
    {
        return password_hash($value, PASSWORD_BCRYPT);
    }
} 