<?php

namespace Mildberry\Kangaroo\Libraries\Adapter;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
trait ExtractCollectionTrait
{
    /**
     * @param object $object
     * @return array
     */
    abstract public function extract($object);

    /**
     * Extracts data from the collection of objects
     *
     * @param object[] $objects
     * @return array
     */
    public function extractCollection($objects)
    {
        $data = [];

        foreach ($objects as $object) {
            $data[] = $this->extract($object);
        }

        return $data;
    }

}