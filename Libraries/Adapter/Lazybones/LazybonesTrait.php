<?php

namespace Mildberry\Kangaroo\Libraries\Adapter\Lazybones;

use RuntimeException;

/**
 * The trait is used to provide lazy loading functionality to entity objects
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
trait LazybonesTrait
{
    /**
     * @var object
     */
    private $callbacks;

    /**
     * @var Delegator
     */
    private $delegator;

    /**
     * Creates an entity instance and sets the provided callbacks to it
     *
     * @param object $callbacks
     * @return static
     */
    public static function make($callbacks)
    {
        $object = new static();
        $object->defineCallbacks($callbacks);

        return $object;
    }

    /**
     * Delegates all the method calls to a callbacks provider instance thru the delegator object.
     *
     * @param bool $settable
     * @return Delegator
     */
    protected function callbacks($settable = false)
    {
        if ($this->delegator === null){
            $this->delegator = new Delegator($this, $this->callbacks);
        }

        $this->delegator->setSettable($settable);

        return $this->delegator;
    }

    /**
     * Sets the callbacks provider object
     *
     * @param $callbacks
     * @throws RuntimeException
     */
    public function defineCallbacks($callbacks)
    {
        if (!is_object($callbacks)) {
            throw new RuntimeException('The callbacks must be an object');
        }

        $this->callbacks = $callbacks;
    }

    /**
     * Calls given method on parent class
     *
     * @param $name
     * @param array $args
     * @return mixed
     */
    public function callOnParent($name, array $args = [])
    {
        return call_user_func_array('parent::' . $name, $args);
    }
} 