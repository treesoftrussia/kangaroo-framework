<?php

namespace Mildberry\Kangaroo\Libraries\Adapter\Lazybones;

use RuntimeException;

/**
 * The class is used to delegate method calls from an entity object to a callbacks provider
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Delegator
{
    /**
     * Entity instance
     *
     * @var object
     */
    private $entity;

    /**
     * Callbacks provider
     *
     * @var object
     */
    private $callbacks;


    /**
     * @var bool
     */
    private $settable = false;

    /**
     * @var array
     */
    private $resolvedValues = [];

    /**
     * @param object $entity
     * @param object $callbacks
     */
    public function __construct($entity, $callbacks)
    {
        $this->entity = $entity;
        $this->callbacks = $callbacks;
    }

    /**
     * @param string $method
     * @param array $args
     * @return mixed
     * @throws RuntimeException
     */
    public function __call($method, array $args = [])
    {
        $value = $this->entity->callOnParent($method, $args);

        if ($value !== null) {

            if ($this->settable){
                return $value;
            }

            throw new RuntimeException('Value has been initialized already.');
        }

        if (array_key_exists($method, $this->resolvedValues)){
            return $this->resolvedValues[$method];
        }

        $value = call_user_func_array([$this->callbacks, $method], $args);

        $this->resolvedValues[$method] = $value;

        return $value;
    }

    /**
     * @param bool $flag
     */
    public function setSettable($flag)
    {
        $this->settable = $flag;
    }
} 