<?php

namespace Mildberry\Kangaroo\Libraries\Adapter;

use Mildberry\Kangaroo\Libraries\Enum\Enum;
use RuntimeException;

/**
 * The base class for all mapper classes
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
abstract class AbstractMapper
{
    /**
     * @var array
     */
    private static $verifiedMappers = [];

    /**
     * @var array
     */
    private static $cache = [];

    /**
     * @return array|Enum[]
     */
    abstract protected function map();


    /**
     * @param array|mixed $value
     * @return Enum
     */
    public static function toCore($value)
    {
        if (!is_array($value)) {
            return (new static())->toEnum($value);
        }

        $collection = [];

        foreach ($value as $item) {
            $collection[] = (new static())->toEnum($item);
        }

        return $collection;
    }

    /**
     * @param Enum|array $enum
     * @return mixed
     */
    public static function fromCore($enum)
    {
        if (!is_array($enum)) {
            return (new static())->fromEnum($enum);
        }

        $collection = [];

        foreach ($enum as $item) {
            $collection[] = (new static())->fromEnum($item);
        }

        return $collection;
    }

    /**
     * @param mixed $value
     * @return Enum
     * @throws RuntimeException
     */
    public function toEnum($value)
    {
        $this->verify();

        $enum = array_take($this->map(), $value);

        if (!$enum) {
            throw new RuntimeException('Unable to resolve the enum object.');
        }

        return $enum;
    }

    /**
     * @param Enum $enum
     * @return mixed
     * @throws RuntimeException
     */
    public function fromEnum(Enum $enum)
    {
        $result = $this->tryFromCache($enum);

        if ($result) {
            return $result;
        }

        $this->verify();

        foreach ($this->map() as $key => $value) {
            if ($value->is($enum->value())) {
                $this->cacheResult($enum, $key);
                return $key;
            }
        }

        throw new RuntimeException('Unable to resolve the value.');
    }

    /**
     * Tries to get key from cache
     *
     * @param Enum $enum
     * @return mixed
     */
    private function tryFromCache(Enum $enum)
    {
        return array_take(self::$cache, get_class($enum) . ':' . $enum->value());
    }

    /**
     * Caches the result
     *
     * @param Enum $enum
     * @param string|int $key
     */
    private function cacheResult(Enum $enum, $key)
    {
        self::$cache[get_class($enum) . ':' . $enum->value()] = $key;
    }

    /**
     * Verifies whether the map is in good format
     *
     * @throws RuntimeException
     */
    private function verify()
    {
        if (in_array(get_called_class(), self::$verifiedMappers)) {
            return;
        }

        $map = $this->map();
        $class = null;

        foreach ($map as $value) {
            if (!$value instanceof Enum) {
                throw new RuntimeException('The value must be an instance of Enum');
            }

            if (!$class) {
                $class = get_class($value);
            }

            if ($class !== get_class($value)) {
                throw new RuntimeException('All the values must be instance of the same enum class.');
            }
        }

        self::$verifiedMappers[] = get_called_class();
    }
} 