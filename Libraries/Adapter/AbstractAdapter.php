<?php

namespace Mildberry\Kangaroo\Libraries\Adapter;

use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Mildberry\Kangaroo\Libraries\Modifier\Manager;
use Mildberry\Kangaroo\Libraries\Modifier\ModifierProviderTrait;
use Mildberry\Kangaroo\Libraries\Resource\Exceptions\RuntimeException;

/**
 * A base class for all adapters
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
abstract class AbstractAdapter
{
    use ModifierProviderTrait;

    const M_SANITIZER = 'sanitizer';
    const M_MODIFIER = 'modifier';

    /**
     * @var Container|null
     */
    protected $container;

    /**
     * AbstractAdapter constructor.
     * @param Container|null $container
     */
    public function __construct(Container $container = null)
    {
        if(!$container){
            $container = app();
        }

        $this->container = $container;
    }

    /**
     * @return Container
     */
    protected function getContainer(){
        return $this->container;
    }

    /**
     * @param EloquentCollection $models
     * @return Collection
     */
    public function transformCollection(EloquentCollection $models)
    {
        if(!method_exists($this, 'transform')){
            throw new RuntimeException("Method transform doesn't exists");
        }

        $result = array();
        foreach ($models as $model) {
            $result[] = $this->transform($model);
        }
        return new Collection($result);
    }

    /**
     * The method provides an ModifierManager setup to use for transformation
     *
     * @return Manager
     */
    protected function modifier()
    {
        return $this->getModifierManager(static::M_MODIFIER);
    }

    /**
     * The method provides an Manager setup to use for extraction
     *
     * @return Manager
     */
    protected function sanitizer()
    {
        return $this->getModifierManager(static::M_SANITIZER);
    }
}