<?php

namespace Mildberry\Kangaroo\Libraries\Adapter;

use Mildberry\Kangaroo\Libraries\Modifier\Manager;
use Mildberry\Kangaroo\Libraries\Modifier\ModifierProviderTrait;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
trait ModifiersSupplierTrait
{
    use ModifierProviderTrait;

    /**
     * @var object
     */
    private $modifiers;

    /**
     * @var object
     */
    private $sanitizers;

    /**
     * @param object $provider
     */
    public function setModifiersProvider($provider)
    {
        $this->modifiers = $provider;
    }

    /**
     * @param object $provider
     */
    public function setSanitizersProvider($provider)
    {
        $this->sanitizers = $provider;
    }

    /**
     * @param Manager $manager
     */
    protected function registerModifiers(Manager $manager)
    {
        $manager->registerProvider($this->modifiers);
    }

    /**
     * @param Manager $manager
     */
    protected function registerSanitizers(Manager $manager)
    {
        $manager->registerProvider($this->sanitizers);
    }

    /**
     * @return Manager
     */
    protected function modifier()
    {
        return $this->getModifierManager('modifiers');
    }

    /**
     * @return Manager
     */
    protected function sanitizer()
    {
        return $this->getModifierManager('sanitizers');
    }
} 