<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Traits;
use Mildberry\Kangaroo\Libraries\Service\UserServiceInterface;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;

/**
 * @author Andrey Vorobiov<andrew.sprw@gmail.com>
 */
trait ImpersonatesUsers
{
    /**
     * Set the currently logged in user for the application.
     *
     * @param  UserEntityInterface  $user
     * @return $this
     */
    public function asUser(UserEntityInterface $user)
    {
        $this->getContainer()->make(UserServiceInterface::class)->setCurrentUser($user);
        return $this;
    }

}