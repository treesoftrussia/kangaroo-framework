<?php
namespace Mildberry\Kangaroo\QA\Endpoints;

use Illuminate\Http\Request;
use Mildberry\Kangaroo\Libraries\Service\UserServiceInterface;
use Mildberry\Kangaroo\Libraries\Specification\Support\SpecificationFactory;
use Mildberry\Kangaroo\Libraries\UserManagement\Interfaces\UserEntityInterface;
use Mildberry\Kangaroo\QA\Endpoints\Traits\ImpersonatesUsers;
use Mildberry\Kangaroo\QA\Endpoints\Validator\CollectionSummary;
use Mildberry\Kangaroo\QA\Endpoints\Validator\RulesProviderInterface;
use Mildberry\Kangaroo\QA\Endpoints\Validator\ValidatorConstraint;
use Illuminate\Foundation\Application as ApplicationInstance;
use Illuminate\Foundation\Testing\TestCase as IlluminateTestCase;
use RuntimeException;
use PHPUnit_Framework_Assert as PHPUnit;
use Faker\Generator;
use Mildberry\Kangaroo\QA\Endpoints\Faker\FakerFactoryInterface;
use Illuminate\Support\Facades\DB;

/**
 * @author Andrew Sparrow<andrew.sprw@gmail.com>
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class TestCase extends IlluminateTestCase
{
    use ImpersonatesUsers;

    /**
     * @var Generator
     */
    protected $faker;

    /**
     * @var ApplicationInstance
     */
    protected $http;

    /**
     * @var ApplicationInstance
     */
    private static $cachedApp;

    /**
     * @var array
     */
    private static $isTablesTruncated = [];

    /**
     * Set FOREIGN_KEY_CHESKS directive. Used for database truncate function
     *
     * @var boolean
     */
    protected $foreignKeyChecks = false;

    /**
     * @var array
     */
    protected $presetHeaders = [];

    /**
     * @return ApplicationInstance
     */
    public function getContainer(){
        return $this->app;
    }

    /**
     * @return ApplicationInstance
     */
    public function createApplication()
    {
        if (self::$cachedApp === null){
            self::$cachedApp = Application::test(function($app){
                $app->make(DbManager::class)->change();
            });
        }

        return self::$cachedApp;
    }

    /**
     * @param $requestData
     * @param $idOrData
     * @param $entityClass
     * @param $constraintClass
     * @param string $message
     */
    public function assertEntity($requestData, $idOrData, $entityClass, $constraintClass, $message = ''){
        if(is_array($idOrData)){
            $entity = $idOrData;
        } else {
            $entity = $this->app->make($entityClass)->newQuery()->find($idOrData)->toArray();
        }

        $constraintClass = new $constraintClass($requestData);

        PHPUnit::assertThat($entity, $constraintClass, $message);
    }

    /**
     * Asserts structure of the response
     *
     * @param array $response
     * @param string $specification
     */
    protected function assertStructure(array $response, $specification)
    {
        $specificationInstance = SpecificationFactory::make($specification);

        $this->assertTrue($specificationInstance->check($response), 'Structure is incorrect:'.print_r($specificationInstance->getErrorMessages(), true));
    }

    /**
     * @param RulesProviderInterface $rules
     * @param string $message
     * @throws
     */
    protected function assertValidator(RulesProviderInterface $rules, $message = '')
    {
        $summary = $this->http->make(CollectionSummary::class)->get();
        $rules = $rules->rules();

        if (!$rules) {
            throw new RuntimeException("Rules shouldn't be empty.");
        }

        $constraint = new ValidatorConstraint($summary);
        PHPUnit::assertThat($rules, $constraint, $message);
    }

    /**
     * Overrides to provide a hook for before test logic execution
     */
    public function setUp()
    {
        parent::setUp();

        //if (!isset(self::$isTablesTruncated[get_called_class()])){
        $this->truncateAllTables();
        //    self::$isTablesTruncated[get_called_class()] = true;
        //}

        $this->beforeTest();
    }

    /**
     * Overrides to provide a hook for after test logic execution
     */
    public function tearDown()
    {
        if(!$this->foreignKeyChecks) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        $this->afterTest();

        if(!$this->foreignKeyChecks) {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }

    /**
     * Executed after the test method is completed
     */
    protected function afterTest()
    {
        $this->truncateAllTables();
    }

    /**
     * Executed before the test method is started
     */
    protected function beforeTest()
    {
        $this->faker = $this->app->make(FakerFactoryInterface::class)->create();

        $this->app->make(UserServiceInterface::class)->purgeCurrentUser();
    }

    private function truncateAllTables()
    {
        $this->app->make(DbManager::class)->truncateAllTables($this->foreignKeyChecks);
    }

    /**
     * Call the given URI and return the Response.
     *
     * @param string $method
     * @param string $uri
     * @param array  $parameters
     * @param array  $cookies
     * @param array  $files
     * @param array  $server
     * @param string $content
     *
     * @return \Illuminate\Http\Response
     */
    public function call($method, $uri, $parameters = [], $cookies = [], $files = [], $server = [], $content = null)
    {
        $kernel = $this->app->make('Illuminate\Contracts\Http\Kernel');

        $this->currentUri = $this->prepareUrlForRequest($uri);

        $this->resetPageContext();

        $request = Request::create(
            $this->currentUri, $method, $parameters,
            $cookies, $files, array_replace($this->serverVariables, $server), $content
        );

        if ($request->isJson()) {
            $request->request = $request->json();
        }

        $response = $kernel->handle($request);

        $kernel->terminate($request, $response);

        return $this->response = $response;
    }

    /**
     * @param int $userId
     *
     * @deprecated use actingAs instead of this.
     */
    protected function withAuth($userId = 1)
    {
        $this->asUser($this->retrieveUserById($userId));
    }

    /**
     * @param $userId
     * @return UserEntityInterface
     */
    protected function retrieveUserById($userId){
        $userService = $this->app->make(UserServiceInterface::class);

        return $userService->getUserById($userId);
    }

    /**
     * @return Generator
     */
    public function getFaker()
    {
        return $this->faker;
    }

    /**
     * @param $needle
     * @param $haystack
     */
    public function assertArrayContainsArray($needle, $haystack)
    {
        foreach ($needle as $key => $val) {
            $this->assertArrayHasKey($key, $haystack);

            if (is_array($val)) {
                $this->assertArrayContainsArray($val, $haystack[$key]);
            } else {
                $this->assertEquals($val, $haystack[$key]);
            }
        }
    }

}