<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Validator;

/**
 * Keeps summary about validation
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class CollectionSummary
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * @param string $field
     * @param string $rule
     */
    public function add($field, $rule)
    {
        $this->data[$field][] = $rule;
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->data;
    }
} 