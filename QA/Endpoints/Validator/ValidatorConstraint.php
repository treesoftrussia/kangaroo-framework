<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Validator;

use PHPUnit_Framework_Constraint;
use PHPUnit_Util_InvalidArgumentHelper;
use Illuminate\Validation\Validator;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ValidatorConstraint extends PHPUnit_Framework_Constraint
{
    /**
     * @var array
     */
    private $summary = [];

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @param $summary
     */
    public function __construct($summary)
    {
        parent::__construct();

        if (!$summary) {
            throw PHPUnit_Util_InvalidArgumentHelper::factory(1, 'CollectionSummary');
        }

        $this->summary = $summary;
    }

    /**
     * @param array $rules
     * @return bool
     */
    public function matches($rules)
    {
        $keys = array_unique(array_merge(array_keys($rules), array_keys($this->summary)));

        foreach ($keys as $field) {
            if (!array_key_exists($field, $rules)) {
                $this->errors[] = "Validation '{$field}' hasn't exist in rules";
                continue;
            }

            if (!array_key_exists($field, $this->summary)) {
                $this->errors[] = "Validation '{$field}' hasn't call in request.";
                continue;
            }

            if (!array_equal($rules[$field], $this->summary[$field])) {
                $this->errors[] = "Asserting validation rules for the '{$field}' field.";
            }
        }

        return count($this->errors) == 0;
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {
        return "\nWith errors: \n" . print_r($this->errors, true);
    }
}