<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Validator;

/**
 * Appects data collector to a validator instance
 *
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
trait DataCollectorTrait
{
    protected function validate($attribute, $rule)
    {
        $this->container->make(CollectionSummary::class)->add($attribute, $rule);
        return parent::validate($attribute, $rule);
    }
}