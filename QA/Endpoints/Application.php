<?php 
namespace Mildberry\Kangaroo\QA\Endpoints;

use Illuminate\Foundation\Application as ApplicationInstance;
use Illuminate\Support\Collection;

/**
 * @author Igor Vorobiov <igor.vorobioff@gmail.com>
 * @author Andrew Sparrow <andrew.sprw@gmail.com>
 */
class Application
{
    /**
     * @var AbstractApplicationFactory
     */
    private static $factory;
    private static $config;

    /**
     * @return Collection
     */
    public static function getConfig()
    {
        if (self::$config === null) {
            self::$config = new Collection();
        }

        return self::$config;
    }

    /**
     * @param mixed $config
     * @return Application
     */
    public static function setConfig($config)
    {
        self::$config = new Collection($config);
    }

    /**
     * @param AbstractApplicationFactory $factory
     */
    public static function setFactory(AbstractApplicationFactory $factory)
    {
        self::$factory = $factory;
    }

    /**
     * @param callable $booted
     * @return ApplicationInstance
     */
    public static function test(callable $booted = null)
    {
        return self::$factory->create(AbstractApplicationFactory::TEST, $booted);
    }

    /**
     * @param callable $booted
     * @return ApplicationInstance
     */
    public static function http(callable $booted = null)
    {
        return self::$factory->create(AbstractApplicationFactory::HTTP, $booted);
    }
}
