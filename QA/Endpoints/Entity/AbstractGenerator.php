<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Entity;

use Faker\Generator as Faker;
use RuntimeException;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
abstract class AbstractGenerator
{
    /**
     * @var AbstractEntity $entity
     */
    protected $entity;

    /**
     * @var Faker
     */
    protected $faker;

    /**
     * @param AbstractEntity $entity
     * @param Faker $faker
     * @throws RuntimeException
     */
    public function __construct(AbstractEntity $entity, Faker $faker)
    {
        $this->entity = $entity;
        $this->faker = $faker;

        if (!method_exists($this,  'generate')){
            throw new RuntimeException('Method "generate" is missing.');
        }
    }

    protected function makeChild($class, $times = 1){
        $args = func_get_args();
        array_shift($args);
        return call_user_func_array([(new $class($this->entity, $this->faker)), 'generate'], $args);
    }
}