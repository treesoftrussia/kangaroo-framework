<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Entity;

use Mildberry\Kangaroo\QA\Endpoints\Faker\ArrayPopulator;
use Illuminate\Support\Collection;
use Mildberry\Kangaroo\QA\Endpoints\DbManager;
use Mildberry\Kangaroo\QA\Endpoints\Faker\Populator;
use Mildberry\Kangaroo\QA\Endpoints\Faker\Scheme;
use Mildberry\Kangaroo\QA\Endpoints\Faker\FakerFactoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Application as ApplicationInstance;
use RuntimeException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
abstract class AbstractEntity
{
    /**
     * @var ApplicationInstance
     */
    private $app;

    /**
     * @var Generator
     */
    protected $faker;

    /**
     * @var Collection
     */
    private $models;

    public function __construct(ApplicationInstance $app)
    {
        $this->app = $app;
        $this->models = new Collection();
        $this->faker = $this->app->make(FakerFactoryInterface::class)->create();
    }

    /**
     * @return array
     */
    abstract protected function generators();

    /**
     * @param ApplicationInstance $app
     */
    public function setApplication(ApplicationInstance $app)
    {
        $this->app = $app;
    }

    /**
     * @return ApplicationInstance
     */
    public function getApplication()
    {
        return $this->app;
    }

    /**
     * @param Model[]|Collection $models
     */
    public function destroy(Collection $models)
    {
        foreach ($models as $model) {
            $model->delete();
        }
    }

    /**
     * @param string $name
     * @param array $args
     * @return mixed
     * @throws RuntimeException
     */
    public function __call($name, array $args = [])
    {
        $generator = array_take($this->generators(), $name, null);

        if ($generator === null) {
            throw new RuntimeException('The "'.$name.'" generator has not been found.');
        }


        $generator = new $generator($this, $this->faker);


        return call_user_func_array([$generator, 'generate'], $args);
    }

    /**
     * @param string $class
     * @param int $times
     * @param array $formatters
     * @return Collection
     */
    public function make($class, $times = 1, array $formatters = [])
    {
        if(is_null($class)){
            $populator = $this->createArrayPopulator();
            $data = $populator->execute($times, $formatters);
        } else {
            $populator = $this->createModelPopulator();
            $data = $populator->execute($class, $times, $formatters);
            $this->models = $this->models->merge($data->toBase());
        }

        return $data;
    }


    /**
     * @return Populator
     */
    protected function createArrayPopulator()
    {
        $populator = new ArrayPopulator(
            $this->faker
        );

        return $populator;
    }


    /**
     * @return Populator
     */
    protected function createModelPopulator()
    {
        $populator = new Populator(
            $this->faker,
            new Scheme($this->getApplication()->make(DbManager::class)->getConnection())
        );

        return $populator;
    }

    /**
     * Removes all created by method "make" models
     */
    public function close()
    {
        $this->destroy($this->models);
        $this->models = new Collection();
    }
}
