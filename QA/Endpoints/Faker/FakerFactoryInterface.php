<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Faker;

interface FakerFactoryInterface
{
    public function create();
}
