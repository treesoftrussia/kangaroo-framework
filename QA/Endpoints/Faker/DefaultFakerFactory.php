<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Faker;

use Faker\Factory;
use Faker\Provider\en_US\Address;

class DefaultFakerFactory implements FakerFactoryInterface
{
    /**
     * Creates new instansce of Faker\Generator object.
     * @return Faker\Generator
     */
    public function create()
    {
        $faker = Factory::create();
        $faker->addProvider(new Address($faker));
        return $faker;
    }
}
