<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Faker;

use Faker\Generator;
use Illuminate\Database\Eloquent\Model;
use Faker\Guesser\Name as NameGuesser;

/**
 * Service class for populating a table through a Doctrine Entity class.
 */
class EntityPopulator
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var array
     */
    private $columnFormatters;

    /**
     * @var Generator
     */
    private $generator;

    /**
     * @var Scheme
     */
    private $scheme;

    /**
     * @param string $class
     * @param Generator $generator
     * @param Scheme $scheme
     * @param array $columnFormatters
     */
    public function __construct(
        $class,
        Generator $generator,
        Scheme $scheme,
        $columnFormatters = []
    )
    {
        $this->class = $class;
        $this->columnFormatters = $columnFormatters;
        $this->generator = $generator;
        $this->scheme = $scheme;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return array
     */
    public function getColumnFormatters()
    {
        $defaultGuesser = new NameGuesser($this->generator);
        $guesser = new ColumnTypeGuesser($this->generator);

        foreach ($this->getColumns() as $column) {
            if ($column->hasAutoincrement()) {
                continue;
            }

            $name = $column->getName();

            if (array_key_exists($name, $this->columnFormatters)) {
                continue;
            }

            if ($formatter = $defaultGuesser->guessFormat($name)) {
                $this->columnFormatters[$name] = $formatter;
                continue;
            }

            if ($formatter = $guesser->guessFormat($column)) {
                $this->columnFormatters[$name] = $formatter;
                continue;
            }
        }

        return $this->columnFormatters;
    }

    /**
     * @return Model
     */
    public function execute()
    {
        /**
         * @var Model $model
         */
        $model = new $this->class();

        foreach ($this->getColumnFormatters() as $column => $formatter) {
            $model->{$column} = is_callable($formatter) ? $formatter() : $formatter;
        }

        $model->save();

        return $model;
    }

    /**
     * @return Column[]
     */
    public function getColumns()
    {
        /**
         * @var Model $model
         */
        $model = new $this->class();

        return $this->scheme->getColumns($model->getTable());
    }
}
