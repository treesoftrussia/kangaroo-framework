<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Faker;

use Illuminate\Database\Connection;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Scheme
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $table
     * @return Column[]
     */
    public function getColumns($table)
    {
        $query = 'select * from information_schema.columns where table_schema = ? and table_name = ?';

        $result = $this->connection->select($query, [
            $this->connection->getDatabaseName(),
            $this->connection->getTablePrefix().$table
        ]);

        $columns = [];

        foreach ($result as $row){
            $columns[] = new Column((array) $row);
        }

        return $columns;
    }
} 