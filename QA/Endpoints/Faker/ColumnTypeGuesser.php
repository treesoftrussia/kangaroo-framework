<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Faker;

use Faker\Generator;

class ColumnTypeGuesser
{
    /**
     * @var Generator
     */
    protected $generator;

    /**
     * @param Generator $generator
     */
    public function __construct(Generator $generator)
    {
        $this->generator = $generator;
    }

    /**
     * @param Column $column
     * @return callable|null
     */
    public function guessFormat(Column $column)
    {
        $generator = $this->generator;

        switch ($column->getType()) {
            case 'tinyint':
                return function () use ($generator) {
                    return (int) $generator->boolean;
                };
            case 'decimal':
                return function () use ($generator, $column) {
                    $precision = $column->getPrecision() ?: 2;
                    $scale = $column->getScale();

                    return $generator->randomNumber(
                        min([$precision, log10(mt_getrandmax())])) / pow(10, $scale);
                };
            case 'smallint':
                return function () {
                    return mt_rand(0, 65535);
                };
            case 'integer':
                return function () {
                    return mt_rand(0, intval('2147483647'));
                };
            case 'bigint':
                return function () {
                    return mt_rand(0, intval('18446744073709551615'));
                };
            case 'float':
                return function () {
                    return mt_rand(0, intval('4294967295'))/mt_rand(1, intval('4294967295'));
                };
            case 'varchar':
                $size = $column->getLength() ? $column->getLength() : 255;
                ;
                return function () use ($generator, $size) {
                    return $generator->text($size);
                };
            case 'text':
                return function () use ($generator) {
                    return $generator->text;
                };
            case 'enum':
                return function() use ($generator, $column){
                    return $generator->randomElement($column->getExtra());
                };
            case 'datetime':
            case 'date':
            case 'time':
                return function () use ($generator) {
                    return $generator->datetime;
                };
            default:
                return null;
        }
    }
}
