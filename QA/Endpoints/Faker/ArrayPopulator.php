<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Faker;

use Mildberry\Kangaroo\Libraries\Elegant\Collection;
use Mildberry\Kangaroo\QA\Endpoints\Entity\AbstractGenerator;
use Faker\Generator;
use InvalidArgumentException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ArrayPopulator
{
    /**
     * @var Generator
     */
    private $generator;

    /**
     * @param Generator $generator
     * @param Scheme $scheme
     */
    public function __construct(Generator $generator)
    {
        $this->generator = $generator;
    }


    /**
     * @param string $class
     * @param int $times
     * @param array $formatters
     * @return Collection
     * @throws InvalidArgumentException
     */
    public function execute($times = 1, array $formatters = [])
    {
        $result = [];

        for($i = 0; $i < $times; $i++){
            $currentSet = array();
            foreach($formatters as $formatterKey => $formatter){
                if(is_callable($formatter)){
                    $currentSet[$formatterKey] = is_callable($formatter) ? $formatter() : $formatter;
                } else {
                    $currentSet[$formatterKey] = $formatter;
                }

            }
            $result[] = skip_empty($currentSet, [null]);
        }
        
        return count($result) == 1 ? $result[0] : new Collection($result);
    }
}
