<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Faker;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Column 
{
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->data['COLUMN_NAME'];
    }

    /**
     * @return bool
     */
    public function hasAutoincrement()
    {
        return $this->data['EXTRA'] === 'auto_increment';
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->data['DATA_TYPE'];
    }

    /**
     * @var null|int
     */
    public function getPrecision()
    {
        return $this->data['NUMERIC_PRECISION'];
    }

    public function getScale() {
        return $this->data['NUMERIC_SCALE'];
    }

    /**
     * @return array|null
     */
    public function getExtra()
    {
        if ($this->getType() !== 'enum'){
            return null;
        }

        $values = [];

        preg_match('/[\'](.*?)[\']/', $this->data['COLUMN_TYPE'], $values);

        array_shift($values);

        return $values;
    }

    /**
     * @return int
     */
    public function getLength()
    {

    }
} 