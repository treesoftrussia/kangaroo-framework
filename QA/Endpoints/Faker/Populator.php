<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Faker;

use Mildberry\Kangaroo\Libraries\Elegant\Collection;
use Faker\Generator;
use InvalidArgumentException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class Populator
{
    /**
     * @var Generator
     */
    private $generator;

    /**
     * @var Scheme
     */
    private $scheme;

    /**
     * @param Generator $generator
     * @param Scheme $scheme
     */
    public function __construct(Generator $generator, Scheme $scheme)
    {
        $this->generator = $generator;
        $this->scheme = $scheme;
    }


    /**
     * @param string $class
     * @param int $times
     * @param array $formatters
     * @return Collection
     * @throws InvalidArgumentException
     */
    public function execute($class, $times = 1, array $formatters = [])
    {
        $inserts = [];

        $entity = new EntityPopulator(
            $class,
            $this->generator,
            $this->scheme,
            $formatters
        );

        for($i = 0; $i < $times; $i++){
            $inserts[] = $entity->execute();
        }

        return new Collection($inserts);
    }
}
