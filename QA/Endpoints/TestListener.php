<?php
namespace Mildberry\Kangaroo\QA\Endpoints;

use Illuminate\Foundation\Application as ApplicationInstance;
//use Illuminate\Foundation\Testing\ApplicationTrait;
use Illuminate\Support\Collection;
use PHPUnit_Framework_Test;
use PHPUnit_Framework_TestSuite;
use Exception;
use PHPUnit_Framework_AssertionFailedError;

/**
 * Class prepares database for the api test suite
 * @author Andrew Sparrow<andrew.sprw@gmail.com>
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class TestListener implements \PHPUnit_Framework_TestListener
{
    //use ApplicationTrait;

    /**
     * @var ApplicationInstance
     */
    private $app;

    /**
     * TestListener constructor.
     */
    public function __construct()
    {
        $this->refreshApplication();

        if (intval(getenv('NOT_REINITIALIZE_DATABASE')) == 0) {
            print "Initialize database ... ";
            $this->app->make(DbManager::class)
                ->createIfNotExists()
                ->change()
                ->dropSchema()
                ->migrate();
            print "[OK]\n";
        }

    }

    /**
     * @return ApplicationInstance
     */
    public function createApplication()
    {
        if ($this->app === null){
            $this->app = Application::test();
        }

        return $this->app;
    }

    /**
     * A test suite started.
     *
     * @param PHPUnit_Framework_TestSuite $suite
     * @since  Method available since Release 2.2.0
     */
    public function startTestSuite(PHPUnit_Framework_TestSuite $suite)
    {
//        if (!$this->isAllowedSuite($suite)) {
//            return;
//        }

        $this->refreshApplication();
    }

    /**
     * Refresh the application instance.
     *
     * @return void
     */
    protected function refreshApplication()
    {
        putenv('APP_ENV=testing');

        $this->app = $this->createApplication();
    }

    /**
     * A test suite ended.
     *
     * @param PHPUnit_Framework_TestSuite $suite
     * @since  Method available since Release 2.2.0
     */
    public function endTestSuite(PHPUnit_Framework_TestSuite $suite)
    {
        if (!$this->isAllowedSuite($suite)) {
            return;
        }

        //$this->app->make(DbManager::class)->drop();
    }

    /**
     * @param PHPUnit_Framework_TestSuite $suite
     * @return bool
     */
    protected function isAllowedSuite(PHPUnit_Framework_TestSuite $suite)
    {
        $suiteName = $this->config()->get('suitesName', 'endpoints');

        if ($suite->getName() === $suiteName) {
            return true;
        }

        return false;
    }

    /**
     * @return Collection
     */
    private function config()
    {
        return Application::getConfig();
    }

    /**
     * An error occurred.
     *
     * @param PHPUnit_Framework_Test $test
     * @param Exception $e
     * @param float $time
     */
    public function addError(PHPUnit_Framework_Test $test, Exception $e, $time)
    {
        // TODO: Implement addError() method.
    }

    /**
     * A failure occurred.
     *
     * @param PHPUnit_Framework_Test $test
     * @param PHPUnit_Framework_AssertionFailedError $e
     * @param float $time
     */
    public function addFailure(PHPUnit_Framework_Test $test, PHPUnit_Framework_AssertionFailedError $e, $time)
    {
        // TODO: Implement addFailure() method.
    }

    /**
     * Incomplete test.
     *
     * @param PHPUnit_Framework_Test $test
     * @param Exception $e
     * @param float $time
     */
    public function addIncompleteTest(PHPUnit_Framework_Test $test, Exception $e, $time)
    {
        // TODO: Implement addIncompleteTest() method.
    }

    /**
     * Risky test.
     *
     * @param PHPUnit_Framework_Test $test
     * @param Exception $e
     * @param float $time
     * @since  Method available since Release 4.0.0
     */
    public function addRiskyTest(PHPUnit_Framework_Test $test, Exception $e, $time)
    {
        // TODO: Implement addRiskyTest() method.
    }

    /**
     * Skipped test.
     *
     * @param PHPUnit_Framework_Test $test
     * @param Exception $e
     * @param float $time
     * @since  Method available since Release 3.0.0
     */
    public function addSkippedTest(PHPUnit_Framework_Test $test, Exception $e, $time)
    {
        // TODO: Implement addSkippedTest() method.
    }

    /**
     * A test started.
     *
     * @param PHPUnit_Framework_Test $test
     */
    public function startTest(PHPUnit_Framework_Test $test)
    {
        // TODO: Implement startTest() method.
    }

    /**
     * A test ended.
     *
     * @param PHPUnit_Framework_Test $test
     * @param float $time
     */
    public function endTest(PHPUnit_Framework_Test $test, $time)
    {
        // TODO: Implement endTest() method.
    }
}