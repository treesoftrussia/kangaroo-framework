<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Specification;

use Mildberry\Kangaroo\Libraries\Enum\Enum;
use RuntimeException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class ValueType extends Enum
{
    const INT = 1;
    const FLOAT = 2;
    const STRING = 3;
    const BOOL = 4;
    const ARRAYTYPE = 5;


    /**
     * Checks whether the value type is matched the current specified
     * @param $value
     * @return bool
     * @throws RuntimeException
     */
    public function isCorrect($value)
    {
        if ($this->is(static::INT)) {
            return is_int($value);
        }

        if ($this->is(static::FLOAT)) {
            return is_float($value) || is_int($value);
        }

        if ($this->is(static::STRING)) {
            return is_string($value);
        }

        if ($this->is(static::BOOL)) {
            return is_bool($value);
        }

        if ($this->is(static::ARRAYTYPE)) {
            return is_array($value);
        }

        throw new RuntimeException('Unknown value type.');
    }
} 