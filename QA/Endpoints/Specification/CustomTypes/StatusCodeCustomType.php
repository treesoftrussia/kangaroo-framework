<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace Mildberry\Kangaroo\QA\Endpoints\Specification\CustomTypes;

class StatusCodeCustomType extends AbstractCustomType
{
    /**
     * @var int $expectedCode
     */
    private $expectedCode;

    /**
     * @param int $expectedCode
     */
    public function __construct($expectedCode=0)
    {
        $this->$expectedCode = $expectedCode;
    }

    /**
     * @return string
     */
    public function getValidationString()
    {
        return $this->expectedCode>0?'same:'.$this->expectedCode:'';
    }
}