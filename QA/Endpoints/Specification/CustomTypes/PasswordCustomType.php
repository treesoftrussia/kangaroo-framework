<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace Mildberry\Kangaroo\QA\Endpoints\Specification\CustomTypes;

class PasswordCustomType extends AbstractCustomType
{

    /**
     * @return string
     */
    public function getValidationString()
    {
        return 'string';
    }
}