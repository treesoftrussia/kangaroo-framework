<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace Mildberry\Kangaroo\QA\Endpoints\Specification\CustomTypes;

class WordCustomType extends AbstractCustomType
{
    /**
     * @return string
     */
    public function getValidationString()
    {
        return 'string';
    }

}