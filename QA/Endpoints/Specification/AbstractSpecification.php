<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Specification;

use Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers\AbstractResolver;
use RuntimeException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
abstract class AbstractSpecification
{
    /**
     * @var bool $whiteList
     */
    protected $whiteList = false;

    /**
     * @var AbstractResolver[]
     */
    private $specification;

    /**
     * @var ResolversManager
     */
    private $resolversManager;

    /**
     * @return ResolversManager
     */
    public function getResolversManager()
    {
        if ($this->resolversManager === null) {
            $this->resolversManager = new ResolversManager($this);
        }

        return $this->resolversManager;
    }

    /**
     * @return array
     */
    abstract public function specification();

    /**
     * @param array $data
     * @return Result
     */
    public function check($data)
    {

        $this->resolve();
        $result = new Result();

        if($this->whiteList) {
            foreach ($data as $key => $currentElement) {
                if (!array_key_exists($key, $this->specification)) {
                    $result->addError($key, 'Element is excess');
                }
            }
        }

        foreach ($this->specification as $field => $specification) {
            if (!$specification->check($field, array_get($data, $field), $result)->isOk()) {
                break;
            }
        }

        return $result;
    }

    protected function resolve()
    {
        if ($this->specification !== null) {
            return;
        }

        $specification = $this->specification();

        if (!$specification) {
            throw new RuntimeException('The specification cannot be empty.');
        }

        $manager = $this->getResolversManager();

        $this->specification = [];

        foreach ($specification as $field => $value) {
            $attributes = $this->getFieldAttributes($field);

            try {
                $this->specification[$attributes['key']] = $manager->createResolver($value, $attributes);
            } catch (ResolverException $e) {
                throw new SpecificationException($field, 0, $e);
            } catch (SpecificationException $e) {
                $nestedField = $e->getFields();

                throw new SpecificationException("{$field}.{$nestedField}", $e->getCode(), $e);
            }
        }
    }

    /**
     * @param $field
     * @return array
     */
    protected function getFieldAttributes($field)
    {
        $matches = [];

        if (!preg_match('/(.+?)(:collection)?(\?)?$/', $field, $matches)) {
            throw new RuntimeException("Cannot parse field '{$field}'");
        }

        $key = $matches[1];
        $isCollection = preg_match('/.+:collection(\?)?$/', $field);
        $isNullable = preg_match('/.+(:collection)?\?$/', $field);

        return [
            'key' => $key,
            'nullable' => $isNullable,
            'collection' => $isCollection
        ];
    }
}