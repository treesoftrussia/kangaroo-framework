<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Specification;

use Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers\AbstractResolver;
use Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers\ArrayResolver;
use Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers\CallableResolver;
use Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers\ClassResolver;
use Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers\CustomTypeResolver;
use Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers\MultipleResolver;
use Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers\ScalarResolver;
use Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers\SimpleResolver;
use Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers\ValueTypeResolver;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ResolversManager
{
    /**
     * @var AbstractSpecification
     */
    private $specification;

    /**
     * @var AbstractResolver[]
     */
    private $resolvers = [
        ArrayResolver::class,
        MultipleResolver::class,
        CallableResolver::class,
        ClassResolver::class,
        ValueTypeResolver::class,
        SimpleResolver::class,
        CustomTypeResolver::class,
        ScalarResolver::class,
    ];

    /**
     * @param AbstractSpecification $specification
     */
    public function __construct(AbstractSpecification $specification)
    {
        $this->specification = $specification;
    }

    /**
     * @param $value
     * @param $attributes
     * @return AbstractResolver
     * @throws ResolverException
     */
    public function createResolver($value, $attributes)
    {
        foreach ($this->resolvers as $resolver) {
            if ($resolver::canResolve($value, $attributes)) {
                return new $resolver($this, $value, $attributes);
            }
        }

        throw new ResolverException('Cannot resolve specification');
    }
}