<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers;

use Mildberry\Kangaroo\QA\Endpoints\Specification\Result;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ScalarResolver extends AbstractResolver
{
    /**
     * @param mixed $value
     * @param array $attributes
     * @return bool
     */
    public static function canResolve($value, $attributes)
    {
        return is_scalar($value);
    }

    /**
     * @param string $field
     * @param mixed $data
     * @param Result $result
     * @return Result
     */
    protected function innerCheck($field, $data, Result $result)
    {
        if ($this->value !== $data) {
            $result->addError($field, "Structure isn't correct");
        }

        return $result;
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    protected function resolve($value)
    {
        return $value;
    }
}