<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers;

use Mildberry\Kangaroo\QA\Endpoints\Specification\CustomTypes\AbstractCustomType;
use Mildberry\Kangaroo\QA\Endpoints\Specification\Result;
use Illuminate\Support\Facades\Validator;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class CustomTypeResolver extends AbstractResolver
{
    /**
     * @param mixed $value
     * @return mixed
     */
    protected function resolve($value)
    {
        return is_object($value) ? $value : new $value();
    }

    /**
     * @param string $field
     * @param mixed $data
     * @param Result $result
     * @return Result
     */
    protected function innerCheck($field, $data, Result $result)
    {

        $validator = Validator::make([$field => $data], [$field => $this->value->getValidationString()]);

        if($validator->fails())
        {
            foreach($validator->messages()->getMessages() as $f=> $messages)
            {
                foreach($messages as $k=>$message) {
                    $result->addError($f.'['.$k.']', $message);
                }
            }
        }

        return $result;
    }

    /**
     * @param mixed $value
     * @param array $attributes
     * @return bool
     */
    public static function canResolve($value, $attributes)
    {
        return is_subclass_of($value, AbstractCustomType::class);
    }
}