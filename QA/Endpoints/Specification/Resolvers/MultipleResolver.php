<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers;

use Mildberry\Kangaroo\QA\Endpoints\Specification\Result;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class MultipleResolver extends AbstractResolver
{
    /**
     * @param mixed $value
     * @param array $attributes
     * @return bool
     */
    public static function canResolve($value, $attributes)
    {
        return is_traversable($value);
    }

    /**
     * @param string $field
     * @param mixed $data
     * @param Result $result
     * @return Result
     */
    protected function innerCheck($field, $data, Result $result)
    {
        $totalResult = new Result();

        /**
         * @var AbstractResolver $resolver
         */
        foreach ($this->value as $resolver) {
            $itemResult = new Result();
            $itemResult = $resolver->check($field, $data, $itemResult);

            if ($itemResult->isOk()) {
                return $itemResult;
            }

            $totalResult->merge($itemResult);
        }

        return $result->merge($totalResult);
    }

    /**
     * @param array $value
     * @return mixed
     */
    protected function resolve($value)
    {
        $resolvers = [];

        foreach ($value as $item) {
            $resolvers[] = $this->manager->createResolver($item, $this->attributes);
        }

        return $resolvers;
    }
}