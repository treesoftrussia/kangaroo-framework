<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers;

use Mildberry\Kangaroo\QA\Endpoints\Specification\ResolversManager;
use Mildberry\Kangaroo\QA\Endpoints\Specification\Result;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
abstract class AbstractResolver
{
    /**
     * @var ResolversManager
     */
    protected $manager;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * @param ResolversManager $manager
     * @param mixed $value
     * @param array $attributes
     */
    public function __construct(ResolversManager $manager, $value, $attributes)
    {
        $this->manager = $manager;
        $this->attributes = $attributes;
        $this->value = $this->resolve($value);
    }

    /**
     * @param mixed $value
     * @param array $attributes
     * @return bool
     */
    public static function canResolve($value, $attributes)
    {
        // Static function can't be abstract
        // Added for PHPStorm code completion
        return false;
    }

    /**
     * @param string $field
     * @param mixed $data
     * @param Result $result
     * @return Result
     */
    public function check($field, $data, Result $result)
    {
        if ($data === null) {
            if (!array_take($this->attributes, 'nullable', false)) {
                $result->addError($field, "Value can't be null");
            }

            return $result;
        }

        $result = $this->innerCheck($field, $data, $result);

        return $result;
    }

    /**
     * @param string $field
     * @param mixed $data
     * @param Result $result
     * @return Result
     */
    abstract protected function innerCheck($field, $data, Result $result);

    /**
     * @param mixed $value
     * @return mixed
     */
    abstract protected function resolve($value);
}