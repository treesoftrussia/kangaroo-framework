<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers;

use Mildberry\Kangaroo\QA\Endpoints\Specification\Result;
use Mildberry\Kangaroo\QA\Endpoints\Specification\AbstractSpecification;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ClassResolver extends AbstractResolver
{
    /**
     * @param mixed $value
     * @return mixed
     */
    protected function resolve($value)
    {
        return is_object($value) ? $value : new $value();
    }

    /**
     * @param string $field
     * @param mixed $data
     * @param Result $result
     * @return Result
     */
    protected function innerCheck($field, $data, Result $result)
    {
        return $result->merge($this->value->check($data), $field);
    }

    /**
     * @param mixed $value
     * @param array $attributes
     * @return bool
     */
    public static function canResolve($value, $attributes)
    {
        return is_subclass_of($value, AbstractSpecification::class);
    }
}