<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers;

use Mildberry\Kangaroo\QA\Endpoints\Specification\Result;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class CallableResolver extends AbstractResolver
{
    /**
     * @param string $field
     * @param mixed $data
     * @param Result $result
     * @return Result
     */
    protected function innerCheck($field, $data, Result $result)
    {
        return $this->value->check($field, $data, $result);
    }

    /**
     * @param mixed $value
     * @param array $attributes
     * @return bool
     */
    public static function canResolve($value, $attributes)
    {
        return is_callable($value);
    }

    /**
     * @param callable $value
     * @return mixed
     */
    protected function resolve($value)
    {
        return $this->manager->createResolver($value(), $this->attributes);
    }
}