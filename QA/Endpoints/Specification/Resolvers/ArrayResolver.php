<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Specification\Resolvers;

use Mildberry\Kangaroo\QA\Endpoints\Specification\Result;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ArrayResolver extends AbstractResolver
{
    /**
     * @param mixed $value
     * @param array $attributes
     * @return bool
     */
    public static function canResolve($value, $attributes)
    {
        return array_take($attributes, 'collection', false);
    }

    /**
     * @param string $field
     * @param array $data
     * @param Result $result
     * @return Result
     */
    protected function innerCheck($field, $data, Result $result)
    {
        foreach ($data as $key => $item) {
            /**
             * @var Result $result
             */
            $result = $this->value->check("{$field}.{$key}", $item, $result);

            if (!$result->isOk()) {
                break;
            }
        }

        return $result;
    }

    /**
     * @param array $value
     * @return mixed
     */
    protected function resolve($value)
    {
        $attributes = $this->attributes;
        unset($attributes['collection']);

        return $this->manager->createResolver($value, $attributes);
    }
}