<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Specification;

use Exception;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class SpecificationException extends Exception
{
    private $fields;

    /**
     * @param string $field
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($field, $code = 0, Exception $previous = null)
    {
        parent::__construct("Cannot resolve specification for field '{$field}'");
        $this->fields = $field;
    }

    /**
     * @return string
     */
    public function getFields()
    {
        return $this->fields;
    }
}