<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Specification;

use Exception;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class ResolverException extends Exception
{

}