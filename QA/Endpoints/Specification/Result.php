<?php
namespace Mildberry\Kangaroo\QA\Endpoints\Specification;

/**
 * @author Sergei Melnikov <me@rnr.name>
 */
class Result
{
    private $errors = [];

    /**
     * @return bool
     */
    public function isOk()
    {
        return count($this->errors) == 0;
    }

    /**
     * @param string $field
     * @param string $message
     */
    public function addError($field, $message)
    {
        $this->errors[$field] = $message;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return print_r($this->errors, true);
    }

    /**
     * @param Result $result
     * @param string $prefix
     * @return $this
     */
    public function merge(Result $result, $prefix = '')
    {
        foreach ($result->getErrors() as $field => $error) {
            $fullKey = strlen($prefix) > 0 ? "{$prefix}.{$field}" : $field;

            if (array_key_exists($fullKey, $this->errors)) {
                if (!is_array($this->errors[$fullKey])) {
                    $this->errors[$fullKey] = [$this->errors[$fullKey]];
                }
                $this->errors[$fullKey][] = $error;
            } else {
                $this->errors[$fullKey] = $error;
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}