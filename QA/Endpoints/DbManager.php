<?php namespace Mildberry\Kangaroo\QA\Endpoints;

use Illuminate\Contracts\Console\Kernel as Artisan;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Connection;

/**
 * Class helps  to prepare database for testing the APIs
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class DbManager
{
    /**
     * @var DatabaseManager|Connection
     */
    private $db;

    /**
     * @var Artisan
     */
    private $artisan;

    /**
     * @var Repository;
     */
    private $config;

    /**
     * @param DatabaseManager $db
     * @param Artisan $artisan
     * @param Repository $config
     */
    public function __construct(DatabaseManager $db, Artisan $artisan, Repository $config)
    {
        $this->db = $db;
        $this->artisan = $artisan;
        $this->config = $config;
    }

    /**
     * @return $this
     */
    public function create()
    {
        $this->db->unprepared('CREATE DATABASE `' . $this->getDbName() . '` character set=utf8');

        return $this;
    }

    /**
     * @return $this
     */
    public function createIfNotExists()
    {
        $this->db->unprepared('CREATE DATABASE IF NOT EXISTS `' . $this->getDbName() . '` character set=utf8');

        return $this;
    }

    /**
     * @return $this
     */
    public function drop()
    {
        $this->db->unprepared('DROP DATABASE IF EXISTS `' . $this->getDbName() . '`');

        return $this;
    }

    /**
     * @return $this
     */
    public function dropSchema()
    {
        $this->db->statement('SET FOREIGN_KEY_CHECKS=0');

        $tables = $this->getConnection()->getDoctrineSchemaManager()->listTableNames();

        foreach($tables as $table) {
            $this->db->unprepared('DROP TABLE `'.$table.'`');
        }

        $this->db->statement('SET FOREIGN_KEY_CHECKS=1');

        return $this;
    }

    /**
     * @param bool $foreignKeyChecks
     * @return $this
     */
    public function truncateAllTables($foreignKeyChecks = false)
    {

        $connection = $this->getConnection();

        $tables = $connection->getDoctrineSchemaManager()->listTableNames();

        if(!$foreignKeyChecks) {
            $this->db->statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        foreach ($tables as $table){
            $this->db->statement("TRUNCATE TABLE ".$table);
        }

        if(!$foreignKeyChecks) {
            $this->db->statement('SET FOREIGN_KEY_CHECKS=1;');
        }

        return $this;

    }

    /**
     * @return $this
     */
    public function change()
    {
        $config = $this->config->get('database.connections.' . $this->db->getDefaultConnection());

        $config['database'] = $this->getDbName();

        $this->config->set('database.connections.' . $this->getDbName(), $config);

        $this->db->setDefaultConnection($this->getDbName());

        $this->db->reconnect();

        return $this;
    }

    private function getDbName()
    {
        return $this->config->get('qa.endpoints.dbName', 'as_endpoints_testing');
    }

    /**
     * Migrates database
     *
     * @return $this
     */
    public function migrate()
    {
        $this->artisan->call('migrate');

        return $this;
    }

    /**
     * @return Connection
     */
    public function getConnection()
    {
        return $this->db->connection();
    }
}