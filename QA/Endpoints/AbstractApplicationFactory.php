<?php
namespace Mildberry\Kangaroo\QA\Endpoints;
use Mildberry\Kangaroo\QA\Endpoints\Validator\CollectionSummary;
use Illuminate\Foundation\Application as ApplicationInstance;
use Illuminate\Foundation\Console\Kernel;
use RuntimeException;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
abstract class AbstractApplicationFactory
{
    const TEST = 1;
    const HTTP = 2;

    /**
     * @return ApplicationInstance
     */
    abstract protected function getApplication();

    /**
     * @param string $type
     * @param callable $booted
     * @return ApplicationInstance
     * @throws RuntimeException
     */
    public function create($type = null, callable $booted = null)
    {
        if ($type === self::TEST){
            return $this->test($booted);
        }

        if ($type === self::HTTP){
            return $this->http($booted);
        }

        throw new RuntimeException('The "'.$type.'" is unsupported type.');
    }

    /**
     * @param callable $booted
     * @return ApplicationInstance
     */
    protected function test(callable $booted = null)
    {
        $app = $this->getApplication();

        if ($booted){
            $app->booted($booted);
        }

        $app->make(Kernel::class)->bootstrap();
        return $app;
    }

    /**
     * @param callable $booted
     * @return ApplicationInstance
     */
    protected function http(callable $booted = null)
    {
        $app = $this->getApplication();

        $app->singleton(CollectionSummary::class, CollectionSummary::class);

        if ($booted){
            $app->booted($booted);
        }

        return $app;
    }
}