<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace Mildberry\Kangaroo\QA\Support\FakeGenerators;
use Illuminate\Support\ServiceProvider as Provider;

class ServiceProvider extends Provider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
    }
    public function boot()
    {
        $this->publishes([
            __DIR__.'/fake_generators.php' => config_path('fake_generators.php'),
        ]);

    }
}