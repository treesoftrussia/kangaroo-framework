<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace Mildberry\Kangaroo\QA\Support\FakeGenerators;


use Mildberry\Kangaroo\Libraries\Kangaroo\Exceptions\RuntimeException;
use Faker\Generator;
use Illuminate\Support\Facades\Config;

class FakeCustomTypeGenerator
{
    /**
     * @param $type
     * @param Generator $faker
     * @return mixed
     */
    public function generate($type, Generator $faker)
    {
        /**
         * @var FakerGeneratorInterface $generator
         */
        $t = is_object($type)? $type : new $type();
        if(!Config::has('fake_generators.'.get_class($t))) {
            throw new RuntimeException();
        }

        $generatorType = Config::get('fake_generators')[get_class($t)];

        if(!is_subclass_of($generatorType, FakerGeneratorInterface::class)) {
            throw new RuntimeException();
        }

        $generator = new $generatorType();
        return $generator->generate($faker);
    }

}