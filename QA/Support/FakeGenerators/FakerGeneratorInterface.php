<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace Mildberry\Kangaroo\QA\Support\FakeGenerators;


use Faker\Generator;

interface FakerGeneratorInterface
{
    public function generate(Generator $faker);
}