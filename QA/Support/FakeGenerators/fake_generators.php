<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */
return [
    Mildberry\Kangaroo\QA\Endpoints\Specification\CustomTypes\PhoneCustomType::class => \Mildberry\Kangaroo\QA\Support\FakeGenerators\Generators\PhoneFakeGenerator::class,
    Mildberry\Kangaroo\QA\Endpoints\Specification\CustomTypes\UnixTimeCustomType::class => \Mildberry\Kangaroo\QA\Support\FakeGenerators\Generators\UnixTimeFakeGenerator::class,
    Mildberry\Kangaroo\QA\Endpoints\Specification\CustomTypes\PasswordCustomType::class => \Mildberry\Kangaroo\QA\Support\FakeGenerators\Generators\PasswordFakeGenerator::class,
    Mildberry\Kangaroo\QA\Endpoints\Specification\CustomTypes\NameCustomType::class => \Mildberry\Kangaroo\QA\Support\FakeGenerators\Generators\NameFakeGenerator::class,
    Mildberry\Kangaroo\QA\Endpoints\Specification\CustomTypes\StatusCodeCustomType::class => \Mildberry\Kangaroo\QA\Support\FakeGenerators\Generators\StatusCodeFakeGenerator::class,
    Mildberry\Kangaroo\QA\Endpoints\Specification\CustomTypes\WordCustomType::class => \Mildberry\Kangaroo\QA\Support\FakeGenerators\Generators\WordFakeGenerator::class,
];