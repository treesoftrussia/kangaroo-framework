<?php
/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */

namespace Mildberry\Kangaroo\QA\Support\FakeGenerators\Generators;


use Mildberry\Kangaroo\QA\Support\FakeGenerators\FakerGeneratorInterface;
use Faker\Generator;

class WordFakeGenerator implements FakerGeneratorInterface
{


    public function generate(Generator $faker)
    {
        return $faker->word;
    }
}