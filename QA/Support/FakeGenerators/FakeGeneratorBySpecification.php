<?php

/**
 * @author Vladimir Barmotin <barmotinvladimir@gmail.com>
 */
namespace Mildberry\Kangaroo\QA\Support\FakeGenerators;

use Mildberry\Kangaroo\QA\Endpoints\Specification\AbstractSpecification;
use Faker\Factory;
use Mildberry\Kangaroo\QA\Endpoints\Specification\ValueType;
use Mildberry\Kangaroo\QA\Endpoints\Specification\CustomTypes\AbstractCustomType;
use Faker\Generator;

class FakeGeneratorBySpecification
{
    /**
     * @param Generator $faker
     */
    public function __construct(Generator $faker)
    {
        $this->faker= $faker;
    }
    /**
     * @var \Faker\Generator $faker
     */
    private $faker;
    /**
     * @param AbstractSpecification $specification
     * @param array $params
     * @return array
     */
    public function generate(AbstractSpecification $specification, $params = [])
    {
        /**
         * #var array $spec
         */
        if($params === null)
            return null;

        $spec = $specification->specification();
        $array = [];

        foreach($spec as $key => $value){
            $valueName = trim($key, "?");
            $keyData = explode(':', $valueName);
            $array[$keyData[0]] =
                isset($keyData[1]) && $keyData[1] === "collection" ?
                    $this->generateCollection($value, rand(2,10),array_key_exists($keyData[0], $params)? $params[$keyData[0]]:[]):
                    $this->generateElement($value, array_key_exists($keyData[0], $params)? $params[$keyData[0]]:[]);
        }

        return $array;

    }

    /**
     * @param $elementType
     * @param $params
     * @return array|bool|float|int|string
     */
    protected function generateElement($elementType, $params)
    {
        if($params === null)
            return null;
        if(is_subclass_of($elementType, AbstractSpecification::class)) {
            return $this->generate(is_object($elementType) ? $elementType : new $elementType(), $params);
        }

        if(is_scalar($params))
            return $params;

        if(is_subclass_of($elementType, AbstractCustomType::class))
        {
            return (new FakeCustomTypeGenerator())->generate($elementType, $this->faker);
        }

        switch($elementType){
            case ValueType::STRING:
                return $this->faker->text(20);
            case ValueType::INT:
                return $this->faker->randomDigit;
            case ValueType::BOOL:
                return $this->faker->boolean();
            case ValueType::FLOAT:
                return $this->faker->randomFloat(4);
        }

    }

    /**
     * @param $elementType
     * @param int $count
     * @param array $params
     * @return array
     */
    protected function generateCollection($elementType, $count=5, $params=[])
    {
        if($params === null)
            return null;
        $array = [];
        if(!empty($params))
            $count = count($params);
        for($i=0;$i<$count;$i++){
            $array[$i] = $this->generateElement($elementType, array_key_exists($i, $params)? $params[$i]:[]);
        }

        return $array;
    }

}