<?php
namespace Mildberry\Kangaroo\QA\Support;
use Illuminate\Http\Request;
use RuntimeException;
use Symfony\Component\HttpFoundation\HeaderBag;

/**
 * @author Igor Vorobiov<igor.vorobioff@gmail.com>
 */
class JsonRequest extends Request
{
    public function __construct($methodOrParams, array $parameters = [], array $options = [])
    {
        if (is_array($methodOrParams)){

            call_user_func_array([$this, 'initialize'], $methodOrParams);
            return ;
        }

        $method = $methodOrParams;

        $this->initialize(
            self::prepareQuery($parameters, $method),
            [],
            [],
            [],
            [],
            self::prepareServer(),
            self::prepareContent($parameters, $method)
        );

        $this->setMethod($method);
    }

    /**
     * @param $url
     * @param $method
     * @param array $parameters
     * @param array $options
     * @return \Symfony\Component\HttpFoundation\Request
     * @throws \RuntimeException
     */
    public static function make($url, $method, $parameters = [], $options = [], $additionalHeaders = [])
    {
        $request = static::create(
            $url,
            $method,
            self::prepareQuery($parameters, $method),
            [],
            [],
            self::prepareServer(),
            self::prepareContent($parameters, $method)
        );

        self::prepareHeaders($request->headers, $options, $additionalHeaders);

        return $request;
    }

    /**
     * The method has a small fix.
     *
     * @param string $uri
     * @param string $method
     * @param array $parameters
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null $content
     * @return array|mixed|\Symfony\Component\HttpFoundation\Request|static
     */
    public static function create($uri, $method = 'GET', $parameters = array(), $cookies = array(), $files = array(), $server = array(), $content = null)
    {


        $server = array_replace(array(
            'SERVER_NAME' => 'localhost',
            'SERVER_PORT' => 80,
            'HTTP_HOST' => 'localhost',
            'HTTP_USER_AGENT' => 'Symfony/2.X',
            'HTTP_ACCEPT' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'HTTP_ACCEPT_LANGUAGE' => 'en-us,en;q=0.5',
            'HTTP_ACCEPT_CHARSET' => 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
            'REMOTE_ADDR' => '127.0.0.1',
            'SCRIPT_NAME' => '',
            'SCRIPT_FILENAME' => '',
            'SERVER_PROTOCOL' => 'HTTP/1.1',
            'REQUEST_TIME' => time(),
        ), $server);

        $server['PATH_INFO'] = '';
        $server['REQUEST_METHOD'] = strtoupper($method);

        $components = parse_url($uri);
        if (isset($components['host'])) {
            $server['SERVER_NAME'] = $components['host'];
            $server['HTTP_HOST'] = $components['host'];
        }

        if (isset($components['scheme'])) {
            if ('https' === $components['scheme']) {
                $server['HTTPS'] = 'on';
                $server['SERVER_PORT'] = 443;
            } else {
                unset($server['HTTPS']);
                $server['SERVER_PORT'] = 80;
            }
        }

        if (isset($components['port'])) {
            $server['SERVER_PORT'] = $components['port'];
            $server['HTTP_HOST'] = $server['HTTP_HOST'].':'.$components['port'];
        }

        if (isset($components['user'])) {
            $server['PHP_AUTH_USER'] = $components['user'];
        }

        if (isset($components['pass'])) {
            $server['PHP_AUTH_PW'] = $components['pass'];
        }

        if (!isset($components['path'])) {
            $components['path'] = '/';
        }

        switch (strtoupper($method)) {
            case 'DELETE':
                if (!isset($server['CONTENT_TYPE'])) {
                    $server['CONTENT_TYPE'] = 'application/x-www-form-urlencoded';
                }
            case 'PATCH':
            case 'POST':
            case 'PUT':
                $decodedContent = json_decode($content, true);
                $request = $decodedContent ? $decodedContent : array();
                $query = array();
                break;
            default:
                $request = array();
                $query = $parameters;
                break;
        }

        // This has been added to fix the problem.

        if (strtolower($method) === 'delete'){
            $query = $parameters;
        }

        $queryString = '';
        if (isset($components['query'])) {
            parse_str(html_entity_decode($components['query']), $qs);

            if (!empty($query)) {
                $query = array_replace($qs, $query);
                $queryString = http_build_query($query, '', '&');
            } else {
                $query = $qs;
                $queryString = $components['query'];
            }
        } elseif ($query) {
            $queryString = http_build_query($query, '', '&');
        }

        $server['REQUEST_URI'] = $components['path'].('' !== $queryString ? '?'.$queryString : '');
        $server['QUERY_STRING'] = $queryString;



        return new static([$query, $request, [], $cookies, $files, $server, $content]);
    }

    /**
     * @param array $parameters
     * @param $method
     * @return null|string
     */
    private static function prepareContent(array $parameters, $method)
    {
        if (!in_array(strtolower($method), ['get', 'delete'])){
            return json_encode($parameters);
        }

        return null;
    }

    /**
     * @return array
     */
    private static function prepareServer()
    {
        return ['CONTENT_TYPE' => 'json/application'];
    }

    /**
     * @param array $parameters
     * @param $method
     * @return array
     */
    private static function prepareQuery(array $parameters, $method)
    {
        if (in_array(strtolower($method), ['get', 'delete'])){
            return $parameters;
        }

        return [];
    }

    /**
     * @param HeaderBag $headers
     * @param array $options
     * @param array $additionalHeaders
     */
    static function prepareHeaders(HeaderBag $headers, array $options, array $additionalHeaders = [])
    {
        if ($options){
            $headers->set('Options', json_encode($options));
        }

        if ($additionalHeaders){
            foreach($additionalHeaders as $headerName => $headerValue) {
                $headers->set($headerName, $headerValue);
            }
        }
    }


    public function isJson()
    {
        return true;
    }
}